GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'SubscriptionType')
	BEGIN
		PRINT N'SubscriptionType exists';
	END
ELSE
	BEGIN
		PRINT N'SubscriptionType added';
		CREATE TABLE [dbo].[SubscriptionType] (
			[SubsciptionTypeId]           INT            IDENTITY (1, 1) NOT NULL,
			[SubscriptionTypeName]        NVARCHAR (100) NULL,
			[SubscriptionTypeDescription] TEXT           NULL,
			[isRealTimeEmail]             BIT            NULL,
			[IsActive]                    BIT            NULL,
			CONSTRAINT [PK_SubscriptionType] PRIMARY KEY CLUSTERED ([SubsciptionTypeId] ASC)
		);
		ALTER TABLE [dbo].[SubscriptionType] ADD CONSTRAINT [DF_SubscriptionType_IsActive] DEFAULT ((1)) FOR [IsActive];

		SET IDENTITY_INSERT [dbo].[SubscriptionType] ON 

		INSERT [dbo].[SubscriptionType] ([SubsciptionTypeId], [SubscriptionTypeName], [SubscriptionTypeDescription], [isRealTimeEmail], [IsActive]) VALUES (1, N'PM Change', N'Notify PM when they are assigned or unassigned from a project', 1, 1)

		INSERT [dbo].[SubscriptionType] ([SubsciptionTypeId], [SubscriptionTypeName], [SubscriptionTypeDescription], [isRealTimeEmail], [IsActive]) VALUES (2, N'3 Days Without Orders', NULL, 1, 1)

		SET IDENTITY_INSERT [dbo].[SubscriptionType] OFF
	END

GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'EmailSubscription')
	BEGIN
		
		IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'SubsciptionTypeId' AND OBJECT_ID = OBJECT_ID(N'EmailSubscription'))
			BEGIN
				PRINT N'SubsciptionTypeId exists';
			END
		ELSE
			BEGIN
				PRINT N'SubsciptionTypeId added';
				ALTER TABLE EmailSubscription ADD SubsciptionTypeId INT NOT NULL
			END
			
		IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'isInternalOnly' AND OBJECT_ID = OBJECT_ID(N'EmailSubscription'))
			BEGIN
				PRINT N'isInternalOnly exists';
			END
		ELSE
			BEGIN
				PRINT N'isInternalOnly added';
				ALTER TABLE EmailSubscription ADD isInternalOnly BIT CONSTRAINT [DF_EmailSubscription_isInternalOnly] DEFAULT ((1)) NULL
			END
	
		IF EXISTS (SELECT * FROM sys.foreign_keys  WHERE object_id = OBJECT_ID(N'[dbo].[FK_179]') AND parent_object_id = OBJECT_ID(N'[dbo].[EmailSubscription]'))
			BEGIN
			    
				PRINT N'Dropping [dbo].[FK_179]...';

				IF EXISTS (SELECT * FROM sys.indexes WHERE name=N'[dbo].[fkIdx_179]' AND object_id = OBJECT_ID(N'[dbo].[EmailSubscription]'))
					BEGIN
				Drop Index fkIdx_179 on [dbo].[EmailSubscription]
					END	

				ALTER TABLE [dbo].[EmailSubscription] DROP CONSTRAINT [FK_179]				

				--ALTER TABLE [dbo].[EmailSubscription] DROP CONSTRAINT [fkIdx_179]
				
				IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'EmailId' AND OBJECT_ID = OBJECT_ID(N'EmailSubscription'))
					BEGIN
						PRINT N'Deleting EmailId';
						ALTER TABLE [dbo].[EmailSubscription] DROP COLUMN EmailId;
					END
				ELSE
					BEGIN
						PRINT N'EmailId not exists';				
					END	

				PRINT N'Created [dbo].[FK_179]...';
				ALTER TABLE [dbo].[EmailSubscription] WITH NOCHECK ADD CONSTRAINT [FK_179] FOREIGN KEY ([SubsciptionTypeId]) REFERENCES [dbo].[SubscriptionType] ([SubsciptionTypeId]) ON DELETE CASCADE;
			END
		ELSE
			BEGIN
				PRINT N'Creating [dbo].[FK_179]...';
				ALTER TABLE [dbo].[EmailSubscription] WITH NOCHECK ADD CONSTRAINT [FK_179] FOREIGN KEY ([SubsciptionTypeId]) REFERENCES [dbo].[SubscriptionType] ([SubsciptionTypeId]) ON DELETE CASCADE;
			END			
		
	END
GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'VerifiedVoltage' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'VerifiedVoltage exists';
		END
	ELSE
		BEGIN
			PRINT N'VerifiedVoltage added';
		ALTER TABLE [dbo].[ShipTo] ADD VerifiedVoltage BIT NULL
		
		ALTER TABLE [dbo].[ShipTo] ADD  CONSTRAINT [DF_ShipTo_VerifiedVoltage]  DEFAULT ((0)) FOR [VerifiedVoltage]
	END
GO

GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'RoleBasedAccess')
	BEGIN
		PRINT N'RoleBasedAccess exists';
	END
ELSE
	BEGIN
		PRINT N'RoleBasedAccess added';
		CREATE TABLE [dbo].[RoleBasedAccess](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[PageName] [varchar](100) NULL,
			[RoleId] [int] NOT NULL,
			[IsEdit] [bit] NULL,
			[IsView] [bit] NULL,
			[IsActive] [bit] NULL,
		    CONSTRAINT [PK_RoleBasedAccess] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]	

		ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsEdit]  DEFAULT ((1)) FOR [IsEdit]
		
		ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsView]  DEFAULT ((1)) FOR [IsView]
		
		ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsActive]  DEFAULT ((1)) FOR [IsActive]
		
		ALTER TABLE [dbo].[RoleBasedAccess]  WITH CHECK ADD  CONSTRAINT [FK_RoleBasedAccess_UserRoles] FOREIGN KEY([RoleId]) REFERENCES [dbo].[UserRoles] ([RoleId])
		
		ALTER TABLE [dbo].[RoleBasedAccess] CHECK CONSTRAINT [FK_RoleBasedAccess_UserRoles]
		
		SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (1, N'Home', 1, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (2, N'User', 1, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (3, N'UserRole', 1, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (4, N'EmailSubscription', 1, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (5, N'Home', 2, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (6, N'User', 2, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (7, N'UserRole', 2, 0, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (8, N'EmailSubscription', 2, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (9, N'Home', 3, 1, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (10, N'User', 3, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (11, N'UserRole', 3, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (12, N'EmailSubscription', 3, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (13, N'Home', 4, 0, 1, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (14, N'User', 4, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (15, N'UserRole', 4, 0, 0, 1)

		INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (16, N'EmailSubscription', 4, 0, 0, 1)

		SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
	END
	

GO
IF EXISTS(select * from RoleBasedAccess where Id=17)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (17, N'Home', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END	
GO
IF EXISTS(select * from RoleBasedAccess where Id=18)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (18, N'User', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=19)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (19, N'UserRole', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=20)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (20, N'EmailSubscription', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=21)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (21, N'Home', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=22)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (22, N'User', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=23)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (23, N'UserRole', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=24)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (24, N'EmailSubscription', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=25)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (25, N'Home', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=26)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (26, N'User', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=27)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (27, N'UserRole', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=28)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (28, N'EmailSubscription', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=29)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (29, N'JobStep', 1, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=30)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (30, N'JobStep', 2, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=31)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (31, N'JobStep', 3, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=32)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (32, N'JobStep', 4, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=33)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (33, N'JobStep', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=34)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (34, N'JobStep', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=35)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (35, N'JobStep', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=36)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (36, N'JobSheet', 1, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=37)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (37, N'JobSheet', 2, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=38)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (38, N'JobSheet', 3, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=39)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (39, N'JobSheet', 4, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=40)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (40, N'JobSheet', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=41)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (41, N'JobSheet', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=42)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (42, N'JobSheet', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=43)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (43, N'ManagerPM', 1, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
	
GO
IF EXISTS(select * from RoleBasedAccess where Id=44)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (44, N'ManagerPM', 2, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=45)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (45, N'ManagerPM', 3, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=46)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (46, N'ManagerPM', 4, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=47)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (47, N'ManagerPM', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=48)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (48, N'ManagerPM', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=49)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (49, N'ManagerPM', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO
IF EXISTS(select * from RoleBasedAccess where Id=50)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (50, N'OutSideSalesRepPM', 1, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO
IF EXISTS(select * from RoleBasedAccess where Id=51)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (51, N'OutSideSalesRepPM', 2, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO
IF EXISTS(select * from RoleBasedAccess where Id=52)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (52, N'OutSideSalesRepPM', 3, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO
IF EXISTS(select * from RoleBasedAccess where Id=53)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (53, N'OutSideSalesRepPM', 4, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO
IF EXISTS(select * from RoleBasedAccess where Id=54)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (54, N'OutSideSalesRepPM', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO
IF EXISTS(select * from RoleBasedAccess where Id=55)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (55, N'OutSideSalesRepPM', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO
IF EXISTS(select * from RoleBasedAccess where Id=56)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (56, N'OutSideSalesRepPM', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO		
IF EXISTS(select * from RoleBasedAccess where Id=57)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (57, N'Home', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
GO	
IF EXISTS(select * from RoleBasedAccess where Id=58)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (58, N'User', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=59)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (59, N'UserRole', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=60)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (60, N'EmailSubscription', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=61)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (61, N'JobStep', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=62)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (62, N'JobSheet', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=63)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (63, N'ManagerPM', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=64)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (64, N'OutSideSalesRepPM', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END


GO	
IF EXISTS(select * from RoleBasedAccess where Id=65)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (65, N'DelegatePM', 1, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO	
IF EXISTS(select * from RoleBasedAccess where Id=66)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (66, N'DelegatePM', 2, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO	
IF EXISTS(select * from RoleBasedAccess where Id=67)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (67, N'DelegatePM', 3, 1, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=68)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (68, N'DelegatePM', 4, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=69)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (69, N'DelegatePM', 5, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=70)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (70, N'DelegatePM', 6, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=71)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (71, N'DelegatePM', 7, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END
		
GO	
IF EXISTS(select * from RoleBasedAccess where Id=72)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] ON 			
			INSERT [dbo].[RoleBasedAccess] ([Id], [PageName], [RoleId], [IsEdit], [IsView], [IsActive]) VALUES (72, N'DelegatePM', 8, 0, 1, 1)
			SET IDENTITY_INSERT [dbo].[RoleBasedAccess] OFF
		END

GO


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ProjectOrderLine')
	BEGIN
		PRINT N'ProjectOrderLine exists';
	END
ELSE
	BEGIN
		PRINT N'ProjectOrderLine added';
		CREATE TABLE [dbo].[ProjectOrderLine](
			[ProjectOrderLineId] [int] IDENTITY(1,1) NOT NULL,
			[OrderNo] [int] NULL,
			[OrderSurf] [int] NULL,
			[LineNumber] [int] NULL,
			[ModelPart] [nvarchar](100) NULL,
			[POAmount] [int] NULL,
			[QuantityOrdered] [decimal](18, 2) NULL,
			[QuantityShipped] [decimal](18, 2) NULL,
			[CustomerRequestedDeliveryDate] [datetime] NULL,
			[VendorName] [decimal](24, 0) NULL,
			[VerifiedVoltage] [bit] NULL,
			[IsActive] [bit] NULL,
			[CustNo] [decimal](15, 0) NULL,
			[ShipTo] [nvarchar](50) NULL,
		 CONSTRAINT [PK_ProjectOrderLine] PRIMARY KEY CLUSTERED 
		(
			[ProjectOrderLineId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD  CONSTRAINT [DF_ProjectOrderLine_VerifiedVoltage]  DEFAULT ((0)) FOR [VerifiedVoltage]
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD  CONSTRAINT [DF_ProjectOrderLine_IsActive]  DEFAULT ((1)) FOR [IsActive]
		
	END

GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'POAmount' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'POAmount exists';
		END
	ELSE
		BEGIN
			PRINT N'POAmount added';
		ALTER TABLE [dbo].[ShipTo] ADD POAmount [decimal](18, 2) NULL
		
		ALTER TABLE [dbo].[ShipTo] ADD CONSTRAINT [DF_ShipTo_POAmount]  DEFAULT ((0)) FOR [POAmount]
	END
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'HelpFile')
	BEGIN
		PRINT N'HelpFile exists';
	END
ELSE
	BEGIN
		PRINT N'HelpFile added';
		CREATE TABLE [dbo].[HelpFile](
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[PageName] [varchar](100) NULL,
			[RoleId] [int] NOT NULL,
			[HelpDescription] [varchar](5000) NULL,
			[IsActive] [bit] NULL,
		    CONSTRAINT [PK_HelpFile] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]	

		ALTER TABLE [dbo].[HelpFile]  WITH CHECK ADD  CONSTRAINT [FK_HelpFile_UserRoles] FOREIGN KEY([RoleId]) REFERENCES [dbo].[UserRoles] ([RoleId])
		
		ALTER TABLE [dbo].[HelpFile] CHECK CONSTRAINT [FK_HelpFile_UserRoles]
		
		SET IDENTITY_INSERT [dbo].[HelpFile] ON 

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive]) VALUES (1,N'Home', 1, 'Help File for Home With role Admin' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (2,N'User', 1, 'Help File for User With role Admin' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (3,N'UserRole', 1, 'Help File for UserRole With role Admin' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (4,N'EmailSubscription',1, 'Help File for EmailSubscription With role Admin' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive]) VALUES (5,N'Home', 2, 'Help File for Home With role Manager' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (6,N'User', 2, 'Help File for User With role Manager' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (7,N'UserRole', 2, 'Help File for UserRole With role Manager' , 1)

		INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (8,N'EmailSubscription',2, 'Help File for EmailSubscription With role Manager' , 1)

		SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		
	END
	
	GO

	IF EXISTS(select * from HelpFile where Id=9)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (9,N'JobStep',1, 'Help File for JobStep With role Admin' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END	
	
	
	GO

	IF EXISTS(select * from HelpFile where Id=10)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (10,N'JobSheet',1, 'Help File for JobSheet With role Admin' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
			GO

	IF EXISTS(select * from HelpFile where Id=11)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (11,N'JobStep',1, 'Help File for JobStep With role Manager' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END	
	
	
	GO

	IF EXISTS(select * from HelpFile where Id=12)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (12,N'JobSheet',1, 'Help File for JobSheet With role Manager' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=13)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (13,N'ManagerPM',1, 'Help File for ManagerPM With role Admin' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=14)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (14,N'ManagerPM',2, 'Help File for ManagerPM With role Manager' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=15)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (15,N'OutSideSalesRepPM',1, 'Help File for OutSideSalesRepPM With role Admin' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=16)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (16,N'OutSideSalesRepPM',2, 'Help File for OutSideSalesRepPM With role Manager' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=17)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (17,N'DelegatePM',1, 'Help File for DelegatePM With role Admin' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
		
	GO

	IF EXISTS(select * from HelpFile where Id=18)
		BEGIN	
			Print N'';
			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[HelpFile] ON
			INSERT [dbo].[HelpFile] ([Id],[PageName], [RoleId], [HelpDescription], [IsActive])  VALUES (18,N'DelegatePM',1, 'Help File for DelegatePM With role Manager' , 1)
			SET IDENTITY_INSERT [dbo].[HelpFile] OFF
		END
GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsActive' AND OBJECT_ID = OBJECT_ID(N'Vendor'))
		BEGIN
		
			IF EXISTS(SELECT * FROM SYS.OBJECTS WHERE TYPE = 'D' AND NAME = 'DF_Vendor_IsActive')
				BEGIN
					PRINT N'IsActive ';	
				END
			ELSE
				BEGIN
					PRINT N'IsActive Default value added';
					ALTER TABLE [dbo].[Vendor] ADD CONSTRAINT [DF_Vendor_IsActive]  DEFAULT ((1)) FOR [IsActive]
				END
			
		END
	ELSE
		BEGIN
			PRINT N'IsActive ';		
		
	END	
	
	
GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'VendorNumber' AND OBJECT_ID = OBJECT_ID(N'ProjectOrderLine'))
		BEGIN
			PRINT N'VendorNumber exists';
		END
	ELSE
		BEGIN
			PRINT N'VendorNumber added';		
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD VendorNumber [decimal](18, 0) NULL
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD CONSTRAINT [DF_ProjectOrderLine_VendorNumber]  DEFAULT ((0)) FOR [VendorNumber]
	END	
	
	
GO

IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'OrderAmount' AND OBJECT_ID = OBJECT_ID(N'ProjectOrderLine'))
		BEGIN
			PRINT N'OrderAmount exists';
		END
	ELSE
		BEGIN
			PRINT N'OrderAmount added';		
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD OrderAmount [decimal](24, 5) NULL
		
		ALTER TABLE [dbo].[ProjectOrderLine] ADD CONSTRAINT [DF_ProjectOrderLine_OrderAmount]  DEFAULT ((0)) FOR [OrderAmount]
	END	
	
	
GO


	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'CreationDate' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'CreationDate exists';
		END
	ELSE
		BEGIN
			PRINT N'CreationDate added';
		ALTER TABLE [dbo].[ShipTo] ADD CreationDate datetime NULL
	END
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'OrderLines')
	BEGIN
		PRINT N'Droping OrderLines';
		Drop table OrderLines
		
	END
ELSE
	BEGIN
		PRINT N'No Drop orderlines';		
	END

GO

IF EXISTS (SELECT * FROM sys.foreign_keys  WHERE object_id = OBJECT_ID(N'[dbo].[FK_233]') AND parent_object_id = OBJECT_ID(N'[dbo].[OrderHeader]'))
			BEGIN
				PRINT N'Droping FK_233';
				ALTER TABLE [dbo].[OrderHeader] DROP CONSTRAINT [FK_233]
			END
ELSE
			BEGIN
				PRINT N'FK_233';
			END
			
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Salesman')
	BEGIN
		PRINT N'Droping Salesman';
		Drop table Salesman
		
	END
ELSE
	BEGIN
		PRINT N'No Drop Salesman';		
	END

GO

IF EXISTS (SELECT * FROM sys.foreign_keys  WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProjectOrderLine_Vendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProjectOrderLine]'))
			BEGIN
				PRINT N'FK_ProjectOrderLine_Vendor';
				
			END
ELSE
			BEGIN
				PRINT N'Added FK_ProjectOrderLine_Vendor';
				ALTER TABLE [dbo].[ProjectOrderLine] WITH NOCHECK ADD CONSTRAINT [FK_ProjectOrderLine_Vendor] FOREIGN KEY ([VendorNumber]) REFERENCES [dbo].[Vendor] ([VendorNumber]) ON DELETE CASCADE;
			END
			
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'JobSteps')
	BEGIN
		PRINT N'JobSteps exists';
	END
ELSE
	BEGIN
		PRINT N'JobSteps added';
		CREATE TABLE [dbo].[JobSteps](
		[JobStepId] [int] IDENTITY(1,1) NOT NULL,
		[StepName] [varchar](100) NOT NULL,
		[StepOrder] [int] NOT NULL,
		[DaysUntilAlert] [int] NOT NULL,
		[IsActive] [bit] NULL,
		 CONSTRAINT [PK_JobSteps] PRIMARY KEY CLUSTERED 
		(
			[JobStepId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	

		ALTER TABLE [dbo].[JobSteps] ADD  CONSTRAINT [DF_JobSteps_IsActive]  DEFAULT ((1)) FOR [IsActive]
		
	END

GO	
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'JobStepLog')
	BEGIN
		PRINT N'JobStepLog exists';
	END
ELSE
	BEGIN
		PRINT N'JobStepLog added';
		CREATE TABLE [dbo].[JobStepLog](
			[JSLogId] [int] IDENTITY(1,1) NOT NULL,
			[JobStepId] [int] NOT NULL,
			[ModifyingUserId] [int] NOT NULL,
			[DateModified] [datetime] NOT NULL,
			[IsActive] [bit] NULL,
			 CONSTRAINT [PK_JobStepLog] PRIMARY KEY CLUSTERED 
			(
				[JSLogId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		

		ALTER TABLE [dbo].[JobStepLog] ADD  CONSTRAINT [DF_JobStepLog_IsActive]  DEFAULT ((1)) FOR [IsActive]
		

		ALTER TABLE [dbo].[JobStepLog]  WITH CHECK ADD  CONSTRAINT [FK_JobStepLog_JobStep] FOREIGN KEY([JobStepId])
		REFERENCES [dbo].[JobSteps] ([JobStepId])
		

		ALTER TABLE [dbo].[JobStepLog] CHECK CONSTRAINT [FK_JobStepLog_JobStep]
		

		ALTER TABLE [dbo].[JobStepLog]  WITH CHECK ADD  CONSTRAINT [FK_JobStepLog_User] FOREIGN KEY([ModifyingUserId])
		REFERENCES [dbo].[User] ([UserId])
		

		ALTER TABLE [dbo].[JobStepLog] CHECK CONSTRAINT [FK_JobStepLog_User]
		
	END

GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsLockRecord' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'IsLockRecord exists';
		END
	ELSE
		BEGIN
			PRINT N'IsLockRecord added';
		ALTER TABLE [dbo].[ShipTo] ADD IsLockRecord bit NULL
		
		ALTER TABLE [dbo].[ShipTo] ADD CONSTRAINT [DF_ShipTo_IsLockRecord]  DEFAULT ((0)) FOR [IsLockRecord]
	END
	
	GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsCcgNotified' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'IsCcgNotified exists';
		END
	ELSE
		BEGIN
			PRINT N'IsCcgNotified added';
		ALTER TABLE [dbo].[ShipTo] ADD IsCcgNotified bit NULL
		
		ALTER TABLE [dbo].[ShipTo] ADD CONSTRAINT [DF_ShipTo_IsCcgNotified]  DEFAULT ((0)) FOR [IsCcgNotified]
	END
	
	GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'Status')
	BEGIN
		PRINT N'Status exists';
	END
ELSE
	BEGIN
		PRINT N'Status added';
		CREATE TABLE [dbo].[Status](
		[StatusId] [int] IDENTITY(1,1) NOT NULL,
		[StatusName] [varchar](50) NULL,
		[IsActive] [bit] NULL,
		 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
		(
			[StatusId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [dbo].[Status] ADD  CONSTRAINT [DF_Status_IsActive]  DEFAULT ((1)) FOR [IsActive]
		
		
		SET IDENTITY_INSERT [dbo].[Status] ON 

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (1, N'Submitted', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (2, N'Incomplete Documentation', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (3, N'Payment Issue', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (4, N'Escalation', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (5, N'Pending Approval', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (6, N'Approved', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (7, N'Completed', 1)

		INSERT [dbo].[Status] ([StatusId], [StatusName], [IsActive]) VALUES (8, N'Rejected', 1)

		SET IDENTITY_INSERT [dbo].[Status] OFF
		
	END

	GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'JobSheet')
	BEGIN
		PRINT N'JobSheet exists';
	END
ELSE
	BEGIN
	PRINT N'JobSheet added';
	CREATE TABLE [dbo].[JobSheet](
	[JobSheetId] [int] IDENTITY(1,1) NOT NULL,
	[JobSheetName] [varchar](500) NOT NULL,
	[Company] [int] NOT NULL,
	[AccountName] [varchar](500) NOT NULL,
	[AccountNumber] [int] NULL,
	[Status] [int] NULL,
	[JobOrShipToNumber] [varchar](50) NULL,
	[ManagementApproved] [bit] NULL,
	[SetUpDate] [date] NULL,
	[SetUpTime] [time](7) NULL,
	[TMCommissionPosition1Id] [int] NOT NULL,
	[TMCommisionPosition1] [varchar](50) NOT NULL,
	[TMCommissionPosition2Id] [int] NULL,
	[TMCommissionPosition2] [varchar](50) NULL,
	[ManagerApproval] [int] NULL,
	[ManagerApprovedDate] [datetime] NULL,
	[SubmittedBy] [int] NOT NULL,
	[Owner] [int] NULL,
	[CreditRep] [int] NULL,
	[JobName] [varchar](500) NOT NULL,
	[CountyOfJob] [varchar](100) NOT NULL,
	[JobStreeAddress] [text] NOT NULL,
	[JobState] [varchar](50) NOT NULL,
	[JobCity] [varchar](50) NOT NULL,
	[JobZip] [int] NOT NULL,
	[GeneralContractor] [varchar](100) NULL,
	[GCMailingAddress] [text] NULL,
	[GCPhoneNumber] [nvarchar](50) NULL,
	[GCCity] [varchar](50) NULL,
	[GCState] [varchar](50) NULL,
	[GCZip] [int] NULL,
	[OwnerName] [varchar](100) NULL,
	[OMailingAddress] [text] NULL,
	[OPhoneNumber] [nvarchar](50) NULL,
	[OCity] [varchar](50) NULL,
	[OState] [varchar](50) NULL,
	[OZip] [int] NULL,
	[BondedJob] [varchar](50) NOT NULL,
	[CopyOfBondAttached] [varchar](50) NULL,
	[BondInformation] [text] NULL,
	[TaxExemption] [varchar](50) NOT NULL,
	[TotalPurchases] [decimal](18, 0) NOT NULL,
	[AnticipatedFirstDelivery] [datetime] NOT NULL,
	[AnticipatedJobClose] [datetime] NOT NULL,
	[JobLine] [varchar](100) NULL,
	[PrelienAmount] [decimal](18, 0) NULL,
	[Prelien] [varchar](50) NULL,
	[PrelienCompany] [varchar](100) NULL,
	[PrelienDate] [datetime] NULL,
	[Notes] [text] NULL,
	[SubmittedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [int] NOT NULL,
	[LastModifiedDate] [datetime] NULL,
	 CONSTRAINT [PK_JobSheet] PRIMARY KEY CLUSTERED 
	(
		[JobSheetId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


	ALTER TABLE [dbo].[JobSheet] ADD  CONSTRAINT [DF_JobSheet_IsActive]  DEFAULT ((1)) FOR [IsActive]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_CreatedUser] FOREIGN KEY([CreatedBy])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_CreatedUser]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_CreditRep] FOREIGN KEY([CreditRep])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_CreditRep]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_ManagerApproval] FOREIGN KEY([ManagerApproval])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_ManagerApproval]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_ModifiedUser] FOREIGN KEY([LastModifiedBy])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_ModifiedUser]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_Owner] FOREIGN KEY([Owner])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_Owner]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_Status] FOREIGN KEY([Status])
	REFERENCES [dbo].[Status] ([StatusId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_Status]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_SubmittedBy] FOREIGN KEY([SubmittedBy])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_SubmittedBy]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_TmCP1] FOREIGN KEY([TMCommissionPosition1Id])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_TmCP1]


	ALTER TABLE [dbo].[JobSheet]  WITH CHECK ADD  CONSTRAINT [FK_JobSheet_TmCP2] FOREIGN KEY([TMCommissionPosition2Id])
	REFERENCES [dbo].[User] ([UserId])


	ALTER TABLE [dbo].[JobSheet] CHECK CONSTRAINT [FK_JobSheet_TmCP2]
	
	END
	
	GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsLockRecord' AND OBJECT_ID = OBJECT_ID(N'JobSheet'))
		BEGIN
			PRINT N'IsLockRecord exists';
		END
	ELSE
		BEGIN
			PRINT N'IsLockRecord added';
		ALTER TABLE [dbo].[JobSheet] ADD IsLockRecord bit NULL
		
		ALTER TABLE [dbo].[JobSheet] ADD CONSTRAINT [DF_JobSheet_IsLockRecord]  DEFAULT ((0)) FOR [IsLockRecord]
	END
	
	GO

	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IsCcgNotified' AND OBJECT_ID = OBJECT_ID(N'JobSheet'))
		BEGIN
			PRINT N'IsCcgNotified exists';
		END
	ELSE
		BEGIN
			PRINT N'IsCcgNotified added';
		ALTER TABLE [dbo].[JobSheet] ADD IsCcgNotified bit NULL
		
		ALTER TABLE [dbo].[JobSheet] ADD CONSTRAINT [DF_JobSheet_IsCcgNotified]  DEFAULT ((0)) FOR [IsCcgNotified]
	END
	
	GO
	
	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'CompanyName' AND OBJECT_ID = OBJECT_ID(N'JobSheet'))
		BEGIN
			PRINT N'CompanyName exists';
		END
	ELSE
		BEGIN
			PRINT N'CompanyName added';
		ALTER TABLE [dbo].[JobSheet] ADD CompanyName varchar(100) NULL
	END
	
	GO
	
	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'ShipToId' AND OBJECT_ID = OBJECT_ID(N'JobSheet'))
		BEGIN
			PRINT N'ShipToId exists';
		END
	ELSE
		BEGIN
			PRINT N'ShipToId added';
		ALTER TABLE [dbo].[JobSheet] ADD ShipToId char(30) NULL
	END
	
	GO
	
	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'ShipToName' AND OBJECT_ID = OBJECT_ID(N'JobSheet'))
		BEGIN
			PRINT N'ShipToName exists';
		END
	ELSE
		BEGIN
			PRINT N'ShipToName added';
		ALTER TABLE [dbo].[JobSheet] ADD ShipToName varchar(50) NULL
	END
	
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'ManagerPM')
	BEGIN
		PRINT N'ManagerPM exists';
	END
ELSE
	BEGIN
		PRINT N'ManagerPM added';
		CREATE TABLE [dbo].[ManagerPM](
	[ManagerPMId] [int] IDENTITY(1,1) NOT NULL,
	[ManagerId] [int] NOT NULL,
	[PMId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[IsActive] [bit] NULL,
	 CONSTRAINT [PK_ManagerPM] PRIMARY KEY CLUSTERED 
	(
		[ManagerPMId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	

	ALTER TABLE [dbo].[ManagerPM] ADD  CONSTRAINT [DF_ManagerPM_IsActive]  DEFAULT ((1)) FOR [IsActive]
	

	ALTER TABLE [dbo].[ManagerPM]  WITH CHECK ADD  CONSTRAINT [FK_ManagerPM_CreatedBy] FOREIGN KEY([CreatedBy])
	REFERENCES [dbo].[User] ([UserId])
	

	ALTER TABLE [dbo].[ManagerPM] CHECK CONSTRAINT [FK_ManagerPM_CreatedBy]
	

	ALTER TABLE [dbo].[ManagerPM]  WITH CHECK ADD  CONSTRAINT [FK_ManagerPM_ManagerUser] FOREIGN KEY([ManagerId])
	REFERENCES [dbo].[User] ([UserId])
	

	ALTER TABLE [dbo].[ManagerPM] CHECK CONSTRAINT [FK_ManagerPM_ManagerUser]
	

	ALTER TABLE [dbo].[ManagerPM]  WITH CHECK ADD  CONSTRAINT [FK_ManagerPM_PMUser] FOREIGN KEY([PMId])
	REFERENCES [dbo].[User] ([UserId])
	

	ALTER TABLE [dbo].[ManagerPM] CHECK CONSTRAINT [FK_ManagerPM_PMUser]
		
	END
	
	GO
	
	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'JobName' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'JobName exists';
		END
	ELSE
		BEGIN
			PRINT N'JobName added';
		ALTER TABLE [dbo].[ShipTo] ADD JobName varchar(500) NULL
	END
	
	GO
	
	IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'JobSheetName' AND OBJECT_ID = OBJECT_ID(N'ShipTo'))
		BEGIN
			PRINT N'JobSheetName exists';
		END
	ELSE
		BEGIN
			PRINT N'JobSheetName added';
		ALTER TABLE [dbo].[ShipTo] ADD JobSheetName varchar(500) NULL
	END
	
	GO
	
	IF EXISTS(select * from UserRoles where RoleId=8)
		BEGIN	
			Print N'';			
		END
	ELSE
		BEGIN
			SET IDENTITY_INSERT [dbo].[UserRoles] ON 			
			INSERT [dbo].[UserRoles] ([RoleId], [RoleName], [Permissions], [IsActive]) VALUES (8, N'OutSide Sales Rep', N'OutSide Sales Rep', 1)
			SET IDENTITY_INSERT [dbo].[UserRoles] OFF
		END
		
	GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'OutSideSalesRepPM')
	BEGIN
		PRINT N'OutSideSalesRepPM exists';
	END
ELSE
	BEGIN
		PRINT N'OutSideSalesRepPM added';
		CREATE TABLE [dbo].[OutSideSalesRepPM](
		[OutSideSalesRepPMId] [int] IDENTITY(1,1) NOT NULL,
		[OutSideSalesRepId] [int] NOT NULL,
		[PMId] [int] NOT NULL,
		[CreatedBy] [int] NOT NULL,
		[IsActive] [bit] NULL,
		 CONSTRAINT [PK_OutSideSalesRepPM] PRIMARY KEY CLUSTERED 
		(
			[OutSideSalesRepPMId] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]


		ALTER TABLE [dbo].[OutSideSalesRepPM] ADD  CONSTRAINT [DF_OutSideSalesRepPM_IsActive]  DEFAULT ((1)) FOR [IsActive]


		ALTER TABLE [dbo].[OutSideSalesRepPM]  WITH CHECK ADD  CONSTRAINT [FK_OutSideSalesRepPM_CreatedBy] FOREIGN KEY([CreatedBy])
		REFERENCES [dbo].[User] ([UserId])


		ALTER TABLE [dbo].[OutSideSalesRepPM] CHECK CONSTRAINT [FK_OutSideSalesRepPM_CreatedBy]


		ALTER TABLE [dbo].[OutSideSalesRepPM]  WITH CHECK ADD  CONSTRAINT [FK_OutSideSalesRepPM_OutSideSalesRepUser] FOREIGN KEY([OutSideSalesRepId])
		REFERENCES [dbo].[User] ([UserId])


		ALTER TABLE [dbo].[OutSideSalesRepPM] CHECK CONSTRAINT [FK_OutSideSalesRepPM_OutSideSalesRepUser]


		ALTER TABLE [dbo].[OutSideSalesRepPM]  WITH CHECK ADD  CONSTRAINT [FK_OutSideSalesRepPM_PMUser] FOREIGN KEY([PMId])
		REFERENCES [dbo].[User] ([UserId])


		ALTER TABLE [dbo].[OutSideSalesRepPM] CHECK CONSTRAINT [FK_OutSideSalesRepPM_PMUser]

	END


	GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'DelegatePM')
	BEGIN
		PRINT N'DelegatePM exists';
	END
ELSE
	BEGIN
		PRINT N'DelegatePM added';
		CREATE TABLE [dbo].[DelegatePM](
	[DelegatePMId] [int] IDENTITY(1,1) NOT NULL,
	[DelegateUserId] [int] NOT NULL,
	[PMId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_DelegatePM] PRIMARY KEY CLUSTERED 
(
	[DelegatePMId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[DelegatePM] ADD  CONSTRAINT [DF_DelegatePM_IsActive]  DEFAULT ((1)) FOR [IsActive]


ALTER TABLE [dbo].[DelegatePM]  WITH CHECK ADD  CONSTRAINT [FK_DelegatePM_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserId])


ALTER TABLE [dbo].[DelegatePM] CHECK CONSTRAINT [FK_DelegatePM_CreatedBy]


ALTER TABLE [dbo].[DelegatePM]  WITH CHECK ADD  CONSTRAINT [FK_DelegatePM_DelegateUser] FOREIGN KEY([DelegateUserId])
REFERENCES [dbo].[User] ([UserId])


ALTER TABLE [dbo].[DelegatePM] CHECK CONSTRAINT [FK_DelegatePM_DelegateUser]


ALTER TABLE [dbo].[DelegatePM]  WITH CHECK ADD  CONSTRAINT [FK_DelegatePM_PMUser] FOREIGN KEY([PMId])
REFERENCES [dbo].[User] ([UserId])


ALTER TABLE [dbo].[DelegatePM] CHECK CONSTRAINT [FK_DelegatePM_PMUser]

	END

PRINT N'Update complete.';
GO



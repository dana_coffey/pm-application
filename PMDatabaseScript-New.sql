GO
/****** Object:  Table [dbo].[ChangeLog]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeLog](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[ModifiedByUserId] [int] NOT NULL,
	[AttributeName] [varchar](30) NOT NULL,
	[PreviousVal] [varchar](2000) NOT NULL,
	[NewVal] [varchar](2000) NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
	[JobId] [char](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustNo] [char](30) NOT NULL,
	[CustomerName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Email]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email](
	[EmailId] [int] IDENTITY(1,1) NOT NULL,
	[EmailFrom] [varchar](1000) NOT NULL,
	[EmailTitle] [varchar](1000) NOT NULL,
	[EmailBody] [varchar](1000) NOT NULL,
	[IsAutomated] [bit] NOT NULL,
	[TriggerId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED 
(
	[EmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailLog]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailLog](
	[EmailLogId] [int] IDENTITY(1,1) NOT NULL,
	[DateSent] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
	[EmailId] [int] NOT NULL,
	[JobId] [char](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_EmailLog] PRIMARY KEY CLUSTERED 
(
	[EmailLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailSubscription]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailSubscription](
	[EmailSubscriptionId] [int] IDENTITY(1,1) NOT NULL,
	[SubsciptionTypeId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[isInternalOnly] [bit] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_EmailSubscription] PRIMARY KEY CLUSTERED 
(
	[EmailSubscriptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTriggers]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTriggers](
	[TriggerId] [int] IDENTITY(1,1) NOT NULL,
	[TriggerName] [varchar](100) NOT NULL,
	[TriggerDescription] [varchar](3000) NOT NULL,
	[TriggerActionTime] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Triggers] PRIMARY KEY CLUSTERED 
(
	[TriggerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HelpFile]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HelpFile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [varchar](100) NULL,
	[RoleId] [int] NOT NULL,
	[HelpDescription] [varchar](5000) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_HelpFile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHeader]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHeader](
	[OrderNumber] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [char](20) NOT NULL,
	[CustNo] [char](30) NOT NULL,
	[ShipToState] [char](2) NOT NULL,
	[ShipToZip] [char](10) NOT NULL,
	[ShipToAddr] [char](30) NOT NULL,
	[SalesRepId] [char](4) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_OeHeader] PRIMARY KEY CLUSTERED 
(
	[OrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Project]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[JobId] [char](20) NOT NULL,
	[ProjectId] [int] IDENTITY(1,1) NOT NULL,
	[JobName] [char](30) NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[EmailContact] [varchar](100) NOT NULL,
	[BilledToDate] [money] NOT NULL,
	[SubmittalStatus] [varchar](50) NOT NULL,
	[VendorOrderStatus] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProjectOrderLine]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectOrderLine](
	[ProjectOrderLineId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [int] NULL,
	[OrderSurf] [int] NULL,
	[LineNumber] [int] NULL,
	[ModelPart] [nvarchar](100) NULL,
	[POAmount] [int] NULL,
	[QuantityOrdered] [decimal](18, 2) NULL,
	[QuantityShipped] [decimal](18, 2) NULL,
	[CustomerRequestedDeliveryDate] [datetime] NULL,
	[VendorName] [decimal](18, 0) NULL,
	[VerifiedVoltage] [bit] NULL,
	[IsActive] [bit] NULL,
	[CustNo] [decimal](15, 0) NULL,
	[ShipTo] [nvarchar](50) NULL,
	[VendorNumber] [decimal](18, 0) NULL,
	[OrderAmount] [decimal](24, 5) NULL,
 CONSTRAINT [PK_ProjectOrderLine] PRIMARY KEY CLUSTERED 
(
	[ProjectOrderLineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleBasedAccess]    Script Date: 1/11/2019 2:00:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleBasedAccess](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [varchar](100) NULL,
	[RoleId] [int] NOT NULL,
	[IsEdit] [bit] NULL,
	[IsView] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_RoleBasedAccess] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShipTo]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShipTo](
	[ShipToCustomerID] [int] IDENTITY(1,1) NOT NULL,
	[ShipToName] [char](30) NOT NULL,
	[SlsRep] [varchar](50) NULL,
	[StatusType] [bit] NOT NULL,
	[CustNo] [char](30) NOT NULL,
	[JobId] [char](20) NOT NULL,
	[OSSalesRep] [varchar](50) NOT NULL,
	[SfOpp] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[ShipToId] [varchar](50) NULL,
	[ShipToAddress] [text] NULL,
	[VerifiedVoltage] [bit] NULL,
	[POAmount] [decimal](18, 2) NULL,
	[CreationDate] [datetime] NULL,
 CONSTRAINT [PK_ShipTo] PRIMARY KEY CLUSTERED 
(
	[ShipToCustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShipToChangeLog]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShipToChangeLog](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[ModifiedByUserId] [int] NOT NULL,
	[AttributeName] [varchar](30) NOT NULL,
	[PreviousVal] [varchar](2000) NOT NULL,
	[NewVal] [varchar](2000) NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
	[ShipToCustomerID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ShipToLog] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SMSN]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSN](
	[SlsRep] [varchar](50) NOT NULL,
	[CoNo] [int] NOT NULL,
	[SlsType] [varchar](50) NULL,
	[Name] [varchar](500) NULL,
	[Email] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SMSN] PRIMARY KEY CLUSTERED 
(
	[SlsRep] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriptionType]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionType](
	[SubsciptionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[SubscriptionTypeName] [nvarchar](100) NULL,
	[SubscriptionTypeDescription] [text] NULL,
	[isRealTimeEmail] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_SubscriptionType] PRIMARY KEY CLUSTERED 
(
	[SubsciptionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[SlsRep] [varchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](300) NOT NULL,
	[Permissions] [varchar](300) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vendor]    Script Date: 1/11/2019 2:00:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vendor](
	[VendorNumber] [decimal](18, 0) NOT NULL,
	[VendorName] [char](30) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[VendorNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_219]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_219] ON [dbo].[ChangeLog]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_87]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_87] ON [dbo].[ChangeLog]
(
	[ModifiedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_169]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_169] ON [dbo].[Email]
(
	[TriggerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_195]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_195] ON [dbo].[EmailLog]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_198]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_198] ON [dbo].[EmailLog]
(
	[EmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_211]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_211] ON [dbo].[EmailLog]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_179]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [FK_179] ON [dbo].[EmailSubscription]
(
	[SubsciptionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [FK_186]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [FK_186] ON [dbo].[EmailSubscription]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_233]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_233] ON [dbo].[OrderHeader]
(
	[SalesRepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_47]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_47] ON [dbo].[OrderHeader]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_68]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_68] ON [dbo].[OrderHeader]
(
	[CustNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_65]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_65] ON [dbo].[ShipTo]
(
	[CustNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [fkIdx_76]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_76] ON [dbo].[ShipTo]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_207]    Script Date: 1/11/2019 2:00:01 PM ******/
CREATE NONCLUSTERED INDEX [fkIdx_207] ON [dbo].[User]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailSubscription] ADD  CONSTRAINT [DF_EmailSubscription_isInternalOnly]  DEFAULT ((1)) FOR [isInternalOnly]
GO
ALTER TABLE [dbo].[ProjectOrderLine] ADD  CONSTRAINT [DF_ProjectOrderLine_VerifiedVoltage]  DEFAULT ((0)) FOR [VerifiedVoltage]
GO
ALTER TABLE [dbo].[ProjectOrderLine] ADD  CONSTRAINT [DF_ProjectOrderLine_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ProjectOrderLine] ADD  CONSTRAINT [DF_ProjectOrderLine_OrderAmount]  DEFAULT ((0)) FOR [OrderAmount]
GO
ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsEdit]  DEFAULT ((1)) FOR [IsEdit]
GO
ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsView]  DEFAULT ((1)) FOR [IsView]
GO
ALTER TABLE [dbo].[RoleBasedAccess] ADD  CONSTRAINT [DF_RoleBasedAccess_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ShipTo] ADD  CONSTRAINT [DF_ShipTo_VerifiedVoltage]  DEFAULT ((0)) FOR [VerifiedVoltage]
GO
ALTER TABLE [dbo].[ShipTo] ADD  CONSTRAINT [DF_ShipTo_POAmount]  DEFAULT ((0)) FOR [POAmount]
GO
ALTER TABLE [dbo].[SubscriptionType] ADD  CONSTRAINT [DF_SubscriptionType_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Vendor] ADD  CONSTRAINT [DF_Vendor_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_219] FOREIGN KEY([JobId])
REFERENCES [dbo].[Project] ([JobId])
GO
ALTER TABLE [dbo].[ChangeLog] CHECK CONSTRAINT [FK_219]
GO
ALTER TABLE [dbo].[ChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_87] FOREIGN KEY([ModifiedByUserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[ChangeLog] CHECK CONSTRAINT [FK_87]
GO
ALTER TABLE [dbo].[Email]  WITH CHECK ADD  CONSTRAINT [FK_169] FOREIGN KEY([TriggerId])
REFERENCES [dbo].[EmailTriggers] ([TriggerId])
GO
ALTER TABLE [dbo].[Email] CHECK CONSTRAINT [FK_169]
GO
ALTER TABLE [dbo].[EmailLog]  WITH CHECK ADD  CONSTRAINT [FK_195] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[EmailLog] CHECK CONSTRAINT [FK_195]
GO
ALTER TABLE [dbo].[EmailLog]  WITH CHECK ADD  CONSTRAINT [FK_198] FOREIGN KEY([EmailId])
REFERENCES [dbo].[Email] ([EmailId])
GO
ALTER TABLE [dbo].[EmailLog] CHECK CONSTRAINT [FK_198]
GO
ALTER TABLE [dbo].[EmailLog]  WITH CHECK ADD  CONSTRAINT [FK_213] FOREIGN KEY([JobId])
REFERENCES [dbo].[Project] ([JobId])
GO
ALTER TABLE [dbo].[EmailLog] CHECK CONSTRAINT [FK_213]
GO
ALTER TABLE [dbo].[EmailSubscription]  WITH NOCHECK ADD  CONSTRAINT [FK_179] FOREIGN KEY([SubsciptionTypeId])
REFERENCES [dbo].[SubscriptionType] ([SubsciptionTypeId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmailSubscription] CHECK CONSTRAINT [FK_179]
GO
ALTER TABLE [dbo].[EmailSubscription]  WITH CHECK ADD  CONSTRAINT [FK_186] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[EmailSubscription] CHECK CONSTRAINT [FK_186]
GO
ALTER TABLE [dbo].[HelpFile]  WITH CHECK ADD  CONSTRAINT [FK_HelpFile_UserRoles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRoles] ([RoleId])
GO
ALTER TABLE [dbo].[HelpFile] CHECK CONSTRAINT [FK_HelpFile_UserRoles]
GO
ALTER TABLE [dbo].[OrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_47] FOREIGN KEY([JobId])
REFERENCES [dbo].[Project] ([JobId])
GO
ALTER TABLE [dbo].[OrderHeader] CHECK CONSTRAINT [FK_47]
GO
ALTER TABLE [dbo].[OrderHeader]  WITH CHECK ADD  CONSTRAINT [FK_68] FOREIGN KEY([CustNo])
REFERENCES [dbo].[Customer] ([CustNo])
GO
ALTER TABLE [dbo].[OrderHeader] CHECK CONSTRAINT [FK_68]
GO
ALTER TABLE [dbo].[RoleBasedAccess]  WITH CHECK ADD  CONSTRAINT [FK_RoleBasedAccess_UserRoles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRoles] ([RoleId])
GO
ALTER TABLE [dbo].[RoleBasedAccess] CHECK CONSTRAINT [FK_RoleBasedAccess_UserRoles]
GO
ALTER TABLE [dbo].[ShipTo]  WITH CHECK ADD  CONSTRAINT [FK_65] FOREIGN KEY([CustNo])
REFERENCES [dbo].[Customer] ([CustNo])
GO
ALTER TABLE [dbo].[ShipTo] CHECK CONSTRAINT [FK_65]
GO
ALTER TABLE [dbo].[ShipTo]  WITH CHECK ADD  CONSTRAINT [FK_ShipTo_SMSN] FOREIGN KEY([SlsRep])
REFERENCES [dbo].[SMSN] ([SlsRep])
GO
ALTER TABLE [dbo].[ShipTo] CHECK CONSTRAINT [FK_ShipTo_SMSN]
GO
ALTER TABLE [dbo].[ShipToChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ShipToChange_ShipTo] FOREIGN KEY([ShipToCustomerID])
REFERENCES [dbo].[ShipTo] ([ShipToCustomerID])
GO
ALTER TABLE [dbo].[ShipToChangeLog] CHECK CONSTRAINT [FK_ShipToChange_ShipTo]
GO
ALTER TABLE [dbo].[ShipToChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ShipToChange_User] FOREIGN KEY([ModifiedByUserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[ShipToChangeLog] CHECK CONSTRAINT [FK_ShipToChange_User]
GO
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_SMSN] FOREIGN KEY([SlsRep])
REFERENCES [dbo].[SMSN] ([SlsRep])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_SMSN]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.orderno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'OrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.jmjobid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'JobId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.custno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'CustNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.shiptost' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'ShipToState'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.shiptozip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'ShipToZip'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.OEEH.shiptoaddr' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader', @level2type=N'COLUMN',@level2name=N'ShipToAddr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'maps to SXE.OEEH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrderHeader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.ARSS.name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'JobId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.ARSS.name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'JobName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'source TBD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'BilledToDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'souce TBD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'SubmittalStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data comes from .dat file in FIX010/utc/incoming from SAP - limited to Carrier Items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project', @level2type=N'COLUMN',@level2name=N'VendorOrderStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'source TBD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Project'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.ARSS.name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShipTo', @level2type=N'COLUMN',@level2name=N'ShipToName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.ARSS.user1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShipTo', @level2type=N'COLUMN',@level2name=N'SlsRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.ARSS.statustype' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShipTo', @level2type=N'COLUMN',@level2name=N'StatusType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.APSV.vendno' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vendor', @level2type=N'COLUMN',@level2name=N'VendorNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SXE.APSV.name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vendor', @level2type=N'COLUMN',@level2name=N'VendorName'
GO


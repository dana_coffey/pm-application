﻿/// <summary>
/// Email class for email notification 
/// </summary>


using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using NLog;

namespace PM_Emailer
{
    using System.Net.Mail;
    using System.Net;
    using System.Configuration;
    using System.Net.Configuration;
    using System;

    public static class Email
    {
        public static void SendMail(string Message, string EmailTo, string EmailSubject, string CC = null, List<Attachment> attachments = null, string replyEmail = null)
        {
            Logger logger = LogManager.GetLogger("ErrorLog");
            try
            {
                var smtpConfig = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                if (smtpConfig == null) return;

                var client = new SmtpClient
                {
                    Port = smtpConfig.Network.Port,
                    Host = smtpConfig.Network.Host,
                    EnableSsl = smtpConfig.Network.EnableSsl,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(smtpConfig.Network.UserName,
                        smtpConfig.Network.Password)
                };


                var header = "<img src=\"cid:header\" width=\"150\"><br /><br />";

                var mailMessage = new MailMessage(smtpConfig.From, EmailTo, EmailSubject, Message)
                {
                    IsBodyHtml = true
                };
                if(!string.IsNullOrEmpty(CC))
                {
                    mailMessage.CC.Add(CC);
                }
                mailMessage.Bcc.Add("dcoffey@mingledorffs.com");

                if (!string.IsNullOrEmpty(replyEmail))
                {
                    mailMessage.ReplyToList.Add(replyEmail);
                }

                var ExcelExportFilePath = ConfigurationManager.AppSettings["EmailHeaderImagePath"] + @"\pm-logo-2.png";

               // var ExportFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ExcelExportFilePath);

               var tmpName = ConfigurationManager.AppSettings["EmailHeaderImagePath"] + Guid.NewGuid().ToString() +
                             ".png";

               File.Copy(ExcelExportFilePath, tmpName);

                var theFile = new FileStream(tmpName, FileMode.Open);
                var headerImage = new LinkedResource(theFile, "image/png") {ContentId = "header"};

                var headerView = AlternateView.CreateAlternateViewFromString(header + Message, null, "text/html");

                headerView.LinkedResources.Add(headerImage);
                mailMessage.AlternateViews.Add(headerView);

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        mailMessage.Attachments.Add(attachment);
                    }
                }

                client.Send(mailMessage);
                theFile.Close();
                File.Delete(tmpName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured while sending email");
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PM_Website;
using PM_Website.Controllers;
using PM_Website.Models;
//using Kendo.Mvc.UI;
//using Kendo.Mvc.Extensions;

namespace PM_Website.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        //[TestMethod]
        //public void About()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.About() as ViewResult;

        //    // Assert
        //    Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        //}

        //[TestMethod]
        //public void Contact()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    // Act
        //    ViewResult result = controller.Contact() as ViewResult;

        //    // Assert
        //    Assert.IsNotNull(result);
        //}

        //[TestMethod]
        //public void TestGetHelpDescription()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();

        //    //get the result back from the controller
        //    var controllerResult = controller.GetHelpDescription("Home");

        //    //cast the results to Json
        //    var jsonResult = controllerResult as JsonResult;

        //    //at runtime, jsonRsult.Data data will return variable of type Kendo.Mvc.UI.DataSourceResult
        //    var kendoResultData = jsonResult.Data as DataSourceResult;


        //    var results = kendoResultData.Data as List<ProjectOrderLineViewModel>;

        //    Assert.AreEqual(19, results.Count);
        //}

        //[TestMethod]
        //public void TestProjectOrderLine_Read()
        //{
        //    // Arrange
        //    HomeController controller = new HomeController();
            
        //    var kendoDataRequest = new DataSourceRequest();

        //    //get the result back from the controller
        //    var controllerResult = controller.ProjectOrderLine_Read(kendoDataRequest, "8", 63450,1,1);

        //    //cast the results to Json
        //    var jsonResult = controllerResult as JsonResult;

        //    //at runtime, jsonRsult.Data data will return variable of type Kendo.Mvc.UI.DataSourceResult
        //    var kendoResultData = jsonResult.Data as DataSourceResult;

            
        //    var results = kendoResultData.Data as List<ProjectOrderLineViewModel>;

        //    Assert.AreEqual(19, results.Count);

        //}      
    }
}

﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NLog;
using PM_Website.Models;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class CarrierSapReportController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        PageManager pageManager = new PageManager(connection);

        // GET: Checklist
        public ActionResult Index()
        {
            try
            {
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0,thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Carrier Sap Report");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        [HttpPost]
        public JsonResult GetSAPReport(DataSourceRequest request)
        {
            return Json(ReportService.GetSAPReport().ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult GetSAPReportLineItems(string SalesOrder, DataSourceRequest request)
        {
            return Json(ReportService.GetSAPReportLineItems(int.Parse(SalesOrder)).ToDataSourceResult(request));
        }

        [HttpGet]
        public JsonResult UploadSAP()
        {
            return Json(ReportService.ImportCarrierSapReport(), JsonRequestBehavior.AllowGet);
        }
    }
}
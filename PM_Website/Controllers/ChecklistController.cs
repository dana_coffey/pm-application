﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using PM_Website.Models;

namespace PM_Website.Controllers
{
    public class ChecklistController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Check List View");
                       
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;
using PM_Website.Services;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace PM_Website.Controllers
{
    public class CreditController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        ProjectsService _projectsService = new ProjectsService(connection);
        HelpService helpService = new HelpService(connection);
        PageManager pageManager = new PageManager(connection);
        SxeQueryService sxeQueryService = new SxeQueryService();
        SqlConnection sqlconnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CommPmApplicationConnectionString"].ToString());

        public ActionResult Index()
        {
            try
            {
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), "CreditView");

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {

                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Credit View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        public ActionResult JobSheetCredit_Read([DataSourceRequest] DataSourceRequest request, string IsActive, string approvalSort)
        {
            return Json(_projectsService.JobSheetCreditRead(IsActive,approvalSort).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult JobSheetCredit_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<JobSheetCreditViewModel> jobsheets, string approvalSort)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();                

                if (jobsheets != null)
                {
                    foreach (var jobsheet in jobsheets)
                    {
                        if(jobsheet.StepName == "Approve")
                        {
                            if(jobsheet.JobOrShipToNumber == "" || jobsheet.JobOrShipToNumber == "null" || jobsheet.JobOrShipToNumber == null)
                            {
                                ModelState.AddModelError("ShipToNumber", "Enter Ship To Number in order to approve");
                            }
                            else
                            {
                                int isShipToExists = dataContext.JobSheet.Where(p => (p.JobSheetId != jobsheet.JobSheetId) && (p.JobOrShipToNumber == jobsheet.JobOrShipToNumber) && (p.AccountNumber == jobsheet.AccountNumber) && (p.IsActive == true)).Count();
                                if(isShipToExists > 0)
                                {
                                    ModelState.AddModelError("ShipToNumber", "Ship To Number is already exists to another JobSheet.");
                                }
                                else
                                {
                                    if (sxeQueryService.ARSS(Convert.ToDecimal(jobsheet.AccountNumber), jobsheet.JobOrShipToNumber, jobsheet.Company, sqlconnection).Tables[0].Rows.Count > 0)
                                    {
                                        _projectsService.JobSheetCreditUpdate(jobsheet);
                                    }
                                    else
                                    {
                                        ModelState.AddModelError("ShipToNumber", "Ship To Number with customer not exists in SXE.");
                                    }

                                }
                                
                            }
                        }
                        else if (jobsheet.StepName == "Reject")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                        }
                        else if (jobsheet.StepName == "Pending Approval")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                        }
                        else if (jobsheet.StepName == "Incomplete Documentation")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                        }
                        else if (jobsheet.StepName == "Payment Issue")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                        }
                        else if (jobsheet.StepName == "Completed")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Credit controller JobSheetCredit_Update action");
            }
            return Json(_projectsService.JobSheetCreditRead("True",approvalSort).ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult JobSheetCreditUpdate(JobSheetCreditViewModel jobSheet)
        {
            string Message = " ";
            ViewBag.JobSheetId = jobSheet.JobSheetId;
            try
            {
                if (jobSheet != null)
                {
                    var jobSheets = new List<JobSheetCreditViewModel> { jobSheet };
                    Message = JobSheetCreditUpdate(jobSheets);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Credit controller JobSheetCreditUpdate action");
            }


            return Json(Message);
        }
        public string JobSheetCreditUpdate(IEnumerable<JobSheetCreditViewModel> jobsheets)
        {
            string Message = " ";
            try
            {
                var dataContext = new CommPmApplicationConnection();

                if (jobsheets != null)
                {
                    foreach (var jobsheet in jobsheets)
                    {
                        if (jobsheet.StepName == "Approve")
                        {
                            if (jobsheet.JobOrShipToNumber == "" || jobsheet.JobOrShipToNumber == "null" || jobsheet.JobOrShipToNumber == null)
                            {
                                Message = "Enter Ship To Number in order to approve";
                            }
                            else
                            {
                                int isShipToExists = dataContext.JobSheet.Where(p => (p.JobSheetId != jobsheet.JobSheetId) && (p.JobOrShipToNumber == jobsheet.JobOrShipToNumber) && (p.AccountNumber == jobsheet.AccountNumber) && (p.IsActive == true)).Count();
                                if (isShipToExists > 0)
                                {
                                    Message = "Ship To Number is already exists to another JobSheet.";
                                }
                                else
                                {
                                    if (sxeQueryService.ARSS(Convert.ToDecimal(jobsheet.AccountNumber), jobsheet.JobOrShipToNumber, jobsheet.Company, sqlconnection).Tables[0].Rows.Count > 0)
                                    {
                                        _projectsService.JobSheetCreditUpdate(jobsheet);
                                        Message = "Credit Approval Update Completed";
                                    }
                                    else
                                    {
                                        Message = "Ship To Number with customer not exists in SXE.";
                                    }

                                }

                            }
                        }
                        else if (jobsheet.StepName == "Reject")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                            Message = "Credit Reject Update Completed";
                        }
                        else if (jobsheet.StepName == "Pending Approval")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                            Message = "Credit Pending Approval Update Completed";
                        }
                        else if (jobsheet.StepName == "Incomplete Documentation")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                            Message = "Credit Incomplete Documentation Update Completed";
                        }
                        else if (jobsheet.StepName == "Payment Issue")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                            Message = "Credit Payment Issue Update Completed";
                        }
                        else if (jobsheet.StepName == "Completed")
                        {
                            _projectsService.JobSheetCreditUpdate(jobsheet);
                            Message = "Credit Completed Update Completed";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
                logger.Error(ex, "Error occured in Credit controller JobSheetCredit_Update action");
            }
            return Message;
        }
    }
}
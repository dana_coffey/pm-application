﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class DelegatePMController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;

        DelegatePMService delegatePMService = new DelegatePMService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                PopulateDelegates();
                PopulatePMs();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Delegate PM  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        private void PopulateDelegates()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();

                int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

                if (HttpContext.Session["userRoleId"].ToString() == "3")
                {
                    int delgateuserid = Convert.ToInt32(HttpContext.Session["userId"].ToString());
                    var delgates = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3) && (e.UserId == delgateuserid) && (e.Cono == conoid)).OrderBy(e => e.Email);

                    ViewData["delgates"] = delgates;
                    ViewData["defaultdelgateId"] = delgates.First().UserId;
                }
                else if (HttpContext.Session["userRoleId"].ToString() == "1")
                {
                    var delgates = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3)).OrderBy(e => e.Email);

                    ViewData["delgates"] = delgates;
                    ViewData["defaultdelgateId"] = delgates.First().UserId;
                }
                else
                {
                    var delgates = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3) && (e.Cono == conoid)).OrderBy(e => e.Email);

                    ViewData["delgates"] = delgates;
                    ViewData["defaultdelgateId"] = delgates.First().UserId;
                }

                    
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePM controller PopulateDelegates action");
            }
        }

        private void PopulatePMs()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();

                int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

                if (HttpContext.Session["userRoleId"].ToString() == "1")
                {

                    var pms = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3))
               .OrderBy(e => e.Email);

                    ViewData["projectms"] = pms;
                    ViewData["defaultPmId"] = pms.First().UserId;
                }
                else
                {
                    var pms = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3) && (e.Cono == conoid))
               .OrderBy(e => e.Email);

                    ViewData["projectms"] = pms;
                    ViewData["defaultPmId"] = pms.First().UserId;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePM controller PopulatePms action");
            }
        }

        public ActionResult DelegatePM_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(delegatePMService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DelegatePM_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<DelegatePMViewModel> delegatePMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<DelegatePMViewModel> checkDelegatePM = new List<DelegatePMViewModel>();

                if (delegatePMs != null)
                {
                    foreach (var delegatePM in delegatePMs)
                    {
                        if (delegatePM.DelegateUserId != delegatePM.PMId)
                        {
                            checkDelegatePM = dataContext.DelegatePM.Select(c => new DelegatePMViewModel
                            {
                                DelegatePMId = c.DelegatePMId,
                                DelegateUserId = c.DelegateUserId,
                                PMId = c.PMId

                            }).Where(e => (e.DelegateUserId == delegatePM.DelegateUserId) && (e.PMId == delegatePM.PMId)).ToList();

                            if (checkDelegatePM.Count > 0)
                            {
                                ModelState.AddModelError("Delegates", "Delagate PM " + getName(delegatePM.DelegateUserId) + " is already a delagate for PM " + getName(delegatePM.PMId) + ". Please check to see if this user is inactive or active");                                
                            }
                            else
                            {
                                delegatePMService.Create(delegatePM);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Delegates", "You cannot assign self PM");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePM controller DelegatePM_Create action");
            }

            return Json(delegatePMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DelegatePM_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<DelegatePMViewModel> delegatePMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<DelegatePMViewModel> checkDelegatePM = new List<DelegatePMViewModel>();

                if (delegatePMs != null)
                {
                    foreach (var delegatePM in delegatePMs)
                    {
                        if (delegatePM.DelegateUserId != delegatePM.PMId)
                        {
                            checkDelegatePM = dataContext.DelegatePM.Select(c => new DelegatePMViewModel
                            {
                                DelegatePMId = c.DelegatePMId,
                                DelegateUserId = c.DelegateUserId,
                                PMId = c.PMId

                            }).Where(e => (e.DelegateUserId == delegatePM.DelegateUserId) && (e.DelegatePMId != delegatePM.DelegatePMId) && (e.PMId == delegatePM.PMId)).ToList();

                            if (checkDelegatePM.Count > 0)
                            {
                                ModelState.AddModelError("Delegates", "Delagate PM " + getName(delegatePM.DelegateUserId) + " is already a delagate for PM " + getName(delegatePM.PMId) + ". Please check to see if this user is inactive or active");                                
                            }
                            else
                            {
                                delegatePMService.Update(delegatePM);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("Delegates", "You cannot assign self PM");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePM controller DelegatePM_Update action");
            }
            return Json(delegatePMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        private string getName(int id)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var dbuser = dataContext.User
                   .FirstOrDefault(p => p.UserId == id);
                return dbuser.SMSN.Name;
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Delegate class getName method");
                return "";
            }
        }

        public JsonResult GetDelegatePMS()
        {
            return Json(delegatePMService.GetDelegatePMS(), JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;

namespace PM_Website.Controllers
{
    public class EmailController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;

        EmailService emailService = new EmailService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                PopulateSubscriptionTypes();
                PopulateUsers();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), "EmailSubscription");

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Email Subscription  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        private void PopulateUsers()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var users = dataContext.User.Select(c => new UserViewModel
                {
                    UserId = c.UserId,
                    Email = c.Email,
                    IsActive = c.IsActive,
                    SMSN = new SMSNViewModel()
                    {
                        Name = c.SMSN.Name
                    }
                }).Where(e => (e.Email != "") && (e.IsActive == true))
                .OrderBy(e => e.Email);

                ViewData["users"] = users;
                ViewData["defaultUserId"] = users.First().UserId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Email controller PopulateUsers action");
            }
        }

        private void PopulateSubscriptionTypes()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var subTypes = dataContext.SubscriptionType
                            .Select(c => new SubscriptionTypeViewModel
                            {
                                SubsciptionTypeId = c.SubsciptionTypeId,
                                SubscriptionTypeName = c.SubscriptionTypeName
                            })
                            .OrderBy(e => e.SubscriptionTypeName);

                ViewData["subscriptionTypes"] = subTypes;
                ViewData["defaultSubscriptionTypeId"] = subTypes.First().SubsciptionTypeId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Email controller PopulateSubscription action");
            }
        }       

        public ActionResult EmailSubscription_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(emailService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmailSubscription_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<EmailSubscriptionViewModel> emailSubscriptions, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<EmailSubscriptionViewModel> checkEmailSubscription = new List<EmailSubscriptionViewModel>();

                if (emailSubscriptions != null)
                {
                    foreach (var emailSubscription in emailSubscriptions)
                    {
                        checkEmailSubscription = dataContext.EmailSubscription.Select(c => new EmailSubscriptionViewModel
                        {
                          UserId = c.UserId,
                          SubsciptionTypeId= c.SubsciptionTypeId

                        }).Where(e => (e.UserId == emailSubscription.UserId) && (e.SubsciptionTypeId == emailSubscription.SubsciptionTypeId)).ToList();

                        if (checkEmailSubscription.Count > 0)
                        {
                            ModelState.AddModelError("Email", "User " + getName(emailSubscription.UserId) + " is already subscribed to this type of email. Please check to see if this user is inactive or active");                            
                        }
                        else
                        {
                            emailService.Create(emailSubscription);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Email controller EmailSubscription_Create action");
            }

            return Json(emailService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EmailSubscription_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<EmailSubscriptionViewModel> emailSubscriptions, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<EmailSubscriptionViewModel> checkEmailSubscription = new List<EmailSubscriptionViewModel>();

                if (emailSubscriptions != null)
                {
                    foreach (var emailSubscription in emailSubscriptions)
                    {
                        checkEmailSubscription = dataContext.EmailSubscription.Select(c => new EmailSubscriptionViewModel
                        {
                            UserId = c.UserId,
                            SubsciptionTypeId = c.SubsciptionTypeId,
                            EmailSubscriptionId = c.EmailSubscriptionId

                        }).Where(e => (e.UserId == emailSubscription.UserId) && (e.SubsciptionTypeId == emailSubscription.SubsciptionTypeId) && (e.EmailSubscriptionId != emailSubscription.EmailSubscriptionId)).ToList();

                        if (checkEmailSubscription.Count > 0)
                        {
                            ModelState.AddModelError("Email", "User " + getName(emailSubscription.UserId) + " is already subscribed to this type of email. Please check to see if this user is inactive or active");                            
                        }
                        else
                        {
                            emailService.Update(emailSubscription);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Email controller EmailSubscription_Update action");
            }
            return Json(emailService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        private string getName(int id)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var dbuser = dataContext.User
                   .FirstOrDefault(p => p.UserId == id);
                return dbuser.SMSN.Name;

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Delegate class getName method");
                return "";
            }
        }
    }
}
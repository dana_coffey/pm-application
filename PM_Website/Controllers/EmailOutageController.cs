﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;
using System.Configuration;
using System.Net.Mail;
using System.Threading;
using System.Web.Hosting;
using System.Web.Mvc;
using Microsoft.ProjectServer.Client;
using PM_Website.Services;


namespace PM_Website.Controllers
{
    public class EmailOutageController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private CommPmApplicationConnection connection;
        // GET: EmailOutage
        public ActionResult Index()
        {
            userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  User Email Notification  View");
            return View();
        }

        public string SendEmailNotification(EmailOutageModel emailOutage)
        {
            try
            {
                var message = HttpUtility.HtmlDecode(emailOutage.Message);
               
                var emailList = emailOutage.ToEmail.Split(',').ToList();

                foreach (var address in emailList)
                {
                    if (address.Trim() == "All")
                    {
                        emailOutage.ToEmail = string.Join(",", GetUserEmail());
                    }
                }

                var toAddresses = emailOutage.ToEmail.Split(',').ToList();

                HostingEnvironment.QueueBackgroundWorkItem(x => SendEmail(message, toAddresses, emailOutage));

                return "The email notification has been sent successfully";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in EmailOutage Controller, SendEmailNotification Object");
                return "Error occurs when you sending email";
            }
        }

        public void SendEmail(string message, List<string> toAddresses, EmailOutageModel emailOutage)
        {
            if (emailOutage.uploadedFiles != null && emailOutage.uploadedFiles.ContentLength > 0)
            {
                var attachments = new List<Attachment>
                {
                    new Attachment(emailOutage.uploadedFiles.InputStream, emailOutage.uploadedFiles.FileName)
                };

                foreach (var address in toAddresses)
                {
                    PM_Emailer.Email.SendMail(message, address, emailOutage.Subject, emailOutage.CCEmail, attachments, null);
                }
            }
            else
            {
                foreach (var address in toAddresses)
                {
                    PM_Emailer.Email.SendMail(message, address, emailOutage.Subject, emailOutage.CCEmail, null, null);
                }
            }
        }

        public JsonResult GetUser()
        {
            connection = new CommPmApplicationConnection();

            var result = connection.User.Select(user => new UserViewModel
            {
                UserId = user.UserId,
                RoleId = user.RoleId,
                IsActive = user.IsActive,
                Email = user.Email,
                SlsRep = user.SlsRep,
                Name = user.SMSN.Name,
                UserRole = new UsersUserRoleViewModel()
                {
                    RoleId = user.UserRoles.RoleId,
                    RoleName = user.UserRoles.RoleName
                },
                SMSN = new SMSNViewModel()
                {
                    SlsRep = user.SMSN.SlsRep,
                    Name = user.SMSN.Name,
                    Email = user.SMSN.Email
                }
            });
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        public List<string> GetUserEmail()
        {
            var dataContext = new CommPmApplicationConnection();
            return (from p in dataContext.User
                    where p.IsActive == true
                    select p.Email).ToList();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NLog;
using PM_Website.Models;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class EmailTemplateController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        EmailTemplateService emailTemplateService = new EmailTemplateService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Email Templete  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        [HttpPost]
        public ActionResult GetAllTemplates([DataSourceRequest] DataSourceRequest request)
        {
            return Json(emailTemplateService.GetAllTemplates().ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult GetEmailTemplate(FormCollection body)
        {
            return Json(emailTemplateService.GetEmailTemplate(int.Parse(body["templateId"])));
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult SaveEmailTemplate(FormCollection body)
        {
            return Json(emailTemplateService.SaveEmailTemplate(body));
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult CreateEmailTemplate(FormCollection body)
        {
            return Json(emailTemplateService.CreateEmailTemplate(body));
        }

        [HttpPost]
        public JsonResult DeleteEmailTemplate(FormCollection body)
        {
            return Json(emailTemplateService.DeleteEmailTemplate(body));
        }

        [HttpGet]
        public JsonResult GetSubscriptionTypes(int? currentId)
        {
            JsonResult result;
            try
            {
                result = Json(emailTemplateService.GetSubscriptionTypes(currentId), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateController class GetSubscriptionTypes method");
                result = Json("false", JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public JsonResult GetSubscriptionPropertyNames(string subscriptionName)
        {
            return Json(emailTemplateService.GetSubscriptionPropertyNames(subscriptionName),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TestEmailTemplate(FormCollection body)
        {
            return Json(emailTemplateService.TestEmailTemplate(body));
        }
    }
}
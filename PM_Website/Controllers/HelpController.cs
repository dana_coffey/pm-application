﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;
using PM_Website.Models;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class HelpController : Controller
    {
        private static CommPmApplicationConnection connection;
        HelpService helpService = new HelpService(connection);


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetHelp()
        {
            var result = Json(helpService.GetHelp(), JsonRequestBehavior.AllowGet);

            return result;
        }
    }
}
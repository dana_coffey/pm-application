﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;
using PM_Website.Services;

namespace PM_Website.Controllers
{

    public class HomeController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        ProjectsService _projectsService = new ProjectsService(connection);
        UserService userService = new UserService(connection);
        HelpService helpService = new HelpService(connection);
        PageManager pageManager = new PageManager(connection);
        EmailService emailService = new EmailService(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Dashboard  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        public ActionResult GetJobSheetStatus([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_projectsService.GetStatus().ToDataSourceResult(request));
        }

        public ActionResult GetChartValues([DataSourceRequest] DataSourceRequest request, string grid)
        {
            return Json(_projectsService.GetChartValues(grid).ToDataSourceResult(request));
        }

        public ActionResult GetVoltage([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_projectsService.GetVoltageGrid().ToDataSourceResult(request));
        }

        public ActionResult ErrorMessage()
        {
            try
            {
                var loggedUser = System.Security.Principal.WindowsIdentity.GetCurrent();


                ViewBag.Message = "";
                //ViewBag.Message = loggedUser.Name + " " + System.Web.HttpContext.Current.User.Identity.Name;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Home Controller ErrorMessage action");
            }
            return View();
        }

        public ActionResult AccessError()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Home Controller AccessError action");
            }
            return View();
        }
       
        [HttpGet]
        public JsonResult GetHelpDescription(string PageName)
        {
            return Json(helpService.GetHelpDescription(PageName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProjectOrderLine_Read([DataSourceRequest] DataSourceRequest request, string ShipTo, int CustNo, int OrderNo, int OrderSurf)
        {
            return Json(_projectsService.OrderLineRead(ShipTo,CustNo, OrderNo, OrderSurf).ToDataSourceResult(request));
        }

        public ActionResult JobOrderLine_Read([DataSourceRequest] DataSourceRequest request, string ShipTo, int? CustNo, int? OrderNo, int? OrderSurf)
        {
            using (var connection = new CommPmApplicationConnection())
            {
                IQueryable<ProjectOrderLine> projectOrderLines = connection.ProjectOrderLine;

                if (ShipTo != null && CustNo != null && OrderNo != null && OrderSurf != null)
                {
                    projectOrderLines = projectOrderLines.Where(p => (p.ShipTo == ShipTo) && (p.CustNo == CustNo) && (p.OrderNo == OrderNo) && (p.OrderSurf == OrderSurf));
                }

                return Json(projectOrderLines.ToDataSourceResult(request, projectorderline => new ProjectOrderLineViewModel
                {
                    ProjectOrderLineId = projectorderline.ProjectOrderLineId,
                    OrderNo = projectorderline.OrderNo,
                    OrderSurf = projectorderline.OrderSurf,
                    LineNumber = projectorderline.LineNumber,
                    ModelPart = projectorderline.ModelPart,
                    POAmount = projectorderline.POAmount,
                    QuantityOrdered = projectorderline.QuantityOrdered,
                    QuantityShipped = projectorderline.QuantityShipped,
                    CustomerRequestedDeliveryDate = projectorderline.CustomerRequestedDeliveryDate,
                    VendorName = projectorderline.VendorName,
                    VerifiedVoltage = projectorderline.VerifiedVoltage,
                    CustNo = projectorderline.CustNo,
                    ShipTo = projectorderline.ShipTo,
                    VendorNumber = projectorderline.VendorNumber,
                    NetAmount = projectorderline.NetAmount,
                    OrderAltNo = projectorderline.OrderAltNo,
                   
                })
                );
            }
        }

        [HttpGet]
        public JsonResult UpdateStatusById(int JobSheetId, string Status, bool Checked)
        {
            return Json(_projectsService.UpdateStatusById(JobSheetId, Status, Checked), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult VerifiedVoltage_Update(VerifyVoltageModel vvModel)
        {
            try
            {
                if (vvModel != null)
                {
                    _projectsService.VerifiedVoltageUpdateAjax(vvModel);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Home controller VerifiedVoltage_Update action");
            }

            return Json("SUCCESS");
        }

        [HttpPost]
        public JsonResult SetHomepage(string theURL)
        {
            var test = Request.Form;
            try
            {
                userService.SetHomepage(test[0], Int32.Parse(HttpContext.Session["userId"].ToString()));
                return Json("SUCCESS");
            }
            catch
            {
                return Json("FAILED");
            }
        }

        [HttpPost]
        public JsonResult RemoveHomepage(string theURL)
        {
            var test = Request.Form;
            try
            {
                userService.RemoveHomepage(test[0], Int32.Parse(HttpContext.Session["userId"].ToString()));
                return Json("SUCCESS");
            }
            catch
            {
                return Json("FAILED");
            }
        }

        [HttpPost]
        public JsonResult SendFeedback(FormCollection form)
        {
            HttpPostedFileBase file = null;
                if(Request.Files.Count > 0)
                {
                    file = Request.Files[0];
                }
            return Json(emailService.FeedbackEmail(form, file));
        }
        public ActionResult SetVariable(string Value)
        {
            System.Web.HttpContext.Current.Session["userCono"] = Value;

            return this.Json(new { success = true });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;

namespace PM_Website.Controllers
{
    public class JobStepController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        JobStepService jobStepService = new JobStepService(connection);
        ChangeLogJobStepService changeLogJobStepService = new ChangeLogJobStepService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Job Step  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        public ActionResult JobStep_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(jobStepService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult JobStep_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<JobStepViewModel> jobSteps, string IsActive)
        {
            try
            {
               

                if (jobSteps != null)
                {
                    foreach (var jobStep in jobSteps)
                    {
                        jobStepService.Create(jobStep);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }

            return Json(jobStepService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult JobStep_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<JobStepViewModel> jobSteps, string IsActive)
        {
            try
            {
                if (jobSteps != null)
                {
                    foreach (var jobStep in jobSteps)
                    {
                        jobStepService.Update(jobStep);                        
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Update action");
            }
            return Json(jobStepService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        public ActionResult ChangeLogJobStep_Read([DataSourceRequest] DataSourceRequest request, int JobSheetId)
        {
            return Json(changeLogJobStepService.ChangeLogJobStep_Read(JobSheetId).ToDataSourceResult(request));
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class ManagerPMController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;

        ManagerPMService managerPMService = new ManagerPMService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                PopulateManagers();
                PopulatePMs();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Manger PM View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        private void PopulateManagers()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();

                int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

                if (HttpContext.Session["userRoleId"].ToString() == "2")
                {
                    int manageruserid = Convert.ToInt32(HttpContext.Session["userId"].ToString());
                    var managers = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 2) && (e.UserId==manageruserid) && (e.Cono ==conoid)).OrderBy(e => e.Email);

                    ViewData["managers"] = managers;
                    ViewData["defaultManagerId"] = managers.First().UserId;
                }
                else if (HttpContext.Session["userRoleId"].ToString() == "1")
                {
                    var managers = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 2)).OrderBy(e => e.Email);

                    ViewData["managers"] = managers;
                    ViewData["defaultManagerId"] = managers.First().UserId;
                }   
                else
                {
                    var managers = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 2) && (e.Cono == conoid)).OrderBy(e => e.Email);

                    ViewData["managers"] = managers;
                    ViewData["defaultManagerId"] = managers.First().UserId;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPM controller PopulateManagers action");
            }
        }

        private void PopulatePMs()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();

                int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

                if (HttpContext.Session["userRoleId"].ToString() == "1")
                {
                    var pms = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3))
               .OrderBy(e => e.Email);

                    ViewData["projectms"] = pms;
                    ViewData["defaultPmId"] = pms.First().UserId;
                }
                else
                {
                    var pms = dataContext.User.Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        Email = c.Email,
                        IsActive = c.IsActive,
                        Cono = c.Cono,
                        SMSN = new SMSNViewModel()
                        {
                            Name = c.SMSN.Name
                        },
                        RoleId = c.RoleId,
                    }).Where(e => (e.Email != "") && (e.RoleId == 3) && (e.Cono == conoid))
               .OrderBy(e => e.Email);

                    ViewData["projectms"] = pms;
                    ViewData["defaultPmId"] = pms.First().UserId;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPM controller PopulatePms action");
            }
        }

        public ActionResult ManagerPM_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(managerPMService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagerPM_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ManagerPMViewModel> managerPMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<ManagerPMViewModel> checkManagerPM = new List<ManagerPMViewModel>();

                if (managerPMs != null)
                {
                    foreach (var managerPM in managerPMs)
                    {
                        checkManagerPM = dataContext.ManagerPM.Select(c => new ManagerPMViewModel
                        {
                            ManagerPMId = c.ManagerPMId,
                            ManagerId = c.ManagerId,
                            PMId = c.PMId

                        }).Where(e => (e.ManagerId == managerPM.ManagerId) && (e.PMId == managerPM.PMId)).ToList();

                        if (checkManagerPM.Count > 0)
                        {
                            ModelState.AddModelError("Manager", "PM " + getName(managerPM.PMId) + " is already assigned to Manager " + getName(managerPM.ManagerId) + ". Please check to see if this user is inactive or active");                            
                        }
                        else
                        {
                            managerPMService.Create(managerPM);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPM controller ManagerPM_Create action");
            }

            return Json(managerPMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManagerPM_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ManagerPMViewModel> managerPMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<ManagerPMViewModel> checkManagerPM = new List<ManagerPMViewModel>();

                if (managerPMs != null)
                {
                    foreach (var managerPM in managerPMs)
                    {
                        checkManagerPM = dataContext.ManagerPM.Select(c => new ManagerPMViewModel
                        {
                            ManagerPMId = c.ManagerPMId,
                            ManagerId = c.ManagerId,
                            PMId = c.PMId

                        }).Where(e => (e.ManagerId == managerPM.ManagerId) && (e.ManagerPMId != managerPM.ManagerPMId) && (e.PMId == managerPM.PMId)).ToList();

                        if (checkManagerPM.Count > 0)
                        {
                            ModelState.AddModelError("Manager", "PM " + getName(managerPM.PMId) + " is already assigned to Manager " + getName(managerPM.ManagerId) + ". Please check to see if this user is inactive or active");                            
                        }
                        else
                        {
                            managerPMService.Update(managerPM);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPM controller ManagerPM_Update action");
            }
            return Json(managerPMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        private string getName(int id)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var dbuser = dataContext.User
                   .FirstOrDefault(p => p.UserId == id);
                return dbuser.SMSN.Name;

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Delegate class getName method");
                return "";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class OutSideSalesRepPMController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;

        OutSideSalesRepPMService outSideSalesRepPMService = new OutSideSalesRepPMService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                PopulateOutSideSalesReps();
                PopulatePMs();
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Outside Sales Rep View ");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        private void PopulateOutSideSalesReps()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var ossreps = dataContext.User.Select(c => new UserViewModel
                {
                    UserId = c.UserId,
                    Email = c.Email,
                    IsActive = c.IsActive,
                    SMSN = new SMSNViewModel()
                    {
                        Name = c.SMSN.Name
                    },
                    RoleId = c.RoleId,
                }).Where(e => (e.Email != "") && (e.IsActive == true) && (e.RoleId == 8))
                .OrderBy(e => e.Email);

                ViewData["ossreps"] = ossreps;
                ViewData["defaultossrepId"] = ossreps.First().UserId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPM controller PopulateOutSideSalesReps action");
            }
        }

        private void PopulatePMs()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var pms = dataContext.User.Select(c => new UserViewModel
                {
                    UserId = c.UserId,
                    Email = c.Email,
                    IsActive = c.IsActive,
                    SMSN = new SMSNViewModel()
                    {
                        Name = c.SMSN.Name
                    },
                    RoleId = c.RoleId,
                }).Where(e => (e.Email != "") && (e.RoleId == 3))
               .OrderBy(e => e.Email);

                ViewData["projectms"] = pms;
                ViewData["defaultPmId"] = pms.First().UserId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPM controller PopulatePms action");
            }
        }

        public ActionResult OutSideSalesRepPM_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(outSideSalesRepPMService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutSideSalesRepPM_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<OutSideSalesRepPMViewModel> ossrepPMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<OutSideSalesRepPMViewModel> checkOssRepPM = new List<OutSideSalesRepPMViewModel>();

                if (ossrepPMs != null)
                {
                    foreach (var ossrepPM in ossrepPMs)
                    {
                        checkOssRepPM = dataContext.OutSideSalesRepPM.Select(c => new OutSideSalesRepPMViewModel
                        {
                            OutSideSalesRepPMId = c.OutSideSalesRepPMId,
                            OutSideSalesRepId = c.OutSideSalesRepId,
                            PMId = c.PMId

                        }).Where(e => (e.OutSideSalesRepId == ossrepPM.OutSideSalesRepId) && (e.PMId == ossrepPM.PMId)).ToList();

                        if (checkOssRepPM.Count > 0)
                        {
                            ModelState.AddModelError("OutSideSales", "PM " + getName(ossrepPM.PMId) + " is already assigned to OutSideSalesRep " + getName(ossrepPM.OutSideSalesRepId) + ". Please check to see if this user is inactive or active");                           
                        }
                        else
                        {
                            outSideSalesRepPMService.Create(ossrepPM);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPM controller OutSideSalesRepPM_Create action");
            }

            return Json(outSideSalesRepPMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OutSideSalesRepPM_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<OutSideSalesRepPMViewModel> ossrepPMs, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<OutSideSalesRepPMViewModel> checkOssRepPM = new List<OutSideSalesRepPMViewModel>();

                if (ossrepPMs != null)
                {
                    foreach (var ossrepPM in ossrepPMs)
                    {
                        checkOssRepPM = dataContext.OutSideSalesRepPM.Select(c => new OutSideSalesRepPMViewModel
                        {
                            OutSideSalesRepPMId = c.OutSideSalesRepPMId,
                            OutSideSalesRepId = c.OutSideSalesRepId,
                            PMId = c.PMId

                        }).Where(e => (e.OutSideSalesRepId == ossrepPM.OutSideSalesRepId) && (e.OutSideSalesRepPMId != ossrepPM.OutSideSalesRepPMId) && (e.PMId == ossrepPM.PMId)).ToList();

                        if (checkOssRepPM.Count > 0)
                        {
                            ModelState.AddModelError("OutSideSales", "PM " + getName(ossrepPM.PMId) + " is already assigned to OutSideSalesRep " + getName(ossrepPM.OutSideSalesRepId) + ". Please check to see if this user is inactive or active");                            
                        }
                        else
                        {
                            outSideSalesRepPMService.Update(ossrepPM);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPM controller OutSideSalesRepPM_Update action");
            }
            return Json(outSideSalesRepPMService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        private string getName(int id)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var dbuser = dataContext.User
                   .FirstOrDefault(p => p.UserId == id);
                return dbuser.SMSN.Name;

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Delegate class getName method");
                return "";
            }
        }
    }
}
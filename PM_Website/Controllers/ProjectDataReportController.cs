﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.Ajax.Utilities;
using PM_Website.Services;
using Newtonsoft.Json;
using NLog;

namespace PM_Website.Controllers
{
    public class ProjectDataReportController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        private static CommPmApplicationConnection connection;

        ProjectDataReportService projectDataReportService = new ProjectDataReportService(connection);
        PageManager pageManager = new PageManager(connection);  

        public ActionResult ProjectDataReport_ReadProjects([DataSourceRequest] DataSourceRequest request)
        {
            return Json(projectDataReportService.ReadProjects().ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult ProjectDataReport_Create(FormCollection data)
        {
            try
            {
                if (data != null)
                {
                    var report = new ProjectDataReportViewModel();
                    var textInfo = new CultureInfo("en-US", false).TextInfo;

                    if (data["date-from"] != "" && data["date-to"] != "")
                    {
                        report.StartDate = DateTime.Parse(data["date-from"]);
                        report.EndDate = DateTime.Parse(data["date-to"]);
                    }

                    report.Generation = data["report-frequency"];

                    report.JobSheetIds =
                        new JavaScriptSerializer().Deserialize<List<string>>(data["schedule-report-ids"]);

                    report.ReportType = textInfo.ToTitleCase(data["schedule-report-type"].ToLower());
                    report.ReportName = data["report-name"];

                    projectDataReportService.Create(report);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectDataReport controller ProjectDataReport_Create action");
                return Json("False");
            }
            return Json("True");
        }

        [HttpGet]
        public JsonResult DeleteProjectDateReportById(string Id)
        {
            try
            {
                if (Id == "" || Id == "0")
                {
                    return Json("FAILED");
                }
                else
                {
                    projectDataReportService.DeleteProjectDateReportById(Id);
                    return Json("SUCCESS");
                }

            }
            catch
            {
                return Json("FAILED");
            }
        }
    }
}
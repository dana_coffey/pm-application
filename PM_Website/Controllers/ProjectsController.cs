﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NLog;
using PM_Website.Models;
using PM_Website.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PM_Website.Controllers
{
    public class ProjectsController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static readonly CommPmApplicationConnection connection;

        ProjectsService _projectsService = new ProjectsService(connection);
        PageManager pageManager = new PageManager(connection);
        ChangeLogJobSheetService changeLogJobSheetService = new ChangeLogJobSheetService(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Project Add/Edit  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        [HttpGet]
        public ActionResult Index(string jobSheetId, string createJob)
        {
            try
            {
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), "JobSheet");

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                        if (string.IsNullOrEmpty(jobSheetId) || jobSheetId == "null")
                        {
                            ViewBag.JobSheetId = null;
                            ViewBag.viewaccount = "False";
                            ViewBag.viewvendor = "False";
                            ViewBag.CreateNewJob = null;
                        }
                        else if (jobSheetId == "account")
                        {
                            ViewBag.JobSheetId = null;
                            ViewBag.viewaccount = "True";
                            ViewBag.viewvendor = "False";
                            ViewBag.CreateNewJob = null;
                        }
                        else if (jobSheetId == "vendor")
                        {
                            ViewBag.JobSheetId = null;
                            ViewBag.viewaccount = "False";
                            ViewBag.viewvendor = "True";
                            ViewBag.CreateNewJob = null;
                        }                        
                        else
                        {
                            var jsId = int.Parse(jobSheetId);
                            if(jsId == 0)
                            {
                                ViewBag.JobSheetId = null;
                                ViewBag.viewaccount = "False";
                                ViewBag.viewvendor = "False";
                                ViewBag.CreateNewJob = "Yes";
                            }
                            else
                            {
                                ViewBag.JobSheetId = int.Parse(jobSheetId);
                                ViewBag.viewaccount = "False";
                                ViewBag.viewvendor = "False";
                                ViewBag.CreateNewJob = "Yes";
                            }                            
                        }

                        if (!string.IsNullOrEmpty(createJob))
                        {
                            ViewBag.CreateJob = bool.TryParse(createJob, out var create) && create;
                        }

                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Project Add/Edit  View");


                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Jobsheet controller Index action");
            }
            return View();
        }

        public ActionResult JobSheet_Read([DataSourceRequest] DataSourceRequest request, string IsActive, string MyProjects, int Delegate)
        {
            return Json(_projectsService.Read(IsActive, MyProjects, Delegate).ToDataSourceResult(request));
        }

        public JsonResult GetUserRoles(string text)
        {
            var dataContext = new CommPmApplicationConnection();
            var user = (from users in dataContext.User
                        join SMSN in dataContext.SMSN on users.SlsRep equals SMSN.SlsRep
                        where users.IsActive == true
                        select new
                        {
                            UserId = users.UserId,
                            Email = users.Email,
                            IsActive = users.IsActive,
                            SlsRep = SMSN.SlsRep,
                            Name = SMSN.Name

                        });
            if (!string.IsNullOrEmpty(text))
            {
                user = user.Where(p => p.Name.Contains(text));
            }

            return Json(user, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetUserRolesForOutside(string text)
        //{
        //    var dataContext = new CommPmApplicationConnection();
        //    var user = (from users in dataContext.User
        //                join SMSN in dataContext.SMSN on users.SlsRep equals SMSN.SlsRep
        //                where (users.IsActive) == true && (users.RoleId == 8)
        //                select new
        //                {
        //                    UserId = users.UserId,
        //                    Email = users.Email,
        //                    IsActive = users.IsActive,
        //                    SlsRep = SMSN.SlsRep,
        //                    Name = SMSN.Name

        //                });
        //    if (!string.IsNullOrEmpty(text))
        //    {
        //        user = user.Where(p => p.Name.Contains(text));
        //    }

        //    return Json(user, JsonRequestBehavior.AllowGet);
        //}
        // Change - added the pm role users
        public JsonResult GetUserRolesForOutside(string text)
        {
            List<int> roleid = new List<int>();
            roleid.Add(3);
            roleid.Add(8);

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var dataContext = new CommPmApplicationConnection();
                var user = (from users in dataContext.User
                            join SMSN in dataContext.SMSN on users.SlsRep equals SMSN.SlsRep
                            where (users.IsActive) == true && (roleid.Contains(users.RoleId))
                            select new
                            {
                                UserId = users.UserId,
                                Email = users.Email,
                                IsActive = users.IsActive,
                                SlsRep = SMSN.SlsRep.ToUpper(),
                                Name = SMSN.Name,
                                Cono = users.Cono,
                                UserIdName = SMSN.SlsRep.ToUpper() + " : " + SMSN.Name

                            });
                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.Name.Contains(text) || p.SlsRep.ToString().Contains(text));
                }

                return Json(user, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var dataContext = new CommPmApplicationConnection();
                var user = (from users in dataContext.User
                            join SMSN in dataContext.SMSN on users.SlsRep equals SMSN.SlsRep
                            where (users.IsActive) == true && (roleid.Contains(users.RoleId)) && (users.Cono == conoid)
                            select new
                            {
                                UserId = users.UserId,
                                Email = users.Email,
                                IsActive = users.IsActive,
                                SlsRep = SMSN.SlsRep,
                                Name = SMSN.Name,
                                Cono = users.Cono,
                                UserIdName = SMSN.SlsRep.ToUpper() + " : " + SMSN.Name

                            });
                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.Name.Contains(text) || p.UserId.ToString().Contains(text));
                }

                return Json(user, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult GetCompanies(string text)
        {

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var dataContext = new CommPmApplicationConnection();
                var conos = (from companies in dataContext.Company
                             where companies.IsActive == true
                             select new
                             {
                                 Cono = companies.Cono,
                                 CompanyName = companies.CompanyName,
                                 IsActive = companies.IsActive,

                             });
                if (!string.IsNullOrEmpty(text))
                {
                    conos = conos.Where(p => p.CompanyName.Contains(text));
                }

                return Json(conos, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var dataContext = new CommPmApplicationConnection();
                var conos = (from companies in dataContext.Company
                             where (companies.IsActive == true) && (companies.Cono == conoid)
                             select new
                             {
                                 Cono = companies.Cono,
                                 CompanyName = companies.CompanyName,
                                 IsActive = companies.IsActive,

                             });
                if (!string.IsNullOrEmpty(text))
                {
                    conos = conos.Where(p => p.CompanyName.Contains(text));
                }

                return Json(conos, JsonRequestBehavior.AllowGet);
            }


        }

        public JsonResult GetJobSheetStatus()
        {
            var dataContext = new CommPmApplicationConnection();
            var status = (from Status in dataContext.JobSteps
                          where Status.IsActive == true
                          select new
                          {
                              StatusId = Status.JobStepId,
                              StatusName = Status.StepName,
                              IsActive = Status.IsActive,

                          });

            return Json(status, JsonRequestBehavior.AllowGet);
        }


        public List<CustomerViewModel> GetCustomer()
        {

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

            var dataContext = new CommPmApplicationConnection();

            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                return (from p in dataContext.Customer
                        where p.IsActive == true
                        select new CustomerViewModel { CustNo = p.CustNo, CustomerName = p.CustNo + " : " + p.CustomerName, IsActive = p.IsActive, Cono = p.Cono }).OrderByDescending(x => x.CustNo).ToList();
            }
            else
            {
                return (from p in dataContext.Customer
                        where p.IsActive == true
                        select new CustomerViewModel { CustNo = p.CustNo, CustomerName = p.CustNo + " : " + p.CustomerName, IsActive = p.IsActive, Cono = p.Cono }).Where(u => u.Cono == conoid).OrderByDescending(x => x.CustNo).ToList();
            }


        }

        public ActionResult Customer_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetCustomer().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Customer_ValueMapper(int[] values)
        {
            var indices = new List<int>();

            if (values != null && values.Any())
            {
                var index = 0;

                foreach (var Customer in GetCustomer())
                {
                    int custn = Convert.ToInt32(Customer.CustNo);
                    if (values.Contains(custn))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void JobSheet_Create(JobSheetViewModel jobSheet)
        {
            try
            {
                if (jobSheet != null)
                {
                    _projectsService.Create(jobSheet);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }
        }

        [HttpGet]
        public JsonResult GetJobAlert()
        {
            return Json(_projectsService.GetJobAlert(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetJobSheetById(int JobId)
        {
            return Json(_projectsService.Read(JobId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChangeLogJobSheet_Read([DataSourceRequest] DataSourceRequest request, int JobSheetId)
        {
            return Json(changeLogJobSheetService.ChangeLogJobSheet_Read(JobSheetId).ToDataSourceResult(request));
        }

        public ActionResult JobOrders_Read([DataSourceRequest] DataSourceRequest request, string ShipTo, int CustNo)
        {
            return Json(_projectsService.OrdersRead(ShipTo, CustNo).ToDataSourceResult(request));
        }

        [HttpPost]
        public void VerifiedVoltage_Update(JobSheetViewModel jobSheet)
        {

            try
            {
                if (jobSheet != null)
                {
                    _projectsService.VerifiedVoltageUpdate(jobSheet);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }
        }

        public ActionResult GetJobSheetStatusById([DataSourceRequest] DataSourceRequest request, int JobSheetId)
        {
            return Json(_projectsService.GetJobSheetStatusById(JobSheetId).ToDataSourceResult(request));
        }

        public ActionResult JobSheettop10_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_projectsService.Readtop10Job().ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult ExcelExport(string jobSheetIds)
        {
            //var jobSheetId = JsonConvert.DeserializeObject<List<int>>(jobSheetIds);
            string Message = string.Empty;
            try
            {
                if (jobSheetIds != null)
                {
                    _projectsService.ExcelExport(jobSheetIds, "excel", HttpContext.Session["userName"].ToString(), HttpContext.Session["firstNameLastName"].ToString());
                    Message = "Export Completed";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }


            return Json(Message);
        }

        [HttpGet]
        public JsonResult InActiveJobSheetById(string JobSheetId)
        {
            try
            {
                if (JobSheetId == "" || JobSheetId == "0")
                {
                    return Json("FAILED");
                }
                else
                {
                    _projectsService.InActiveJobSheetById(JobSheetId);
                    return Json("SUCCESS");
                }

            }
            catch
            {
                return Json("FAILED");
            }
        }

        [HttpPost]
        public JsonResult ExcelExportCount(List<int> jobSheetId)
        {
            int count = 0;
            try
            {
                if (jobSheetId != null)
                {
                    count = _projectsService.ExcelExportCount(jobSheetId);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }


            return Json(count);
        }

        [HttpPost]
        public JsonResult EmailExcelExport(List<int> jobSheetId, string emailAddress, string username)
        {
            //var jobSheetId = JsonConvert.DeserializeObject<List<int>>(jobSheetIds);
            string Message = string.Empty;
            try
            {
                if (jobSheetId != null)
                {
                    HostingEnvironment.QueueBackgroundWorkItem(ctx => ExcelExportEmail(jobSheetId, emailAddress, username));
                    Message = "Export Completed";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobStep_Create action");
            }
            return Json(Message);
        }

        private void ExcelExportEmail(List<int> jobSheetIds, string emailAddress, string username)
        {
            Thread.Sleep(5000);
            string jobSheetId = string.Join(",", jobSheetIds.ToArray());
            _projectsService.ExcelExport(jobSheetId, "email", emailAddress, username);
        }

        [HttpGet]
        public JsonResult DeleteJobSheetById(string JobSheetId)
        {
            try
            {
                if (JobSheetId == "" || JobSheetId == "0")
                {
                    return Json("FAILED");
                }
                else
                {
                    _projectsService.DeleteJobSheetById(JobSheetId);
                    return Json("SUCCESS");
                }

            }
            catch
            {
                return Json("FAILED");
            }
        }

        public ActionResult JobSheet_ReadCCGFiles([DataSourceRequest] DataSourceRequest request, string JobSheetId)
        {
            return Json(_projectsService.ReadCCGFiles(JobSheetId).ToDataSourceResult(request));
        }

        [HttpGet]
        public JsonResult InActiveJobSheetCCGFileById(string CCGFileId)
        {
            try
            {
                if (CCGFileId == "" || CCGFileId == "0")
                {
                    return Json("FAILED");
                }
                else
                {
                    _projectsService.InActiveJobSheetCCGFileById(CCGFileId);
                    return Json("SUCCESS");
                }

            }
            catch
            {
                return Json("FAILED");
            }
        }

        [HttpPost]
        public ActionResult JobSheetAlertUpdate(JobSheetAlertViewModel jobSheetAlert)
        {

            try
            {
                if (jobSheetAlert != null)
                {
                    _projectsService.JobSheetAlertUpdate(jobSheetAlert);
                    return Json("SUCCESS");
                }
                else
                {
                    return Json("FAILED");
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Jobsheet controller JobSheetAlertUpdate action");
                return Json("FAILED");
            }


        }

        [HttpPost]
        public void JobSheetAlertDelete()
        {

            try
            {
                _projectsService.JobSheetAlertDelete();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep controller JobSheetAlertUpdate action");
            }
        }

        public ActionResult JobSheet_ProjectReportsRead([DataSourceRequest] DataSourceRequest request, string IsActive, string Id, string ReportType)
        {
            return Json(_projectsService.ProjectReportsRead(IsActive, Id, ReportType).ToDataSourceResult(request));
        }

        [HttpPost]
        public JsonResult GetVendorData(DataSourceRequest request, string PurchaseOrder, string Material, string qty)
        {
            return Json(_projectsService.GetVendorData(PurchaseOrder, Material, qty).ToDataSourceResult(request));
        }
        public JsonResult GetJobsheetName(string text)
        {
            var dataContext = new CommPmApplicationConnection();
            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            var jobsheet = (from c in dataContext.JobSheet
                            where (c.IsActive == true) && (c.Company == conoid)
                            select new { c.JobSheetName, c.JobSheetId });

            if (!string.IsNullOrEmpty(text))
            {
                jobsheet = jobsheet.Where(p => p.JobSheetName.Contains(text));
            }

            return Json(jobsheet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJobName(string text)
        {
            var dataContext = new CommPmApplicationConnection();
            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            var jobsheet = (from c in dataContext.JobSheet
                            where (c.IsActive == true) && (c.Company == conoid)
                            select new { c.JobName, c.JobSheetId });

            if (!string.IsNullOrEmpty(text))
            {
                jobsheet = jobsheet.Where(p => p.JobName.Contains(text));
            }

            return Json(jobsheet, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult JobSheetDetails(string jobSheetId, string previousPage)
        {
            JobSheet jobSheet = new JobSheet();
            try
            {
                var dataContext = new CommPmApplicationConnection();
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();

                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), "JobSheet");

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                        if (previousPage == "" || previousPage == null || previousPage == "null")
                        {
                            ViewBag.PreviousPage = "JobSheet";
                        }
                        else
                        {
                            ViewBag.PreviousPage = previousPage;
                        }
                        if (jobSheetId == "" || jobSheetId == null || jobSheetId == "null")
                        {
                            ViewBag.JobSheetId = "0";
                            ViewBag.ProjectName = "";
                            ViewBag.AccountNumber = "";
                            ViewBag.JobOrShipToNumber = "";
                        }
                        else
                        {
                            ViewBag.JobSheetId = int.Parse(jobSheetId);

                            int jobsid = int.Parse(jobSheetId);
                            string projectName = dataContext.JobSheet.Where(j => (j.IsActive == true) && (j.JobSheetId == jobsid)).Select(j => j.JobName).Single();
                            string accountNumber = dataContext.JobSheet.Where(j => (j.IsActive == true) && (j.JobSheetId == jobsid)).Select(j => j.AccountNumber).Single();
                            string jobshiptonumber = dataContext.JobSheet.Where(j => (j.IsActive == true) && (j.JobSheetId == jobsid)).Select(j => j.JobOrShipToNumber).Single();
                            ViewBag.ProjectName = projectName.Trim();
                            if (accountNumber == "" || accountNumber == null || accountNumber == "null")
                            {
                                ViewBag.AccountNumber = "0";
                            }
                            else
                            {
                                ViewBag.AccountNumber = accountNumber.Trim();
                            }
                            if (jobshiptonumber == "" || jobshiptonumber == null || jobshiptonumber == "null")
                            {
                                ViewBag.JobOrShipToNumber = "0";
                            }
                            else
                            {
                                ViewBag.JobOrShipToNumber = jobshiptonumber.Trim();
                            }
                            //ViewBag.AccountNumber = accountNumber.Trim();

                    
                            jobSheet = dataContext.JobSheet.Where(p => p.JobSheetId == jobsid).FirstOrDefault();

                            if(jobSheet.TMCommissionPosition1Id != 0)
                            {
                                string salesrepIn = dataContext.User.Where(j => (j.IsActive == true) && (j.UserId == jobSheet.TMCommissionPosition1Id)).Select(j => j.SlsRep).Single();
                                if(salesrepIn != string.Empty)
                                {
                                    ViewBag.salesrepIn = salesrepIn.ToUpper();
                                }
                              
                            }
                            if (jobSheet.TMCommissionPosition2Id != null)
                            {
                                string salesrepOut = dataContext.User.Where(j => (j.IsActive == true) && (j.UserId == jobSheet.TMCommissionPosition2Id)).Select(j => j.SlsRep).Single();
                                if (salesrepOut != string.Empty)
                                {
                                    ViewBag.salesrepOut = salesrepOut.ToUpper();
                                }

                            }
                            if (jobSheet.ProjectManager != 0)
                            {
                                string salesrepPM = dataContext.User.Where(j => (j.IsActive == true) && (j.UserId == jobSheet.ProjectManager)).Select(j => j.SlsRep).Single();
                                if (salesrepPM != string.Empty)
                                {
                                    ViewBag.salesrepPM = salesrepPM.ToUpper();
                                }

                            }
                        }



                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  Project Details  View");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " JobSheetDetails action");
            }

            return View(jobSheet);
        }

        [HttpGet]
        public JsonResult GetAlerts(int jobSheetId)
        {
            return Json(_projectsService.GetAlerts(jobSheetId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update_JobOrderShipDate(DataSourceRequest request, JobOrderViewModel jobOrder)
        {
            return Json(_projectsService.UpdateJobOrderShipDate(jobOrder), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCompanyCreditUser(string text)
        {
            var dataContext = new CommPmApplicationConnection();
            var conos = (from companies in dataContext.Company
                         where companies.IsActive == true
                         select new
                         {
                             Cono = companies.Cono,
                             CompanyName = companies.CompanyName,
                             IsActive = companies.IsActive,

                         });
            if (!string.IsNullOrEmpty(text))
            {
                conos = conos.Where(p => p.CompanyName.Contains(text));
            }

            return Json(conos, JsonRequestBehavior.AllowGet);
        }
    }


}
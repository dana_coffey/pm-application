﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NLog;
using PM_Website.Models;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class ReportsController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        ProjectsService _projectsService = new ProjectsService(connection);
        private static CommPmApplicationConnection connection;
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName : Project  Report View");

                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        public JsonResult getProjects(string IsActive, string Id, string ReportType)
        {
            return Json(_projectsService.getProjects(IsActive,Id,ReportType),JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomers()
        {
            return Json(_projectsService.GetCustomer(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProjectManagers()
        {
            return Json(_projectsService.GetProjectManagers(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOutsideSalesReps()
        {
            return Json(_projectsService.GetOutsideSalesReps(), JsonRequestBehavior.AllowGet);
        }
    }
}
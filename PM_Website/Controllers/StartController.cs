﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NLog;
using PM_Website.Models;
using PM_Website.Services;

namespace PM_Website.Controllers
{
    public class StartController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        public ActionResult Index()
        {
            try
            {
                string theURL = HttpContext.Session["userHomepage"].ToString();
                Response.Redirect(theURL != "" ? theURL : "/Home/Index");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Start Controller Index action");
            }

            return View();
        }
    }
}
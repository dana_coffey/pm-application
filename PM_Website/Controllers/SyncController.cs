﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using NLog;
using PM_Website.Models;

namespace PM_Website.Controllers
{
    public class SyncController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        private static CommPmApplicationConnection connection;

        PageManager pageManager = new PageManager(connection);
        ProjectsService _projectsService = new ProjectsService(connection);

        public ActionResult Index()
        {
            try
            {
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {

                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        [HttpPost]
        public ActionResult GetSyncFailed([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_projectsService.GetSyncFailed().ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult JobSheet_Create(JobSheetViewModel jobSheet)
        {
            try
            {
                if (jobSheet != null)
                {
                    _projectsService.Create(jobSheet);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex,"Error occured in JobStep controller JobStep_Create action");
            }

            Response.Redirect("/JobSheet/Index");

            return View();
        }
    }
}
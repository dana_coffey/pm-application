﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using NLog;

namespace PM_Website.Controllers
{
   
    public class UserController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;

        UserService userService = new UserService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                PopulateUserRoles();
                PopulateSMSNs();
                PopulateConos();
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :User View");

                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        public ActionResult UserFilter()
        {
            try
            {               
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :User Filter");
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " UserFilter action");
            }

            return View();
        }

        private void PopulateUserRoles()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<UserRoleViewModel> uroles = new List<UserRoleViewModel>();                

                if(HttpContext.Session["userRoleId"].ToString() == "2")
                {
                    uroles = dataContext.UserRoles
                            .Select(c => new UserRoleViewModel
                            {
                                RoleId = c.RoleId,
                                RoleName = c.RoleName
                            }).Where(p => (p.RoleId == 3) || (p.RoleId == 8)).ToList();                    
                }
                else if (HttpContext.Session["userRoleId"].ToString() == "7")
                {
                    uroles = dataContext.UserRoles
                            .Select(c => new UserRoleViewModel
                            {
                                RoleId = c.RoleId,
                                RoleName = c.RoleName
                            }).Where(p => (p.RoleId == 5) || (p.RoleId == 6) || (p.RoleId == 7)).ToList();                    
                }
                else
                {
                    uroles = dataContext.UserRoles
                            .Select(c => new UserRoleViewModel
                            {
                                RoleId = c.RoleId,
                                RoleName = c.RoleName
                            }).ToList();
                }

                ViewData["userRoles"] = uroles;
                ViewData["defaultUserRoleId"] = uroles.First().RoleId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller PopulateUserRoles action");
            }
        }

        private void PopulateConos()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<CompanyViewModel> uconos = new List<CompanyViewModel>();

                if (HttpContext.Session["userRoleId"].ToString() == "1")
                {
                    uconos = dataContext.Company
                            .Select(c => new CompanyViewModel
                            {
                                Cono = c.Cono,
                                CompanyName = c.CompanyName,
                                IsActive = c.IsActive
                            }).Where(p => (p.IsActive == true)).ToList();
                }
                else
                {
                    int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
                    uconos = dataContext.Company
                            .Select(c => new CompanyViewModel
                            {
                                Cono = c.Cono,
                                CompanyName = c.CompanyName,
                                IsActive = c.IsActive
                            }).Where(p => (p.IsActive == true) && (p.Cono == conoid)).ToList();
                }
                

                ViewData["userConos"] = uconos;
                ViewData["defaultConoId"] = uconos.First().Cono;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller PopulateUserConos action");
            }
        }

        public JsonResult GetProjectManagers()
        {           

            var dataContext = new CommPmApplicationConnection();

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var pmIds = dataContext.User.Where(p => (p.IsActive == true) && (p.RoleId == 3)).Select(p => p.SlsRep).ToList();


                var smsns = dataContext.SMSN.Select(c => new SMSNViewModel
                {
                    SlsRep = c.SlsRep,
                    Name = c.Name,
                    Email = c.Email
                }).Where(e => (e.Name != "") && (pmIds.Contains(e.SlsRep)))
                .OrderBy(e => e.Name);

                return Json(smsns, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var pmIds = dataContext.User.Where(p => (p.IsActive == true) && (p.RoleId == 3) && (p.Cono==conoid)).Select(p => p.SlsRep).ToList();


                var smsns = dataContext.SMSN.Select(c => new SMSNViewModel
                {
                    SlsRep = c.SlsRep,
                    Name = c.Name,
                    Email = c.Email
                }).Where(e => (e.Name != "") && (pmIds.Contains(e.SlsRep)))
                .OrderBy(e => e.Name);

                return Json(smsns, JsonRequestBehavior.AllowGet);
            }            
        }

        public JsonResult GetCreditReps(string text)
        {

            var dataContext = new CommPmApplicationConnection();

            List<UserViewModel> user = new List<UserViewModel>();

            List<int> PMId = new List<int>();

            List<int> roleid = new List<int>();
            roleid.Add(5);
            

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                user = dataContext.User
                         .Select(c => new UserViewModel
                         {
                             UserId = c.UserId,
                             SMSN = new SMSNViewModel()
                             {
                                 SlsRep = c.SlsRep.ToUpper(),
                                 Name = c.SMSN.Name
                             },
                             IsActive = c.IsActive,
                             RoleId = c.RoleId,
                             Cono = c.Cono,
                             UserIdName = c.SMSN.SlsRep.ToUpper() + " : " + c.SMSN.Name
                         }).Where(e => (e.IsActive == true) && (roleid.Contains(e.RoleId))).ToList();

                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.SMSN.Name.Contains(text) || p.SMSN.SlsRep.ToString().Contains(text)).ToList();
                }
            }
            else
            {
                user = dataContext.User
                         .Select(c => new UserViewModel
                         {
                             UserId = c.UserId,
                             SMSN = new SMSNViewModel()
                             {
                                 SlsRep = c.SlsRep.ToUpper(),
                                 Name = c.SMSN.Name
                             },
                             IsActive = c.IsActive,
                             RoleId = c.RoleId,
                             Cono = c.Cono,
                             UserIdName = c.SMSN.SlsRep.ToUpper() + " : " + c.SMSN.Name
                         }).Where(e => (e.IsActive == true) && (roleid.Contains(e.RoleId)) && (e.Cono == conoid)).ToList();

                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.SMSN.Name.Contains(text) || p.SMSN.SlsRep.ToString().Contains(text)).ToList();
                }
            }

            return Json(user, JsonRequestBehavior.AllowGet);
        }



        //public JsonResult GetJobSheetProjectManager(string text)
        //{
        //    var dataContext = new CommPmApplicationConnection();
        //    var user = (from users in dataContext.User
        //                join SMSN in dataContext.SMSN on users.SlsRep equals SMSN.SlsRep
        //                where (users.IsActive) == true && (users.RoleId == 3)
        //                select new
        //                {
        //                    UserId = users.UserId,
        //                    Email = users.Email,
        //                    IsActive = users.IsActive,
        //                    SlsRep = SMSN.SlsRep,
        //                    Name = SMSN.Name

        //                });
        //    if (!string.IsNullOrEmpty(text))
        //    {
        //        user = user.Where(p => p.Name.Contains(text));
        //    }

        //    return Json(user, JsonRequestBehavior.AllowGet);
        //}


        //public JsonResult GetJobSheetProjectManager(string text)
        //{
        //    var dataContext = new CommPmApplicationConnection();

        //    List<UserViewModel> user = new List<UserViewModel>();

        //    List<int> PMId = new List<int>();

        //    if (HttpContext.Session["userRoleId"].ToString() == "2")
        //    {
        //        int Mangerid = Convert.ToInt32(HttpContext.Session["userId"].ToString());
        //        PMId = dataContext.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == Mangerid)).Select(p => p.PMId).ToList();

        //        user = dataContext.User
        //                  .Select(c => new UserViewModel
        //                  {
        //                      UserId = c.UserId,
        //                      SMSN = new SMSNViewModel()
        //                      {
        //                          Name = c.SMSN.Name
        //                      },
        //                      IsActive = c.IsActive,
        //                      RoleId = c.RoleId
        //                  }).Where(e => (e.IsActive == true) && (e.RoleId == 3)).ToList();

        //        user = dataContext.User
        //            .Select(c => new UserViewModel
        //            {
        //                UserId = c.UserId,
        //                SMSN = new SMSNViewModel()
        //                {
        //                    Name = c.SMSN.Name
        //                },
        //                IsActive = c.IsActive,
        //                RoleId = c.RoleId
        //            }).Where(u => (PMId.Contains(u.UserId))).ToList();

        //        if (!string.IsNullOrEmpty(text))
        //        {
        //            user = user.Where(p => p.SMSN.Name.Contains(text)).ToList();
        //        }
        //    }
        //    else
        //    {

        //        user = dataContext.User
        //                 .Select(c => new UserViewModel
        //                 {
        //                     UserId = c.UserId,
        //                     SMSN = new SMSNViewModel()
        //                     {
        //                         Name = c.SMSN.Name
        //                     },
        //                     IsActive = c.IsActive,
        //                     RoleId = c.RoleId
        //                 }).Where(e => (e.IsActive == true) && (e.RoleId == 3)).ToList();

        //        if (!string.IsNullOrEmpty(text))
        //        {
        //            user = user.Where(p => p.SMSN.Name.Contains(text)).ToList();
        //        }
        //    }

        //    return Json(user, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetJobSheetProjectManager(string text)
        {
            var dataContext = new CommPmApplicationConnection();

            List<UserViewModel> user = new List<UserViewModel>();

            List<int> PMId = new List<int>();

            List<int> roleid = new List<int>();
            roleid.Add(3);
            roleid.Add(8);

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());

            if (HttpContext.Session["userRoleId"].ToString() == "2")
            {
                int Mangerid = Convert.ToInt32(HttpContext.Session["userId"].ToString());
                PMId = dataContext.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == Mangerid)).Select(p => p.PMId).ToList();

                user = dataContext.User
                          .Select(c => new UserViewModel
                          {
                              UserId = c.UserId,
                              SMSN = new SMSNViewModel()
                              {
                                  SlsRep = c.SlsRep,
                                  Name = c.SMSN.Name
                              },
                              IsActive = c.IsActive,
                              RoleId = c.RoleId,
                              Cono = c.Cono,
                              UserIdName =  c.SMSN.SlsRep.ToUpper() + " : "+ c.SMSN.Name
                          }).Where(e => (e.IsActive == true) && (e.RoleId == 3) && (e.Cono == conoid)).ToList();

                user = dataContext.User
                    .Select(c => new UserViewModel
                    {
                        UserId = c.UserId,
                        SMSN = new SMSNViewModel()
                        {
                            SlsRep = c.SlsRep.ToUpper(),
                            Name = c.SMSN.Name
                        },
                        IsActive = c.IsActive,
                        RoleId = c.RoleId,
                        Cono = c.Cono,
                        UserIdName = c.SMSN.SlsRep.ToUpper() + " : " + c.SMSN.Name
                    }).Where(u => (PMId.Contains(u.UserId))).ToList();

                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.SMSN.Name.Contains(text) || p.SMSN.SlsRep.ToString().Contains(text)).ToList();
                }              
            }
            else if (HttpContext.Session["userRoleId"].ToString() == "1")
            { 
                user = dataContext.User
                         .Select(c => new UserViewModel
                         {
                             UserId = c.UserId,
                             SMSN = new SMSNViewModel()
                             {
                                 SlsRep = c.SlsRep.ToUpper(),
                                 Name = c.SMSN.Name
                             },
                             IsActive = c.IsActive,
                             RoleId = c.RoleId,
                             Cono = c.Cono,
                             UserIdName = c.SMSN.SlsRep.ToUpper() + " : " + c.SMSN.Name
                         }).Where(e => (e.IsActive == true) && (roleid.Contains(e.RoleId))).ToList();

                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.SMSN.Name.Contains(text) || p.SMSN.SlsRep.ToString().Contains(text)).ToList();
                }
            }
            else
            {
                user = dataContext.User
                         .Select(c => new UserViewModel
                         {
                             UserId = c.UserId,
                             SMSN = new SMSNViewModel()
                             {
                                 SlsRep = c.SlsRep.ToUpper(),
                                 Name = c.SMSN.Name
                             },
                             IsActive = c.IsActive,
                             RoleId = c.RoleId,
                             Cono = c.Cono,
                             UserIdName = c.SMSN.SlsRep.ToUpper() + " : " + c.SMSN.Name
                         }).Where(e => (e.IsActive == true) && (roleid.Contains(e.RoleId)) && (e.Cono == conoid)).ToList();

                if (!string.IsNullOrEmpty(text))
                {
                    user = user.Where(p => p.SMSN.Name.Contains(text) || p.SMSN.SlsRep.ToString().Contains(text)).ToList();
                }
            }

            return Json(user, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersByRole(string RoleId)
        {
            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            int roleid = 0;
            if (RoleId == "")
            {

            }
            else
            {
                roleid = Convert.ToInt32(RoleId);
            }


            var dataContext = new CommPmApplicationConnection();

            if(HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var users = dataContext.User
                           .Select(c => new UserViewModel
                           {
                               UserId = c.UserId,
                               SMSN = new SMSNViewModel()
                               {
                                   Name = c.SMSN.Name
                               },
                               IsActive = c.IsActive,
                               RoleId = c.RoleId,
                               Cono = c.Cono
                           }).Where(e => (e.IsActive == true) && (e.RoleId == roleid));

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var users = dataContext.User
                           .Select(c => new UserViewModel
                           {
                               UserId = c.UserId,
                               SMSN = new SMSNViewModel()
                               {
                                   Name = c.SMSN.Name
                               },
                               IsActive = c.IsActive,
                               RoleId = c.RoleId,
                               Cono = c.Cono
                           }).Where(e => (e.IsActive == true) && (e.RoleId == roleid) && (e.Cono==conoid));

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult GetUserDetails(string UserId)
        {
            int userid = Convert.ToInt32(UserId);
           
            var dataContext = new CommPmApplicationConnection();
            var users = dataContext.User
                           .Select(c => new UserViewModel
                           {
                               UserId = c.UserId,
                               RoleId = c.RoleId,
                               IsActive = c.IsActive,
                               Email = c.Email,
                               SlsRep = c.SlsRep,
                               Cono = c.Cono,
                               UserRole = new UsersUserRoleViewModel()
                               {
                                   RoleId = c.UserRoles.RoleId,
                                   RoleName = c.UserRoles.RoleName
                               },
                               SMSN = new SMSNViewModel()
                               {
                                   SlsRep = c.SMSN.SlsRep,
                                   Name = c.SMSN.Name,
                                   Email = c.SMSN.Email
                               }
                           }).Where(e => (e.IsActive == true) && (e.UserId == userid));

            if (users == null || users.Count() == 0)
            {
                return Json(new { redirecturl = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                foreach (UserViewModel user in users)
                {
                    HttpContext.Session.Add("userRole", user.UserRole.RoleName);
                    HttpContext.Session.Add("userRoleId", user.UserRole.RoleId);
                    HttpContext.Session.Add("userId", user.UserId);
                    HttpContext.Session.Add("userName", user.Email);
                    HttpContext.Session.Add("firstNameLastName", user.SMSN.Name);
                    HttpContext.Session.Add("userHomepage", userService.GetHomepage(user.UserId));                    
                    HttpContext.Session.Add("userCono", user.Cono);
                }
                return Json(new { redirecturl = "/" }, JsonRequestBehavior.AllowGet);
            }               
        }

        public void PopulateSMSNs()
        {
            try
            {

                var dataContext = new CommPmApplicationConnection();

                var smsns = dataContext.SMSN.Select(c => new SMSNViewModel
                {
                    SlsRep = c.SlsRep,
                    Name = c.Name,
                    Email = c.Email,
                    IsActive = c.IsActive
                }).Where(e => (e.Name != "") && (e.IsActive == true))
                .OrderBy(e => e.Name);

                ViewData["userssmns"] = smsns;
                ViewData["defaultUserSMSNId"] = smsns.First().SlsRep;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller PopulateSMSNs action");
            }
        }

        public JsonResult GetSMSNs()
        {          

            var dataContext = new CommPmApplicationConnection();


            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var smsns = dataContext.SMSN.Select(c => new SMSNViewModel
                {
                    SlsRep = c.SlsRep,
                    Name = c.Name,
                    Email = c.Email,
                    IsActive = c.IsActive
                }).Where(e => (e.Name != "") && (e.IsActive == true))
           .OrderBy(e => e.Name);

                return Json(smsns, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var smsns = dataContext.SMSN.Select(c => new SMSNViewModel
                {
                    SlsRep = c.SlsRep,
                    Name = c.Name,
                    Email = c.Email,
                    IsActive = c.IsActive,
                    CoNo = c.CoNo
                }).Where(e => (e.Name != "") && (e.IsActive == true) && (e.CoNo==conoid))
           .OrderBy(e => e.Name);

                return Json(smsns, JsonRequestBehavior.AllowGet);
            }

           
        }

        public JsonResult GetEstimators(string text)
        {
            var dataContext = new CommPmApplicationConnection();

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());


            if (HttpContext.Session["userRoleId"].ToString() == "1")
            {
                var estimator = (from estimators in dataContext.SMSN
                                 where (estimators.IsActive) == true
                                 select new
                                 {
                                     SlsRep = estimators.SlsRep,
                                     Email = estimators.Email,
                                     IsActive = estimators.IsActive,
                                     Name = estimators.Name
                                 });
                if (!string.IsNullOrEmpty(text))
                {
                    estimator = estimator.Where(p => p.Name.Contains(text));
                }

                return Json(estimator, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var estimator = (from estimators in dataContext.SMSN
                                 where (estimators.IsActive == true) && (estimators.CoNo==conoid)
                                 select new
                                 {
                                     SlsRep = estimators.SlsRep,
                                     Email = estimators.Email,
                                     IsActive = estimators.IsActive,
                                     Name = estimators.Name,
                                     CoNo = estimators.CoNo
                                 });
                if (!string.IsNullOrEmpty(text))
                {
                    estimator = estimator.Where(p => p.Name.Contains(text));
                }

                return Json(estimator, JsonRequestBehavior.AllowGet);
            }

           
        }

        public JsonResult GetUserRoles()
        {
            var dataContext = new CommPmApplicationConnection();
            var uroles = dataContext.UserRoles
                           .Select(c => new UserRoleViewModel
                           {
                               RoleId = c.RoleId,
                               RoleName = c.RoleName,
                               IsActive = c.IsActive
                           }).Where(e => e.IsActive == true)
                           .OrderBy(e => e.RoleName);

            //ViewData["userRoles"] = uroles;
            ViewData["defaultUserRoleId"] = uroles.First().RoleId;

            return Json(uroles, JsonRequestBehavior.AllowGet);
        }

        public ActionResult User_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(userService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult User_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserViewModel> users, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();                
                List<UserViewModel> checkUser = new List<UserViewModel>();

                if (users != null)
                {
                    foreach (var user in users)
                    {
                        checkUser = dataContext.User.Select(c => new UserViewModel
                        {
                            Email = c.Email,
                            IsActive =c.IsActive
                        }).Where(e => (e.Email == user.SMSN.Email)).ToList();

                        if(checkUser.Count > 0)
                        {
                            ModelState.AddModelError("User", "User " + user.SMSN.Name + " already exists in our system. Please check to see if this user is inactive or active");
                        }
                        else
                        {
                            userService.Create(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller User_Create action");
            }

            return Json(userService.Read(IsActive).ToDataSourceResult(request,ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult User_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserViewModel> users, string IsActive)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<UserViewModel> checkUser = new List<UserViewModel>();

                if (users != null)
                {
                    foreach (var user in users)
                    {
                        checkUser = dataContext.User.Select(c => new UserViewModel
                        {
                            Email = c.Email,
                            IsActive = c.IsActive,
                            UserId = c.UserId
                        }).Where(e => (e.Email == user.SMSN.Email) && (e.UserId != user.UserId)).ToList();

                        if (checkUser.Count > 0)
                        {
                            ModelState.AddModelError("User", "User " + user.SMSN.Name + " already exists in our system. Please check to see if this user is inactive or active");                          
                        }
                        else
                        {
                            int role = dataContext.User.Where(p => (p.UserId == user.UserId)).Select(p => p.RoleId).SingleOrDefault();
                            if(role == 2 || role == 3 || role == 8)
                            {
                                if (role == 2)
                                {
                                    List<ManagerPMViewModel> checkManager = new List<ManagerPMViewModel>();
                                    checkManager = dataContext.ManagerPM.Select(c => new ManagerPMViewModel
                                    {
                                        ManagerId = c.ManagerId
                                    }).Where(e => (e.ManagerId == user.UserId)).ToList();

                                    if (checkManager.Count > 0)
                                    {
                                        ModelState.AddModelError("User", "You cannot change the User role (Manager) for " + user.SMSN.Name + ". This user role Manager already delegated to Project Manager. Please update Manager with another Manager to the Project Manager then change the role");
                                    }
                                    else
                                    {
                                        userService.Update(user);
                                    }
                                }
                                else if (role == 3)
                                {
                                    List<DelegatePMViewModel> checkPM = new List<DelegatePMViewModel>();
                                    checkPM = dataContext.DelegatePM.Select(c => new DelegatePMViewModel
                                    {
                                        DelegateUserId = c.DelegateUserId
                                    }).Where(e => (e.DelegateUserId == user.UserId)).ToList();

                                    if (checkPM.Count > 0)
                                    {
                                        ModelState.AddModelError("User", "You cannot change the User role (Project Manager) for " + user.SMSN.Name + ". This user role Project Manager already delegated to another Project Manager. Please update Project Manager with another Project Manager to the Project Manager then change the role");
                                    }
                                    else
                                    {
                                        List<DelegatePMViewModel> checkPMs = new List<DelegatePMViewModel>();
                                        checkPMs = dataContext.DelegatePM.Select(c => new DelegatePMViewModel
                                        {
                                            PMId = c.PMId
                                        }).Where(e => (e.PMId == user.UserId)).ToList();

                                        if (checkPMs.Count > 0)
                                        {
                                            ModelState.AddModelError("User", "You cannot change the User role (Project Manager) for " + user.SMSN.Name + ". This user role Project Manager already delegates to Project Manager. Please update Project Manager with another Project Manager to the Delegate Project Manager then change the role");
                                        }
                                        else
                                        {
                                            userService.Update(user);
                                        }
                                        //userService.Update(user);
                                    }
                                }
                                else if (role == 8)
                                {
                                    List<JobSheetViewModel> checkOSS = new List<JobSheetViewModel>();
                                    checkOSS = dataContext.JobSheet.Select(c => new JobSheetViewModel
                                    {
                                        TMCommissionPosition1Id = c.TMCommissionPosition1Id
                                    }).Where(e => (e.TMCommissionPosition1Id == user.UserId)).ToList();

                                    if (checkOSS.Count > 0)
                                    {
                                        ModelState.AddModelError("User", "You cannot change the User role (Outside Sales Rep) for " + user.SMSN.Name + ". This user already assigned to JobSheet for Outside Sales Rep. Please update jobsheet with another Outside Sales Rep then change the role for this user");
                                    }
                                    else
                                    {
                                        userService.Update(user);
                                    }
                                }
                            }
                            else
                            {
                                userService.Update(user);
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller User_Update action");
            }
            return Json(userService.Read(IsActive).ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetEmail(string id)
        {
            return Json(userService.GetEmailByUser(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult User_ReadByRoleId([DataSourceRequest] DataSourceRequest request, int RoleId)
        {
            return Json(userService.ReadByRoleId(RoleId).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult User_UpdateByRoleId([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserViewModel> users, int RoleId)
        {
            try
            {
                if (users != null)
                {
                    foreach (var user in users)
                    {
                        userService.Update(user);
                    }
                }
                
                        
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller User_UpdateByRoleId action");
            }
            return Json(userService.ReadByRoleId(RoleId).ToDataSourceResult(request, ModelState));
        }

        private string getName(int id)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var dbuser = dataContext.User
                   .FirstOrDefault(p => p.UserId == id);
                return dbuser.SMSN.Name;

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Delegate class getName method");
                return "";
            }
        }

        [HttpPost]
        public JsonResult SaveFilters(string gridName, string filter, string filterName, bool isDefault)
        {
            userService.SaveFilters(gridName, filter, filterName, isDefault);

            return Json(true);
        }

        [HttpGet]
        public JsonResult GetFilter(string gridName)
        {
            return Json(userService.GetFilter(gridName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllFilters()
        {
            return Json(userService.GetAllFilters(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFilterBySelect(string gridName, string filterid)
        {
            return Json(userService.GetFilterBySelect(gridName, filterid), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteFilterById(string UserFilterId)
        {
          return Json(userService.DeleteFilterById(UserFilterId), JsonRequestBehavior.AllowGet);                
        }

        [HttpGet]
        public JsonResult CheckFilterName(string filterName, bool isDefault)
        {
            return Json(userService.CheckFilterName(filterName, isDefault), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserFilters_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(userService.FiltersRead().ToDataSourceResult(request));
        }

        [HttpGet]
        public JsonResult IsDefault_Update(int UserId, int FilterId, bool check)
        {
            return Json(userService.IsDefault_Update(UserId, FilterId, check), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUserDefaultFilterId()
        {
            return Json(userService.GetUserDefaultFilterId(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetConos()
        {

            var dataContext = new CommPmApplicationConnection();

            int conoid = Convert.ToInt32(HttpContext.Session["userCono"].ToString());
            int roleid = Convert.ToInt32(HttpContext.Session["userRoleId"].ToString());
            
            var conos = dataContext.Company.Select(c => new CompanyViewModel
            {
                Cono = c.Cono,
                CompanyName = c.CompanyName,               
                IsActive = c.IsActive
            }).Where(e => (e.IsActive == true))
            .OrderBy(e => e.CompanyName);
            if (roleid == 1)
            {
                
            }
            else
            {
                conos.Where(e => e.Cono == conoid);
            }
            return Json(conos, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreditUserRoles(int userid, int roleid)
        {
            try
            {
                PopulateCreditRoles();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :Credit User Role View");
                        ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                        ViewBag.UserId = userid;
                        ViewBag.RoleId = roleid;

                        var dataContext = new CommPmApplicationConnection();

                        ViewBag.UserName = dataContext.User.Where(u => u.UserId == userid).Select(e => e.SMSN.Name).FirstOrDefault().ToString();
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller CreditUserRoles action");
            }
            return View();
        }

        private void PopulateCreditRoles()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<UserRoleViewModel> uroles = new List<UserRoleViewModel>();

                uroles = dataContext.UserRoles
                           .Select(c => new UserRoleViewModel
                           {
                               RoleId = c.RoleId,
                               RoleName = c.RoleName
                           }).Where(p => (p.RoleId == 5) || (p.RoleId == 6) || (p.RoleId == 7)).ToList();

                ViewData["credituserRoles"] = uroles;
                ViewData["defaultCreditUserRoleId"] = uroles.First().RoleId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller PopulateCreditRoles action");
            }
        }

        public ActionResult CreditUserRole_Read([DataSourceRequest] DataSourceRequest request, string IsActive, int CUserId, int CRoleId)
        {
            return Json(userService.CreditUserRoleRead(IsActive, CUserId, CRoleId).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreditUserRole_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CreditUserRoleViewModel> credituserRoles, string IsActive, int CUserId, int CRoleId)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<CreditUserRoleViewModel> checkCreditUserRole = new List<CreditUserRoleViewModel>();
                
                    foreach (var creditUserRole in credituserRoles)
                    {
                        if (creditUserRole.UserId == null)
                        {
                            if (CRoleId == creditUserRole.RoleId)
                            {
                                ModelState.AddModelError("CreditUserRole", "Selected Role already exists to this user as a main Role. You cannot create a main role again");
                            }
                            else
                            {
                                checkCreditUserRole = dataContext.CreditUserRole.Select(c => new CreditUserRoleViewModel
                                {
                                    RoleId = c.RoleId,
                                    UserId = c.UserId,
                                    IsActive = c.IsActive
                                }).Where(e => (e.UserId == CUserId) && (e.RoleId == creditUserRole.RoleId)).ToList();

                                if (checkCreditUserRole.Count > 0)
                                {
                                    ModelState.AddModelError("CreditUserRole", "Selected Role already exists to this user. Please check to see if this role is inactive or active");
                                }
                                else
                                {
                                    userService.CreditUserRoleCreate(creditUserRole, CUserId, CRoleId);
                                }
                            }
                        }

                        
                    }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller CreditUserRole_Create action");
            }

            return Json(userService.CreditUserRoleRead(IsActive, CUserId, CRoleId).ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreditUserRole_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CreditUserRoleViewModel> credituserRoles, string IsActive, int CUserId, int CRoleId)
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                List<CreditUserRoleViewModel> checkCreditUserRole = new List<CreditUserRoleViewModel>();

                if (credituserRoles != null)
                {
                    foreach (var creditUserRole in credituserRoles)
                    {
                        if (CRoleId == creditUserRole.RoleId)
                        {
                            ModelState.AddModelError("CreditUserRole", "Selected Role already exists to this user as a main Role. You cannot create a main role again");
                        }
                        else
                        {
                            checkCreditUserRole = dataContext.CreditUserRole.Select(c => new CreditUserRoleViewModel
                            {
                                RoleId = c.RoleId,
                                UserId = c.UserId,
                                IsActive = c.IsActive,
                                CreditUserRoleId = c.CreditUserRoleId
                            }).Where(e => (e.UserId == CUserId) && (e.RoleId == creditUserRole.RoleId) && (e.CreditUserRoleId != creditUserRole.CreditUserRoleId)).ToList();

                            if (checkCreditUserRole.Count > 0)
                            {
                                ModelState.AddModelError("CreditUserRole", "Selected Role already exists to this user. Please check to see if this role is inactive or active");
                            }
                            else
                            {
                                userService.CreditUserRoleUpdate(creditUserRole, CUserId, CRoleId);
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller CreditUserRole_Update action");
            }
            return Json(userService.CreditUserRoleRead(IsActive, CUserId, CRoleId).ToDataSourceResult(request, ModelState));
        }

    }
}
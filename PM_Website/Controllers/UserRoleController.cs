﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PM_Website.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using NLog;

namespace PM_Website.Controllers
{
    public class UserRoleController : Controller
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        Logger userLogger = LogManager.GetLogger("UserLog");

        private static CommPmApplicationConnection connection;
        UserRoleService UserRoleService = new UserRoleService(connection);
        PageManager pageManager = new PageManager(connection);

        public ActionResult Index()
        {
            try
            {
                PopulateUserRoles();
                ViewBag.Session = HttpContext.Session["userRoleId"].ToString();
                var thisType = GetType().Name;
                var rolebasedaccesss = pageManager.getRoleBasedAccess(Int32.Parse(HttpContext.Session["userRoleId"].ToString()), thisType.Substring(0, thisType.Length - 10));

                if (rolebasedaccesss != null)
                {
                    if (rolebasedaccesss.IsView == true)
                    {
                        userLogger.Info("Date :" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "  " + "UserName" + " " + HttpContext.Session["mainUserfirstNameLastName"].ToString() + " " + "PageName :  User Role View");
                       
                    }
                    else
                    {
                        Response.Redirect("/Home/AccessError");
                    }
                }
                else
                {
                    Response.Redirect("/Home/AccessError");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in " + this.GetType().Name + " Index action");
            }

            return View();
        }

        private void PopulateUserRoles()
        {
            try
            {
                var dataContext = new CommPmApplicationConnection();
                var uroles = dataContext.UserRoles
                            .Select(c => new UserRoleViewModel
                            {
                                RoleId = c.RoleId,
                                RoleName = c.RoleName
                            })
                            .OrderBy(e => e.RoleName);

                ViewData["userRoles"] = uroles;
                ViewData["defaultUserRoleId"] = uroles.First().RoleId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User controller PopulateUserRoles action");
            }
        }

        public ActionResult Role_Read([DataSourceRequest] DataSourceRequest request, string IsActive)
        {
            return Json(UserRoleService.Read(IsActive).ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Role_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserRoleViewModel> roles, string IsActive)
        {
            try
            {
                if (roles != null)
                {
                    foreach (var role in roles)
                    {
                        UserRoleService.Update(role);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserRole controller Role_Update action");
            }
            return Json(UserRoleService.Read(IsActive), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserRoles(string text)
        {
            var dataContext = new CommPmApplicationConnection();
            var uroles = dataContext.UserRoles
                           .Select(c => new UserRoleViewModel
                           {
                               RoleId = c.RoleId,
                               RoleName = c.RoleName,
                               IsActive = c.IsActive
                           }).Where(e => e.IsActive == true)
                           .OrderBy(e => e.RoleName);

            if (!string.IsNullOrEmpty(text))
            {
                uroles = uroles.Where(p => p.RoleName.Contains(text)).OrderBy(p=>p.RoleName);
            }

            return Json(uroles, JsonRequestBehavior.AllowGet);
        }
    }
}
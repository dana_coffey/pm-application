﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PM_Website.Models;

namespace PM_Website
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //private static CommPmApplicationConnection connection;
        //UserService userService = new UserService(connection);
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            // ...
            routes.IgnoreRoute("elmah.axd");
            // ...
        }
        
        protected void Session_Start(object sender, EventArgs e)
        {
            string sessionId = Session.SessionID;

            var securityHelper = new SecurityHelper();
            securityHelper.Authenticate();
            HttpContext.Current.Session.Timeout = 540;

            if(HttpContext.Current.Session["userRoleId"].ToString()=="0")
            {
                Response.Redirect("/Home/AccessError");
            }
            
        }
        protected void ErrorMailTest_Mailing(object sender, Elmah.ErrorMailEventArgs e)
        {
            e.Mail.Priority = System.Net.Mail.MailPriority.High;
            var errorType = e.Error.Type;
            e.Mail.Subject = "This is a high priority email from PM Tracker" + "  " +  errorType;
            string Exception = e.Error.Exception.ToString();
            string message = e.Error.Message.ToString();
            
            string emailText = string.Empty;

            emailText += "<table style='width:100%'>";
            emailText += "<tr>";
            emailText += "<td>";
            emailText += "<b>Exception</b>" + " " +  Exception;
            emailText += "</td>";
            emailText += "</tr>";
            emailText += "<tr>";
            emailText += "<td>";
            emailText += "<b>Error Message</b>" + " " + message;
            emailText += "</td>";
            emailText += "</tr>";
            emailText += "<tr>";
            emailText += "<td>";
            emailText += "<b>Error Type</b>" + " " + errorType;
            emailText += "</td>";
            emailText += "</tr>";
            emailText += "</table>";

            string toEmail = "";

            if (ConfigurationManager.AppSettings["isTestMode"] != null)
            {
                if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                {
                    if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                    {
                        toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                    }
                }
                else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                {

                    toEmail = ConfigurationManager.AppSettings["DistributionEmail"].ToString();
                }
            }
            PM_Emailer.Email.SendMail(emailText, toEmail, e.Mail.Subject);
          
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class AdhocReportModel
    {

        public int JobSheetId { get; set; }
        public string UserName { get; set; }
        public string Filename { get; set; }
        public string url { get; set; }
    }
}
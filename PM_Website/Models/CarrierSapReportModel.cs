﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class CarrierSapReportModel
    {
        public int Id { get; set; }
        public int SalesOrder { get; set; }
        public string PurchaseOrder { get; set; }
        public DateTime LastChanged { get; set; }
        public string Consignee { get; set; }
        public string OverallOrderStatus { get; set; }
        public DateTime DateEntered { get; set; }
    }

    public class CarrierSapReportLineModel
    {
        public int Id { get; set; }
        public int SalesOrder { get; set; } 
        public int LineNumber { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public decimal OrderQty { get; set; }
        public decimal ShippedQty { get; set; }
        public DateTime CRSD { get; set; }
        public DateTime CPSD { get; set; }
        public DateTime PrevCPSD { get; set; }
        public decimal Rate { get; set; }
        public decimal NetPrice { get; set; }
        public decimal ItemValue { get; set; }
    }
}
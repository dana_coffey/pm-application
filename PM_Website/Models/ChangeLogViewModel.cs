﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class ChangeLogViewModel
    {
        public int LogId { get; set; }
        public int ModifiedByUserId { get; set; }
        public string AttributeName { get; set; }
        public string PreviousVal { get; set; }
        public string NewVal { get; set; }
        public System.DateTime ChangeDate { get; set; }
        public string JobId { get; set; }
        public bool IsActive { get; set; }

        public virtual UserViewModel User { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class CompanyViewModel
    {
        public int Cono { get; set; }
        public string CompanyName { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class CreateNewUserModel
    {
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public virtual UsersUserRoleViewModel UserRole { get; set; }
        public virtual SMSNViewModel SMSN { get; set; }

    }
}
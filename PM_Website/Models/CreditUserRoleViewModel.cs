﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class CreditUserRoleViewModel
    {
        public int CreditUserRoleId { get; set; }
        public Nullable<int> UserId { get; set; }
        public int RoleId { get; set; }
        public Nullable<bool> IsActive { get; set; }

        [UIHint("UserRoles")]
        //[Required]        
        public virtual UsersUserRoleViewModel UserRoles { get; set; }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class CustomerCreditRepsViewModel
    {
        public int CustCreditRep { get; set; }
        public string CustNo { get; set; }
        public string SlsRep { get; set; }
        public Nullable<int> Cono { get; set; }
        public bool IsActive { get; set; }

        public virtual Customer Customer { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class DashboardChartViewModel
    {
        public string JobSheetName { get; set; }
        public int JobSheetId { get; set; }
        public string SubAccount { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string account { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class DashboardModel
    {
        public DashboardChartViewModel dashboardChartViewModel { get; set; }
        public DashbordJobStatusViewModel dashbordJobStatusViewModel { get; set; }
    }
}
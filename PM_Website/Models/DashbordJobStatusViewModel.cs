﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class DashbordJobStatusViewModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public bool sheetCreated { get; set; }
        public bool submittedToContractor { get; set; }
        public bool returnedFromContractor { get; set; }
        public bool submittedToCredit { get; set; }
        public bool approvedByCredit { get; set; }
        public bool voltageVerified { get; set; }
        public bool ordersCreated { get; set; }
        public bool subaccountCreated { get; set; }
        public bool sheetDeactivated { get; set; }
        public bool jobSynced { get; set; }

        public bool approvedByContractor { get; set; }
        public bool reviewedByContractor { get; set; }       
        public bool rejectedByCredit { get; set; }
        public bool jobclosed { get; set; }
        public bool resubmittedToContractor { get; set; }

        public string jobName { get; set; }

        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string JobOrShipToNumber { get; set; }

        public string voltageVerifiedURL { get; set; }

        public string submittedToContractorCreatedDate { get; set; }
        public string returnedFromContractorCreatedDate { get; set; }
        public string submittedToCreditCreatedDate { get; set; }
        public string approvedByCreditCreatedDate { get; set; }
        public string approvedByContractorCreatedDate { get; set; }
        public string reviewedByContractorCreatedDate { get; set; }
        public string rejectedByCreditCreatedDate { get; set; }        
        public string resubmittedToContractorCreatedDate { get; set; }


        public string submittedToContractorModifiedDate { get; set; }
        public string returnedFromContractorModifiedDate { get; set; }
        public string submittedToCreditModifiedDate { get; set; }
        public string approvedByCreditModifiedDate { get; set; }
        public string approvedByContractorModifiedDate { get; set; }
        public string reviewedByContractorModifiedDate { get; set; }
        public string rejectedByCreditModifiedDate { get; set; }
        public string resubmittedToContractorModifiedDate { get; set; }

        public virtual CustomerViewModel Customer { get; set; }

        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public virtual JobSheetUserViewModel User3 { get; set; }
        public string VerifiedVoltageDoc { get; set; }
        public string VerifiedVoltageWhom { get; set; }
        public string VerifiedVoltageCreatedDate { get; set; }
        public string ShipToName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class DelegatePMViewModel
    {
        public int DelegatePMId { get; set; }
        [Required]
        public int DelegateUserId { get; set; }
        [Required]
        public int PMId { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual JobSheetUserViewModel User { get; set; }
        public virtual JobSheetUserViewModel User1 { get; set; }
        public virtual JobSheetUserViewModel User2 { get; set; }
    }
}
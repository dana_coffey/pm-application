﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PM_Website.Models
{
    public class EmailOutageModel
    {
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public HttpPostedFileBase uploadedFiles { get; set; }
        public string CCEmail { get; set; }

    }
}
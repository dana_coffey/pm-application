﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class EmailSubscriptionViewModel
    {
        public int EmailSubscriptionId { get; set; }
        public int UserId { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public int SubsciptionTypeId { get; set; }
        public Nullable<bool> isInternalOnly { get; set; }

        [UIHint("Users")]
        
        public virtual EmailSubscriptionUserViewModel User { get; set; }

        [UIHint("SubscriptionTypes")]
        
        public virtual SubscriptionTypeViewModel SubscriptionType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class EmailTemplateEditModel
    {
        public int Id { get; set; }
        public string TemplateName { get; set; }
        public string Template { get; set; }
        public string SubscriptionName { get; set; }
        public string TemplateSubject { get; set; }
        public List<string> ModelValues { get; set; }
        public int SubscriptionId { get; set; }
    }
}
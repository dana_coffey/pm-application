﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class EmailTemplateGridViewModel
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateView { get; set; }
        public string TemplateSubject { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class FeedbackModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Topic { get; set; }
        public string SubTopic { get; set; }
        public string Feedback { get; set; }
        public string URL { get; set; }
        public string Browser { get; set; }
    }
}
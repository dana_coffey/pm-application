﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class HelpPageModel
    {
        public int Id { get; set; }
        public string PageName { get; set; }
    }
}
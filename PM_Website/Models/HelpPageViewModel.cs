﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class HelpPageViewModel
    {
        public int Id { get; set; }
        public string PageName { get; set; }
        public List<HelpPageViewModel> hasChildren { get; set; }
        public bool encoded = false;
    }
}
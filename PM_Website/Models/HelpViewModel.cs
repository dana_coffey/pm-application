﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class HelpViewModel
    {
        public string PageName { get; set; }
        public string Description { get; set; }
        public int RoleId { get; set; }

        public bool? IsActive { get; set; }

    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PM_Website.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class JobOrderShipDates
    {
        public int Id { get; set; }
        public int JobOrderId { get; set; }
        public Nullable<System.DateTime> CRSD { get; set; }
        public Nullable<System.DateTime> CPSD { get; set; }
        public Nullable<System.DateTime> PrevCPSD { get; set; }
        public int CreatedUser { get; set; }
        public int ModifiedUser { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual JobOrders JobOrders { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}

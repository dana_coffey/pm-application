﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobOrderViewModel
    {
        public long? OrderId { get; set; }
        public int? OrderNo { get; set; }
        public int? OrderSurf { get; set; }
        public decimal? CustNo { get; set; }
        public string ShipTo { get; set; }
        public string CustPO { get; set; }
        public int? InvoiceNo { get; set; }
        public decimal? TotalOrderAmount { get; set; }
        public long? IsActive { get; set; }
        public DateTime? OrderCreatedDate { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? CRSD { get; set; }
        public DateTime? CPSD { get; set; }
        public bool ManualUpdate { get; set; }
        public DateTime? PrevCPSD { get; set; }
    }
}
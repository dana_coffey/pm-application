//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PM_Website.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class JobSheet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobSheet()
        {
            this.CreditDocuments = new HashSet<CreditDocuments>();
            this.ProjectDataReportJobSheets = new HashSet<ProjectDataReportJobSheets>();
            this.JobSheetAlert = new HashSet<JobSheetAlert>();
            this.JobSheetCCGFiles = new HashSet<JobSheetCCGFiles>();
            this.JobSheetChangeLog = new HashSet<JobSheetChangeLog>();
            this.JobSheetCompletedStep = new HashSet<JobSheetCompletedStep>();
            this.JobStepLog = new HashSet<JobStepLog>();
        }
    
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public int Company { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public Nullable<int> Status { get; set; }
        public string JobOrShipToNumber { get; set; }
        public Nullable<bool> ManagementApproved { get; set; }
        public Nullable<System.DateTime> SetUpDate { get; set; }
        public Nullable<System.TimeSpan> SetUpTime { get; set; }
        public int TMCommissionPosition1Id { get; set; }
        public string TMCommisionPosition1 { get; set; }
        public Nullable<int> TMCommissionPosition2Id { get; set; }
        public string TMCommissionPosition2 { get; set; }
        public Nullable<int> ManagerApproval { get; set; }
        public Nullable<System.DateTime> ManagerApprovedDate { get; set; }
        public int SubmittedBy { get; set; }
        public Nullable<int> Owner { get; set; }
        public Nullable<int> CreditRep { get; set; }
        public string JobName { get; set; }
        public string CountyOfJob { get; set; }
        public string JobStreeAddress { get; set; }
        public string JobState { get; set; }
        public string JobCity { get; set; }
        public int JobZip { get; set; }
        public string GeneralContractor { get; set; }
        public string GCMailingAddress { get; set; }
        public string GCPhoneNumber { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public Nullable<int> GCZip { get; set; }
        public string OwnerName { get; set; }
        public string OMailingAddress { get; set; }
        public string OPhoneNumber { get; set; }
        public string OCity { get; set; }
        public string OState { get; set; }
        public Nullable<int> OZip { get; set; }
        public string BondedJob { get; set; }
        public string CopyOfBondAttached { get; set; }
        public string BondInformation { get; set; }
        public string TaxExemption { get; set; }
        public decimal TotalPurchases { get; set; }
        public System.DateTime AnticipatedFirstDelivery { get; set; }
        public System.DateTime AnticipatedJobClose { get; set; }
        public string JobLine { get; set; }
        public Nullable<decimal> PrelienAmount { get; set; }
        public string Prelien { get; set; }
        public string PrelienCompany { get; set; }
        public Nullable<System.DateTime> PrelienDate { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> SubmittedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public int LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public Nullable<bool> IsLockRecord { get; set; }
        public Nullable<bool> IsCcgNotified { get; set; }
        public string CompanyName { get; set; }
        public string ShipToName { get; set; }
        public Nullable<decimal> POAmount { get; set; }
        public Nullable<decimal> OrderAmount { get; set; }
        public Nullable<bool> VerifiedVoltage { get; set; }
        public string ShipToAddress { get; set; }
        public int ProjectManager { get; set; }
        public Nullable<System.DateTime> ContractDate { get; set; }
        public string VerifiedVoltageDoc { get; set; }
        public string VerifiedVoltageDocOther { get; set; }
        public string VerifiedVoltageWhom { get; set; }
        public Nullable<System.DateTime> VendorPromisedShipDate { get; set; }
        public Nullable<System.DateTime> CustomerRequestedDeliveryDate { get; set; }
        public string CreditNotes { get; set; }
        public string VerifiedVoltageDocURL { get; set; }
        public Nullable<System.DateTime> VerifiedVoltageCreatedDate { get; set; }
        public Nullable<System.DateTime> VerifiedVoltageModifiedDate { get; set; }
        public string Estimator { get; set; }
        public bool IsVerifiedVoltageReq { get; set; }
        public Nullable<decimal> Material { get; set; }
        public Nullable<decimal> Labor { get; set; }
        public Nullable<decimal> Returnage { get; set; }
        public Nullable<decimal> Duration { get; set; }
        public string ReqShipToNumber { get; set; }
        public string Engineer { get; set; }
        public string SalesForceURL { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditDocuments> CreditDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectDataReportJobSheets> ProjectDataReportJobSheets { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }
        public virtual User User3 { get; set; }
        public virtual User User4 { get; set; }
        public virtual User User5 { get; set; }
        public virtual JobSteps JobSteps { get; set; }
        public virtual User User6 { get; set; }
        public virtual User User7 { get; set; }
        public virtual User User8 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobSheetAlert> JobSheetAlert { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobSheetCCGFiles> JobSheetCCGFiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobSheetChangeLog> JobSheetChangeLog { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobSheetCompletedStep> JobSheetCompletedStep { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobStepLog> JobStepLog { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual SMSN SMSN { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobSheetAlertViewModel
    {
        public int AlertId { get; set; }
        public int UserId { get; set; }
        public string AlertText { get; set; }
        public Nullable<int> NumberofDaysSnooze { get; set; }
        public Nullable<System.DateTime> AlertSnoozeDate { get; set; }
        public Nullable<int> JobSheetId { get; set; }
        public virtual User User { get; set; }
        
    }
}
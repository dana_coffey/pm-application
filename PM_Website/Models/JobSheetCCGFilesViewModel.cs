﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobSheetCCGFilesViewModel
    {
        public int CCGFileId { get; set; }
        public Nullable<int> JobSheetId { get; set; }
        public string CCGFileURL { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CCGFileName { get; set; }

        public virtual JobSheet JobSheet { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobSheetCompletedStepViewModel
    {
        
            public int StatusId { get; set; }
            public int JobSheetId { get; set; }
            public int JobStepId { get; set; }
            public int CreatedBy { get; set; }
            public System.DateTime CreatedDate { get; set; }
            public Nullable<int> LastModifiedBy { get; set; }
            public Nullable<System.DateTime> LastModifiedDate { get; set; }
            public Nullable<bool> IsActive { get; set; }

       
      
    }
}
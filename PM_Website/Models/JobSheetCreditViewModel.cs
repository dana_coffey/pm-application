﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobSheetCreditViewModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string JobName { get; set; }
        public decimal TotalPurchases { get; set; }
        [Required]
        public string StepName { get; set; }
        //[Required]
        public string JobOrShipToNumber { get; set; }
        [Required]
        public string CreditNotes { get; set; }
        [UIHint("Customers")]
        public virtual CustomerViewModel Customer { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsLockRecord { get; set; }

        public int TMCommissionPosition1Id { get; set; }
        public Nullable<int> TMCommissionPosition2Id { get; set; }
        public int ProjectManager { get; set; }
        public decimal? OrderAmount { get; set; }
        public int Company { get; set; }
        public string CompanyName { get; set; }
        public string RequestedShipToNumber { get; set; }
        public string ShipToName { get; set; }
        public string LastStepCompleted { get; set; }

        public virtual JobSheetUserViewModel User7 { get; set; }
        public virtual JobSheetUserViewModel User8 { get; set; }
        public virtual JobSheetUserViewModel User1 { get; set; }
        public DateTime? ManagerApprovedDate { get; set; }
        public string CreditRepName { get; set; }

    }
}
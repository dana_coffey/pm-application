﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class JobSheetUserViewModel
    {
        public int UserId { get; set; }

        public int RoleId { get; set; }
        
        public bool IsActive { get; set; }

        public string Email { get; set; }

        public string SlsRep { get; set; }

        public virtual SMSNViewModel SMSN { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class JobSheetViewModel
    {

        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public int Company { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public int? Status { get; set; }
        public string JobOrShipToNumber { get; set; }
        public bool? ManagementApproved { get; set; }
        public DateTime? SetUpDate { get; set; }
        public TimeSpan? SetUpTime { get; set; }
        public int TMCommissionPosition1Id { get; set; }
        public string TMCommisionPosition1 { get; set; }
        public int? TMCommissionPosition2Id { get; set; }
        public string TMCommissionPosition2 { get; set; }
        public int? ManagerApproval { get; set; }
        public DateTime? ManagerApprovedDate { get; set; }
        public int SubmittedBy { get; set; }
        public int? Owner { get; set; }
        public int? CreditRep { get; set; }
        public string JobName { get; set; }
        public string CountyOfJob { get; set; }
        public string JobStreeAddress { get; set; }
        public string JobState { get; set; }
        public string JobCity { get; set; }
        public int JobZip { get; set; }
        public string GeneralContractor { get; set; }
        public string GCMailingAddress { get; set; }
        public string GCPhoneNumber { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public int? GCZip { get; set; }
        public string OwnerName { get; set; }
        public string OMailingAddress { get; set; }
        public string OPhoneNumber { get; set; }
        public string OCity { get; set; }
        public string OState { get; set; }
        public int? OZip { get; set; }
        public string BondedJob { get; set; }
        public string CopyOfBondAttached { get; set; }
        public string BondInformation { get; set; }
        public string TaxExemption { get; set; }
        public decimal TotalPurchases { get; set; }
        public DateTime AnticipatedFirstDelivery { get; set; }
        public DateTime AnticipatedJobClose { get; set; }
        public string JobLine { get; set; }
        public decimal? PrelienAmount { get; set; }
        public string Prelien { get; set; }
        public string PrelienCompany { get; set; }
        public DateTime? PrelienDate { get; set; }
        public string Notes { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public bool? IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsLockRecord { get; set; }
        public bool? IsCcgNotified { get; set; }
        public string CompanyName { get; set; }
        public string ShipToName { get; set; }
        public decimal? POAmount { get; set; }
        public decimal? OrderAmount { get; set; }
        public bool? VerifiedVoltage { get; set; }
        
        public int ProjectManager { get; set; }
        public string ShipToAddress { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? VendorPromisedShipDate { get; set; }
        public DateTime? CustomerRequestedDeliveryDate { get; set; }
        public string VerifiedVoltageDocURL { get; set; }
        public string Estimator { get; set; }

        public IEnumerable<HttpPostedFileBase> ccgfiles { get; set; }
        public decimal? Material { get; set; }
        public decimal? Labor { get; set; }
        public decimal? Returnage { get; set; }
        public decimal? Duration { get; set; }
        public string Engineer { get; set; }

        public virtual JobSheetUserViewModel User { get; set; }
        public virtual JobSheetUserViewModel User1 { get; set; }
        public virtual JobSheetUserViewModel User2 { get; set; }
        public virtual JobSheetUserViewModel User3 { get; set; }
        public virtual JobSheetUserViewModel User4 { get; set; }
        public virtual JobStepViewModel JobStep { get; set; }
        public virtual JobSheetUserViewModel User5 { get; set; }
        public virtual JobSheetUserViewModel User6 { get; set; }
        public virtual JobSheetUserViewModel User7 { get; set; }
        [UIHint("Customers")]
        public virtual CustomerViewModel Customer { get; set; }
        
        public virtual JobSheetUserViewModel User8 { get; set; }

        public int id { get; set; }
        public string VerifiedVoltageDoc1 { get; set; }
        public string VerifiedVoltageDocOther1 { get; set; }
        public string VerifiedVoltageWhom1 { get; set; }
        public bool? VerifiedVoltageVal { get; set; }
        public int? PreSubmittalId { get; set; }
        public bool? PreSubmittal { get; set; }
        public int? SubmittedtoEngineerId { get; set; }
        public bool? SubmittedtoEngineer { get; set; }
        public int? ReviewedbyEngineerId { get; set; }
        public bool? ReviewedbyEngineer { get; set; }
        public int? ApprovedbyEngineerId { get; set; }
        public bool? ApprovedbyEngineer { get; set; }
        public int? ReturnedbyEngineerId { get; set; }
        public bool? ReturnedbyEngineer { get; set; }
        public int? OrdersCreatedId { get; set; }
        public bool? OrdersCreated { get; set; }
        public int? SubmittedtoCreditId { get; set; }
        public bool? SubmittedtoCredit { get; set; }
        public int? RejectedbyCreditId { get; set; }
        public bool? RejectedbyCredit { get; set; }
        public int? ResubmittedtoEngineerId { get; set; }
        public bool? ResubmittedtoEngineer { get; set; }
        public int? JobClosedId { get; set; }
        public bool? JobClosed { get; set; }
        public int? ApprovedbyCreditId { get; set; }
        public bool? ApprovedbyCredit { get; set; }
        public virtual JobOrderViewModel JobOrder { get; set; }
        public virtual ProjectOrderLineViewModel ProjectOrderLine { get; set; }
        public bool? SubmittedtoCreditValue { get; set; }
        public string VerifiedVoltageDoc { get; set; }
        public string VerifiedVoltageDocOther { get; set; }
        public string VerifiedVoltageWhom { get; set; }
        public string CreditNotes { get; set; }
        public int LastUpdatedStep { get; set; }
        public string creditRepName { get; set; }
        public Dictionary<string, Dictionary<string ,string>> alerts { get; set; }
        public string RequestedShipToNumber { get; set; }
        public string SalesForceURL { get; set; }

        public bool? SaveAndSubmit { get; set; }
    }
}
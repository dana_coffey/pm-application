﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class JobStepLogViewModel
    {
        public int JSLogId { get; set; }
        [Required]
        public int JobStepId { get; set; }
        [Required]
        public int ModifyingUserId { get; set; }
        [Required]
        public System.DateTime DateModified { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> StepStatus { get; set; }
        public int JobSheetId { get; set; }

        public virtual JobStepViewModel JobStep { get; set; }
        public virtual UserViewModel User { get; set; }
        public virtual JobSheetViewModel JobSheet { get; set; }
    }
}
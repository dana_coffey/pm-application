﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class JobStepViewModel
    {
        public int JobStepId { get; set; }
        [Required]
        public string StepName { get; set; }
        [Required]
        public int StepOrder { get; set; }
        [Required]
        public int DaysUntilAlert { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual JobStepLogViewModel JobStepLogs { get; set; }
    }
}
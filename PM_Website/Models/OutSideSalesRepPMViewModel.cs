﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class OutSideSalesRepPMViewModel
    {
        public int OutSideSalesRepPMId { get; set; }
        [Required]
        public int OutSideSalesRepId { get; set; }
        [Required]
        public int PMId { get; set; }
        public int CreatedBy { get; set; }
        [Required]
        public Nullable<bool> IsActive { get; set; }

        public virtual JobSheetUserViewModel User { get; set; }
        public virtual JobSheetUserViewModel User1 { get; set; }
        public virtual JobSheetUserViewModel User2 { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class PMChangeModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string JobName { get; set; }
        public string CustomerName { get; set; }
        public string CustNo { get; set; }
        public string OldName { get; set; }
        public string OldEmail { get; set; }
        public string NewName { get; set; }
        public string NewEmail { get; set; }
    }
}
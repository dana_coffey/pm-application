﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class ProjectCreatedModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string ProjectManger { get; set; }
        public string JobName { get; set; }
        public string AccountName { get; set; }
        public Decimal TotalPurchases { get; set; }
        public string url { get; set; }
        public string changeDescription { get; set; }


    }
}
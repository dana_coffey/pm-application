﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class ProjectDataReportViewModel
    {
        public int ProjectDataReportId { get; set; }
        public string ReportType { get; set; }
        public string ReportName { get; set; }
        public string Generation { get; set; }
        public List<string> JobSheetIds { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual UserViewModel User { get; set; }
    }
}
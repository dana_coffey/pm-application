﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class ProjectOrderLineViewModel
    {
        public Nullable<int> ProjectOrderLineId { get; set; }
        public Nullable<int> OrderNo { get; set; }
        public Nullable<int> OrderSurf { get; set; }
        public Nullable<int> LineNumber { get; set; }
        public string ModelPart { get; set; }
        public Nullable<int> POAmount { get; set; }
        public Nullable<decimal> QuantityOrdered { get; set; }
        public Nullable<decimal> QuantityShipped { get; set; }
        public Nullable<System.DateTime> CustomerRequestedDeliveryDate { get; set; }
        public Nullable<decimal> VendorName { get; set; }
        public Nullable<bool> VerifiedVoltage { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> CustNo { get; set; }
        public string ShipTo { get; set; }
        public Nullable<decimal> VendorNumber { get; set; }
        public Nullable<decimal> OrderAmount { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public Nullable<int> OrderAltNo { get; set; }
        public Nullable<System.DateTime> OrderLineCreatedDate { get; set; }

        public virtual VendorViewModel Vendor { get; set; }
    }
}
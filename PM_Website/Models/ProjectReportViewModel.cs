﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class ProjectReportViewModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public int LastModifiedBy { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string LastModifiedUser { get; set; }
        public string JobOrShipToNumber { get; set; }
        //public virtual JobSheetUserViewModel LastModifiedUser { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int ProjectManager { get; set; }
        public int TMCommissionPosition1Id { get; set; }
        public Nullable<int> TMCommissionPosition2Id { get; set; }
        public string JobName { get; set; }
        public string ShipToName { get; set; }
    }
}
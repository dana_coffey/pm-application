﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class ProjectViewModel
    {
        public string JobId { get; set; }
        public int ProjectId { get; set; }
        [UIHint("String")]
        [Required]
        [Remote("IsJobName_Available", "Validation")]
        public string JobName { get; set; }
        [UIHint("DateTime")]
        [Required]
        public System.DateTime ModifiedDate { get; set; }
        [UIHint("EmailAddress")]
        [Required]
        public string EmailContact { get; set; }
        [Required]
        public decimal BilledToDate { get; set; }
        [UIHint("String")]
        [Required]
        public string SubmittalStatus { get; set; }
        [UIHint("String")]
        [Required]
        public string VendorOrderStatus { get; set; }
        public bool IsActive { get; set; }
    }
}
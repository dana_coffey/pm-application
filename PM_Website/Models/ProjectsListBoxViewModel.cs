﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class ProjectsListBoxViewModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
    }
}
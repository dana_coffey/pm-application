﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class PromiseDateVerificationModel
    {

        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string ProjectManger { get; set; }
        public string JobName { get; set; }
        public string AccountName { get; set; }
        public string CustomerRequestedDeliveryDate { get; set; }
        public string VendorPromisedShipDate { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class RoleBasedAccessViewModel
    {
        public int Id { get; set; }
        public string PageName { get; set; }
        public int RoleId { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsView { get; set; }
        public bool? IsActive { get; set; }

        public virtual UsersUserRoleViewModel UserRole { get; set; }
    }
}
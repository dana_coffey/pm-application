﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class ShipToViewModel
    {
        public int ShipToCustomerID { get; set; }
        //[UIHint("String")]
        //[Required]
        public string ShipToName { get; set; }

        [UIHint("ProjectManagers")]
        [Required]
        public string SlsRep { get; set; }

        //public string JobName { get; set; }

        //[UIHint("String")]
        //[Required]
        //public string CustomerName { get; set; }

        public bool StatusType { get; set; }

        //[Required(ErrorMessage = "Customer required")]
        public string CustNo { get; set; }

        public string JobId { get; set; }

        //public string CustomerProjectManager { get; set; }
        //[Required]
        public bool IsActive { get; set; }

        //[UIHint("String")]
        //[Required]
        public string OSSalesRep { get; set; }

        public string ShipToAddress { get; set; }

        [UIHint("Url")]
        [Required]
        public string SfOpp { get; set; }

        public string ShipToId { get; set; }

        [Required]
        public bool? VerifiedVoltage { get; set; }

        public Nullable<decimal> POAmount { get; set; }

        public Nullable<System.DateTime> CreationDate { get; set; }

        public string JobName { get; set; }
        public string JobSheetName { get; set; }

        public Nullable<decimal> OrderAmount { get; set; }

        [UIHint("Customers")]
        //[Required]
        public virtual CustomerViewModel Customer { get; set; }

        [UIHint("ProjectManagers")]
        [Required]
        public virtual SMSNViewModel SMSN { get; set; }

        public virtual JobSheetViewModel JobSheet { get; set; }
    }
}
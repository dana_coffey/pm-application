﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class StatusViewModel
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
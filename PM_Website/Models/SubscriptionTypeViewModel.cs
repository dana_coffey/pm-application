﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class SubscriptionTypeViewModel
    {
        public int SubsciptionTypeId { get; set; }
        public string SubscriptionTypeName { get; set; }
        public string SubscriptionTypeDescription { get; set; }
        public Nullable<bool> isRealTimeEmail { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class SyncGridViewModel
    {
        public int JobSheetId { get; set; }
        public string JobName { get; set; }
        public string Customer { get; set; }
        public string SubAccount { get; set; }
    }
}
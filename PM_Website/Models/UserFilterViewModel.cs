﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class UserFilterViewModel
    {
        public int UserFilterId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string Filter { get; set; }
        public string FilterName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDefault { get; set; }
    }
}
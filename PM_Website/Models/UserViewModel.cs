﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }  
       
        public int RoleId { get; set; }

        [Required]
        public bool IsActive { get; set; }

        
        [UIHint("EmailAddress")]
        //[Required]        
        public string Email { get; set; }
        public string Name { get; set; }


        public string SlsRep { get; set; }

        public int Cono { get; set; }

        [UIHint("UserRoles")]
        //[Required]        
        public virtual UsersUserRoleViewModel UserRole { get; set; }

        [UIHint("SMSNs")]
        [Required(ErrorMessage = "* Please Select a User")]
        public virtual SMSNViewModel SMSN { get; set; }

        public virtual CompanyViewModel Company { get; set; }
        public string UserIdName { get; set; }
    }
}
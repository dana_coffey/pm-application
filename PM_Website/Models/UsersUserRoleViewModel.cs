﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class UsersUserRoleViewModel
    {
        public int RoleId { get; set; }
        //[Required]
        public string RoleName { get; set; }
        //[Required]
        public string Permissions { get; set; }
        //[Required]
        public bool IsActive { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PM_Website.Models
{
    public class VendorViewModel
    {
        public decimal VendorNumber { get; set; }
        public string VendorName { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> Cono { get; set; }
    }
}
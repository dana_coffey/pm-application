﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class VerifyVoltageModel
    {
        public int id { get; set; }
        public bool VerifiedVoltageVal { get; set; }
        public string VerifiedVoltageDoc1 { get; set; }
        public string VerifiedVoltageDocOther1 { get; set; }
        public string VerifiedVoltageWhom1 { get; set; }
        public HttpPostedFileBase uploadedFiles { get; set; }
        public Nullable<System.DateTime> VerifiedVoltageCreatedDate { get; set; }
        public Nullable<System.DateTime> VerifiedVoltageModifiedDate { get; set; }
        public bool IsVerifiedVoltageReq { get; set; }
    }
}
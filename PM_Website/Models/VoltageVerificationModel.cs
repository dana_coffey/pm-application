﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PM_Website.Models
{
    public class VoltageVerificationModel
    {
        public int JobSheetId { get; set; }
        public string JobSheetName { get; set; }
        public string ProjectManger { get; set; }
        public string JobName { get; set; }
        public string url { get; set; }

    }
}
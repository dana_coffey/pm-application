﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using NLog;
using System.Data.Entity;

namespace PM_Website.Services
{
    public class ChangeLogJobSheetService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public ChangeLogJobSheetService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IEnumerable<JobSheetChangeLogViewModel> ChangeLogJobSheet_Read(int JobSheetId)
        {
            return GetAllJobSheetLogs(JobSheetId);
        }

        public IList<JobSheetChangeLogViewModel> GetAllJobSheetLogs(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();
            List<JobSheetChangeLogViewModel> result = new List<JobSheetChangeLogViewModel>();

            try
            {
                result = connection.JobSheetChangeLog.Select(ChangeLog => new JobSheetChangeLogViewModel
                {
                    JobSheetId = ChangeLog.JobSheetId,
                    LogId = ChangeLog.LogId,
                    ModifiedByUserId = ChangeLog.ModifiedByUserId,
                    AttributeName = ChangeLog.AttributeName,
                    PreviousVal = ChangeLog.PreviousVal,
                    NewVal = ChangeLog.NewVal,
                    ChangeDate = ChangeLog.ChangeDate,
                    IsActive = ChangeLog.IsActive,

                    User = new UserViewModel()
                    {
                        UserId = ChangeLog.User.UserId,
                        Email = ChangeLog.User.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = ChangeLog.User.SMSN.Name
                        }
                    }

                }).Where(p => (p.IsActive == true) && (p.JobSheetId == JobSheetId)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ChangeLogJobSheetService class GetAllJobSheetLogs method");
            }

            return result;
        }
    }
}
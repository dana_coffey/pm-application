﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using NLog;
using System.Data.Entity;
using PM_Emailer;
using System.Configuration;

namespace PM_Website
{

    public class ChangeLogJobStepService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public ChangeLogJobStepService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IEnumerable<JobStepLogViewModel> ChangeLogJobStep_Read(int JobSheetId)
        {
            return GetAllJobStepLogs(JobSheetId);
        }

        public IList<JobStepLogViewModel> GetAllJobStepLogs(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();
            List<JobStepLogViewModel> result = new List<JobStepLogViewModel>();

            try
            {
                result = connection.JobStepLog.Select(ChangeLog => new JobStepLogViewModel
                {
                    JobSheetId = ChangeLog.JobSheetId,
                    JSLogId = ChangeLog.JSLogId,
                    JobStepId = ChangeLog.JobStepId,
                    ModifyingUserId = ChangeLog.ModifyingUserId,
                    DateModified = ChangeLog.DateModified,
                    StepStatus = ChangeLog.StepStatus,
                    IsActive = (bool)ChangeLog.IsActive,

                    JobStep = new JobStepViewModel()
                    {
                        StepName = ChangeLog.JobSteps.StepName
                    },
                    User = new UserViewModel()
                    {
                        UserId = ChangeLog.User.UserId,
                        Email = ChangeLog.User.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = ChangeLog.User.SMSN.Name
                        }
                    },
                    JobSheet = new JobSheetViewModel()
                    {
                        JobSheetId = ChangeLog.JobSheet.JobSheetId,
                        JobSheetName = ChangeLog.JobSheet.JobSheetName,
                        JobName = ChangeLog.JobSheet.JobName
                    }

                }).Where(p => (p.IsActive == true) && (p.JobSheetId == JobSheetId)).OrderByDescending(x => x.DateModified).ToList();


                foreach(JobStepLogViewModel step in result)
                {
                    if (step.JobStep.StepName == "Submitted to Engineer")
                        step.JobStep.StepName = "Submitted to Contractor";
                    if (step.JobStep.StepName == "Reviewed by Engineer")
                        step.JobStep.StepName = "Reviewed by Contractor";
                    if (step.JobStep.StepName == "Approved by Engineer")
                        step.JobStep.StepName = "Approved by Contractor";
                    if (step.JobStep.StepName == "Returned by Engineer")
                        step.JobStep.StepName = "Returned by Contractor";
                    if (step.JobStep.StepName == "Returned by Engineer")
                        step.JobStep.StepName = "Returned from Contractor";
                    if (step.JobStep.StepName == "Rejected by Credit")
                        step.JobStep.StepName = "Returned By Credit";
                    if (step.JobStep.StepName == "Resubmitted to Engineer")
                        step.JobStep.StepName = "Resubmitted to Contractor";

                }
          
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ChangeLogJobStepService class GetAllJobStepLogs method");
            }

            return result;
        }
    }
}
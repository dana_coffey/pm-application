﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;

namespace PM_Website.Services
{
    public class DelegatePMService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public DelegatePMService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<DelegatePMViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<DelegatePMViewModel> result = new List<DelegatePMViewModel>();

            try
            {
                result = connection.DelegatePM.Select(DelegatePM => new DelegatePMViewModel
                {
                    DelegatePMId = DelegatePM.DelegatePMId,
                    DelegateUserId = DelegatePM.DelegateUserId,
                    PMId = DelegatePM.PMId,
                    User = new JobSheetUserViewModel()
                    {
                        UserId = DelegatePM.User.UserId,
                        Email = DelegatePM.User.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = DelegatePM.User.SMSN.Name
                        }
                    },
                    User1 = new JobSheetUserViewModel()
                    {
                        UserId = DelegatePM.User1.UserId,
                        Email = DelegatePM.User1.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = DelegatePM.User1.SMSN.Name
                        }
                    },
                    User2 = new JobSheetUserViewModel()
                    {
                        UserId = DelegatePM.User2.UserId,
                        Email = DelegatePM.User2.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = DelegatePM.User2.SMSN.Name
                        }
                    },
                    IsActive = DelegatePM.IsActive
                }).ToList();

                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    List<int> pmIds = new List<int>();
                    int pmuserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    pmIds = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == pmuserid)).Select(p => p.PMId).ToList();
                    if (IsActive == "True")
                    {
                        return result.Where(u => (u.IsActive == true) && (pmIds.Contains(u.PMId))).ToList();
                    }
                    else if (IsActive == "False")
                    {
                        return result.Where(u => (u.IsActive == false) && (pmIds.Contains(u.PMId))).ToList();
                    }
                    else
                    {
                        return result.Where(u => (pmIds.Contains(u.PMId))).ToList();
                    }
                }
                else
                {
                    if (IsActive == "True")
                    {
                        return result.Where(u => u.IsActive == true).ToList();
                    }
                    else if (IsActive == "False")
                    {
                        return result.Where(u => u.IsActive == false).ToList();
                    }
                    else
                    {
                        return result.ToList();
                    }
                }                  

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePMService class GetAll method");
                return result.ToList();
            }
        }

        public IEnumerable<DelegatePMViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(DelegatePMViewModel delegatePM)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var entity = new DelegatePM();

                entity.DelegateUserId = delegatePM.DelegateUserId;

                entity.PMId = delegatePM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = delegatePM.IsActive;

                connection.DelegatePM.Add(entity);
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePMService class Create method");
            }
        }

        public void Update(DelegatePMViewModel delegatePM)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var entity = new DelegatePM();

                entity.DelegatePMId = delegatePM.DelegatePMId;

                entity.DelegateUserId = delegatePM.DelegateUserId;

                entity.PMId = delegatePM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = delegatePM.IsActive;


                connection.DelegatePM.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;

                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in DelegatePMService class Update method");
            }
        }

        public List<UserViewModel> GetDelegatePMS()
        {
            connection = new CommPmApplicationConnection();
            List<int> pmIds = new List<int>();

            int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            pmIds = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();

            var user = connection.User.Select(c => new UserViewModel()
            {
                UserId = c.UserId,
                IsActive = c.IsActive,
                SMSN = new SMSNViewModel()
                {
                    Name = c.SMSN.Name
                }
            }).Where(u => (u.IsActive == true) && (pmIds.Contains(u.UserId))).ToList();

            return user;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;
using System.Configuration;
using System.Net.Mail;
using System.Web.Mvc;
using PM_Website.Services;

namespace PM_Website
{
    public class EmailService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;
      


        public EmailService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<EmailSubscriptionViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<EmailSubscriptionViewModel> result = new List<EmailSubscriptionViewModel>();

            try
            {
                result = connection.EmailSubscription.Select(emailSubscription => new EmailSubscriptionViewModel
                {
                    EmailSubscriptionId = emailSubscription.EmailSubscriptionId,
                    SubsciptionTypeId = emailSubscription.SubsciptionTypeId,
                    UserId = emailSubscription.UserId,
                    isInternalOnly = emailSubscription.isInternalOnly,
                    IsActive = emailSubscription.IsActive,
                    User = new EmailSubscriptionUserViewModel()
                    {
                        UserId = emailSubscription.User.UserId,
                        Email = emailSubscription.User.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = emailSubscription.User.SMSN.Name
                        }
                    },
                    SubscriptionType = new SubscriptionTypeViewModel()
                    {
                        SubsciptionTypeId = emailSubscription.SubscriptionType.SubsciptionTypeId,
                        SubscriptionTypeName = emailSubscription.SubscriptionType.SubscriptionTypeName
                    }
                }).ToList();

                if (IsActive == "True")
                {
                    return result.Where(u => u.IsActive == true).ToList();
                }
                else if (IsActive == "False")
                {
                    return result.Where(u => u.IsActive == false).ToList();
                }
                else
                {
                    return result.ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailService class GetAll method");
                return result.ToList();
            }
        }

        public IEnumerable<EmailSubscriptionViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(EmailSubscriptionViewModel emailSubscription)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var entity = new EmailSubscription();

                entity.SubsciptionTypeId = emailSubscription.SubsciptionTypeId;

                entity.IsActive = emailSubscription.IsActive;

                entity.isInternalOnly = true;

                entity.UserId = emailSubscription.UserId;
               

                connection.EmailSubscription.Add(entity);
                connection.SaveChanges();

                List<UserViewModel> userViewModels = new List<UserViewModel>();

                userViewModels = connection.User.Select(c => new UserViewModel
                {
                    UserId = c.UserId,
                    RoleId = c.RoleId

                }).Where(e => (e.UserId == emailSubscription.UserId) && (e.RoleId == 3)).ToList();

                if (userViewModels.Count > 0)
                {
                    List<EmailSubscriptionViewModel> checkEmailSubscription = new List<EmailSubscriptionViewModel>();

                    checkEmailSubscription = connection.EmailSubscription.Select(c => new EmailSubscriptionViewModel
                    {
                        UserId = c.UserId,
                        SubsciptionTypeId = c.SubsciptionTypeId

                    }).Where(e => (e.UserId == emailSubscription.UserId) && (e.SubsciptionTypeId == 1)).ToList();

                    if (checkEmailSubscription.Count > 0)
                    {

                    }
                    else
                    {
                        var entitypm = new EmailSubscription();

                        entitypm.SubsciptionTypeId = 1;

                        entitypm.IsActive = true;

                        entitypm.isInternalOnly = true;

                       
                        entitypm.UserId = emailSubscription.UserId;
                        

                        connection.EmailSubscription.Add(entitypm);
                        connection.SaveChanges();
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailService class Create method");
            }
        }

        public void Update(EmailSubscriptionViewModel emailSubscription)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var entity = new EmailSubscription();

                entity.EmailSubscriptionId = emailSubscription.EmailSubscriptionId;

                entity.SubsciptionTypeId = emailSubscription.SubsciptionTypeId;

                entity.IsActive = emailSubscription.IsActive;

               
                entity.UserId = emailSubscription.UserId;
               

                connection.EmailSubscription.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;

                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailService class Update method");
            }
        }
        public void SubmittedToCreditEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);

            var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
            
            try
            {                
                if (jobSheet.TotalPurchases > 15000)
                {
                    //email to Credit rep
                    int? creditrep = jobSheet.CreditRep;
                    //email to credit manager
                    //List<UserViewModel> users = new List<UserViewModel>();
                    var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                    var creditAdmin = connection.User.Where(p => p.RoleId == 7).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                    var users = connection.User.Where(p => p.UserId == creditrep).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var creditManager = connection.User.Where(p => p.RoleId == 6).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var submittedToCreditModel = new SubmittedToCreditModel();

                    submittedToCreditModel.JobSheetId = jobSheet.JobSheetId;
                    submittedToCreditModel.JobSheetName = jobSheet.JobName ?? "null";
                    submittedToCreditModel.UserName = "null";
                    submittedToCreditModel.JobName = jobSheet.JobName ?? "null";
                    submittedToCreditModel.AccountName = jobSheet.AccountName ?? "null";
                    submittedToCreditModel.TotalPurchases = jobSheet.TotalPurchases;
                    var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Submitted to Credit", submittedToCreditModel);
                    string newEmailText = emailTemplate.TemplateBody;
                    string newSubjectText = emailTemplate.TemplateSubject;

                    string toEmail = "";
                    string toCC = "";

                    if (ConfigurationManager.AppSettings["isTestMode"] != null)
                    {
                        if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                        {
                            if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                            {
                                toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                            }
                        }
                        else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                        {
                            //if (users.Count != 0)
                            //{
                            //    toEmail = users[0].Email;
                            //}

                            if (pmName.Count != 0)
                            {
                                toCC = pmName[0].Email;
                            }

                            if (creditAdmin.Count != 0)
                            {
                                foreach (var ca in creditAdmin)
                                {
                                    toEmail = toEmail + "," + ca.Email;
                                }
                            }

                            if (creditManager.Count != 0)
                            {
                                foreach (var cm in creditManager)
                                {
                                    toCC = toCC + "," + cm.Email;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //email to all credit users
                   // List<UserViewModel> users = new List<UserViewModel>();

                    var creditRoles = new int[] { 5, 7 };
                    int? creditrep = jobSheet.CreditRep;
                    var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                    var creditAdmin = connection.User.Where(p => p.RoleId == 7).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var users = connection.User.Where(p => p.UserId == creditrep).Select(p => new { p.SMSN.Name, p.Email }).ToList();                    

                    var submittedToCreditModel = new SubmittedToCreditModel();

                    submittedToCreditModel.JobSheetId = jobSheet.JobSheetId;
                    submittedToCreditModel.JobSheetName = jobSheet.JobName ?? "null";
                    submittedToCreditModel.UserName = users[0].Name ?? "null";
                    submittedToCreditModel.JobName = jobSheet.JobName ?? "null";
                    submittedToCreditModel.AccountName = jobSheet.AccountName ?? "null";
                    submittedToCreditModel.TotalPurchases = jobSheet.TotalPurchases;
                    var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Submitted to Credit", submittedToCreditModel);
                    string newEmailText = emailTemplate.TemplateBody;
                    string newSubjectText = emailTemplate.TemplateSubject;

                    string toEmail = "";
                    string toCC = "";

                    if (ConfigurationManager.AppSettings["isTestMode"] != null)
                    {
                        if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                        {
                            if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                            {
                                toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                            }
                        }
                        else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                        {
                            if (users.Count != 0)
                            {
                                toEmail = users[0].Email;
                            }

                            if (pmName.Count != 0)
                            {
                                toCC = pmName[0].Email;
                            }
                        }
                    }

                    PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText);

                    
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class SubmittedToCreditEmail method");

            }

        }
        public void CreateUpdateJobEmail(int jobSheetId, string status)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            try
            {
                connection = new CommPmApplicationConnection();
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == jobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var emailTemplate = new EmailTemplates();

                var projectCreatedModel = new ProjectCreatedModel();
                    projectCreatedModel.JobSheetId = jobSheet.JobSheetId;
                    projectCreatedModel.JobSheetName = jobSheet.JobName ?? "null";
                    projectCreatedModel.ProjectManger = pmName[0].Name ?? "null";
                    projectCreatedModel.JobName = jobSheet.JobName ?? "null";
                    projectCreatedModel.AccountName = jobSheet.AccountName ?? "null";
                    projectCreatedModel.TotalPurchases = jobSheet.TotalPurchases;
                  

                if(status == "new")
                     emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Project Created", projectCreatedModel);
                else if (status == "update")
                     emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Project Update", projectCreatedModel);

                string newEmailText = emailTemplate.TemplateBody;
                    string newSubjectText = emailTemplate.TemplateSubject;

                    string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {

                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        if (salesRep.Count != 0)
                            toCC = salesRep[0].Email;
                        if (salesRepOut.Count != 0)
                        {
                            if(toCC.Contains(salesRepOut[0].Email))
                            {
                                //toCC = toCC + "," + salesRepOut[0].Email;
                            }
                            else
                            {
                                toCC = toCC + "," + salesRepOut[0].Email;
                            }
                        }
                            
                    }
                }
                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateUpdateJobEmail method");
            }

        }
        public void VoltageVerificationEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {
                int pmId = connection.JobSheet.Where(p => p.JobSheetId == JobSheetId).Select(p => p.ProjectManager).Single();

                var pmName = connection.User.Where(p => p.UserId == pmId).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                int mangerid = connection.ManagerPM.Where(p => p.PMId == pmId).Select(p => p.ManagerId).Single();
                
                var MangerName = connection.User.Where(p => p.UserId == mangerid).Select(p => new { p.SMSN.Name, p.Email }).ToList();


                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
               
                var voltageVerificationModel = new VoltageVerificationModel();

                voltageVerificationModel.JobSheetId = jobSheet.JobSheetId;
                voltageVerificationModel.JobSheetName = jobSheet.JobName ?? "null";
                voltageVerificationModel.ProjectManger = pmName[0].Name ?? "null";
                voltageVerificationModel.JobName = jobSheet.JobName ?? "null";
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Voltage Verification", voltageVerificationModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;

                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        if(pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        if(MangerName.Count != 0)
                            toCC = MangerName[0].Email;
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class VoltageVerificationEmail method");
            }
            

        }
        public void PMchangeEmail(JobSheet oldrec, JobSheetViewModel newrec)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var emailTemplateService = new EmailTemplateService(connection);
                var jobSheetService = new ProjectsService(connection);
                var oldrow = connection.User.SingleOrDefault(r => r.UserId == oldrec.ProjectManager);
                var newrow = connection.User.SingleOrDefault(r => r.UserId == newrec.ProjectManager);

                var theModel = new PMChangeModel();

                theModel.JobSheetId = oldrec.JobSheetId;
                theModel.JobSheetName = oldrec.JobSheetName ?? "null";
                theModel.JobName = oldrec.JobName ?? "null";
                theModel.CustomerName = oldrec.AccountName != null ? oldrec.AccountName : "null";
                theModel.CustNo = oldrec.AccountNumber != null ? oldrec.AccountNumber : "null";
                theModel.OldName = oldrow.SMSN.Name ?? "null";
                theModel.OldEmail = oldrow.SMSN.Email ?? "null";
                theModel.NewName = newrow.SMSN.Name ?? "null";
                theModel.NewEmail = newrow.SMSN.Email ?? "null";


                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("PM Change", theModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                
                string toEmail = "ecommerce@mingledorffs.com";

                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        bool oldpm = jobSheetService.getPM(oldrow.SMSN.Email);

                        bool newpm = jobSheetService.getPM(newrow.SMSN.Email);

                        if (oldpm)
                        {
                            toEmail = oldrow.SMSN.Email;
                        }

                        if (newpm)
                        {
                            toEmail = newrow.SMSN.Email;
                        }

                        if (oldpm && newpm)
                        {
                            toEmail = oldrow.SMSN.Email + "," + newrow.SMSN.Email;
                        }

                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class EmailPMchange method");
            }
        }
        public void CreditApprovedEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {                

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditApprovedModel = new CreditApprovedModel();

                creditApprovedModel.JobSheetId = jobSheet.JobSheetId;
                creditApprovedModel.JobSheetName = jobSheet.JobName ?? "null";
                creditApprovedModel.ProjectManger = pmName[0].Name ?? "null";
                creditApprovedModel.JobName = jobSheet.JobName ?? "null";
                creditApprovedModel.AccountName = jobSheet.AccountName ?? "null";
                creditApprovedModel.TotalPurchases = jobSheet.TotalPurchases;
                creditApprovedModel.CreditNotes = jobSheet.CreditNotes;
               
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Approved by Credit", creditApprovedModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;

                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {

                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        //if (salesRep.Count != 0)
                        //    toCC = salesRep[0].Email;
                        //if (salesRepOut.Count != 0)
                        //{
                        //    if (toCC.Contains(salesRepOut[0].Email))
                        //    {
                        //        //toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //    else
                        //    {
                        //        toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //}
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);

                
                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditApprovedEmail method");
            }

        }
        public void CreditRejectEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {                

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var creditRejectModel = new CreditRejectModel();

                creditRejectModel.JobSheetId = jobSheet.JobSheetId;
                creditRejectModel.JobSheetName = jobSheet.JobName ?? "null";
                creditRejectModel.ProjectManger = pmName[0].Name ?? "null";
                creditRejectModel.JobName = jobSheet.JobName ?? "null";
                creditRejectModel.AccountName = jobSheet.AccountName ?? "null";
                creditRejectModel.TotalPurchases = jobSheet.TotalPurchases;
                creditRejectModel.CreditNotes = jobSheet.CreditNotes;
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Rejected by Credit", creditRejectModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        if(pmName.Count != 0)
                          toEmail = pmName[0].Email;
                        //if (salesRep.Count != 0)
                        //    toCC = salesRep[0].Email;
                        //if (salesRepOut.Count != 0)
                        //{
                        //    if (toCC.Contains(salesRepOut[0].Email))
                        //    {
                        //        //toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //    else
                        //    {
                        //        toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //}
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);

                
                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditRejectEmail method");
            }

        }

        public void CreditPendingEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditAdmin = connection.User.Where(p => p.RoleId == 7).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditManager = connection.User.Where(p => p.RoleId == 6).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditRejectModel = new CreditRejectModel();

                creditRejectModel.JobSheetId = jobSheet.JobSheetId;
                creditRejectModel.JobSheetName = jobSheet.JobName ?? "null";
                creditRejectModel.ProjectManger = "null";
                creditRejectModel.JobName = jobSheet.JobName ?? "null";
                creditRejectModel.AccountName = jobSheet.AccountName ?? "null";
                creditRejectModel.TotalPurchases = jobSheet.TotalPurchases;
                //if (jobSheet.TotalPurchases > 15000)
                //{
                //    creditRejectModel.CreditNotes = jobSheet.CreditNotes + " - Total Purchases greater than 150000";
                //}
                //else
                //{
                    creditRejectModel.CreditNotes = jobSheet.CreditNotes;
                //}
                
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Pending by Credit", creditRejectModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        //if (jobSheet.TotalPurchases > 15000)
                        //{
                        //    if (creditManager.Count != 0)
                        //    {
                        //        foreach (var cm in creditManager)
                        //        {
                        //            toEmail = toEmail + "," + cm.Email;
                        //        }
                        //    }

                        //    if (pmName.Count != 0)
                        //    {
                        //        toCC = pmName[0].Email;
                        //    }

                        //    if (creditAdmin.Count != 0)
                        //    {
                        //        foreach (var ca in creditAdmin)
                        //        {
                        //            toCC = toCC + "," + ca.Email;
                        //        }
                        //    }
                        //}
                        //else
                        //{
                            if (pmName.Count != 0)
                            {
                                toCC = pmName[0].Email;
                            }

                            //if (salesRep.Count != 0)
                            //{
                            //    toCC = toCC + "," + salesRep[0].Email;
                            //}

                            //if (salesRepOut.Count != 0)
                            //{
                            //    if (toCC.Contains(salesRepOut[0].Email))
                            //    {
                            //        //toCC = toCC + "," + salesRepOut[0].Email;
                            //    }
                            //    else
                            //    {
                            //        toCC = toCC + "," + salesRepOut[0].Email;
                            //    }
                            //}

                            if (creditAdmin.Count != 0)
                            {
                                foreach (var ca in creditAdmin)
                                {
                                    toEmail = toEmail + "," + ca.Email;
                                }
                            }

                            //if (creditManager.Count != 0)
                            //{
                            //    foreach (var cm in creditManager)
                            //    {
                            //        toCC = toCC + "," + cm.Email;
                            //    }
                            //}
                        //}                      
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);


                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditRejectEmail method");
            }

        }

        public void CreditIncompleteEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditManager = connection.User.Where(p => p.RoleId == 6).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var CreditApprovedModel = new CreditRejectModel();

                CreditApprovedModel.JobSheetId = jobSheet.JobSheetId;
                CreditApprovedModel.JobSheetName = jobSheet.JobName ?? "null";
                CreditApprovedModel.ProjectManger = pmName[0].Name ?? "null";
                CreditApprovedModel.JobName = jobSheet.JobName ?? "null";
                CreditApprovedModel.AccountName = jobSheet.AccountName ?? "null";
                CreditApprovedModel.TotalPurchases = jobSheet.TotalPurchases;
                CreditApprovedModel.CreditNotes = jobSheet.CreditNotes;
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Incomplete Documentation by Credit", CreditApprovedModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        //if (salesRep.Count != 0)
                        //    toCC = salesRep[0].Email;
                        //if (salesRepOut.Count != 0)
                        //{
                        //    if (toCC.Contains(salesRepOut[0].Email))
                        //    {
                        //        //toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //    else
                        //    {
                        //        toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //}

                        //if (creditManager.Count != 0)
                        //{
                        //    foreach (var cm in creditManager)
                        //    {
                        //        toEmail = toEmail + "," + cm.Email;
                        //    }
                        //}
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);


                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditIncompleteEmail method");
            }

        }

        public void CreditPaymentEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditManager = connection.User.Where(p => p.RoleId == 6).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var CreditApprovedModel = new CreditRejectModel();

                CreditApprovedModel.JobSheetId = jobSheet.JobSheetId;
                CreditApprovedModel.JobSheetName = jobSheet.JobName ?? "null";
                CreditApprovedModel.ProjectManger = pmName[0].Name ?? "null";
                CreditApprovedModel.JobName = jobSheet.JobName ?? "null";
                CreditApprovedModel.AccountName = jobSheet.AccountName ?? "null";
                CreditApprovedModel.TotalPurchases = jobSheet.TotalPurchases;
                CreditApprovedModel.CreditNotes = jobSheet.CreditNotes;
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Payment Issue by Credit", CreditApprovedModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        //if (salesRep.Count != 0)
                        //    toCC = salesRep[0].Email;
                        //if (salesRepOut.Count != 0)
                        //{
                        //    if (toCC.Contains(salesRepOut[0].Email))
                        //    {
                        //        //toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //    else
                        //    {
                        //        toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //}

                        //if (creditManager.Count != 0)
                        //{
                        //    foreach (var cm in creditManager)
                        //    {
                        //        toEmail = toEmail + "," + cm.Email;
                        //    }
                        //}
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);


                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditPaymentEmail method");
            }

        }

        public void CreditCompletedEmail(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();

            var emailTemplateService = new EmailTemplateService(connection);
            try
            {

                //send email to this job PM
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var creditManager = connection.User.Where(p => p.RoleId == 6).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var CreditApprovedModel = new CreditRejectModel();

                CreditApprovedModel.JobSheetId = jobSheet.JobSheetId;
                CreditApprovedModel.JobSheetName = jobSheet.JobName ?? "null";
                CreditApprovedModel.ProjectManger = pmName[0].Name ?? "null";
                CreditApprovedModel.JobName = jobSheet.JobName ?? "null";
                CreditApprovedModel.AccountName = jobSheet.AccountName ?? "null";
                CreditApprovedModel.TotalPurchases = jobSheet.TotalPurchases;
                CreditApprovedModel.CreditNotes = jobSheet.CreditNotes;
                var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Completed by Credit", CreditApprovedModel);
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;
                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        //if (salesRep.Count != 0)
                        //    toCC = salesRep[0].Email;
                        //if (salesRepOut.Count != 0)
                        //{
                        //    if (toCC.Contains(salesRepOut[0].Email))
                        //    {
                        //        //toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //    else
                        //    {
                        //        toCC = toCC + "," + salesRepOut[0].Email;
                        //    }
                        //}

                        //if (creditManager.Count != 0)
                        //{
                        //    foreach (var cm in creditManager)
                        //    {
                        //        toEmail = toEmail + "," + cm.Email;
                        //    }
                        //}
                    }
                }

                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);


                // }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured EmailService class CreditCompletedEmail method");
            }

        }

        public void ExcelExportEmail(string emailAddress, string worksheetName, string UserName, string filename)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            //System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(worksheetName);
            List<System.Net.Mail.Attachment> attachments = new List<System.Net.Mail.Attachment>();
            //attachments.Add(attachment);
            string toEmail = string.Empty;
            
            AdhocReportModel adhocReportModel = new AdhocReportModel();

            adhocReportModel.UserName = UserName;
            adhocReportModel.Filename = filename;
            adhocReportModel.url = worksheetName;
           
            var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Ad hoc Report", adhocReportModel);
            string newEmailText = emailTemplate.TemplateBody;
            string newSubjectText = emailTemplate.TemplateSubject;

            if (ConfigurationManager.AppSettings["isTestMode"] != null)
            {
                if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                {
                    if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                    {
                        toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                    }
                }
                else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                {
                    toEmail = emailAddress;
                }
            }

            PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, null, attachments);

            //attachment.Dispose();
            //attachments.Clear();
            GC.Collect();
        }
        public void PromiseDateVerificationEmail(int jobSheetId)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            try
            {
                connection = new CommPmApplicationConnection();
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == jobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
               
                var emailTemplate = new EmailTemplates();

                var promiseDateVerificationModel = new PromiseDateVerificationModel();
                promiseDateVerificationModel.JobSheetId = jobSheet.JobSheetId;
                promiseDateVerificationModel.JobSheetName = jobSheet.JobName ?? "null";
                promiseDateVerificationModel.ProjectManger = pmName[0].Name ?? "null";
                promiseDateVerificationModel.JobName = jobSheet.JobName ?? "null";
                promiseDateVerificationModel.AccountName = jobSheet.AccountName ?? "null";
                promiseDateVerificationModel.CustomerRequestedDeliveryDate = jobSheet.CustomerRequestedDeliveryDate.ToString() ?? "null";
                promiseDateVerificationModel.VendorPromisedShipDate = jobSheet.VendorPromisedShipDate.ToString() ?? "null";

                emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Promise Date Verification ", promiseDateVerificationModel);
                
                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;

                string toEmail = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {

                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                    }
                }
                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, null);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateUpdateJobEmail method");
            }

        }

        public string FeedbackEmail(FormCollection form, HttpPostedFileBase file)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            var emailTo = System.Configuration.ConfigurationManager.AppSettings["FeedbackEmails"];

            var feedback = new FeedbackModel();
            feedback.Feedback = form["feedback"].ToString();
            feedback.FirstName = form["firstname"].ToString();
            feedback.LastName = form["lastname"].ToString();
            feedback.Topic = form["topic"].ToString();
            feedback.SubTopic = form["subtopic"].ToString();
            feedback.URL = form["url"].ToString();
            feedback.Browser = form["browser"].ToString();
            

            var emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Feedback", feedback);

            var emailText = emailTemplate.TemplateBody;
            var emailSubject = emailTemplate.TemplateSubject;

            var attachments = new List<Attachment>();
            if (file != null && file.ContentLength > 0)
            {
                attachments.Add(new Attachment(file.InputStream, file.FileName));
                PM_Emailer.Email.SendMail(emailText, emailTo, emailSubject, null, attachments,form["email"]);
            }
            else
            {
                PM_Emailer.Email.SendMail(emailText, emailTo, emailSubject,null,null, form["email"]);
            }

            return "Done";
        }
        public void CreateNewUserEmail(int UserId)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            try
            {
                connection = new CommPmApplicationConnection();
                var result = connection.User.Select(user => new UserViewModel
                {
                    UserId = user.UserId,
                    RoleId = user.RoleId,
                    IsActive = user.IsActive,
                    Email = user.Email,
                    SlsRep = user.SlsRep,
                    Name = user.SMSN.Name,
                    UserRole = new UsersUserRoleViewModel()
                    {
                        RoleId = user.UserRoles.RoleId,
                        RoleName = user.UserRoles.RoleName
                    },
                    SMSN = new SMSNViewModel()
                    {
                        SlsRep = user.SMSN.SlsRep,
                        Name = user.SMSN.Name,
                        Email = user.SMSN.Email
                    }
                }).Where( x => x.UserId == UserId ).ToList().FirstOrDefault();

              
                var emailTemplate = new EmailTemplates();

                var createNewUserModel = new CreateNewUserModel();
                createNewUserModel.UserEmail = result.Email;
                createNewUserModel.UserName = result.Name ?? "";
                createNewUserModel.RoleName = result.UserRole.RoleName ?? "null";


                emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Create New User", createNewUserModel);

                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;

                string toEmail = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {
                        toEmail = result.Email;
                    }
                }
                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, null);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateNewUserEmail method");
            }

        }
        public void UpdateJobEmail(JobSheet oldrec, JobSheetViewModel jobSheet)
        {
            var emailTemplateService = new EmailTemplateService(connection);

            string emailText = "";
            connection = new CommPmApplicationConnection();
            try
            {
                List<string> JobsheetChang = new List<string>();
                bool result = getJobSheetChange(oldrec, jobSheet, JobsheetChang);
                if (result == true)
                {
                    var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                    var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                    var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var emailTemplate = new EmailTemplates();

                    var projectCreatedModel = new ProjectCreatedModel();
                    projectCreatedModel.JobSheetId = jobSheet.JobSheetId;
                    projectCreatedModel.JobSheetName = jobSheet.JobName ?? "null";
                    projectCreatedModel.ProjectManger = pmName[0].Name ?? "null";
                    projectCreatedModel.JobName = jobSheet.JobName ?? "null";
                   

                    foreach (string change in JobsheetChang)
                    {
                        emailText += "<tr>";
                        emailText += "<td>";
                        emailText += "<ul>";
                        emailText += "<li>";
                        emailText += change.ToString() + "";
                        emailText += "</li>";
                        emailText += "</ul>";
                        emailText += "</td>";
                        emailText += "</tr>";

                    }
                    emailText += "</table>";

                    projectCreatedModel.changeDescription = HttpUtility.HtmlDecode(emailText);
                    emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Project Update", projectCreatedModel);

                    string newEmailText = emailTemplate.TemplateBody;
                    string newSubjectText = emailTemplate.TemplateSubject;

                    string toEmail = "";
                    string toCC = "";
                    if (ConfigurationManager.AppSettings["isTestMode"] != null)
                    {
                        if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                        {
                            if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                            {
                                toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                            }
                        }
                        else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                        {

                            if (pmName.Count != 0)
                                toEmail = pmName[0].Email;
                            if (salesRep.Count != 0)
                                toCC = salesRep[0].Email;
                            if (salesRepOut.Count != 0)
                            {
                                if (toCC.Contains(salesRepOut[0].Email))
                                {
                                    //toCC = toCC + "," + salesRepOut[0].Email;
                                }
                                else
                                {
                                    toCC = toCC + "," + salesRepOut[0].Email;
                                }
                            }
                        }
                    }
                    PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateUpdateJobEmail method");
            }

        }

        public void UpdateTaxExemptionToCreditRep(JobSheet oldrec, JobSheetViewModel jobSheet, string type)
        {
            var emailTemplateService = new EmailTemplateService(connection);

            string emailText = "";
            connection = new CommPmApplicationConnection();
            try
            {
                List<string> JobsheetChang = new List<string>();
                bool result;

                if(type == "Update")
                {
                    result = getJobSheetChange(oldrec, jobSheet, JobsheetChang);
                }
                else
                {
                    result = true;
                }          


                if (result == true)
                {
                    var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var creditRep = connection.User.Where(p => p.RoleId == 5).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                    var emailTemplate = new EmailTemplates();

                    var projectCreatedModel = new ProjectCreatedModel();
                    projectCreatedModel.JobSheetId = jobSheet.JobSheetId;
                    projectCreatedModel.JobSheetName = jobSheet.JobName ?? "null";
                    projectCreatedModel.ProjectManger = pmName[0].Name ?? "null";
                    projectCreatedModel.JobName = jobSheet.JobName ?? "null";

                    if(JobsheetChang.Count > 0)
                    {
                        foreach (string change in JobsheetChang)
                        {
                            emailText += "<tr>";
                            emailText += "<td>";
                            emailText += "<ul>";
                            emailText += "<li>";
                            emailText += change.ToString() + "";
                            emailText += "</li>";
                            emailText += "</ul>";
                            emailText += "</td>";
                            emailText += "</tr>";

                        }
                    }
                    else
                    {
                        emailText += "<tr>";
                        emailText += "<td>";
                        emailText += "<ul>";
                        emailText += "<li>";
                        emailText += "Tax Exemption changed to Yes";
                        emailText += "</li>";
                        emailText += "</ul>";
                        emailText += "</td>";
                        emailText += "</tr>";
                    }
                    
                    emailText += "</table>";

                    projectCreatedModel.changeDescription = HttpUtility.HtmlDecode(emailText);
                    emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Tax Exemption to Credit Rep", projectCreatedModel);

                    string newEmailText = emailTemplate.TemplateBody;
                    string newSubjectText = emailTemplate.TemplateSubject;

                    string toEmail = "";
                    string toCC = "";
                    if (ConfigurationManager.AppSettings["isTestMode"] != null)
                    {
                        if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                        {
                            if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                            {
                                toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                            }
                        }
                        else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                        {
                            if (creditRep.Count != 0)
                            {
                                foreach (var cr in creditRep)
                                {
                                    toCC = toCC + "," + cr.Email;
                                }
                            }
                        }
                    }
                    PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateUpdateJobEmail method");
            }

        }

        private bool getJobSheetChange(JobSheet oldrec, JobSheetViewModel jobSheet, List<string> JobsheetChang)
        {
            var jobSheetService = new ProjectsService(connection);
            bool isUpdate = false;

            if (oldrec.JobSheetName != jobSheet.JobSheetName )
            {
                string Description = "Job Sheet Name has changed " + " " + oldrec.JobSheetName + " to " + " " + jobSheet.JobSheetName;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.CompanyName != jobSheet.CompanyName)
            {
                string Description = "Company has changed " + " " + oldrec.CompanyName + " to " + " " + jobSheet.CompanyName;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.SetUpDate != jobSheet.SetUpDate)
            {
                string Description = "Set Up Date has changed " + " " + oldrec.SetUpDate + " to " + " " + jobSheet.SetUpDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.ContractDate != jobSheet.ContractDate)
            {
                string Description = "Contract Date has changed " + " " + oldrec.ContractDate + " to " + " " + jobSheet.ContractDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.AccountName != jobSheet.AccountName)
            {
                string Description = "Account Name has changed " + " " + oldrec.AccountName + " to " + " " + jobSheet.AccountName;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobName != jobSheet.JobName)
            {
                string Description = "Job Name has changed " + " " + oldrec.JobName + " to " + " " + jobSheet.JobName;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.CountyOfJob != jobSheet.CountyOfJob)
            {
                string Description = "County Of Job has changed " + " " + oldrec.CountyOfJob + " to " + " " + jobSheet.CountyOfJob;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobStreeAddress != jobSheet.JobStreeAddress)
            {
                string Description = "Job Stree Address has changed " + " " + oldrec.JobStreeAddress + " to " + " " + jobSheet.JobStreeAddress;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobState != jobSheet.JobState)
            {
                string Description = "Job State has changed " + " " + oldrec.JobState + " to " + " " + jobSheet.JobState;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobCity != jobSheet.JobCity)
            {
                string Description = "Job City has changed " + " " + oldrec.JobCity + " to " + " " + jobSheet.JobCity;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobCity != jobSheet.JobCity)
            {
                string Description = "Job City has changed " + " " + oldrec.JobCity + " to " + " " + jobSheet.JobCity;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.JobZip != jobSheet.JobZip)
            {
                string Description = "Job Zip Code has changed " + " " + oldrec.JobZip + " to " + " " + jobSheet.JobZip;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            
            if (oldrec.TMCommisionPosition1 != jobSheet.TMCommisionPosition1)
            {
                string Description = "OutSide Sales Representative has changed " + " " + oldrec.TMCommisionPosition1 + " to " + " " + jobSheet.TMCommisionPosition1;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.GeneralContractor != jobSheet.GeneralContractor) && (!string.IsNullOrEmpty(jobSheet.GeneralContractor)))
            {
                string Description = "General Contractor has changed " + " " + oldrec.GeneralContractor + " to " + " " + jobSheet.GeneralContractor;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.GCMailingAddress != jobSheet.GCMailingAddress) && (!string.IsNullOrEmpty(jobSheet.GCMailingAddress)))
            {
                string Description = "General Contractor Address has changed " + " " + oldrec.GCMailingAddress + " to " + " " + jobSheet.GCMailingAddress;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.GCPhoneNumber != jobSheet.GCPhoneNumber) && (!string.IsNullOrEmpty(jobSheet.GCPhoneNumber)))
            {
                string Description = "General Contractor Phone Number has changed " + " " + oldrec.GCPhoneNumber + " to " + " " + jobSheet.GCPhoneNumber;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.GCState != jobSheet.GCState) && (!string.IsNullOrEmpty(jobSheet.GCState)))
            {
                string Description = "General Contractor State has changed " + " " + oldrec.GCState + " to " + " " + jobSheet.GCState;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.GCCity != jobSheet.GCCity) &&  (!string.IsNullOrEmpty(jobSheet.GCCity)))
            {
                string Description = "General Contractor City has changed " + " " + oldrec.GCCity + " to " + " " + jobSheet.GCCity;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.GCZip != jobSheet.GCZip )
            {
                string Description = "General Contractor Zip Code has changed " + " " + oldrec.GCZip + " to " + " " + jobSheet.GCZip;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.OwnerName != jobSheet.OwnerName) && (!string.IsNullOrEmpty(jobSheet.OwnerName)))
            {
                string Description = "Owner Name has changed " + " " + oldrec.OwnerName + " to " + " " + jobSheet.OwnerName;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.OMailingAddress != jobSheet.OMailingAddress) && (!string.IsNullOrEmpty(jobSheet.OMailingAddress)))
            {
                string Description = "Owner Mailing Address has changed " + " " + oldrec.OMailingAddress + " to " + " " + jobSheet.OMailingAddress;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.OPhoneNumber != jobSheet.OPhoneNumber) && (!string.IsNullOrEmpty(jobSheet.OPhoneNumber)))
            {
                string Description = "Owner Phone Number has changed " + " " + oldrec.OPhoneNumber + " to " + " " + jobSheet.OPhoneNumber;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.OState != jobSheet.OState) && (!string.IsNullOrEmpty(jobSheet.OState)))
            {
                string Description = "Owner State has changed " + " " + oldrec.OState + " to " + " " + jobSheet.OState;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.OCity != jobSheet.OCity) && (!string.IsNullOrEmpty(jobSheet.OCity)))
            {
                string Description = "Owner City has changed " + " " + oldrec.OCity + " to " + " " + jobSheet.OCity;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.OZip != jobSheet.OZip)
            {
                string Description = "Owner Zip Code has changed " + " " + oldrec.OZip + " to " + " " + jobSheet.OZip;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.BondedJob != jobSheet.BondedJob) && (!string.IsNullOrEmpty(jobSheet.BondedJob)))
            {
                string Description = "Bonded Job has changed " + " " + oldrec.BondedJob + " to " + " " + jobSheet.BondedJob;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.BondInformation != jobSheet.BondInformation) && (!string.IsNullOrEmpty(jobSheet.BondInformation)))
            {
                string Description = "Bonded Information changed " + " " + oldrec.BondInformation + " to " + " " + jobSheet.BondInformation;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }

            if ((oldrec.TaxExemption != jobSheet.TaxExemption) && (!string.IsNullOrEmpty(jobSheet.TaxExemption)))
            {
                string Description = "Tax Exemption changed " + " " + oldrec.TaxExemption + " to " + " " + jobSheet.TaxExemption;
                JobsheetChang.Add(Description);                
                if(jobSheet.TaxExemption == "Yes")
                {
                   string taxDescription = "Tax Exemption changed to Yes";
                   JobsheetChang.Add(taxDescription);

                }
                isUpdate = true;
            }
            if (oldrec.AnticipatedFirstDelivery != jobSheet.AnticipatedFirstDelivery)
            {
                string Description = "Anticipated First Delivery changed " + " " + oldrec.AnticipatedFirstDelivery + " to " + " " + jobSheet.AnticipatedFirstDelivery;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.AnticipatedJobClose != jobSheet.AnticipatedJobClose)
            {
                string Description = "Anticipated Job Close Date changed " + " " + oldrec.AnticipatedJobClose + " to " + " " + jobSheet.AnticipatedJobClose;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.AnticipatedJobClose != jobSheet.AnticipatedJobClose)
            {
                string Description = "Anticipated Job Close Date changed " + " " + oldrec.AnticipatedJobClose + " to " + " " + jobSheet.AnticipatedJobClose;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.VendorPromisedShipDate != jobSheet.VendorPromisedShipDate)
            {
                string Description = "Vendor Promised ShipDate Date changed " + " " + oldrec.VendorPromisedShipDate + " to " + " " + jobSheet.VendorPromisedShipDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.VendorPromisedShipDate != jobSheet.VendorPromisedShipDate)
            {
                string Description = "Vendor Promised ShipDate Date changed " + " " + oldrec.VendorPromisedShipDate + " to " + " " + jobSheet.VendorPromisedShipDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.CustomerRequestedDeliveryDate != jobSheet.CustomerRequestedDeliveryDate)
            {
                string Description = "Customer Requested Delivery Date changed " + " " + oldrec.CustomerRequestedDeliveryDate + " to " + " " + jobSheet.CustomerRequestedDeliveryDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.JobLine != jobSheet.JobLine) && (!string.IsNullOrEmpty(jobSheet.JobLine)))
            {
                string Description = "Job Line changed " + " " + oldrec.JobLine + " to " + " " + jobSheet.JobLine;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.PrelienAmount != jobSheet.PrelienAmount)
            {
                string Description = "Prelien Amount changed " + " " + oldrec.PrelienAmount + " to " + " " + jobSheet.PrelienAmount;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.Prelien != jobSheet.Prelien) && (!string.IsNullOrEmpty(jobSheet.Prelien)))
            {
                string Description = "Prelien changed " + " " + oldrec.Prelien + " to " + " " + jobSheet.Prelien;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.PrelienCompany != jobSheet.PrelienCompany) && (!string.IsNullOrEmpty(jobSheet.PrelienCompany)))
            {
                string Description = "Prelien Company changed " + " " + oldrec.PrelienCompany + " to " + " " + jobSheet.PrelienCompany;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.PrelienDate != jobSheet.PrelienDate)
            {
                string Description = "Prelien Date changed " + " " + oldrec.PrelienDate + " to " + " " + jobSheet.PrelienDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if ((oldrec.Notes != jobSheet.Notes) && (!string.IsNullOrEmpty(jobSheet.Notes)))
            {
                string Description = "Notes changed " + " " + oldrec.Notes + " to " + " " + jobSheet.Notes;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.SubmittedDate != jobSheet.SubmittedDate)
            {
                string Description = "Submitted Date changed " + " " + oldrec.SubmittedDate + " to " + " " + jobSheet.SubmittedDate;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.Estimator != jobSheet.Estimator)
            {
                string oldrep = jobSheetService.getSalesRep(oldrec.Estimator);
                string newrep = jobSheetService.getSalesRep(jobSheet.Estimator);
                string Description = "CCG Estimator changed " + " " + oldrep + " to " + " " + newrep;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.Material != jobSheet.Material)
            {
                string Description = "CCG Material changed " + " " + oldrec.Material + " to " + " " + jobSheet.Material;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.Labor != jobSheet.Labor)
            {
                string Description = "CCG Labor changed " + " " + oldrec.Labor + " to " + " " + jobSheet.Labor;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.Returnage != jobSheet.Returnage)
            {
                string Description = "CCG Returnage changed " + " " + oldrec.Returnage + " to " + " " + jobSheet.Returnage;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }
            if (oldrec.Duration != jobSheet.Duration)
            {
                string Description = "CCG Duration changed " + " " + oldrec.Duration + " to " + " " + jobSheet.Duration;
                JobsheetChang.Add(Description);
                isUpdate = true;
            }

            return isUpdate;
          
        }
        public void UpdateJobStepEmail(int JobSheetId, bool Checked, int jobStepId)
        {
            var emailTemplateService = new EmailTemplateService(connection);
            try
            {
                connection = new CommPmApplicationConnection();
                var jobSheet = connection.JobSheet.SingleOrDefault(r => r.JobSheetId == JobSheetId && r.IsActive == true);
                var pmName = connection.User.Where(p => p.UserId == jobSheet.ProjectManager).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRep = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition1Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();
                var salesRepOut = connection.User.Where(p => p.UserId == jobSheet.TMCommissionPosition2Id).Select(p => new { p.SMSN.Name, p.Email }).ToList();

                var jobStep = connection.JobSteps.SingleOrDefault(r => r.JobStepId == jobStepId && r.IsActive == true);
                string emailText = string.Empty;
                string stepName = string.Empty;
                var emailTemplate = new EmailTemplates();

                var projectCreatedModel = new ProjectCreatedModel();
                projectCreatedModel.JobSheetId = jobSheet.JobSheetId;
                projectCreatedModel.JobSheetName = jobSheet.JobName ?? "null";
                projectCreatedModel.ProjectManger = pmName[0].Name ?? "null";
                projectCreatedModel.JobName = jobSheet.JobName ?? "null";

                if(jobStepId == 2)
                {
                    stepName = "Submitted to Contractor";
                }
                else if (jobStepId == 5)
                {
                    stepName = "Returned from Contractor";
                }
                else if (jobStepId == 8)
                {
                    stepName = "Returned By Credit";
                }
                else
                {
                    stepName = jobStep.StepName;
                }

                if (Checked == true)
                {
                    emailText += "<tr>";
                    emailText += "<td>";
                    emailText += "<ul>";
                    emailText += "<li>";
                    emailText += "Project status change to " + " " + stepName;
                    emailText += "</li>";
                    emailText += "</ul>";
                    emailText += "</td>";
                    emailText += "</tr>";
                    emailText += "</table>";
                }
                else
                {
                    emailText += "<tr>";
                    emailText += "<td>";
                    emailText += "<ul>";
                    emailText += "<li>";
                    emailText += "Project status is un-check from" + " " + stepName;
                    emailText += "</li>";
                    emailText += "</ul>";
                    emailText += "</td>";
                    emailText += "</tr>";
                    emailText += "</table>";
                  
                }
                projectCreatedModel.changeDescription = HttpUtility.HtmlDecode(emailText);
                emailTemplate = emailTemplateService.GetEmailTemplateWithModel("Project Update", projectCreatedModel);

                string newEmailText = emailTemplate.TemplateBody;
                string newSubjectText = emailTemplate.TemplateSubject;

                string toEmail = "";
                string toCC = "";
                if (ConfigurationManager.AppSettings["isTestMode"] != null)
                {
                    if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "yes")
                    {
                        if (ConfigurationManager.AppSettings["testModeEmail"] != null)
                        {
                            toEmail = ConfigurationManager.AppSettings["testModeEmail"].ToString();
                        }
                    }
                    else if (ConfigurationManager.AppSettings["isTestMode"].ToString() == "no")
                    {

                        if (pmName.Count != 0)
                            toEmail = pmName[0].Email;
                        if (salesRep.Count != 0)
                            toCC = salesRep[0].Email;
                        if (salesRepOut.Count != 0)
                        {
                            if (toCC.Contains(salesRepOut[0].Email))
                            {
                                //toCC = toCC + "," + salesRepOut[0].Email;
                            }
                            else
                            {
                                toCC = toCC + "," + salesRepOut[0].Email;
                            }
                        }
                    }
                }
                PM_Emailer.Email.SendMail(newEmailText, toEmail, newSubjectText, toCC);

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Emailservice class CreateUpdateJobEmail method");
            }

        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Web;
using System.Web.Mvc;
using NLog;
using PM_Website.Controllers;
using PM_Website.Models;
using RazorEngine;
using RazorEngine.Templating;

namespace PM_Website.Services
{
    public class EmailTemplateService
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        private CommPmApplicationConnection connection;

        public EmailTemplateService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IEnumerable<EmailTemplateGridViewModel> GetAllTemplates()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<EmailTemplateGridViewModel>();

            try
            {
                result = connection.EmailTemplates.Where(tmp=>tmp.Deleted == false).Select(emailTemplate => new EmailTemplateGridViewModel
                {
                    TemplateId = emailTemplate.Id,
                    TemplateName = emailTemplate.TemplateName,
                    TemplateView = emailTemplate.SubscriptionType.SubscriptionTypeName,
                    TemplateSubject = emailTemplate.TemplateSubject
                }).OrderBy(t=>t.TemplateName).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class GetAllTemplates method");
            }

            return result;
        }

        public List<string> GetSubscriptionPropertyNames(string SubscriptionName)
        {
            connection = new CommPmApplicationConnection();

            var subscriptionPropertyNames = new List<string>();

            try
            {
                var subscription = connection.SubscriptionType.First(sub => sub.SubscriptionTypeName == SubscriptionName);
                string subscriptionModelName;
                if (subscription.SubscriptionTypeModel != null)
                {
                    var subName = subscription.SubscriptionTypeModel;
                    subscriptionModelName = "PM_Website.Models." + subName;
                }
                else
                {
                    var subName = SubscriptionName.Replace(" ", "");
                    if (int.TryParse(subName.Substring(0, 1), out int n))
                    {
                        subName = "_" + subName;
                    }
                    subscriptionModelName = "PM_Website.Models." + subName + "Model";
                }
                
                var subscriptionModel = Type.GetType(subscriptionModelName);
                var subscriptionProperties = subscriptionModel.GetProperties();

                foreach (var prop in subscriptionProperties)
                {
                    var pType = prop.PropertyType.FullName ?? string.Empty;
                    if (!pType.Contains("System."))
                    {
                        
                    }
                    else
                    {
                        subscriptionPropertyNames.Add("Model." + prop.Name);
                    }
                }
            }
            catch
            {

            }

            return subscriptionPropertyNames;
        }

        public Type GetSubscriptionModel(string SubscriptionName)
        {
            connection = new CommPmApplicationConnection();
            var subscription = connection.SubscriptionType.First(sub => sub.SubscriptionTypeName == SubscriptionName);

            Type subscriptionModel;
            try
            {
                string subscriptionModelName;
                if (subscription.SubscriptionTypeModel != null)
                {
                    var subName = subscription.SubscriptionTypeModel;
                    subscriptionModelName = "PM_Website.Models." + subName;
                }
                else
                {
                    var subName = SubscriptionName.Replace(" ", "");
                    if (int.TryParse(subName.Substring(0, 1), out int n))
                    {
                        subName = "_" + subName;
                    }
                    subscriptionModelName = "PM_Website.Models." + subName + "Model";
                }

                subscriptionModel = Type.GetType(subscriptionModelName);
            }
            catch
            {
                subscriptionModel = null;
            }

            return subscriptionModel;
        }

        public EmailTemplateEditModel GetEmailTemplate(int templateId)
        {
            connection = new CommPmApplicationConnection();
            var result = new EmailTemplateEditModel();

            try
            {
                result = connection.EmailTemplates.Select(et=>new EmailTemplateEditModel
                {
                    Id = et.Id,
                    TemplateName = et.TemplateName,
                    Template = et.TemplateBody,
                    SubscriptionName = et.SubscriptionType.SubscriptionTypeName,
                    SubscriptionId = et.SubscriptionType.SubsciptionTypeId,
                    TemplateSubject = et.TemplateSubject
                }).First(et=>et.Id == templateId);

                result.ModelValues = GetSubscriptionPropertyNames(result.SubscriptionName);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class GetEmailTemplate method");
            }

            return result;
        }

        public string SaveEmailTemplate(FormCollection body)
        {
            connection = new CommPmApplicationConnection();

            try
            {
                int templateId = int.Parse(body["templateId"]);
                string templateBody = body["templateValue"];
                string templateSubject = body["templateSubject"];
                string templateName = body["templateName"];
                int subscriptionId = int.Parse(body["subscriptionType"]);

                var template = connection.EmailTemplates.First(et => et.Id == templateId);

                template.TemplateBody = templateBody;
                template.TemplateSubject = templateSubject;
                template.TemplateName = templateName;
                template.Subscription = subscriptionId;
                template.ModifiedDate = DateTime.Now;
                template.ModifiedUser = int.Parse(HttpContext.Current.Session["userId"].ToString());

                connection.EmailTemplates.Attach(template);
                connection.Entry(template).State = EntityState.Modified;
                connection.SaveChanges();

                return "Success";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    var s1 =
                        $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var s2 = $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
                    }
                }
                logger.Error(e, "Error occured in EmailTemplateService class SaveEmailTemplate method");
                return "Failed";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class SaveEmailTemplate method");
                return "Failed";
            }
        }

        public string DeleteEmailTemplate(FormCollection body)
        {
            connection = new CommPmApplicationConnection();

            try
            {
                int templateId = int.Parse(body["templateId"]);

                var template = connection.EmailTemplates.First(et => et.Id == templateId);

                template.Deleted = true;
                template.ModifiedDate = DateTime.Now;
                template.ModifiedUser = int.Parse(HttpContext.Current.Session["userId"].ToString());

                connection.EmailTemplates.Attach(template);
                connection.Entry(template).State = EntityState.Modified;
                connection.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class DeleteEmailTemplate method");
                return "Failed";
            }
        }

        public string CreateEmailTemplate(FormCollection body)
        {
            connection = new CommPmApplicationConnection();

            try
            {
                string templateBody = body["templateValue"];
                string templateName = body["templateName"];
                string templateSubject = body["templateSubject"];
                int subscription = int.Parse(body["subscriptionType"]);

                var template = new EmailTemplates
                {
                    TemplateName = templateName,
                    TemplateBody = templateBody,
                    Subscription = subscription,
                    TemplateSubject = templateSubject,
                    CreatedDate = DateTime.Now,
                    CreatedUser = int.Parse(HttpContext.Current.Session["userId"].ToString()),
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = int.Parse(HttpContext.Current.Session["userId"].ToString())
                };


                connection.EmailTemplates.Attach(template);
                connection.Entry(template).State = EntityState.Added;
                connection.SaveChanges();

                return "Success";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class CreateEmailTemplate method");
                return "Failed";
            }
        }

        public List<SubscriptionTypeViewModel> GetSubscriptionTypes(int? current = 0)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<SubscriptionTypeViewModel>();

            try
            {
                var emailTempaltes = connection.EmailTemplates.Where(et=>!et.Deleted).Select(et=>et.Subscription).Where(et=>et.Value != current).Distinct().ToList();

                result = connection.SubscriptionType.Select(subtype => new SubscriptionTypeViewModel
                {
                    SubscriptionTypeName = subtype.SubscriptionTypeName,
                    SubsciptionTypeId = subtype.SubsciptionTypeId,
                    SubscriptionTypeDescription = subtype.SubscriptionTypeDescription
                }).Where(sub=> !emailTempaltes.Contains(sub.SubsciptionTypeId)).ToList();
            }
            catch (Exception e)
            {
                logger.Error(e, "Error occured in EmailTemplateService class GetSubscriptionTypes method");
            }

            return result;
        }

        public EmailTemplates GetEmailTemplateWithModel(string SubscriptionType, object modelVals)
        {
            connection = new CommPmApplicationConnection();
            var template = connection.EmailTemplates.First(et => et.SubscriptionType.SubscriptionTypeName == SubscriptionType);
            var model = Type.GetType(modelVals.GetType().FullName);

            var modifiedMilliseconds = ((template.ModifiedDate ?? DateTime.Now) - new DateTime(1970,1,1)).TotalMilliseconds;

            var templateBodyName = SubscriptionType.Replace(" ", "") + modifiedMilliseconds;
            var templateSubjectName = SubscriptionType.Replace(" ", "") + "Subject" + modifiedMilliseconds;

            template.TemplateBody = Engine.Razor.IsTemplateCached(templateBodyName, model) ? Engine.Razor.Run(templateBodyName, model, modelVals) : Engine.Razor.RunCompile(template.TemplateBody, templateBodyName, model, modelVals);
            template.TemplateSubject = Engine.Razor.IsTemplateCached(templateSubjectName, model) ? Engine.Razor.Run(templateSubjectName, model, modelVals) : Engine.Razor.RunCompile(template.TemplateSubject, templateSubjectName, model, modelVals);
            
            return template;
        }

        public string TestEmailTemplate(FormCollection body)
        {
            string response = "yes";
            connection = new CommPmApplicationConnection();
            EmailService emailService = new EmailService(connection);
            ProjectsService projectsService = new ProjectsService(connection);

            try
            {
                var user = connection.User.OrderBy(r => Guid.NewGuid()).First();
                var templateId = int.Parse(body["templateId"]);

                var template = connection.EmailTemplates.First(et => et.Id == templateId);
                var project = connection.JobSheet.OrderBy(r => Guid.NewGuid()).First();

                switch (template.SubscriptionType.SubscriptionTypeModel)
                {
                    case "CreateNewUserModel":
                        emailService.CreateNewUserEmail(user.UserId);
                        break;

                    case "CreditApprovedModel":
                        var jobSheetId = connection.JobSheet.OrderBy(r => Guid.NewGuid()).First(js =>
                                js.JobSheetCompletedStep.OrderByDescending(s => s.CreatedDate).FirstOrDefault().JobStepId == 11)
                            .JobSheetId;
                        emailService.CreditApprovedEmail(jobSheetId);
                        break;

                    case "PMChangeModel":
                        var newUser = connection.User.OrderBy(r => Guid.NewGuid()).First(u => u.RoleId == 3);
                        var newPm = new JobSheetViewModel
                        {
                            JobSheetId = project.JobSheetId,
                            JobSheetName = project.JobSheetName,
                            AccountName = project.AccountName ?? "",
                            AccountNumber = project.AccountNumber ?? "",
                            Customer = new CustomerViewModel()
                            {
                                CustNo = project.Customer.CustNo ?? "",
                                CustomerName = project.Customer.CustomerName ?? ""

                            },
                            JobOrShipToNumber = project.JobOrShipToNumber ?? "",
                            ShipToName = project.ShipToName ?? "",
                            JobName = project.JobName,
                            IsActive = project.IsActive,
                            CreatedBy = project.CreatedBy,
                            CreatedDate = project.CreatedDate,
                            LastModifiedDate = project.LastModifiedDate,
                            ProjectManager = newUser.UserId,
                            TMCommissionPosition1Id = project.TMCommissionPosition1Id,
                            User3 = new JobSheetUserViewModel()
                            {
                                UserId = project.User3.UserId,
                                SlsRep = project.User3.SlsRep,
                                SMSN = new SMSNViewModel()
                                {
                                    SlsRep = project.User3.SMSN.SlsRep,
                                    Name = project.User3.SMSN.Name
                                }
                            }
                        };
                        emailService.PMchangeEmail(project, newPm);
                        break;

                    case "ProjectCreatedModel":
                        emailService.CreateUpdateJobEmail(project.JobSheetId,
                            template.TemplateName ==
                            "Project Created"
                                ? "new"
                                : "update");
                        break;

                    case "PromiseDateVerificationModel":
                        emailService.PromiseDateVerificationEmail(project.JobSheetId);
                        break;

                    case "CreditRejectModel":
                        var jobSheet = connection.JobSheet.OrderBy(r => Guid.NewGuid()).First(js =>
                            js.JobSheetCompletedStep.OrderByDescending(s => s.CreatedDate).FirstOrDefault().JobStepId ==
                            8);
                        emailService.CreditRejectEmail(jobSheet.JobSheetId);
                        break;

                    case "subAccountShiptoCreatedModel":
                        //emailService.
                        break;

                    case "SubmittedToCreditModel":
                        if (template.TemplateName.Contains("15000"))
                        {
                            project = connection.JobSheet.OrderBy(r => Guid.NewGuid())
                                .First(js => js.TotalPurchases > 15000);
                        }
                        emailService.SubmittedToCreditEmail(project.JobSheetId);
                        break;

                    case "AdhocReportModel":
                        var jsController = new ProjectsController();
                        jsController.EmailExcelExport(new List<int> {project.JobSheetId}, user.Email, user.SMSN.Name);
                        break;

                    case "VoltageVerificationModel":
                        emailService.VoltageVerificationEmail(project.JobSheetId);
                        break;

                    default:
                        response = "no";
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in EmailTemplateService class TestEmailTemplate method");
                response = "no";
            }

            return response;
        }
    }
}
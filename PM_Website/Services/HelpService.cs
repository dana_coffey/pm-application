﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using NLog;
using PM_Website.Controllers;
using PM_Website.Models;

namespace PM_Website.Services
{
    public class HelpService
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public HelpService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }
        public List<HelpViewModel> GetHelpDescription(string Name)
        {
            connection = new CommPmApplicationConnection();
            List<HelpViewModel> result = new List<HelpViewModel>();

            try
            {
                var RoleId = Convert.ToInt32(HttpContext.Current.Session["userRoleId"].ToString());
                result = connection.HelpFile.Select(HelpFiles => new HelpViewModel
                {
                    PageName = HelpFiles.PageName,
                    Description = HelpFiles.HelpDescription,
                    IsActive = HelpFiles.IsActive,
                    RoleId = HelpFiles.RoleId

                }).Where(p =>  p.PageName == Name && p.RoleId == RoleId && p.IsActive == true).ToList();

                if (result.Count == 0)
                {
                    result = connection.HelpFile.Select(HelpFiles => new HelpViewModel
                    {
                        PageName = HelpFiles.PageName,
                        Description = HelpFiles.HelpDescription,
                        IsActive = HelpFiles.IsActive,
                        RoleId = HelpFiles.RoleId

                    }).Where(p => p.PageName == Name && p.IsActive == true).ToList();

                    if (result.Count == 0)
                    {
                        result.Add(AddEmptyHelpFile(Name));
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class GetHelpDescription method");
            }

            return result;
        }

        public HelpViewModel AddEmptyHelpFile(string Name)
        {
            var helpView = new HelpViewModel();
            var helpFile = new HelpFile();
            var userRoleId = int.Parse(HttpContext.Current.Session["userRoleId"].ToString());
            var userRole = connection.UserRoles.First(ur => ur.RoleId == userRoleId).RoleName;
            helpFile.PageName = Name;
            helpView.PageName = Name;
            helpFile.HelpDescription = "Help File for " + Name + " with role " + userRole;
            helpView.Description = "Help File for " + Name + " with role " + userRole;
            helpFile.RoleId = userRoleId;
            helpFile.IsActive = true;

            try
            {
                connection.HelpFile.Attach(helpFile);
                connection.Entry(helpFile).State = EntityState.Added;
                connection.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                logger.Error(e, "Error occured in HelpService class AddEmptyHelpFile method");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class AddEmptyHelpFile method");
            }

            return helpView;
        }

        public List<HelpPageModel> GetHelpPages()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<HelpPageModel>();

            try
            {
                result = connection.HelpFile.OrderBy(hf=>hf.PageName).ThenBy(hf=>hf.UserRoles.RoleName).Select(hf=>new HelpPageModel
                {
                    Id = hf.Id,
                    PageName = hf.PageName + " - " + hf.UserRoles.RoleName
                }).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class GetHelpPages method");
            }

            return result;
        }

        public HelpEditModel GetHelpFile(int helpId)
        {
            connection = new CommPmApplicationConnection();
            var result = new HelpEditModel();

            try
            {
                result = connection.HelpFile.Select(hf => new HelpEditModel
                {
                    Id = hf.Id,
                    HelpDescription = hf.HelpDescription,
                    HelpTopic = hf.HelpTopic,
                    PageName = hf.PageName,
                    PageUrl = hf.PageUrl
                }).First(hf=>hf.Id == helpId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class GetHelpFile method");
            }

            return result;
        }

        public string SaveHelpFile(FormCollection body)
        {
            connection = new CommPmApplicationConnection();
            string response;

            try
            {
                var helpId = int.Parse(body["helpId"]);
                var helpTopic = body["helpTopic"];
                var helpText = body["helpText"];

                var helpFile = connection.HelpFile.First(hf => hf.Id == helpId);
                helpFile.HelpDescription = helpText;
                helpFile.HelpTopic = helpTopic;

                connection.HelpFile.Attach(helpFile);
                connection.Entry(helpFile).State = EntityState.Modified;
                connection.SaveChanges();

                response = "True";
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class SaveHelpFile method");
                response = "False";
            }

            return response;
        }

        public List<HelpPageViewModel> GetHelp()
        {
            connection = new CommPmApplicationConnection();
            var response = new List<HelpPageViewModel>();

            try
            {
                var helpPages = connection.HelpFile.Select(u => u.PageName).Distinct();
                foreach (var page in helpPages)
                {
                    var pageTopics = connection.HelpFile.Where(u => u.PageName == page);

                    foreach (var topic in pageTopics)
                    {
                        if (!string.IsNullOrEmpty(topic.HelpTopic))
                        {
                            response.Add(new HelpPageViewModel
                            {
                                PageName = topic.HelpTopic,
                                hasChildren = new List<HelpPageViewModel>
                                    {
                                        new HelpPageViewModel {PageName = topic.HelpDescription}
                                    }
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in HelpService class GetHelp method");
            }

            return response;
        }
    }
}
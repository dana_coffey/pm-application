﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using NLog;
using System.Data.Entity;
using PM_Emailer;
using System.Configuration;

namespace PM_Website
{
    public class JobStepService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public JobStepService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<JobStepViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<JobStepViewModel> result = new List<JobStepViewModel>();

            try
            {
                result = connection.JobSteps.Select(jobStep => new JobStepViewModel
                {
                    JobStepId = jobStep.JobStepId,
                    StepName = jobStep.StepName,
                    StepOrder = jobStep.StepOrder,
                    DaysUntilAlert = jobStep.DaysUntilAlert,
                    IsActive = jobStep.IsActive

                }).ToList();

                if (IsActive == "True")
                {
                    return result.Where(u => u.IsActive == true).ToList();
                }
                else if (IsActive == "False")
                {
                    return result.Where(u => u.IsActive == false).ToList();
                }
                else
                {
                    return result.ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStepService class GetAll method");
                return result.ToList();
            }            
        }
        public IEnumerable<JobStepViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(JobStepViewModel jobStep)
        {
            try
            {
                var entity = new JobSteps();

                entity.StepName = jobStep.StepName;

                entity.StepOrder = jobStep.StepOrder;

                entity.DaysUntilAlert = jobStep.DaysUntilAlert;

                entity.IsActive = jobStep.IsActive;

                connection = new CommPmApplicationConnection();

                connection.JobSteps.Add(entity);
                connection.SaveChanges();

                jobStep.JobStepId = entity.JobStepId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStepService class Create method");
            }
        }

        public void Update(JobStepViewModel jobStep)
        {
            try
            {
                var entity = new JobSteps();

                entity.JobStepId = jobStep.JobStepId;

                entity.StepName = jobStep.StepName;

                entity.StepOrder = jobStep.StepOrder;

                entity.DaysUntilAlert = jobStep.DaysUntilAlert;

                entity.IsActive = jobStep.IsActive;                

                connection = new CommPmApplicationConnection();
                connection.JobSteps.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;
                connection.SaveChanges();

                CreateAuditTrail(jobStep.JobStepId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStepService class Update method");
            }
        }

        private void CreateAuditTrail(int jobStepId)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var changeLogEntity = new JobStepLog();

                changeLogEntity.ModifyingUserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                changeLogEntity.DateModified = System.DateTime.Now;
                changeLogEntity.JobStepId = jobStepId;
                changeLogEntity.IsActive = true;               

                connection.JobStepLog.Add(changeLogEntity);
                connection.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobStep class CreateausitTrail method");
            }
        }
    }
}
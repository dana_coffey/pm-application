﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;

namespace PM_Website.Services
{
    public class ManagerPMService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public ManagerPMService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<ManagerPMViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();

            try
            {
                IQueryable<ManagerPMViewModel> result = connection.ManagerPM.Select(ManagerPM => new ManagerPMViewModel
                {
                    ManagerPMId = ManagerPM.ManagerPMId,
                    ManagerId = ManagerPM.ManagerId,
                    PMId = ManagerPM.PMId,
                    User = new JobSheetUserViewModel()
                    {
                        UserId = ManagerPM.User.UserId,
                        Email = ManagerPM.User.Email,
                        SMSN =new SMSNViewModel()
                        {
                            Name = ManagerPM.User.SMSN.Name
                        }
                    },
                    User1 = new JobSheetUserViewModel()
                    {
                        UserId = ManagerPM.User1.UserId,
                        Email = ManagerPM.User1.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = ManagerPM.User1.SMSN.Name
                        }
                    },
                    User2 = new JobSheetUserViewModel()
                    {
                        UserId = ManagerPM.User2.UserId,
                        Email = ManagerPM.User2.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = ManagerPM.User2.SMSN.Name
                        }
                    },
                    IsActive = ManagerPM.IsActive                    
                });


                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var managerpmIds = new List<int>();
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.User2.RoleId == 3) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();

                    switch (IsActive)
                    {
                        case "True":
                            return result.Where(u => (u.IsActive == true) && (managerpmIds.Contains(u.PMId)) && (u.ManagerId == manageruserid)).ToList();
                        case "False":
                            return result.Where(u => (u.IsActive == false) && (managerpmIds.Contains(u.PMId)) && (u.ManagerId == manageruserid)).ToList();
                        default:
                            return result.Where(u => (managerpmIds.Contains(u.PMId)) && (u.ManagerId == manageruserid)).ToList();
                    }
                }

                switch (IsActive)
                {
                    case "True":
                        return result.Where(u => u.IsActive == true).ToList();
                    case "False":
                        return result.Where(u => u.IsActive == false).ToList();
                    default:
                        return result.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPMService class GetAll method");
                return new List<ManagerPMViewModel>();
            }
        }

        public IEnumerable<ManagerPMViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(ManagerPMViewModel managerPM)
        {
            try
            {
                connection = new CommPmApplicationConnection();               

                var entity = new ManagerPM();               

                entity.ManagerId = managerPM.ManagerId;

                entity.PMId = managerPM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = managerPM.IsActive;

                connection.ManagerPM.Add(entity);
                connection.SaveChanges();               
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPMService class Create method");
            }
        }

        public void Update(ManagerPMViewModel managerPM)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var entity = new ManagerPM();

                entity.ManagerPMId = managerPM.ManagerPMId;

                entity.ManagerId = managerPM.ManagerId;

                entity.PMId = managerPM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = managerPM.IsActive;               
                               

                connection.ManagerPM.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;

                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ManagerPMService class Update method");
            }
        }
    }
}
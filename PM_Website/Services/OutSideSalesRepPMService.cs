﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;

namespace PM_Website.Services
{
    public class OutSideSalesRepPMService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public OutSideSalesRepPMService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<OutSideSalesRepPMViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<OutSideSalesRepPMViewModel> result = new List<OutSideSalesRepPMViewModel>();

            try
            {
                result = connection.OutSideSalesRepPM.Select(OutSideSalesRepPM => new OutSideSalesRepPMViewModel
                {
                    OutSideSalesRepPMId= OutSideSalesRepPM.OutSideSalesRepPMId,
                    OutSideSalesRepId = OutSideSalesRepPM.OutSideSalesRepId,
                    PMId = OutSideSalesRepPM.PMId,
                    User = new JobSheetUserViewModel()
                    {
                        UserId = OutSideSalesRepPM.User.UserId,
                        Email = OutSideSalesRepPM.User.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = OutSideSalesRepPM.User.SMSN.Name
                        }
                    },
                    User1 = new JobSheetUserViewModel()
                    {
                        UserId = OutSideSalesRepPM.User1.UserId,
                        Email = OutSideSalesRepPM.User1.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = OutSideSalesRepPM.User1.SMSN.Name
                        }
                    },
                    User2 = new JobSheetUserViewModel()
                    {
                        UserId = OutSideSalesRepPM.User2.UserId,
                        Email = OutSideSalesRepPM.User2.Email,
                        SMSN = new SMSNViewModel()
                        {
                            Name = OutSideSalesRepPM.User2.SMSN.Name
                        }
                    },
                    IsActive = OutSideSalesRepPM.IsActive
                }).ToList();

                if (IsActive == "True")
                {
                    return result.Where(u => u.IsActive == true).ToList();
                }
                else if (IsActive == "False")
                {
                    return result.Where(u => u.IsActive == false).ToList();
                }
                else
                {
                    return result.ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPMService class GetAll method");
                return result.ToList();
            }
        }

        public IEnumerable<OutSideSalesRepPMViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(OutSideSalesRepPMViewModel outSideSalesRepPM)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var entity = new OutSideSalesRepPM();

                entity.OutSideSalesRepId = outSideSalesRepPM.OutSideSalesRepId;

                entity.PMId = outSideSalesRepPM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = outSideSalesRepPM.IsActive;

                connection.OutSideSalesRepPM.Add(entity);
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPMService class Create method");
            }
        }

        public void Update(OutSideSalesRepPMViewModel outSideSalesRepPM)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var entity = new OutSideSalesRepPM();

                entity.OutSideSalesRepPMId = outSideSalesRepPM.OutSideSalesRepPMId;

                entity.OutSideSalesRepId = outSideSalesRepPM.OutSideSalesRepId;

                entity.PMId = outSideSalesRepPM.PMId;

                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                entity.IsActive = outSideSalesRepPM.IsActive;


                connection.OutSideSalesRepPM.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;

                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in OutSideSalesRepPMService class Update method");
            }
        }
    }
}
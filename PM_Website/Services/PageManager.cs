﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;

namespace PM_Website
{
    public class PageManager : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        public PageManager(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public RoleBasedAccess getRoleBasedAccess(int roleId, string pageName)
        {
            connection = new CommPmApplicationConnection();
            RoleBasedAccess result = new RoleBasedAccess();
            try
            {
                result = connection.RoleBasedAccess.FirstOrDefault(u => (u.IsActive == true) && (u.PageName == pageName) && (u.RoleId == roleId));               
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in PageManager class getRoleBasedAccess method");
                
            }

            return result;
        }

    }
}
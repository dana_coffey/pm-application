﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;
using System.Configuration;
using System.Security.Policy;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using System.Net;
using System.Security;
using Microsoft.SharePoint.Client;
using PM_Website.Controllers;
using PM_Website.Services;

namespace PM_Website.Services
{
    public class ProjectDataReportService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        private CommPmApplicationConnection connection;

        string url = HttpContext.Current.Request.Url.Authority;        

        public ProjectDataReportService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<ProjectDataReportViewModel> GetAll()
        {

            connection = new CommPmApplicationConnection();
            var result = new List<ProjectDataReportViewModel>();           

            try
            {
                IQueryable<ProjectDataReport> reportList = connection.ProjectDataReport;

                List<int> pmIds = new List<int>();
                List<int> managerpmIds = new List<int>();
                List<int> mpmslsrep = new List<int>();
                List<int> delegates = new List<int>();
                List<int> outsidesalesrepIds = new List<int>();

                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    delegates = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();
                    delegates.Add(userid);

                    pmIds = connection.User.Where(p => (p.IsActive == true) && (p.RoleId == 3) && (delegates.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    reportList = connection.ProjectDataReport.Where(u => (pmIds.Contains(u.UserId ?? 0)) && (u.IsActive == true));
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    int manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();

                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    reportList = connection.ProjectDataReport.Where(u => (mpmslsrep.Contains(u.UserId ?? 0)) && (u.IsActive == true));
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.RoleId == 8) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    reportList = connection.ProjectDataReport.Where(u => (outsidesalesrepIds.Contains(u.UserId ?? 0)) && (u.IsActive == true));
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() != "1")
                {
                    int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    reportList = connection.ProjectDataReport.Where(p => (p.IsActive == true) && p.UserId == userid);
                }

                return result = reportList.Select(p => new ProjectDataReportViewModel
                {
                    ProjectDataReportId = p.ProjectDataReportId,
                    ReportType = p.ReportType,
                    ReportName = p.ReportName,
                    IsActive = p.IsActive,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    Generation = p.Generation,
                    User = new UserViewModel
                    {
                        UserId = p.User.UserId,
                        Email = p.User.Email,
                        IsActive = p.User.IsActive,
                        Name = p.User.SMSN.Name,
                        RoleId = p.User.RoleId,
                        SlsRep = p.User.SlsRep
                    }
                }).Where(u=>u.Generation != "Now").ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectDataReportService class GetAll method");
                return result.ToList();
            }

            // return result;
        }

        public IEnumerable<ProjectDataReportViewModel> ReadProjects()
        {
            return GetAll();
        }

        public void Create(ProjectDataReportViewModel data)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                
                var entity = new ProjectDataReport();

                entity.ReportType = data.ReportType;
                entity.ReportName = data.ReportName;
                entity.Generation = data.Generation;
                entity.StartDate = data.StartDate;
                entity.EndDate = data.EndDate;
                entity.UserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.IsActive = true;

                connection.ProjectDataReport.Add(entity);
                connection.SaveChanges();

                data.ProjectDataReportId = entity.ProjectDataReportId;

                foreach (var jobSheetId in data.JobSheetIds)
                {
                    var jobSheet = new ProjectDataReportJobSheets();
                    jobSheet.JobSheetId = int.Parse(jobSheetId);
                    jobSheet.ProjectDataReportId = entity.ProjectDataReportId;

                    connection.ProjectDataReportJobSheets.Attach(jobSheet);
                    connection.Entry(jobSheet).State = EntityState.Added;
                    connection.SaveChanges();
                }

                if (entity.Generation == "Now")
                {
                    var jobSheetController = new ProjectsController();

                    var theIds = new List<int>();
                    foreach (var id in data.JobSheetIds)
                    {
                        theIds.Add(Int32.Parse(id));
                    }

                    var user = connection.User.First(u => u.UserId == entity.UserId);

                    var sendEmail = jobSheetController.EmailExcelExport(theIds, user.Email, user.SMSN.Name);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectDataReport class Create method");
            }

        }

        public string DeleteProjectDateReportById(string Id)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                int id = Convert.ToInt32(Id);
               
                var entity =  connection.ProjectDataReport.Where(u=>u.ProjectDataReportId == id).FirstOrDefault();
                entity.IsActive = false;

                connection.SaveChanges();
                
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return "FAILED";
            }
        }
    }
}
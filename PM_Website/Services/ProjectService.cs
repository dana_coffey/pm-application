﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using NLog;
using System.Data.Entity;

namespace PM_Website
{
    public class ProjectService
    {
        /*Logger logger = LogManager.GetLogger("ErrorLog");
        
        private CommPmApplicationConnection connection;

        public ProjectService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IEnumerable<ProjectOrderLineViewModel> OrderLineRead(string ShipTo, int CustNo, int OrderNo, int OrderSurf)
        {
            return GetAllOrderLines(ShipTo,CustNo, OrderNo, OrderSurf);
        }

        public IList<ProjectOrderLineViewModel> GetAllOrderLines(string ShipTo, int CustNo, int OrderNo, int OrderSurf)
        {
            connection = new CommPmApplicationConnection();
            List<ProjectOrderLineViewModel> result = new List<ProjectOrderLineViewModel>();

            try
            {               
                    result = connection.ProjectOrderLine.Select(projectorderline => new ProjectOrderLineViewModel
                    {
                        ProjectOrderLineId = projectorderline.ProjectOrderLineId,
                        OrderNo = projectorderline.OrderNo,                       
                        OrderSurf = projectorderline.OrderSurf,
                        LineNumber = projectorderline.LineNumber,
                        ModelPart = projectorderline.ModelPart,
                        POAmount = projectorderline.POAmount,
                        QuantityOrdered = projectorderline.QuantityOrdered,
                        QuantityShipped = projectorderline.QuantityShipped,
                        CustomerRequestedDeliveryDate = projectorderline.CustomerRequestedDeliveryDate,
                        VendorName = projectorderline.VendorName,
                        VerifiedVoltage = projectorderline.VerifiedVoltage,
                        CustNo = projectorderline.CustNo,
                        ShipTo = projectorderline.ShipTo,
                        VendorNumber = projectorderline.VendorNumber,
                        OrderAmount = projectorderline.OrderAmount * projectorderline.QuantityOrdered,
                        NetAmount = projectorderline.NetAmount,
                        OrderAltNo = projectorderline.OrderAltNo,
                        OrderLineCreatedDate =projectorderline.OrderLineCreatedDate,
                        Vendor = new VendorViewModel()
                        {
                            VendorNumber = projectorderline.Vendor.VendorNumber,
                            VendorName = projectorderline.Vendor.VendorName
                        }
                    }).Where(p => (p.ShipTo == ShipTo) && (p.CustNo == CustNo) && (p.OrderNo == OrderNo) && (p.OrderSurf == OrderSurf)).ToList();
              

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectService class GetAllOrderLines method");
            }

            return result;
        }*/
    }
}
﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.SharePoint.Client;
using NLog;
using PM_Website.Models;
using PM_Website.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Web;

namespace PM_Website
{
    public class ProjectsService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");

        private CommPmApplicationConnection connection;

        // string url = HttpContext.Current.Request.Url.Authority;

        SxeQueryService sxeQueryService = new SxeQueryService();
        private readonly SqlConnection sqlconnection;

        public ProjectsService(CommPmApplicationConnection connection, SqlConnection sqlconnection = null)
        {
            this.connection = connection;
            this.sqlconnection = sqlconnection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<JobSheetViewModel> GetAll(string IsActive, string MyProjects, int Delegate)
        {

            connection = new CommPmApplicationConnection();
            this.connection.Database.CommandTimeout = 180;
            var result = new List<JobSheetViewModel>();

            var pmIds = new List<int>();
            var outsidesalesrepIds = new List<int>();
            var managerpmIds = new List<int>();

            var mpmslsrep = new List<int>();

            var pmossIds = new List<int>();



            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                var sheets = connection.JobSheet
                    .Select(jobSheet => new JobSheetViewModel
                    {
                        JobSheetId = jobSheet.JobSheetId,
                        JobSheetName = jobSheet.JobSheetName,
                        Company = jobSheet.Company,
                        AccountName = string.IsNullOrEmpty(jobSheet.AccountName) ? string.Empty : jobSheet.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheet.AccountNumber) ? string.Empty : jobSheet.AccountNumber,
                        JobStep =
                        new JobStepViewModel()
                        {
                            JobStepId = jobSheet.JobSteps.JobStepId,
                            StepName = jobSheet.JobSteps.StepName,
                        },
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheet.JobOrShipToNumber) ? string.Empty : jobSheet.JobOrShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheet.ShipToName) ? string.Empty : jobSheet.ShipToName,
                        ManagementApproved = jobSheet.ManagementApproved,
                        JobName = jobSheet.JobName,
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheet.User7.UserId,
                            SlsRep = jobSheet.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheet.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheet.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheet.User8.UserId,
                            SlsRep = jobSheet.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheet.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheet.User8.SMSN.Name
                            }
                        },
                        User5 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheet.User5.UserId,
                            SlsRep = jobSheet.User5.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheet.User5.SMSN.SlsRep.ToUpper(),
                                Name = jobSheet.User5.SMSN.Name
                            }
                        },
                        IsLockRecord = jobSheet.IsLockRecord,
                        CompanyName = jobSheet.CompanyName,
                        IsActive = jobSheet.IsActive,
                        Customer =
                        new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheet.Customer.CustNo) ? string.Empty : jobSheet.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheet.Customer.CustomerName) ? string.Empty : jobSheet.Customer.CustomerName
                        },
                        POAmount = jobSheet.POAmount,
                        OrderAmount = jobSheet.OrderAmount,
                        VerifiedVoltage = jobSheet.VerifiedVoltage,
                        ShipToAddress = jobSheet.ShipToAddress,
                        ProjectManager = jobSheet.ProjectManager,
                        VerifiedVoltageDocURL = jobSheet.VerifiedVoltageDocURL,
                        VerifiedVoltageDoc = jobSheet.VerifiedVoltageDoc,
                        TMCommissionPosition1Id = jobSheet.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheet.TMCommissionPosition2Id,
                        Estimator = jobSheet.Estimator,
                        Material = jobSheet.Material,
                        Labor = jobSheet.Labor,
                        Returnage = jobSheet.Returnage,
                        Duration = jobSheet.Duration,
                        TotalPurchases = jobSheet.TotalPurchases
                    }).ToList();

                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    if (Delegate > 0)
                    {
                        var userid = Delegate;
                        pmIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    }
                    else
                    {
                        var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                        pmIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    }

                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();

                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();
                }

                if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                }


                if (IsActive == "True")
                {
                    if (MyProjects == "MyDirectReports")
                    {
                        return sheets.Where(u => u.IsActive == true).Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.Company == conoid)).ToList();
                    }
                    else
                    {
                        if (pmIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();
                        }
                        else if (outsidesalesrepIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();
                            //return sheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();
                        }
                        else
                        {
                            if (HttpContext.Current.Session["userRoleId"].ToString() == "4" || HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                            {
                                return sheets.Where(u => (u.IsActive == true) && (u.Company == conoid)).ToList();
                            }
                            else
                            {
                                return sheets.Where(u => u.IsActive == true).ToList();
                            }

                        }
                    }
                }
                else if (IsActive == "False")
                {
                    if (MyProjects == "MyDirectReports")
                    {
                        return sheets.Where(u => u.IsActive == false).Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.Company == conoid)).ToList();
                    }
                    else
                    {
                        if (pmIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => u.IsActive == false).Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => u.IsActive == false).Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();

                            //return sheets.Where(u => u.IsActive == false).Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                        }
                        else if (outsidesalesrepIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => u.IsActive == false).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => u.IsActive == false).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();
                            //return sheets.Where(u => u.IsActive == false).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();
                        }
                        else
                        {
                            if (HttpContext.Current.Session["userRoleId"].ToString() == "4" || HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                            {
                                return sheets.Where(u => (u.IsActive == false) && (u.Company == conoid)).ToList();
                            }
                            else
                            {
                                return sheets.Where(u => u.IsActive == false).ToList();
                            }
                        }
                    }
                }
                else
                {
                    if (MyProjects == "MyDirectReports")
                    {
                        return sheets.Where(u => mpmslsrep.Contains(u.ProjectManager)).ToList();
                    }
                    else
                    {
                        if (pmIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();
                            //return sheets.Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                        }
                        else if (outsidesalesrepIds.Count > 0)
                        {
                            var pmsusers = sheets.Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).ToList();
                            var ossusers = sheets.Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();

                            var allusers = pmsusers.Union(ossusers);

                            return allusers.Where(u => u.Company == conoid).ToList();
                            //return sheets.Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();
                        }
                        else
                        {
                            if (HttpContext.Current.Session["userRoleId"].ToString() == "4" || HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                            {
                                return sheets.Where(u => u.Company == conoid).ToList();
                            }
                            else
                            {
                                return sheets.ToList();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAll method");
                return result.ToList();
            }

            // return result;
        }


        public IEnumerable<JobSheetViewModel> Read(string IsActive, string MyProjects, int Delegate)
        {
            return GetAll(IsActive, MyProjects, Delegate);
        }

        public IEnumerable<DashbordJobStatusViewModel> GetStatus()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<DashbordJobStatusViewModel>();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                var pmIds = new List<int>();

                var managerpmIds = new List<int>();

                var mpmslsrep = new List<int>();

                var delegates = new List<int>();
                var outsidesalesrepIds = new List<int>();
                var sheets = connection.JobSheet.ToList();


                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());

                    delegates = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();

                    delegates.Add(userid);

                    pmIds = connection.User.Where(p => (p.IsActive == true) && (delegates.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    var pid = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    var pmsusers = connection.JobSheet.Where(u => (pmIds.Contains(u.ProjectManager)) && (u.IsActive == true)).ToList();
                    var ossusers = connection.JobSheet.Where(u => (pid.Contains(u.TMCommissionPosition1Id)) && (u.IsActive == true)).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    sheets = allusers.Where(u => u.Company == conoid).ToList();

                    //sheets = connection.JobSheet.Where(u => (pmIds.Contains(u.ProjectManager)) && (u.IsActive == true)).ToList();



                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();

                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    sheets = connection.JobSheet.Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.IsActive == true) && (u.Company == conoid)).ToList();

                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.RoleId == 8) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    //sheets = connection.JobSheet.Where(u => (outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)) && (u.IsActive == true)).ToList();
                    var pmsusers = connection.JobSheet.Where(u => (outsidesalesrepIds.Contains(u.ProjectManager)) && (u.IsActive == true)).ToList();
                    var ossusers = connection.JobSheet.Where(u => (outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)) && (u.IsActive == true)).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    sheets = allusers.Where(u => u.Company == conoid).ToList();
                }

                if (HttpContext.Current.Session["userRoleId"].ToString() == "4" || HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                {
                    sheets = connection.JobSheet.Where(u => u.Company == conoid).ToList();
                }

                foreach (var sheet in sheets)
                {
                    if (sheet.IsActive != null && sheet.IsActive.Value)
                    {
                        var model = new DashbordJobStatusViewModel();

                        model.JobSheetId = sheet.JobSheetId;
                        model.JobSheetName = sheet.JobSheetName;
                        model.jobName = sheet.JobName;
                        model.AccountName = string.IsNullOrEmpty(sheet.AccountName) ? string.Empty : sheet.AccountName;
                        model.AccountNumber = sheet.AccountNumber;
                        model.JobOrShipToNumber = string.IsNullOrEmpty(sheet.JobOrShipToNumber) ? string.Empty : sheet.JobOrShipToNumber;
                        model.voltageVerified = sheet.VerifiedVoltage != null && sheet.VerifiedVoltage.Value;
                        model.subaccountCreated = !string.IsNullOrEmpty(sheet.JobOrShipToNumber);
                        model.sheetDeactivated = sheet.IsActive == false;
                        model.voltageVerifiedURL = sheet.VerifiedVoltageDocURL;
                        model.VerifiedVoltageDoc = sheet.VerifiedVoltageDoc;
                        model.VerifiedVoltageWhom = sheet.VerifiedVoltageWhom;
                        model.VerifiedVoltageCreatedDate = sheet.VerifiedVoltageCreatedDate.ToString();
                        model.ShipToName = string.IsNullOrEmpty(sheet.ShipToName) ? string.Empty : sheet.JobOrShipToNumber + " : " + sheet.ShipToName;

                        var jobSheetStatus =
                            connection.JobSheetCompletedStep.Where(p => p.JobSheetId == sheet.JobSheetId);

                        foreach (var jobStatus in jobSheetStatus)
                        {
                            if (jobStatus.JobSteps.StepName == "Pre-Submittal")
                            {
                                model.sheetCreated = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Submitted to Engineer")
                            {
                                model.submittedToContractor = jobStatus.IsActive == true;
                                if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                    model.submittedToContractorCreatedDate = jobStatus.CreatedDate.ToString();
                                else
                                    model.submittedToContractorCreatedDate = jobStatus.LastModifiedDate.ToString();
                            }

                            if (jobStatus.JobSteps.StepName == "Reviewed by Engineer")
                            {
                                model.reviewedByContractor = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Approved by Engineer")
                            {
                                model.approvedByContractor = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Returned by Engineer")
                            {
                                model.returnedFromContractor = jobStatus.IsActive == true;
                                if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                    model.returnedFromContractorCreatedDate = jobStatus.CreatedDate.ToString();
                                else
                                    model.returnedFromContractorCreatedDate = jobStatus.LastModifiedDate.ToString();

                            }

                            if (jobStatus.JobSteps.StepName == "Resubmitted to Engineer")
                            {
                                model.resubmittedToContractor = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Submitted to Credit")
                            {
                                model.submittedToCredit = jobStatus.IsActive == true;
                                if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                    model.submittedToCreditCreatedDate = jobStatus.CreatedDate.ToString();
                                else
                                    model.submittedToCreditCreatedDate = jobStatus.LastModifiedDate.ToString();

                            }

                            if (jobStatus.JobSteps.StepName == "Approved by Credit")
                            {
                                model.approvedByCredit = jobStatus.IsActive == true;
                                if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                    model.approvedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                                else
                                    model.approvedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();

                            }

                            if (jobStatus.JobSteps.StepName == "Rejected by Credit")
                            {
                                model.rejectedByCredit = jobStatus.IsActive == true;
                                if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                    model.rejectedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                                else
                                    model.rejectedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();

                            }

                            if (jobStatus.JobSteps.StepName == "Orders Created")
                            {
                                model.ordersCreated = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Job Closed")
                            {
                                model.jobclosed = jobStatus.IsActive == true;
                            }

                            if (jobStatus.JobSteps.StepName == "Job Synced")
                            {
                                model.jobSynced = jobStatus.IsActive == true;
                            }
                        }

                        result.Add(model);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetStatus method");
                return result.ToList();
            }
        }

        public IEnumerable<DashboardChartViewModel> GetChartValues(string grid)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<DashboardChartViewModel>();
            var pmIds = new List<int>();
            var outsidesalesrepIds = new List<int>();
            var managerpmIds = new List<int>();
            var Delegate = new List<int>();
            var mpmslsrep = new List<int>();
            var sheets2 = new List<JobSheet>();
            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                //Filter for PM 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    Delegate = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();
                    Delegate.Add(userid);
                    pmIds = connection.User.Where(p => (p.IsActive == true) && (Delegate.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    var pmsusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    sheets2 = allusers.Where(u => u.Company == conoid).ToList();
                    //sheets2 = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                }
                //Filter for Manager 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();
                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    sheets2 = connection.JobSheet.Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.IsActive == true) && (u.Company == conoid)).OrderByDescending(u => u.CreatedDate).ToList();

                }
                //Filter for Otside Sales 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    //sheets2 = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();
                    var pmsusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    sheets2 = allusers.Where(u => u.Company == conoid).ToList();

                }
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "4" || HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                {
                    sheets2 = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).OrderByDescending(u => u.CreatedDate).ToList();
                }
                else
                {
                    sheets2 = connection.JobSheet.Where(u => u.IsActive == true).OrderByDescending(u => u.CreatedDate).ToList();

                }
                switch (grid)
                {
                    case "newAccts":
                        var sheets = connection.Customer;
                        foreach (var sheet in sheets)
                        {
                            if (sheet.CreatedDate != null)
                            {
                                if ((DateTime.Now - sheet.CreatedDate).TotalDays <= 7)
                                {
                                    var row = new DashboardChartViewModel
                                    {
                                        /*JobSheetName = sheet.ShipToName,
                                        JobSheetId = int.Parse(sheet.ShipToId),
                                        ModifiedBy = "",
                                        ModifiedDate = DateTime.Now,*/
                                        SubAccount = sheet.CustNo + " : " + sheet.CustomerName
                                    };
                                    result.Add(row);
                                }
                            }
                        }
                        break;

                    case "newJobs":
                        foreach (var sheet in sheets2)
                        {
                            if (sheet.CreatedDate != null)
                            {
                                if ((DateTime.Now - sheet.CreatedDate.Value).TotalDays <= 7)
                                {
                                    var modifiedBy = connection.User.First(usr => usr.UserId == sheet.LastModifiedBy);

                                    var row = new DashboardChartViewModel
                                    {
                                        JobSheetName = sheet.JobName,
                                        JobSheetId = sheet.JobSheetId,
                                        ModifiedBy = modifiedBy.SMSN.Name,
                                        ModifiedDate = sheet.LastModifiedDate ?? DateTime.Now,
                                        account = string.IsNullOrEmpty(sheet.Customer.CustNo) ? string.Empty : sheet.Customer.CustNo + " : " + sheet.Customer.CustomerName,
                                        SubAccount = string.IsNullOrEmpty(sheet.JobOrShipToNumber) ? string.Empty : " - " + sheet.JobOrShipToNumber + " : " + sheet.ShipToName

                                    };
                                    result.Add(row);
                                }
                            }
                        }
                        break;

                    case "awaitingVV":
                        foreach (var sheet in sheets2)
                        {
                            if (sheet.VerifiedVoltage == null || !sheet.VerifiedVoltage.Value)
                            {
                                var modifiedBy = connection.User.First(usr => usr.UserId == sheet.LastModifiedBy);
                                var row = new DashboardChartViewModel
                                {
                                    JobSheetName = sheet.JobName,
                                    JobSheetId = sheet.JobSheetId,
                                    ModifiedBy = modifiedBy.SMSN.Name,
                                    ModifiedDate = sheet.LastModifiedDate ?? DateTime.Now,
                                    account = string.IsNullOrEmpty(sheet.Customer.CustNo) ? string.Empty : sheet.Customer.CustNo + " : " + sheet.Customer.CustomerName,
                                    SubAccount = string.IsNullOrEmpty(sheet.JobOrShipToNumber) ? string.Empty : " - " + sheet.JobOrShipToNumber + " : " + sheet.ShipToName
                                };
                                result.Add(row);
                            }
                        }
                        break;

                    case "returnOfSubmittals":
                        foreach (var sheet in sheets2)
                        {
                            var jobStep = connection.JobSteps.First(st => st.JobStepId == sheet.JobSteps.JobStepId);
                            if (jobStep.StepName == "Submitted to Engineer" ||
                                jobStep.StepName == "Resubmitted to Engineer")
                            {
                                var modifiedBy = connection.User.First(usr => usr.UserId == sheet.LastModifiedBy);
                                var row = new DashboardChartViewModel
                                {
                                    JobSheetName = sheet.JobName,
                                    JobSheetId = sheet.JobSheetId,
                                    ModifiedBy = modifiedBy.SMSN.Name,
                                    ModifiedDate = sheet.LastModifiedDate ?? DateTime.Now,
                                    account = string.IsNullOrEmpty(sheet.Customer.CustNo) ? string.Empty : sheet.Customer.CustNo + " : " + sheet.Customer.CustomerName,
                                    SubAccount = string.IsNullOrEmpty(sheet.JobOrShipToNumber) ? string.Empty : " - " + sheet.JobOrShipToNumber + " : " + sheet.ShipToName
                                };
                                result.Add(row);
                            }
                        }
                        break;

                    case "creditApproval":
                        foreach (var sheet in sheets2)
                        {
                            var jobStep = connection.JobSteps.First(st => st.JobStepId == sheet.JobSteps.JobStepId);
                            if (jobStep.StepName == "Submitted to Credit")
                            {
                                var modifiedBy = connection.User.First(usr => usr.UserId == sheet.LastModifiedBy);
                                var row = new DashboardChartViewModel
                                {
                                    JobSheetName = sheet.JobName,
                                    JobSheetId = sheet.JobSheetId,
                                    ModifiedBy = modifiedBy.SMSN.Name,
                                    ModifiedDate = sheet.LastModifiedDate ?? DateTime.Now,
                                    account = string.IsNullOrEmpty(sheet.Customer.CustNo) ? string.Empty : sheet.Customer.CustNo + " : " + sheet.Customer.CustomerName,
                                    SubAccount = string.IsNullOrEmpty(sheet.JobOrShipToNumber) ? string.Empty : " - " + sheet.JobOrShipToNumber + " : " + sheet.ShipToName
                                };
                                result.Add(row);
                            }
                        }
                        break;

                    default:
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetChartValues method");
                return result.ToList();
            }
        }

        public IEnumerable<DashboardVoltageViewModel> GetVoltageGrid()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<DashboardVoltageViewModel>();
            var pmIds = new List<int>();
            var outsidesalesrepIds = new List<int>();
            var managerpmIds = new List<int>();
            var Delegate = new List<int>();
            var mpmslsrep = new List<int>();


            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                //Filter for PM 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    Delegate = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();
                    Delegate.Add(userid);
                    pmIds = connection.User.Where(p => (p.IsActive == true) && (Delegate.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    //var theList = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var pmsusers = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var theList = pmsusers.Union(ossusers);


                    foreach (var JobSheet in theList)
                    {
                        if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                        {
                            if (JobSheet.IsActive != null && JobSheet.IsActive.Value)
                            {
                                result.Add(new DashboardVoltageViewModel()
                                {
                                    JobSheetId = JobSheet.JobSheetId,
                                    JobSheetName = JobSheet.JobName,
                                    waitingSince = JobSheet.CreatedDate ?? new DateTime(1970, 1, 1)
                                });
                            }
                        }
                    }

                }
                //Filter for Manager 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();
                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    var theList = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => mpmslsrep.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    foreach (var JobSheet in theList)
                    {
                        if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                        {
                            if (JobSheet.IsActive != null && JobSheet.IsActive.Value)
                            {
                                result.Add(new DashboardVoltageViewModel()
                                {
                                    JobSheetId = JobSheet.JobSheetId,
                                    JobSheetName = JobSheet.JobName,
                                    waitingSince = JobSheet.CreatedDate ?? new DateTime(1970, 1, 1)
                                });
                            }
                        }
                    }

                }
                //Filter for Otside Sales 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    //var theList = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();
                    var pmsusers = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var theList = pmsusers.Union(ossusers);
                    foreach (var JobSheet in theList)
                    {
                        if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                        {
                            if (JobSheet.IsActive != null && JobSheet.IsActive.Value)
                            {
                                result.Add(new DashboardVoltageViewModel()
                                {
                                    JobSheetId = JobSheet.JobSheetId,
                                    JobSheetName = JobSheet.JobName,
                                    waitingSince = JobSheet.CreatedDate ?? new DateTime(1970, 1, 1)
                                });
                            }
                        }
                    }
                }
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    var theList = connection.JobSheet.Where(u => u.IsActive == true).OrderByDescending(u => u.CreatedDate).ToList();
                    foreach (var JobSheet in theList)
                    {
                        if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                        {
                            if (JobSheet.IsActive != null && JobSheet.IsActive.Value)
                            {
                                result.Add(new DashboardVoltageViewModel()
                                {
                                    JobSheetId = JobSheet.JobSheetId,
                                    JobSheetName = JobSheet.JobName,
                                    waitingSince = JobSheet.CreatedDate ?? new DateTime(1970, 1, 1)
                                });
                            }
                        }
                    }
                }
                else
                {
                    var theList = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).OrderByDescending(u => u.CreatedDate).ToList();
                    foreach (var JobSheet in theList)
                    {
                        if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                        {
                            if (JobSheet.IsActive != null && JobSheet.IsActive.Value)
                            {
                                result.Add(new DashboardVoltageViewModel()
                                {
                                    JobSheetId = JobSheet.JobSheetId,
                                    JobSheetName = JobSheet.JobName,
                                    waitingSince = JobSheet.CreatedDate ?? new DateTime(1970, 1, 1)
                                });
                            }
                        }
                    }

                }
                return result;

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetVoltageGrid method");
                return result.ToList();
            }
        }

        public void Create(JobSheetViewModel jobSheet)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var emailService = new EmailService(connection);
                var entity = new JobSheet();
                MapJobSheet(jobSheet, entity);
                if (entity.JobSheetId != 0)
                {
                    var dbRec = connection.JobSheet
                        .Include(p => p.JobSteps)
                        .FirstOrDefault(p => p.JobSheetId == jobSheet.JobSheetId);
                    if (dbRec == null) return;

                    MapJobSheetEditStore(dbRec, entity);
                    MapJobSheet(jobSheet, dbRec);
                    connection.SaveChanges();
                    CreateAuditTrail(entity, jobSheet);
                    emailService.UpdateJobEmail(entity, jobSheet);
                    //check Notify PM when Promised ship date is after customer requested Date 
                    if (jobSheet.VendorPromisedShipDate > jobSheet.CustomerRequestedDeliveryDate)
                    {
                        emailService.PromiseDateVerificationEmail(jobSheet.JobSheetId);
                    }
                    if (jobSheet.TaxExemption == "Yes")
                    {
                        emailService.UpdateTaxExemptionToCreditRep(entity, jobSheet, "Update");
                    }
                    UploadCCGFiles(jobSheet, jobSheet.JobSheetId);

                    if (jobSheet.CompanyName == "Mingledorff's Inc.")
                    {
                        jobSheet.Company = 1;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }
                    else if (jobSheet.CompanyName == "George Holden & Associates, Inc")
                    {
                        jobSheet.Company = 2;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }
                    else
                    {
                        jobSheet.Company = 1;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }

                    

                }
                else
                {
                    connection.JobSheet.Add(entity);
                    connection.SaveChanges();
                    jobSheet.JobSheetId = entity.JobSheetId;
                    CreateNewJobsheetAudit(jobSheet, entity);
                    emailService.CreateUpdateJobEmail(jobSheet.JobSheetId, "new");
                    //check Notify PM when Promised ship date is after customer requested Date 
                    if (jobSheet.VendorPromisedShipDate > jobSheet.CustomerRequestedDeliveryDate)
                    {
                        emailService.PromiseDateVerificationEmail(jobSheet.JobSheetId);
                    }
                    if (jobSheet.TaxExemption == "Yes")
                    {
                        emailService.UpdateTaxExemptionToCreditRep(entity, jobSheet, "Create");
                    }
                    UploadCCGFiles(jobSheet, jobSheet.JobSheetId);
                    if (jobSheet.SaveAndSubmit == true)
                    {
                        UpdateStatusById(jobSheet.JobSheetId, "Submitted To Credit", true);
                    }
                    else
                    {
                        JobSheetUpdateCreditJobsheet(jobSheet.JobSheetId);
                    }
                    if (jobSheet.CompanyName == "Mingledorff's Inc.")
                    {
                        jobSheet.Company = 1;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }
                    else if (jobSheet.CompanyName == "George Holden & Associates, Inc")
                    {
                        jobSheet.Company = 2;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }
                    else
                    {
                        jobSheet.Company = 1;
                        updateCreditReps(jobSheet.JobSheetId, jobSheet.AccountNumber, jobSheet.Company);
                    }
                    
                    //
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class Create method");
            }

        }

        private void updateCreditReps(int jobSheetId, string accountNumber, int company)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                string creditreps = connection.CustomerCreditReps.Where(u => u.CustNo == accountNumber && u.Cono == company).Select(e => e.SlsRep).FirstOrDefault();
                if(creditreps!=null)
                {
                    checkUserCreditRepUser(creditreps, jobSheetId, accountNumber, company);
                }
                else
                {
                    updateUserByJobId(jobSheetId, company);
                }
                //foreach (var cr in creditreps)
                //{
                //    checkUserCreditRepUser(cr, jobSheetId, accountNumber);

                //    checkCustomerCreditRep(cr, accountNumber);
                //}
               
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class updateCreditReps method");
            }

        }

        private void updateUserByJobId(int jobSheetId, int Company)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                int UserId = connection.User.Where(u => u.RoleId==5).Select(e => e.UserId).FirstOrDefault();
                updateCreditRepByJobId(jobSheetId, UserId);
            }
            catch(Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class updateUserByJobId method");
            }
        }

        private void updateCreditRepByJobId(int jobSheetId, int userId)
        {
            try
            {
                connection = new CommPmApplicationConnection();                

                var result = connection.JobSheet.FirstOrDefault(p => (p.JobSheetId == jobSheetId));

                result.CreditRep = userId;

                connection.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class updateCreditRepByJobId method");
            }
        }

        //private void checkCustomerCreditRep(CreditRep cr, string accountNumber)
        //{
        //    try
        //    {
        //        connection = new CommPmApplicationConnection();
        //        List<CustomerCreditRepsViewModel> checkcustomer = new List<CustomerCreditRepsViewModel>();
        //        checkcustomer = connection.CustomerCreditReps.Select(c => new CustomerCreditRepsViewModel
        //        {
        //            SlsRep = c.SlsRep,
        //            CustNo = c.CustNo
        //        }).Where(e => (e.CustNo == accountNumber) && (e.SlsRep == cr.SlsRep)).ToList();
        //        if (checkcustomer.Count > 0)
        //        {

        //        }
        //        else
        //        {
        //            insertCustomerCreditRep(cr, accountNumber);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error occured in ProjectsService class checkCustomerCreditRep method");
        //    }
        //}

        //private void insertCustomerCreditRep(CreditRep cr, string accountNumber)
        //{
        //    try
        //    {
        //        var entity = new CustomerCreditReps();

        //        entity.CustNo = accountNumber;

        //        entity.IsActive = true;

        //        entity.Cono = cr.CoNo;


        //        entity.SlsRep = cr.SlsRep;


        //        connection = new CommPmApplicationConnection();

        //        connection.CustomerCreditReps.Add(entity);
        //        connection.SaveChanges();

        //        int userid = entity.CustCreditRep;
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex, "Error occured in ProjectsService class insertCustomerCreditRep method");
        //    }
        //}

        private void checkUserCreditRepUser(string slsRep, int jobSheetId, string accountNumber, int Company)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                string email = connection.SMSN.Where(u => u.SlsRep == slsRep && u.CoNo == Company).Select(e => e.Email).FirstOrDefault();

                if (email != null || email != string.Empty || email != "")
                {
                    List<UserViewModel> checkUser = new List<UserViewModel>();
                    checkUser = connection.User.Select(c => new UserViewModel
                    {
                        Email = c.Email,
                        IsActive = c.IsActive,
                        UserId = c.UserId
                    }).Where(e => (e.Email == email)).ToList();
                    if (checkUser.Count > 0)
                    {
                        foreach(var user in checkUser)
                        {
                            updateCreditRepByJobId(jobSheetId, user.UserId);
                        }
                    }
                    else
                    {
                        insertUserCreditRep(slsRep, jobSheetId, accountNumber, Company, email);
                    }
                }
                else
                {
                    updateUserByJobId(jobSheetId, Company);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class checkUserCreditRepUser method");
            }
        }

        private void insertUserCreditRep(string slsRep, int jobSheetId, string accountNumber, int Company, string Email)
        {
            try
            {
                var entity = new Models.User();

                entity.RoleId = 5;

                entity.IsActive = true;

                entity.Cono = Company;


                entity.SlsRep = slsRep;
                entity.Email = Email;

                connection = new CommPmApplicationConnection();

                connection.User.Add(entity);
                connection.SaveChanges();

                int userid = entity.UserId;

                updateCreditRepByJobId(jobSheetId, userid);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class insertUserCreditRep method");
            }
        }

        private void UploadCCGFiles(JobSheetViewModel jobSheet, int jobSheetId)
        {
            try
            {

                connection = new CommPmApplicationConnection();

                if (jobSheet.ccgfiles != null)
                {
                    foreach (var file in jobSheet.ccgfiles)
                    {
                        Web WebClient;
                        using (var clientContext = new ClientContext("https://mingledorffs.sharepoint.com/InformationTechnology"))
                        {
                            var securePassword = new SecureString();
                            foreach (var c in "Pr0jMgm+!")
                            {
                                securePassword.AppendChar(c);
                            }

                            clientContext.Credentials = new SharePointOnlineCredentials("projectmanagement@Mingledorffs.onmicrosoft.com", securePassword);
                            WebClient = clientContext.Web;
                            using (var fs = file.InputStream)
                            {
                                clientContext.Load(WebClient, website => website.Lists, website => website.ServerRelativeUrl);
                                clientContext.ExecuteQuery();
                                var fi = new FileInfo(file.FileName);

                                var filename = fi.Name.Split('.')[0].ToString() + '-' + DateTime.Now.ToString("MM-dd-yy") + "." + fi.Name.Split('.')[1].ToString();
                                //string filextension = fi.Name.Split('.')[1].ToString();

                                var folder =
                                    WebClient.GetFolderByServerRelativeUrl(
                                        "Shared Documents/Software Development/PM Application/CCG");
                                clientContext.Load(folder);
                                clientContext.ExecuteQuery();
                                var fileUrl = string.Format("{0}/{1}", folder.ServerRelativeUrl, filename);

                                Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, fs, true);

                                //string url = HttpContext.Current.Request.Url.AbsoluteUri;
                                var jobsheetccgfiles = new JobSheetCCGFiles();

                                jobsheetccgfiles.JobSheetId = jobSheetId;
                                jobsheetccgfiles.CCGFileURL = "https://mingledorffs.sharepoint.com" + fileUrl;
                                jobsheetccgfiles.CreatedDate = System.DateTime.Now;
                                jobsheetccgfiles.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                                jobsheetccgfiles.ModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                                jobsheetccgfiles.ModifiedDate = System.DateTime.Now;
                                jobsheetccgfiles.CCGFileName = filename;
                                jobsheetccgfiles.IsActive = true;
                                connection.JobSheetCCGFiles.Add(jobsheetccgfiles);
                                connection.SaveChanges();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class UploadCCG method");
            }
        }

        private void CreateNewJobsheetAudit(JobSheetViewModel jobSheet, JobSheet entity)
        {
            try
            {
                var changeLogEntity = new JobSheetChangeLog();
                connection = new CommPmApplicationConnection();
                var emailService = new EmailService(connection);
                changeLogEntity.ModifiedByUserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                changeLogEntity.ChangeDate = System.DateTime.Now;
                changeLogEntity.JobSheetId = entity.JobSheetId;
                changeLogEntity.IsActive = true;
                changeLogEntity.AttributeName = "Project created";
                changeLogEntity.PreviousVal = "Created By";
                changeLogEntity.NewVal = HttpContext.Current.Session["firstNameLastName"].ToString() + " on " + System.DateTime.Now;
                changeLogEntity.Description = "New Project created by " + HttpContext.Current.Session["firstNameLastName"].ToString() + " on " + System.DateTime.Now;
                connection.JobSheetChangeLog.Add(changeLogEntity);
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class CreateNewJobsheetAudit method");
            }

        }

        private void CreateAuditTrail(JobSheet oldrec, JobSheetViewModel jobSheet)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var emailService = new EmailService(connection);
                var changeLogEntity = new JobSheetChangeLog();

                changeLogEntity.ModifiedByUserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                changeLogEntity.ChangeDate = System.DateTime.Now;
                changeLogEntity.JobSheetId = jobSheet.JobSheetId;
                changeLogEntity.IsActive = true;

                if (oldrec.Status != jobSheet.Status)
                {
                    var oldrow = connection.JobSteps.SingleOrDefault(r => r.JobStepId == oldrec.Status);
                    var oldvalue = oldrow != null ? oldrow.StepName : string.Empty;

                    var newrow = connection.JobSteps.SingleOrDefault(r => r.JobStepId == jobSheet.Status);
                    var newvalue = newrow != null ? newrow.StepName : string.Empty;

                    changeLogEntity.AttributeName = "Status";
                    changeLogEntity.PreviousVal = oldvalue.ToString();
                    changeLogEntity.NewVal = newvalue.ToString();
                    changeLogEntity.Description = System.DateTime.Now + " " + oldrec.Status + " Changed to " + jobSheet.Status + " by " + HttpContext.Current.Session["firstNameLastName"].ToString();

                    connection.JobSheetChangeLog.Add(changeLogEntity);
                    connection.SaveChanges();

                }

                if (oldrec.ProjectManager != jobSheet.ProjectManager)
                {

                    var oldrow = connection.User.SingleOrDefault(r => r.UserId == oldrec.ProjectManager);
                    var oldvalue = oldrow != null ? oldrow.SMSN.Name : string.Empty;

                    var newrow = connection.User.SingleOrDefault(r => r.UserId == jobSheet.ProjectManager);
                    var newvalue = newrow != null ? newrow.SMSN.Name : string.Empty;

                    changeLogEntity.AttributeName = "ProjectManager";
                    changeLogEntity.PreviousVal = oldvalue;
                    changeLogEntity.NewVal = newvalue;

                    connection.JobSheetChangeLog.Add(changeLogEntity);
                    connection.SaveChanges();
                    emailService.PMchangeEmail(oldrec, jobSheet);
                }

                if (oldrec.IsActive != jobSheet.IsActive)
                {
                    changeLogEntity.AttributeName = "IsActive";
                    changeLogEntity.PreviousVal = oldrec.IsActive.ToString();
                    changeLogEntity.NewVal = jobSheet.IsActive.ToString();

                    connection.JobSheetChangeLog.Add(changeLogEntity);
                    connection.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Shipto class CreateausitTrail method");
            }
        }

        public bool getPM(string email)
        {
            try
            {
                var dbuser = connection.User
                   .FirstOrDefault(p => p.Email == email);

                if (dbuser != null)
                {
                    var dbsubscription = connection.EmailSubscription.FirstOrDefault(e => (e.UserId == dbuser.UserId) && (e.SubsciptionTypeId == 1) && (e.IsActive == true));

                    if (dbsubscription != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class getPM method");
                return false;
            }
        }

        private static void MapJobSheet(JobSheetViewModel jobSheet, JobSheet entity)
        {

            entity.JobSheetId = jobSheet.JobSheetId;
            entity.JobSheetName = jobSheet.JobSheetName;
            entity.CompanyName = jobSheet.CompanyName;
            if (jobSheet.CompanyName == "Mingledorff's Inc.")
            {
                entity.Company = 1;
            }
            else if (jobSheet.CompanyName == "George Holden & Associates, Inc")
            {
                entity.Company = 2;
            }
            else
            {
                entity.Company = 1;
            }
            entity.AccountName = jobSheet.AccountName;
            entity.AccountNumber = jobSheet.AccountNumber;
            entity.Status = 1; // passing default value 
            entity.ManagementApproved = jobSheet.ManagementApproved;
            entity.SetUpDate = jobSheet.SetUpDate;
            entity.TMCommissionPosition1Id = jobSheet.TMCommissionPosition1Id;
            entity.TMCommisionPosition1 = jobSheet.TMCommisionPosition1;
            //entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition2Id;
            //entity.TMCommissionPosition2 = jobSheet.TMCommissionPosition2;

            if (jobSheet.TMCommissionPosition2Id == null || jobSheet.TMCommissionPosition2Id == 0)
            {
                entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition1Id;
                entity.TMCommissionPosition2 = jobSheet.TMCommisionPosition1;
            }
            else
            {
                entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition2Id;
                entity.TMCommissionPosition2 = jobSheet.TMCommissionPosition2;
            }


            //entity.TMCommissionPosition2Id = 1;
            entity.SubmittedBy = Convert.ToInt32(HttpContext.Current.Session["userId"]);
            entity.Owner = jobSheet.Owner ?? Convert.ToInt32(HttpContext.Current.Session["userId"]);
            entity.JobName = jobSheet.JobName;
            entity.CountyOfJob = jobSheet.CountyOfJob;
            entity.JobStreeAddress = jobSheet.JobStreeAddress;
            entity.JobState = jobSheet.JobState;
            entity.JobCity = jobSheet.JobCity;
            entity.JobZip = jobSheet.JobZip;
            entity.GeneralContractor = jobSheet.GeneralContractor;
            entity.GCMailingAddress = jobSheet.GCMailingAddress;
            entity.GCPhoneNumber = jobSheet.GCPhoneNumber;
            entity.GCCity = jobSheet.GCCity;
            entity.GCState = jobSheet.GCState;
            entity.GCZip = jobSheet.GCZip;
            entity.OwnerName = jobSheet.OwnerName;
            entity.OMailingAddress = jobSheet.OMailingAddress;
            entity.OPhoneNumber = jobSheet.OPhoneNumber;
            entity.OCity = jobSheet.OCity;
            entity.OState = jobSheet.OState;
            entity.OZip = jobSheet.OZip;
            if (entity.BondedJob == null)
                entity.BondedJob = "-None-";
            else
                entity.BondedJob = jobSheet.BondedJob;
            entity.BondInformation = jobSheet.BondInformation;
            entity.TaxExemption = jobSheet.TaxExemption;
            entity.TotalPurchases = jobSheet.TotalPurchases;
            entity.AnticipatedFirstDelivery = jobSheet.AnticipatedFirstDelivery;
            entity.AnticipatedJobClose = jobSheet.AnticipatedJobClose;
            entity.JobLine = jobSheet.JobLine;
            entity.PrelienAmount = jobSheet.PrelienAmount;
            entity.Prelien = jobSheet.Prelien;
            entity.PrelienCompany = jobSheet.PrelienCompany;
            entity.PrelienDate = jobSheet.PrelienDate;
            entity.Notes = jobSheet.Notes;
            entity.Engineer = jobSheet.Engineer;
            entity.SubmittedDate = jobSheet.SubmittedDate;

            if (jobSheet.JobSheetId > 0)
            {
                entity.IsActive = jobSheet.IsActive;

            }
            else
            {
                entity.CreatedDate = DateTime.Today;
                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.IsActive = true;
            }
            entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            entity.LastModifiedDate = DateTime.Today;
            if (jobSheet.TaxExemption == "Yes")
            {
                entity.IsLockRecord = true;
            }
            else
            {
                entity.IsLockRecord = jobSheet.IsLockRecord;
            }

            entity.IsCcgNotified = false;
            entity.VerifiedVoltage = jobSheet.VerifiedVoltage;
            entity.ProjectManager = jobSheet.ProjectManager;
            entity.ContractDate = jobSheet.ContractDate;
            entity.CustomerRequestedDeliveryDate = jobSheet.CustomerRequestedDeliveryDate;
            entity.VendorPromisedShipDate = jobSheet.VendorPromisedShipDate;
            if (jobSheet.VerifiedVoltage == true)
            {
                entity.VerifiedVoltageDoc = jobSheet.VerifiedVoltageDoc;
                entity.VerifiedVoltageDocOther = jobSheet.VerifiedVoltageDocOther;
                entity.VerifiedVoltageWhom = jobSheet.VerifiedVoltageWhom;
            }
            entity.Estimator = jobSheet.Estimator;
            entity.Material = jobSheet.Material;
            entity.Labor = jobSheet.Labor;
            entity.Returnage = jobSheet.Returnage;
            entity.Duration = jobSheet.Duration;
            entity.ReqShipToNumber = jobSheet.RequestedShipToNumber;
            entity.SalesForceURL = jobSheet.SalesForceURL;

        }

        private static void MapJobSheetEditStore(JobSheet jobSheet, JobSheet entity)
        {

            entity.JobSheetId = jobSheet.JobSheetId;
            entity.JobSheetName = jobSheet.JobSheetName;
            entity.CompanyName = jobSheet.CompanyName;
            if (jobSheet.CompanyName == "Mingledorff's Inc.")
            {
                entity.Company = 1;
            }
            else if (jobSheet.CompanyName == "George Holden & Associates, Inc")
            {
                entity.Company = 2;
            }
            else
            {
                entity.Company = 1;
            }
            entity.AccountName = jobSheet.AccountName;
            entity.AccountNumber = jobSheet.AccountNumber;
            entity.Status = 1; // passing default value 
            entity.ManagementApproved = jobSheet.ManagementApproved;
            entity.SetUpDate = jobSheet.SetUpDate;
            entity.TMCommissionPosition1Id = jobSheet.TMCommissionPosition1Id;
            entity.TMCommisionPosition1 = jobSheet.TMCommisionPosition1;

            if (jobSheet.TMCommissionPosition2Id == null || jobSheet.TMCommissionPosition2Id == 0)
            {
                entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition1Id;
                entity.TMCommissionPosition2 = jobSheet.TMCommisionPosition1;
            }
            else
            {
                entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition2Id;
                entity.TMCommissionPosition2 = jobSheet.TMCommissionPosition2;
            }

            //entity.TMCommissionPosition2Id = 1;
            entity.SubmittedBy = Convert.ToInt32(HttpContext.Current.Session["userId"]);
            entity.Owner = jobSheet.Owner ?? Convert.ToInt32(HttpContext.Current.Session["userId"]);
            entity.JobName = jobSheet.JobName;
            entity.CountyOfJob = jobSheet.CountyOfJob;
            entity.JobStreeAddress = jobSheet.JobStreeAddress;
            entity.JobState = jobSheet.JobState;
            entity.JobCity = jobSheet.JobCity;
            entity.JobZip = jobSheet.JobZip;
            entity.GeneralContractor = jobSheet.GeneralContractor;
            entity.GCMailingAddress = jobSheet.GCMailingAddress;
            entity.GCPhoneNumber = jobSheet.GCPhoneNumber;
            entity.GCCity = jobSheet.GCCity;
            entity.GCState = jobSheet.GCState;
            entity.GCZip = jobSheet.GCZip;
            entity.OwnerName = jobSheet.OwnerName;
            entity.OMailingAddress = jobSheet.OMailingAddress;
            entity.OPhoneNumber = jobSheet.OPhoneNumber;
            entity.OCity = jobSheet.OCity;
            entity.OState = jobSheet.OState;
            entity.OZip = jobSheet.OZip;
            if (entity.BondedJob == null)
                entity.BondedJob = "-None-";
            else
                entity.BondedJob = jobSheet.BondedJob;
            entity.BondInformation = jobSheet.BondInformation;
            entity.TaxExemption = jobSheet.TaxExemption;
            entity.TotalPurchases = jobSheet.TotalPurchases;
            entity.AnticipatedFirstDelivery = jobSheet.AnticipatedFirstDelivery;
            entity.AnticipatedJobClose = jobSheet.AnticipatedJobClose;
            entity.JobLine = jobSheet.JobLine;
            entity.PrelienAmount = jobSheet.PrelienAmount;
            entity.Prelien = jobSheet.Prelien;
            entity.PrelienCompany = jobSheet.PrelienCompany;
            entity.PrelienDate = jobSheet.PrelienDate;
            entity.Notes = jobSheet.Notes;
            entity.Engineer = jobSheet.Engineer;
            entity.SubmittedDate = jobSheet.SubmittedDate;

            if (jobSheet.JobSheetId > 0)
            {
                entity.IsActive = jobSheet.IsActive;

            }
            else
            {
                entity.CreatedDate = DateTime.Today;
                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.IsActive = true;
            }
            entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            entity.LastModifiedDate = DateTime.Today;
            entity.IsLockRecord = jobSheet.IsLockRecord;
            entity.IsCcgNotified = false;
            entity.VerifiedVoltage = jobSheet.VerifiedVoltage;
            entity.ProjectManager = jobSheet.ProjectManager;
            entity.ContractDate = jobSheet.ContractDate;
            entity.CustomerRequestedDeliveryDate = jobSheet.CustomerRequestedDeliveryDate;
            entity.VendorPromisedShipDate = jobSheet.VendorPromisedShipDate;
            if (jobSheet.VerifiedVoltage == true)
            {
                entity.VerifiedVoltageDoc = jobSheet.VerifiedVoltageDoc;
                entity.VerifiedVoltageDocOther = jobSheet.VerifiedVoltageDocOther;
                entity.VerifiedVoltageWhom = jobSheet.VerifiedVoltageWhom;
            }
            entity.Estimator = jobSheet.Estimator;
            entity.Material = jobSheet.Material;
            entity.Labor = jobSheet.Labor;
            entity.Returnage = jobSheet.Returnage;
            entity.Duration = jobSheet.Duration;
            entity.ReqShipToNumber = jobSheet.ReqShipToNumber;
            entity.SalesForceURL = jobSheet.SalesForceURL;

        }

        public IEnumerable<JobSheetViewModel> Read(int JobSheetId)
        {
            return GetAll(JobSheetId);
        }
        public IList<JobSheetViewModel> GetAll(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<JobSheetViewModel>();
            var resultStep = new List<JobSheetCompletedStepViewModel>();
            var entity = new JobSheetViewModel();

            try
            {
                var jobsheet = connection.JobSheet
                    .FirstOrDefault(p => p.JobSheetId == JobSheetId);

                resultStep = connection.JobSheetCompletedStep.Select(step => new JobSheetCompletedStepViewModel
                {
                    StatusId = step.StatusId,
                    JobSheetId = step.JobSheetId,
                    JobStepId = step.JobStepId,
                    CreatedBy = step.CreatedBy,
                    CreatedDate = step.CreatedDate,
                    LastModifiedBy = step.LastModifiedBy,
                    LastModifiedDate = step.LastModifiedDate,
                    IsActive = step.IsActive
                }).Where(p => (p.JobSheetId == JobSheetId) && (p.IsActive == true)).ToList();

                foreach (var steps in resultStep)
                {
                    if (steps.JobStepId == 7)
                    {
                        entity.SubmittedtoCreditValue = steps.IsActive;
                    }
                    else if (steps.JobStepId == 8)
                    {
                        entity.SubmittedtoCreditValue = steps.IsActive;
                    }
                    else if (steps.JobStepId == 11)
                    {
                        entity.SubmittedtoCreditValue = steps.IsActive;
                    }
                    else if (steps.JobStepId == 12)
                    {
                        entity.SubmittedtoCreditValue = steps.IsActive;
                    }
                    //else if(steps.JobStepId == 13)
                    //{
                    //    entity.SubmittedtoCreditValue = steps.IsActive;
                    //}
                    //else if(steps.JobStepId == 14)
                    //{
                    //    entity.SubmittedtoCreditValue = steps.IsActive;
                    //}
                    else if (steps.JobStepId == 15)
                    {
                        entity.SubmittedtoCreditValue = steps.IsActive;
                    }

                }

                var LastUpdatedStep = connection.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8 || u.JobStepId == 11 || u.JobStepId == 12 || u.JobStepId == 13 || u.JobStepId == 14 || u.JobStepId == 15)) && (u.IsActive == true) && (u.JobSheetId == JobSheetId)).OrderByDescending(x => x.LastModifiedDate).FirstOrDefault();


                if (LastUpdatedStep != null)
                    entity.LastUpdatedStep = LastUpdatedStep.JobStepId;

                if (jobsheet != null)
                {
                    MapProject(jobsheet, entity);
                }

                var ownerName = connection.User.Where(n => n.UserId == entity.Owner);
                var creditRepName = connection.User.Where(n => n.UserId == entity.ManagerApproval).ToList();
                //entity.OwnerName = ownerName.First().SMSN.Name;
                if (creditRepName.Count != 0)
                    entity.creditRepName = creditRepName.First().SMSN.Name;
                else
                    entity.creditRepName = string.Empty;
                result.Add(entity);
            }

            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAll method");
            }

            return result;
        }
        private static void MapProject(JobSheet jobSheet, JobSheetViewModel entity)
        {

            entity.JobSheetId = jobSheet.JobSheetId;
            entity.JobSheetName = jobSheet.JobSheetName;
            entity.CompanyName = jobSheet.CompanyName;
            entity.AccountName = jobSheet.AccountName;
            entity.AccountNumber = jobSheet.AccountNumber;
            entity.Status = 1; // passing default value 
            entity.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
            entity.ManagementApproved = jobSheet.ManagementApproved;
            entity.SetUpDate = jobSheet.SetUpDate;
            entity.TMCommissionPosition1Id = jobSheet.TMCommissionPosition1Id;
            entity.TMCommisionPosition1 = jobSheet.TMCommisionPosition1;
            entity.TMCommissionPosition2Id = jobSheet.TMCommissionPosition2Id;
            entity.TMCommissionPosition2 = jobSheet.TMCommissionPosition2;
            entity.ManagerApproval = jobSheet.ManagerApproval;
            entity.ManagerApprovedDate = jobSheet.ManagerApprovedDate;
            entity.SubmittedBy = jobSheet.SubmittedBy;
            entity.Owner = jobSheet.Owner ?? Convert.ToInt32(HttpContext.Current.Session["userId"]);
            entity.CreditRep = jobSheet.CreditRep;
            entity.JobName = jobSheet.JobName;
            entity.CountyOfJob = jobSheet.CountyOfJob;
            entity.JobStreeAddress = jobSheet.JobStreeAddress;
            entity.JobState = jobSheet.JobState;
            entity.JobCity = jobSheet.JobCity;
            entity.JobZip = jobSheet.JobZip;
            entity.GeneralContractor = jobSheet.GeneralContractor;
            entity.GCMailingAddress = jobSheet.GCMailingAddress;
            entity.GCPhoneNumber = jobSheet.GCPhoneNumber;
            entity.GCCity = jobSheet.GCCity;
            entity.GCState = jobSheet.GCState;
            entity.GCZip = jobSheet.GCZip;
            entity.OwnerName = jobSheet.OwnerName;
            entity.OMailingAddress = jobSheet.OMailingAddress;
            entity.OPhoneNumber = jobSheet.OPhoneNumber;
            entity.OCity = jobSheet.OCity;
            entity.OState = jobSheet.OState;
            entity.OZip = jobSheet.OZip;
            entity.BondedJob = jobSheet.BondedJob;
            entity.BondInformation = jobSheet.BondInformation;
            entity.TaxExemption = jobSheet.TaxExemption;
            entity.TotalPurchases = jobSheet.TotalPurchases;
            entity.AnticipatedFirstDelivery = jobSheet.AnticipatedFirstDelivery;
            entity.AnticipatedJobClose = jobSheet.AnticipatedJobClose;
            entity.JobLine = jobSheet.JobLine;
            entity.PrelienAmount = jobSheet.PrelienAmount;
            entity.Prelien = jobSheet.Prelien;
            entity.PrelienCompany = jobSheet.PrelienCompany;
            entity.PrelienDate = jobSheet.PrelienDate;
            entity.Notes = jobSheet.Notes;
            entity.Engineer = jobSheet.Engineer;
            entity.SubmittedDate = jobSheet.SubmittedDate;

            if (jobSheet.JobSheetId > 0)
            {
                entity.IsActive = jobSheet.IsActive;
            }
            else
            {
                entity.CreatedDate = DateTime.Today;
                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.IsActive = true;
            }
            entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            entity.LastModifiedDate = DateTime.Today;
            entity.IsLockRecord = jobSheet.IsLockRecord;
            entity.IsCcgNotified = false;
            entity.VerifiedVoltage = jobSheet.VerifiedVoltage;
            entity.ProjectManager = jobSheet.ProjectManager;
            entity.ContractDate = jobSheet.ContractDate;
            entity.CustomerRequestedDeliveryDate = jobSheet.CustomerRequestedDeliveryDate;
            entity.VendorPromisedShipDate = jobSheet.VendorPromisedShipDate;
            if (jobSheet.VerifiedVoltage == true)
            {
                entity.VerifiedVoltageDoc = jobSheet.VerifiedVoltageDoc;
                entity.VerifiedVoltageDocOther = jobSheet.VerifiedVoltageDocOther;
                entity.VerifiedVoltageWhom = jobSheet.VerifiedVoltageWhom;
            }
            entity.CreditNotes = jobSheet.CreditNotes;
            entity.Estimator = jobSheet.Estimator;
            entity.Material = jobSheet.Material;
            entity.Labor = jobSheet.Labor;
            entity.Returnage = jobSheet.Returnage;
            entity.Duration = jobSheet.Duration;
            entity.RequestedShipToNumber = jobSheet.ReqShipToNumber;
            entity.SalesForceURL = jobSheet.SalesForceURL;
        }
        public IEnumerable<JobOrderViewModel> OrdersRead(string ShipTo, int CustNo)
        {
            return GetAllOrders(ShipTo, CustNo);
        }

        public IList<JobOrderViewModel> GetAllOrders(string ShipTo, int CustNo)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<JobOrderViewModel>();

            try
            {
                result = connection.JobOrders.Select(jobOrder => new JobOrderViewModel
                {
                    OrderId = jobOrder.OrderId,
                    OrderNo = jobOrder.OrderNo,
                    OrderSurf = jobOrder.OrderSurf,
                    CustNo = jobOrder.CustNo,
                    ShipTo = jobOrder.ShipTo,
                    CustPO = jobOrder.CustPO,
                    InvoiceNo = jobOrder.InvoiceNo,
                    TotalOrderAmount = jobOrder.TotalOrderAmount,
                    OrderCreatedDate = jobOrder.OrderCreatedDate,
                    InvoiceDate = jobOrder.InvoiceDate,
                    CPSD = jobOrder.JobOrderShipDates.OrderByDescending(sd => sd.ModifiedDate).FirstOrDefault().CPSD,
                    ManualUpdate = jobOrder.JobOrderShipDates.OrderByDescending(sd => sd.ModifiedDate).FirstOrDefault().User1.SMSN.Name != "System Account",
                    CRSD = jobOrder.JobOrderShipDates.OrderByDescending(sd => sd.ModifiedDate).FirstOrDefault().CRSD,
                    PrevCPSD = jobOrder.JobOrderShipDates.OrderByDescending(sd => sd.ModifiedDate).FirstOrDefault().PrevCPSD
                }).Where(p => (p.ShipTo == ShipTo) && (p.CustNo == CustNo)).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAllOrders method");
            }

            return result;
        }
        public void VerifiedVoltageUpdate(JobSheetViewModel jobSheet)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var dbRec = connection.JobSheet
                       .Include(p => p.JobSteps)
                       .FirstOrDefault(p => p.JobSheetId == jobSheet.id);

                if (dbRec != null)
                {
                    //Save into Audit table for voltage change 

                    CreateAuditTrailVerifiedVoltage(dbRec, jobSheet);


                    var jobsheet = (from x in connection.JobSheet
                                    where x.JobSheetId == jobSheet.id
                                    select x).First();
                    jobsheet.VerifiedVoltage = jobSheet.VerifiedVoltageVal;
                    jobsheet.VerifiedVoltageDoc = jobSheet.VerifiedVoltageDoc1;
                    jobsheet.VerifiedVoltageWhom = jobSheet.VerifiedVoltageWhom1;
                    jobsheet.VerifiedVoltageDocURL = jobSheet.VerifiedVoltageDocURL;
                    connection.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Verified Voltage Update action");
            }

        }
        public void VerifiedVoltageUpdateAjax(VerifyVoltageModel vvModel)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var dbRec = connection.JobSheet
                    .Include(p => p.JobSteps)
                    .FirstOrDefault(p => p.JobSheetId == vvModel.id);

                if (dbRec != null)
                {
                    //Save into Audit table for voltage change 

                    CreateAuditTrailVerifiedVoltageAjax(dbRec, vvModel);


                    var jobsheet = (from x in connection.JobSheet
                                    where x.JobSheetId == vvModel.id
                                    select x).First();
                    jobsheet.VerifiedVoltage = vvModel.VerifiedVoltageVal;
                    jobsheet.VerifiedVoltageDoc = vvModel.VerifiedVoltageDoc1;
                    jobsheet.VerifiedVoltageDocOther = vvModel.VerifiedVoltageDocOther1;
                    jobsheet.VerifiedVoltageWhom = vvModel.VerifiedVoltageWhom1;
                    jobsheet.VerifiedVoltageCreatedDate = DateTime.Today;
                    jobsheet.IsVerifiedVoltageReq = vvModel.IsVerifiedVoltageReq;
                    connection.SaveChanges();

                    if (vvModel.uploadedFiles != null && vvModel.uploadedFiles.ContentLength > 0)
                    {
                        Web WebClient;
                        using (var clientContext = new ClientContext("https://mingledorffs.sharepoint.com/InformationTechnology"))
                        {
                            var securePassword = new SecureString();
                            foreach (var c in "Pr0jMgm+!")
                            {
                                securePassword.AppendChar(c);
                            }

                            clientContext.Credentials = new SharePointOnlineCredentials("projectmanagement@Mingledorffs.onmicrosoft.com", securePassword);
                            WebClient = clientContext.Web;
                            using (var fs = vvModel.uploadedFiles.InputStream)
                            {
                                clientContext.Load(WebClient, website => website.Lists, website => website.ServerRelativeUrl);
                                clientContext.ExecuteQuery();
                                var fi = new FileInfo(vvModel.uploadedFiles.FileName);
                                var folder =
                                    WebClient.GetFolderByServerRelativeUrl(
                                        "Shared Documents/Software Development/PM Application/VoltageVerificationUpload");
                                clientContext.Load(folder);
                                clientContext.ExecuteQuery();
                                var fileUrl = string.Format("{0}/{1}", folder.ServerRelativeUrl, fi.Name);

                                Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, fs, true);

                                //string url = HttpContext.Current.Request.Url.AbsoluteUri;
                                jobsheet.VerifiedVoltageDocURL = "https://mingledorffs.sharepoint.com" + fileUrl;
                                connection.SaveChanges();

                            }
                        }
                    }
                    else
                    {
                        jobsheet.VerifiedVoltageDocURL = string.Empty;
                        connection.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Verified Voltage Update action");
            }

        }
        private void CreateAuditTrailVerifiedVoltage(JobSheet oldrec, JobSheetViewModel jobSheet)
        {
            try
            {
                if (oldrec.VerifiedVoltage != jobSheet.VerifiedVoltageVal)
                {
                    VoltageChangeAudit(jobSheet.id, "Verified Voltage Value", oldrec.VerifiedVoltage.ToString(), jobSheet.VerifiedVoltageVal.ToString());
                }
                if (oldrec.VerifiedVoltageDoc != jobSheet.VerifiedVoltageDoc1)
                {
                    var VerifiedVoltageDocOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageDoc) ? "N/A" : oldrec.VerifiedVoltageDoc;
                    var VerifiedVoltageDocNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageDoc1) ? "N/A" : jobSheet.VerifiedVoltageDoc1;
                    VoltageChangeAudit(jobSheet.id, "Verified Voltage Document", VerifiedVoltageDocOld, VerifiedVoltageDocNew);

                }
                if (oldrec.VerifiedVoltageDocOther != jobSheet.VerifiedVoltageDocOther1)
                {
                    var VerifiedVoltageDocOtherOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageDocOther) ? "N/A" : oldrec.VerifiedVoltageDoc;
                    var VerifiedVoltageDocOtherNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageDocOther1) ? "N/A" : jobSheet.VerifiedVoltageDocOther1;
                    VoltageChangeAudit(jobSheet.id, "Voltage Document Other", VerifiedVoltageDocOtherOld, VerifiedVoltageDocOtherNew);

                }
                if (oldrec.VerifiedVoltageWhom != jobSheet.VerifiedVoltageWhom1)
                {
                    var VerifiedVoltageWhomOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageWhom) ? "N/A" : oldrec.VerifiedVoltageWhom;
                    var VerifiedVoltageWhomNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageWhom1) ? "N/A" : jobSheet.VerifiedVoltageWhom1;
                    VoltageChangeAudit(jobSheet.id, "Voltage From Whom", VerifiedVoltageWhomOld, VerifiedVoltageWhomNew);

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Shipto class CreateausitTrail method");
            }
        }
        private void CreateAuditTrailVerifiedVoltageAjax(JobSheet oldrec, VerifyVoltageModel jobSheet)
        {
            try
            {
                if (oldrec.VerifiedVoltage != jobSheet.VerifiedVoltageVal)
                {
                    VoltageChangeAudit(jobSheet.id, "Verified Voltage Value", oldrec.VerifiedVoltage.ToString(), jobSheet.VerifiedVoltageVal.ToString());
                }
                if (oldrec.VerifiedVoltageDoc != jobSheet.VerifiedVoltageDoc1)
                {
                    var VerifiedVoltageDocOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageDoc) ? "N/A" : oldrec.VerifiedVoltageDoc;
                    var VerifiedVoltageDocNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageDoc1) ? "N/A" : jobSheet.VerifiedVoltageDoc1;
                    VoltageChangeAudit(jobSheet.id, "Verified Voltage Document", VerifiedVoltageDocOld, VerifiedVoltageDocNew);

                }
                if (oldrec.VerifiedVoltageDocOther != jobSheet.VerifiedVoltageDocOther1)
                {
                    var VerifiedVoltageDocOtherOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageDocOther) ? "N/A" : oldrec.VerifiedVoltageDoc;
                    var VerifiedVoltageDocOtherNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageDocOther1) ? "N/A" : jobSheet.VerifiedVoltageDocOther1;
                    VoltageChangeAudit(jobSheet.id, "Voltage Document Other", VerifiedVoltageDocOtherOld, VerifiedVoltageDocOtherNew);

                }
                if (oldrec.VerifiedVoltageWhom != jobSheet.VerifiedVoltageWhom1)
                {
                    var VerifiedVoltageWhomOld = string.IsNullOrEmpty(oldrec.VerifiedVoltageWhom) ? "N/A" : oldrec.VerifiedVoltageWhom;
                    var VerifiedVoltageWhomNew = string.IsNullOrEmpty(jobSheet.VerifiedVoltageWhom1) ? "N/A" : jobSheet.VerifiedVoltageWhom1;
                    VoltageChangeAudit(jobSheet.id, "Voltage From Whom", VerifiedVoltageWhomOld, VerifiedVoltageWhomNew);

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in Shipto class CreateausitTrail method");
            }
        }

        private void VoltageChangeAudit(int JobSheetId, string AttributeName, string PreviousVal, string NewVal)
        {
            connection = new CommPmApplicationConnection();

            var changeLogEntity = new JobSheetChangeLog();

            changeLogEntity.ModifiedByUserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            changeLogEntity.ChangeDate = System.DateTime.Now;
            changeLogEntity.JobSheetId = JobSheetId;
            changeLogEntity.IsActive = true;
            changeLogEntity.AttributeName = AttributeName;
            changeLogEntity.PreviousVal = PreviousVal;
            changeLogEntity.NewVal = NewVal;
            changeLogEntity.Description = System.DateTime.Now + " " + PreviousVal + " Changed to " + NewVal + " by " + HttpContext.Current.Session["firstNameLastName"].ToString();

            connection.JobSheetChangeLog.Add(changeLogEntity);
            connection.SaveChanges();
        }

        public string UpdateStatusById(int JobSheetId, string Status, bool Checked)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var emailService = new EmailService(connection);
                var jobStepId = connection.JobSteps.Where(p => p.StepName.ToLower() == Status.ToLower()).Select(p => p.JobStepId).Single();

                var isStepExists = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == JobSheetId) && (p.JobStepId == jobStepId)).Count();

                if (jobStepId == 7)
                {
                    if (Checked)
                    {
                        //send Notification email 
                        updateSteps(JobSheetId, Checked, isStepExists, jobStepId);
                        JobSheetUpdateCreditJobsheet(JobSheetId);
                        emailService.SubmittedToCreditEmail(JobSheetId);
                        return "True";

                    }
                    else
                    {
                        updateSteps(JobSheetId, Checked, isStepExists, jobStepId);
                        JobSheetUpdateCreditJobsheet(JobSheetId);
                        return "True";

                    }
                }
                else if (jobStepId == 9)
                {
                    if (Checked)
                    {
                        var isVolatgeVerified = connection.JobSheet.Where(p => (p.JobSheetId == JobSheetId) && (p.VerifiedVoltage == true)).Count();

                        if (isVolatgeVerified > 0)
                        {
                            updateSteps(JobSheetId, Checked, isStepExists, jobStepId);
                            JobSheetUpdateCreditJobsheet(JobSheetId);
                            return "True";
                        }
                        else
                        {
                            //commented because voltage verification email notfication will go from Nightly job 
                            //emailService.VoltageVerificationEmail(JobSheetId);
                            return "NotVerified";
                        }
                    }
                    else
                    {
                        updateSteps(JobSheetId, Checked, isStepExists, jobStepId);
                        JobSheetUpdateCreditJobsheet(JobSheetId);
                        return "True";
                    }

                }
                else
                {
                    updateSteps(JobSheetId, Checked, isStepExists, jobStepId);
                    JobSheetUpdateCreditJobsheet(JobSheetId);
                    return "True";
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class UpdateStatusById method");
                return "False";

            }
        }

        private void updateSteps(int JobSheetId, bool Checked, int isStepExists, int jobStepId)
        {
            var emailService = new EmailService(connection);
            if (isStepExists > 0)
            {
                var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == JobSheetId) && (p.JobStepId == jobStepId));

                jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                if (Checked)
                {
                    jobSheetCompleted.IsActive = true;
                }
                else
                {
                    jobSheetCompleted.IsActive = false;
                }
                connection.SaveChanges();

                //lockUnlock(jobStepId, Checked, JobSheetId);

                CreateAuditTrailJobSteps(jobStepId, Checked, JobSheetId);
                emailService.UpdateJobStepEmail(JobSheetId, Checked, jobStepId);
            }
            else
            {
                var entity = new JobSheetCompletedStep();
                //entity.StatusId = statusId;
                entity.JobSheetId = JobSheetId;
                entity.JobStepId = jobStepId;
                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.CreatedDate = System.DateTime.Now;
                entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.LastModifiedDate = System.DateTime.Now;
                if (Checked)
                {
                    entity.IsActive = true;
                }
                else
                {
                    entity.IsActive = false;
                }

                connection.JobSheetCompletedStep.Add(entity);
                connection.SaveChanges();

                //lockUnlock(jobStepId, Checked, JobSheetId);

                CreateAuditTrailJobSteps(jobStepId, Checked, JobSheetId);
                emailService.UpdateJobStepEmail(JobSheetId, Checked, jobStepId);
            }


        }

        private void CreateAuditTrailJobSteps(int jobStepId, bool Checked, int jobSheetId)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var jobStepLogEntity = new JobStepLog();

                jobStepLogEntity.ModifyingUserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                jobStepLogEntity.DateModified = System.DateTime.Now;
                jobStepLogEntity.JobStepId = jobStepId;
                jobStepLogEntity.IsActive = true;
                jobStepLogEntity.StepStatus = Checked;
                jobStepLogEntity.JobSheetId = jobSheetId;

                connection.JobStepLog.Add(jobStepLogEntity);
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet Service CreateAuditTrailJobSteps method");
            }
        }

        private void lockUnlock(int jobStepId, bool Checked, int JobSheetId)
        {
            try
            {
                if (jobStepId == 7)
                {
                    connection = new CommPmApplicationConnection();
                    var jobSheets = connection.JobSheet.Where(p => p.JobSheetId == JobSheetId).FirstOrDefault();
                    //var entity = new JobSheet();
                    if (Checked)
                    {
                        jobSheets.JobSheetId = JobSheetId;
                        jobSheets.IsLockRecord = true;
                        connection.SaveChanges();
                    }
                    else
                    {
                        jobSheets.JobSheetId = JobSheetId;
                        jobSheets.IsLockRecord = false;
                        connection.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in lockUnlock ProjectsService");
            }
        }

        public IEnumerable<DashbordJobStatusViewModel> GetJobSheetStatusById(int JobSheetId)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<DashbordJobStatusViewModel>();

            try
            {
                var sheets = connection.JobSheet.Where(p => p.JobSheetId == JobSheetId);

                foreach (var sheet in sheets)
                {
                    var model = new DashbordJobStatusViewModel();

                    model.JobSheetId = sheet.JobSheetId;
                    model.JobSheetName = sheet.JobSheetName;
                    model.jobName = sheet.JobName;
                    model.AccountName = sheet.AccountName;
                    model.AccountNumber = sheet.AccountNumber;
                    model.JobOrShipToNumber = sheet.JobOrShipToNumber;
                    model.voltageVerified = sheet.VerifiedVoltage != null && sheet.VerifiedVoltage.Value;
                    model.subaccountCreated = sheet.JobOrShipToNumber != null && sheet.JobOrShipToNumber != "";
                    model.sheetDeactivated = sheet.IsActive == false;

                    model.voltageVerifiedURL = sheet.VerifiedVoltageDocURL;
                    //model.sheetCreated

                    var jobSheetStatus = connection.JobSheetCompletedStep.Where(p => p.JobSheetId == sheet.JobSheetId);

                    foreach (var jobStatus in jobSheetStatus)
                    {
                        if (jobStatus.JobSteps.StepName == "Pre-Submittal")
                        {
                            model.sheetCreated = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Submitted to Engineer")
                        {
                            model.submittedToContractor = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                model.submittedToContractorCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                model.submittedToContractorCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Reviewed by Engineer")
                        {
                            model.reviewedByContractor = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Approved by Engineer")
                        {
                            model.approvedByContractor = jobStatus.IsActive == true;

                        }

                        if (jobStatus.JobSteps.StepName == "Returned by Engineer")
                        {
                            model.returnedFromContractor = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                model.returnedFromContractorCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                model.returnedFromContractorCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Resubmitted to Engineer")
                        {
                            model.resubmittedToContractor = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Submitted to Credit")
                        {
                            model.submittedToCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                model.submittedToCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                model.submittedToCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Approved by Credit")
                        {
                            model.approvedByCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                model.approvedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                model.approvedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Rejected by Credit")
                        {
                            model.rejectedByCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                model.rejectedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                model.rejectedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Orders Created")
                        {
                            model.ordersCreated = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Job Closed")
                        {
                            model.jobclosed = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Job Synced")
                        {
                            model.jobSynced = jobStatus.IsActive == true;
                        }
                    }

                    result.Add(model);
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetStatus method");
                return result.ToList();

            }
        }

        public Dictionary<string, Dictionary<string, string>> GetJobAlert()
        {
            var result = new Dictionary<string, Dictionary<string, string>>();
            var pmIds = new List<int>();
            var outsidesalesrepIds = new List<int>();
            var managerpmIds = new List<int>();
            var Delegate = new List<int>();
            var mpmslsrep = new List<int>();
            var theList = new List<JobSheet>();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                connection = new CommPmApplicationConnection();

                //Filter for PM 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    Delegate = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();
                    Delegate.Add(userid);
                    pmIds = connection.User.Where(p => (p.IsActive == true) && (Delegate.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    var pmsusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    theList = allusers.Where(u => u.Company == conoid).ToList();
                    //theList = connection.JobSheet.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                }
                //Filter for Manager 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();
                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    theList = connection.JobSheet.Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.IsActive == true) && (u.Company == conoid)).OrderByDescending(u => u.CreatedDate).ToList();

                }
                //Filter for Otside Sales 
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.RoleId == 8) && (p.UserId == userid)).Select(p => p.UserId).ToList();
                    //theList = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var pmsusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = connection.JobSheet.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    theList = allusers.Where(u => u.Company == conoid).ToList();
                }
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var testList = connection.JobSheet.Where(u => u.IsActive == true).Where(u => u.CreatedBy == userid).OrderByDescending(u => u.CreatedDate).ToList();

                    theList = testList.Any() ? testList : connection.JobSheet.Where(u => u.IsActive == true).OrderByDescending(u => u.CreatedDate).ToList();
                }
                else
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var testList = connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => u.CreatedBy == userid).OrderByDescending(u => u.CreatedDate).ToList();

                    theList = testList.Any() ? testList : connection.JobSheet.Where(u => (u.IsActive == true) && (u.Company == conoid)).OrderByDescending(u => u.CreatedDate).ToList();
                }

                foreach (var JobSheet in theList)
                {
                    if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                    {
                        var values = new Dictionary<string, string>();
                        var orders = OrdersRead(JobSheet.JobOrShipToNumber, int.Parse(JobSheet.Customer.CustNo)).ToList();

                        if (orders.Any())
                        {
                            var AlertText = JobSheet.JobSheetId.ToString() + "-" + " " + JobSheet.JobSheetName + " - " + JobSheet.JobName + " has" + orders.Count().ToString() + " orders created and still does not have voltage verified.Please update voltage verification.";
                            connection = new CommPmApplicationConnection();
                            var AlertId = string.Empty;
                            var alertlist = connection.JobSheetAlert.Select(JobSheetAlert => new JobSheetAlertViewModel
                            {
                                AlertId = JobSheetAlert.AlertId,
                                AlertText = JobSheetAlert.AlertText,
                                AlertSnoozeDate = JobSheetAlert.AlertSnoozeDate,
                            }).Where(p => p.AlertText == AlertText).ToList();
                            if (alertlist.Count == 0)
                            {
                                AlertId = CreateJobSheetAlert(AlertText, JobSheet.JobSheetId).ToString();
                                values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                values.Add("NumberOfOrders", orders.Count().ToString());
                                values.Add("AlertId", AlertId);
                                result.Add("voltageVerified", values);
                                break;
                            }
                            else
                            {
                                foreach (var alert in alertlist)
                                {
                                    if (alert.AlertSnoozeDate < DateTime.Today || alert.AlertSnoozeDate == null)
                                    {
                                        values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                        values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                        values.Add("NumberOfOrders", orders.Count().ToString());
                                        values.Add("AlertId", alert.AlertId.ToString());
                                        result.Add("voltageVerified", values);

                                    }

                                }

                            }
                            break;
                        }
                    }
                }

                foreach (var JobSheet in theList)
                {
                    if (JobSheet.VendorPromisedShipDate != null && JobSheet.CustomerRequestedDeliveryDate != null &&
                        JobSheet.VendorPromisedShipDate.Value > JobSheet.CustomerRequestedDeliveryDate.Value)
                    {
                        var AlertText = JobSheet.JobSheetId.ToString() + "-" + " " + JobSheet.JobSheetName + " - " + JobSheet.JobName + " has a Promised Ship Date AFTER the Customer Request Date.";
                        connection = new CommPmApplicationConnection();

                        var alertlist = connection.JobSheetAlert.Select(JobSheetAlert => new JobSheetAlertViewModel
                        {
                            AlertId = JobSheetAlert.AlertId,
                            AlertText = JobSheetAlert.AlertText,
                            AlertSnoozeDate = JobSheetAlert.AlertSnoozeDate,
                        }).Where(p => p.AlertText == AlertText).ToList();

                        var AlertId = string.Empty;
                        if (alertlist.Count == 0)
                        {
                            AlertId = CreateJobSheetAlert(AlertText, JobSheet.JobSheetId).ToString();
                            var values = new Dictionary<string, string>
                            {
                                {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                {"AlertId", AlertId}
                            };

                            result.Add("shipDate", values);
                            break;
                        }
                        else
                        {
                            foreach (var alert in alertlist)
                            {
                                if (alert.AlertSnoozeDate < DateTime.Today || alert.AlertSnoozeDate == null)
                                {
                                    var values = new Dictionary<string, string>
                                    {
                                        {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                        {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                        {"AlertId", alert.AlertId.ToString()}
                                    };

                                    result.Add("shipDate", values);

                                }
                            }

                        }
                        break;
                    }
                }

                foreach (var JobSheet in theList)
                {
                    try
                    {
                        var jobStep = JobSheet.JobSheetCompletedStep.OrderByDescending(js => js.JobSheetId).ThenByDescending(js => js.CreatedDate).First();
                        if (jobStep.JobStepId != 0 && jobStep.JobStepId == 8)
                        {
                            var AlertText = JobSheet.JobSheetId.ToString() + "-" + " " + JobSheet.JobSheetName + " - " + JobSheet.JobName + " has been rejected by Credit.";
                            connection = new CommPmApplicationConnection();

                            var alertlist = connection.JobSheetAlert.Select(JobSheetAlert => new JobSheetAlertViewModel
                            {
                                AlertId = JobSheetAlert.AlertId,
                                AlertText = JobSheetAlert.AlertText,
                                AlertSnoozeDate = JobSheetAlert.AlertSnoozeDate,
                            }).Where(p => p.AlertText == AlertText).ToList();

                            var AlertId = string.Empty;
                            if (alertlist.Count == 0)
                            {
                                AlertId = CreateJobSheetAlert(AlertText, JobSheet.JobSheetId).ToString();
                                var values = new Dictionary<string, string>
                                {
                                    {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                    {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                    {"AlertId", AlertId}
                                };
                                result.Add("creditRejected", values);
                                break;

                            }
                            else
                            {
                                foreach (var alert in alertlist)
                                {
                                    if (alert.AlertSnoozeDate < DateTime.Today || alert.AlertSnoozeDate == null)
                                    {
                                        var values = new Dictionary<string, string>
                                        {
                                            {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                            {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                            {"AlertId", alert.AlertId.ToString()}
                                        };
                                        result.Add("creditRejected", values);

                                    }

                                }

                            }
                            break;
                        }
                    }
                    catch { }
                }
                foreach (var JobSheet in theList)
                {
                    var values = new Dictionary<string, string>();
                    var orders = OrdersRead(JobSheet.JobOrShipToNumber, int.Parse(JobSheet.Customer.CustNo)).ToList();
                    decimal TotalAmount = 0;

                    if (orders.Any())
                    {
                        foreach (var jobOrder in orders)
                        {
                            TotalAmount += Convert.ToDecimal(jobOrder.TotalOrderAmount);
                        }

                        var AlertText = JobSheet.JobSheetId.ToString() + "-" + " " + JobSheet.JobSheetName + " - " + JobSheet.JobName + " has total billed amount" + TotalAmount.ToString() + " is Higher than po amount $" + JobSheet.TotalPurchases.ToString();
                        connection = new CommPmApplicationConnection();

                        var alertlist = connection.JobSheetAlert.Select(JobSheetAlert => new JobSheetAlertViewModel
                        {
                            AlertId = JobSheetAlert.AlertId,
                            AlertText = JobSheetAlert.AlertText,
                            AlertSnoozeDate = JobSheetAlert.AlertSnoozeDate,
                        }).Where(p => p.AlertText == AlertText).ToList();

                        var AlertId = string.Empty;
                        if (alertlist.Count == 0)
                        {

                            if (TotalAmount > JobSheet.TotalPurchases)
                            {
                                AlertId = CreateJobSheetAlert(AlertText, JobSheet.JobSheetId).ToString();
                                values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                values.Add("TotalAmount", TotalAmount.ToString());
                                values.Add("POAmount", JobSheet.TotalPurchases.ToString());
                                values.Add("AlertId", AlertId);
                                result.Add("totalpoamount", values);
                            }
                            break;
                        }
                        else
                        {
                            foreach (var alert in alertlist)
                            {
                                if (alert.AlertSnoozeDate < DateTime.Today || alert.AlertSnoozeDate == null)
                                {
                                    if (TotalAmount > JobSheet.TotalPurchases)
                                    {
                                        values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                        values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                        values.Add("TotalAmount", TotalAmount.ToString());
                                        values.Add("POAmount", JobSheet.TotalPurchases.ToString());
                                        values.Add("AlertId", alert.AlertId.ToString());
                                        result.Add("totalpoamount", values);
                                    }

                                }
                            }

                        }
                        break;
                    }


                }

                foreach (var JobSheet in theList)
                {
                    var values = new Dictionary<string, string>();
                    var orders = OrdersRead(JobSheet.JobOrShipToNumber, int.Parse(JobSheet.Customer.CustNo)).ToList();
                    if (orders.Count != 0)
                    {

                        var OrderwithoutInvoice = orders.Where(x => (x.InvoiceDate >= DateTime.MinValue || x.InvoiceDate == null)).ToList();
                        if (OrderwithoutInvoice.Any())
                        {
                            foreach (var orderlist in OrderwithoutInvoice)
                            {

                                var AlertText = JobSheet.JobSheetId.ToString() + "-" + " " + JobSheet.JobSheetName + " - " + JobSheet.JobName + " has orders created" + "  " + "#" + " " + orderlist.OrderNo + "But still does not have Invoice Date";
                                connection = new CommPmApplicationConnection();
                                var AlertId = string.Empty;
                                var alertlist = connection.JobSheetAlert.Select(JobSheetAlert => new JobSheetAlertViewModel
                                {
                                    AlertId = JobSheetAlert.AlertId,
                                    AlertText = JobSheetAlert.AlertText,
                                    AlertSnoozeDate = JobSheetAlert.AlertSnoozeDate,
                                }).Where(p => p.AlertText == AlertText).ToList();
                                if (alertlist.Count == 0)
                                {
                                    AlertId = CreateJobSheetAlert(AlertText, JobSheet.JobSheetId).ToString();
                                    values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                    values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                    values.Add("OrderNo", orderlist.OrderNo.ToString());
                                    values.Add("AlertId", AlertId);
                                    result.Add("InvoiceVerified", values);
                                    break;
                                }
                                else
                                {
                                    foreach (var alert in alertlist)
                                    {
                                        if (alert.AlertSnoozeDate < DateTime.Today || alert.AlertSnoozeDate == null)
                                        {
                                            values.Add("JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName);
                                            values.Add("JobSheetId", JobSheet.JobSheetId.ToString());
                                            values.Add("OrderNo", orderlist.OrderNo.ToString());
                                            values.Add("AlertId", alert.AlertId.ToString());
                                            result.Add("InvoiceVerified", values);

                                        }

                                    }
                                }
                            }

                        }

                        break;


                    }



                }
            }
            catch (Exception e)
            {
                logger.Debug(e, "Error occured in JobSheet class GetJobAlert method");
            }

            return result;
        }
        private int CreateJobSheetAlert(string AlertText, int JobSheetId)
        {
            connection = new CommPmApplicationConnection();
            var JobSheetAlertEntity = new JobSheetAlert();
            JobSheetAlertEntity.UserId = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            JobSheetAlertEntity.AlertText = AlertText;
            JobSheetAlertEntity.JobSheetId = JobSheetId;
            connection.JobSheetAlert.Add(JobSheetAlertEntity);
            connection.SaveChanges();
            return JobSheetAlertEntity.AlertId;
        }

        public IEnumerable<SyncGridViewModel> GetSyncFailed()
        {
            var result = new List<SyncGridViewModel>();

            try
            {
                connection = new CommPmApplicationConnection();
                var theList = connection.JobSheet;

                foreach (var jobSheet in theList)
                {
                    var hasSynced = false;

                    if (!hasSynced)
                    {
                        result.Add(new SyncGridViewModel()
                        {
                            Customer = jobSheet.Customer.CustomerName,
                            JobName = jobSheet.JobName,
                            JobSheetId = jobSheet.JobSheetId,
                            SubAccount = jobSheet.ShipToName
                        });
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Error occured in JobSheet class GetSyncFailed method");
            }

            return result;
        }
        public IEnumerable<JobSheetViewModel> Readtop10Job()
        {
            return GetTop10Job();
        }


        public IList<JobSheetViewModel> GetTop10Job()
        {

            connection = new CommPmApplicationConnection();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());

                var jobSheets = connection.JobSheet.Select(jobSheetService => new JobSheetViewModel
                {
                    JobSheetId = jobSheetService.JobSheetId,
                    JobSheetName = jobSheetService.JobSheetName,
                    Company = jobSheetService.Company,
                    AccountName = jobSheetService.AccountName ?? "",
                    AccountNumber = jobSheetService.AccountNumber ?? "",
                    Customer = new CustomerViewModel()
                    {
                        CustNo = jobSheetService.Customer.CustNo ?? "",
                        CustomerName = jobSheetService.Customer.CustomerName ?? ""

                    },
                    JobOrShipToNumber = jobSheetService.JobOrShipToNumber ?? "",
                    ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : " - " + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    JobName = jobSheetService.JobName,
                    IsActive = jobSheetService.IsActive,
                    CreatedBy = jobSheetService.CreatedBy,
                    CreatedDate = jobSheetService.CreatedDate,
                    LastModifiedDate = jobSheetService.LastModifiedDate,
                    ProjectManager = jobSheetService.ProjectManager,
                    TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                    TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                    User3 = new JobSheetUserViewModel()
                    {
                        UserId = jobSheetService.User3.UserId,
                        SlsRep = jobSheetService.User3.SlsRep,
                        SMSN = new SMSNViewModel()
                        {
                            SlsRep = jobSheetService.User3.SMSN.SlsRep,
                            Name = jobSheetService.User3.SMSN.Name
                        }
                    }

                });
                //Filter for PM 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var delegatePMs = connection.DelegatePM.Where(p => (p.IsActive == true) && (p.DelegateUserId == userid)).Select(p => p.PMId).ToList();
                    var PMs = connection.User.Where(p => (p.IsActive == true) && (delegatePMs.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    var pmIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    var pmsusers = jobSheets.Where(u => u.IsActive == true).Where(u => PMs.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = jobSheets.Where(u => u.IsActive == true).Where(u => pmIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);
                    return allusers.Where(u => u.Company == conoid).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                    //return jobSheets.Where(u => u.IsActive == true).Where(u => PMs.Contains(u.ProjectManager)).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                }
                //Filter for Manager 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();
                    var mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();
                    return jobSheets.Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.IsActive == true)).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                }
                //Filter for Otside Sales 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    var pmsusers = jobSheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).OrderByDescending(u => u.CreatedDate).ToList();
                    var ossusers = jobSheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).OrderByDescending(u => u.CreatedDate).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    return allusers.Where(u => u.Company == conoid).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                    //return jobSheets.Where(u => u.IsActive == true).Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                }
                //Other 
                if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var testForUser = jobSheets.Where(u => u.IsActive == true).Where(u => u.CreatedBy == userid).Take(10).OrderByDescending(u => u.CreatedDate).ToList();

                    return testForUser.Any() ? testForUser : jobSheets.Where(u => u.IsActive == true).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                }
                else
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    var testForUser = jobSheets.Where(u => (u.IsActive == true) && (u.Company == conoid)).Where(u => u.CreatedBy == userid).Take(10).OrderByDescending(u => u.CreatedDate).ToList();

                    return testForUser.Any() ? testForUser : jobSheets.Where(u => (u.IsActive == true) && (u.Company == conoid)).Take(10).OrderByDescending(u => u.CreatedDate).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetTop10Job method");
                return new List<JobSheetViewModel>();
            }


        }
        public void ExcelExport(string jobSheetIds, string exportType, string emailAddress, string username)
        {
            //string sourceLoc = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data");
            var ExcelExportFilePath = System.Configuration.ConfigurationManager.AppSettings["ExcelExportFilePath"];
            var sourceLoc = Path.Combine(ExcelExportFilePath);
            //string sourceLoc = Path.Combine(HttpRuntime.AppDomainAppPath, "App_Data");
            var worksheetName = sourceLoc + @"\Report.xlsx";
            //GC.Collect();

            var emailService = new EmailService(connection);
            using (var document = SpreadsheetDocument.Create(worksheetName, SpreadsheetDocumentType.Workbook))
            {
                //Create Excel sheet 
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();
                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet();

                ////Added code to freeze job sheet name col
                var sheetViews = new SheetViews();
                var sheetView = new SheetView() { TabSelected = true, WorkbookViewId = (UInt32Value)0U };
                var pane = new Pane() { ActivePane = PaneValues.TopRight, HorizontalSplit = 1D, State = PaneStateValues.Frozen, TopLeftCell = "B1" };
                var selection = new Selection() { Pane = PaneValues.TopRight };
                sheetView.Append(pane);
                sheetView.Append(selection);
                sheetViews.Append(sheetView);

                ////Added code to freeze header
                var sheetView1 = new SheetView() { TabSelected = true, WorkbookViewId = (UInt32Value)0U };
                var pane1 = new Pane() { VerticalSplit = 1D, TopLeftCell = "A2", ActivePane = PaneValues.BottomLeft, State = PaneStateValues.Frozen };
                var selection1 = new Selection() { Pane = PaneValues.TopRight };
                sheetView1.Append(pane1);
                sheetView1.Append(selection1);
                sheetViews.Append(sheetView1);

                var stylePart = workbookPart.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var columns = ColWidth();
                worksheetPart.Worksheet.Append(sheetViews);
                worksheetPart.Worksheet.AppendChild(columns);


                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                var sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "PM Tracker Project" };
                sheets.Append(sheet);
                workbookPart.Workbook.Save();

                connection = new CommPmApplicationConnection();

                var jobId = string.Empty;
                jobId = jobSheetIds.Replace(@"[", "");
                jobId = jobId.Replace(@"]", "");
                var result = connection.stp_getDataForHVACReport(jobId).ToList();
                var sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                // Constructing header
                var row = new Row();

                row.Append(
                    ConstructCell("Project Name" ?? "", CellValues.String),
                    ConstructCell("Account Name" ?? "", CellValues.String),
                    ConstructCell("Account Number" ?? "", CellValues.String),
                    ConstructCell("Job Or ShipTo Number" ?? "", CellValues.String),
                    ConstructCell("ManagementApproved" ?? "", CellValues.String),
                    ConstructCell("Set Up Date" ?? "", CellValues.String),
                    ConstructCell("OutSide Sales Representative" ?? "", CellValues.String),
                    ConstructCell("Manager Approval" ?? "", CellValues.String),
                    ConstructCell("Manager Approved Date" ?? "", CellValues.String),
                    ConstructCell("SubmittedBy" ?? "", CellValues.String),
                    ConstructCell("Owner" ?? "", CellValues.String),
                    ConstructCell("Owner Mailing Address" ?? "", CellValues.String),
                    ConstructCell("Owner Phone Number" ?? "", CellValues.String),
                    ConstructCell("Owner City" ?? "", CellValues.String),
                    ConstructCell("Owner State" ?? "", CellValues.String),
                    ConstructCell("Owner Zip" ?? "", CellValues.String),
                    ConstructCell("Credit Representative" ?? "", CellValues.String),
                    ConstructCell("Project Name" ?? "", CellValues.String),
                    ConstructCell("County Of Project" ?? "", CellValues.String),
                    ConstructCell("Project Address" ?? "", CellValues.String),
                    ConstructCell("Project City" ?? "", CellValues.String),
                    ConstructCell("Project Zip" ?? "", CellValues.String),
                    ConstructCell("General Contractor" ?? "", CellValues.String),
                    ConstructCell("General Contractor Mailing Address" ?? "", CellValues.String),
                    ConstructCell("General Contractor Phone Number" ?? "", CellValues.String),
                    ConstructCell("General Contractor City" ?? "", CellValues.String),
                    ConstructCell("General Contractor State" ?? "", CellValues.String),
                    ConstructCell("General Contractor Zip" ?? "", CellValues.String),
                    ConstructCell("Bonded Job?" ?? "", CellValues.String),
                    ConstructCell("Bond Information" ?? "", CellValues.String),
                    ConstructCell("Tax Exempt?" ?? "", CellValues.String),
                    ConstructCell("Total of Mingledorff's purchases w/o tax $" ?? "", CellValues.String),
                    ConstructCell("Anticipated First Delivery" ?? "", CellValues.String),
                    ConstructCell("Anticipated Job Close" ?? "", CellValues.String),
                    ConstructCell("Job Line" ?? "", CellValues.String),
                    ConstructCell("Prelien Amount $" ?? "", CellValues.String),
                    ConstructCell("Prelien ?" ?? "", CellValues.String),
                    ConstructCell("Prelien Company" ?? "", CellValues.String),
                    ConstructCell("Prelien Date" ?? "", CellValues.String),
                    ConstructCell("Notes" ?? "", CellValues.String),
                    ConstructCell("Submitted Date" ?? "", CellValues.String),
                    ConstructCell("Created By" ?? "", CellValues.String),
                    ConstructCell("Created Date" ?? "", CellValues.String),
                    ConstructCell("LastModified By" ?? "", CellValues.String),
                    ConstructCell("LastModified Date" ?? "", CellValues.String),
                    ConstructCell("IsLockRecord" ?? "", CellValues.String),
                    ConstructCell("Company Name" ?? "", CellValues.String),
                    ConstructCell("ShipTo Name" ?? "", CellValues.String),
                    ConstructCell("POAmount $" ?? "", CellValues.String),
                    ConstructCell("OrderAmount $" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltage" ?? "", CellValues.String),
                    ConstructCell("ShipTo Address" ?? "", CellValues.String),
                    ConstructCell("Project Manager" ?? "", CellValues.String),
                    ConstructCell("Contract Date" ?? "", CellValues.String),
                    ConstructCell("Customer Requested Delivery Date" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltage Doc" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltageWhom" ?? "", CellValues.String),
                    ConstructCell("Vendor PromisedShipDate" ?? "", CellValues.String),
                    ConstructCell("CreditNotes" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltageDocURL" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltage CreatedDate" ?? "", CellValues.String),
                    ConstructCell("VerifiedVoltage ModifiedDate" ?? "", CellValues.String),
                    ConstructCell("Estimator" ?? "", CellValues.String),
                    ConstructCell("IsVerifiedVoltageReq" ?? "", CellValues.String),
                    ConstructCell("Material" ?? "", CellValues.String),
                    ConstructCell("Labor" ?? "", CellValues.String),
                    ConstructCell("Returnage" ?? "", CellValues.String),
                    ConstructCell("Duration" ?? "", CellValues.String),
                    //order info 
                    ConstructCell("Order No" ?? "", CellValues.String),
                    ConstructCell("Order Suff" ?? "", CellValues.String),
                    ConstructCell("Cust Name" ?? "", CellValues.String),
                    ConstructCell("ShipTo" ?? "", CellValues.String),
                    ConstructCell("Cust PO" ?? "", CellValues.String),
                    ConstructCell("Invoice No" ?? "", CellValues.String),
                    ConstructCell("Total Order Amount $" ?? "", CellValues.String),
                    ConstructCell("Order CreatedDate" ?? "", CellValues.String),
                    ConstructCell("Invoice Date" ?? "", CellValues.String),
                    //orderline info 
                    ConstructCell("Order No" ?? "", CellValues.String),
                    ConstructCell("Order Suff" ?? "", CellValues.String),
                    ConstructCell("Line Number" ?? "", CellValues.String),
                    ConstructCell("Model Part" ?? "", CellValues.String),
                    ConstructCell("PO Amount $" ?? "", CellValues.String),
                    ConstructCell("Quantity Ordered" ?? "", CellValues.String),
                    ConstructCell("Quantity Shipped" ?? "", CellValues.String),
                    ConstructCell("Customer Requested Delivery Date" ?? "", CellValues.String),
                    ConstructCell("Vendor Name" ?? "", CellValues.String),
                    ConstructCell("Verified Voltage" ?? "", CellValues.String),
                    ConstructCell("Cust Name" ?? "", CellValues.String),
                    ConstructCell("ShipTo" ?? "", CellValues.String),
                    ConstructCell("Vendor Number" ?? "", CellValues.String),
                    ConstructCell("Order Amount $" ?? "", CellValues.String),
                    ConstructCell("Net Amount $" ?? "", CellValues.String),
                    ConstructCell("Order AltNo" ?? "", CellValues.String),
                    ConstructCell("Order Line CreatedDate" ?? "", CellValues.String),
                    //CarrierSapReportHeader
                    ConstructCell("Carrier Sales Order" ?? "", CellValues.String),
                    ConstructCell("Carrier Purchase Order" ?? "", CellValues.String),
                    ConstructCell("Carrier Last Changed" ?? "", CellValues.String),
                    ConstructCell("Carrier Consignee" ?? "", CellValues.String),
                    ConstructCell("Carrier Overall OrderStatus" ?? "", CellValues.String),
                    ConstructCell("Carrier Date Entered" ?? "", CellValues.String),
                    //CarrierSapReportLineItems
                    ConstructCell("Carrier Sales Order" ?? "", CellValues.String),
                    ConstructCell("Carrier Line Number" ?? "", CellValues.String),
                    ConstructCell("Carrier Material" ?? "", CellValues.String),
                    ConstructCell("Carrier Description" ?? "", CellValues.String),
                    ConstructCell("Carrier Order Qty" ?? "", CellValues.String),
                    ConstructCell("Carrier Shipped Qty" ?? "", CellValues.String),
                    ConstructCell("Carrier CRSD" ?? "", CellValues.String),
                    ConstructCell("Carrier CPSD" ?? "", CellValues.String),
                    ConstructCell("Carrier Previous CPSD" ?? "", CellValues.String),
                    ConstructCell("Carrier Rate" ?? "", CellValues.String),
                    ConstructCell("Carrier Net Price" ?? "", CellValues.String),
                    ConstructCell("Carrier Item Value" ?? "", CellValues.String)

                    );
                sheetData.AppendChild(row);

                var ManagementApproved = string.Empty;
                var SubmittedByName = string.Empty;
                var ManagerApprovalName = string.Empty;
                var CreditRepName = string.Empty;
                var OwnerName = string.Empty;
                var CreatedByName = string.Empty;
                var LastModifiedByName = string.Empty;
                var ProjectManagerName = string.Empty;
                var VendorName = string.Empty;
                var CustName = string.Empty;

                foreach (var selectedItem in result)
                {

                    if (selectedItem.ManagementApproved == true)
                        ManagementApproved = "Approved";
                    else
                        ManagementApproved = "Not Approved";

                    if (selectedItem.SubmittedBy != 0)
                        SubmittedByName = getUserName(selectedItem.SubmittedBy).ToString();

                    if (selectedItem.ManagerApproval != null)
                        ManagerApprovalName = getUserName(Convert.ToInt32(selectedItem.ManagerApproval)).ToString();

                    if (selectedItem.CreditRep != 0)
                        CreditRepName = getUserName(Convert.ToInt32(selectedItem.CreditRep)).ToString();

                    if (selectedItem.Owner != 0)
                        OwnerName = getUserName(Convert.ToInt32(selectedItem.Owner)).ToString();

                    if (selectedItem.CreatedBy != 0)
                        CreatedByName = getUserName(Convert.ToInt32(selectedItem.CreatedBy)).ToString();

                    if (selectedItem.LastModifiedBy != 0)
                        LastModifiedByName = getUserName(Convert.ToInt32(selectedItem.LastModifiedBy)).ToString();

                    if (selectedItem.ProjectManager != 0)
                        ProjectManagerName = getUserName(Convert.ToInt32(selectedItem.ProjectManager)).ToString();

                    if (selectedItem.ProjectOrderLine_VendorNumber != null)
                        VendorName = geVendorName(Convert.ToInt32(selectedItem.ProjectOrderLine_VendorNumber)).ToString();

                    row = new Row();

                    row.Append(
                        ConstructCell(selectedItem.JobSheetName ?? "", CellValues.String),
                        ConstructCell(selectedItem.AccountName ?? "", CellValues.String),
                        ConstructCell(selectedItem.AccountNumber ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobOrShipToNumber ?? "", CellValues.String),
                        ConstructCell(ManagementApproved ?? "", CellValues.String),
                        ConstructCell(selectedItem.SetUpDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.TMCommisionPosition1 ?? "", CellValues.String),
                        ConstructCell(ManagerApprovalName ?? "", CellValues.String),
                        ConstructCell(selectedItem.ManagerApprovedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(SubmittedByName.ToString() ?? "", CellValues.String),
                        ConstructCell(OwnerName ?? "", CellValues.String),
                        ConstructCell(selectedItem.OMailingAddress ?? "", CellValues.String),
                        ConstructCell(selectedItem.OPhoneNumber ?? "", CellValues.String),
                        ConstructCell(selectedItem.OCity ?? "", CellValues.String),
                        ConstructCell(selectedItem.OState ?? "", CellValues.String),
                        ConstructCell(selectedItem.OZip.ToString() ?? "", CellValues.String),
                        ConstructCell(CreditRepName ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobName ?? "", CellValues.String),
                        ConstructCell(selectedItem.CountyOfJob ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobStreeAddress ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobCity ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobZip.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.GeneralContractor ?? "", CellValues.String),
                        ConstructCell(selectedItem.GCMailingAddress ?? "", CellValues.String),
                        ConstructCell(selectedItem.GCPhoneNumber ?? "", CellValues.String),
                        ConstructCell(selectedItem.GCCity ?? "", CellValues.String),
                        ConstructCell(selectedItem.GCState ?? "", CellValues.String),
                        ConstructCell(selectedItem.GCZip.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.BondedJob ?? "", CellValues.String),
                        ConstructCell(selectedItem.BondInformation ?? "", CellValues.String),
                        ConstructCell(selectedItem.TaxExemption ?? "", CellValues.String),
                        ConstructCell(selectedItem.TotalPurchases.ToString() == string.Empty ? "" : selectedItem.TotalPurchases.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.AnticipatedFirstDelivery.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.AnticipatedJobClose.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobLine ?? "", CellValues.String),
                        ConstructCell(selectedItem.PrelienAmount.ToString() == string.Empty ? "" : selectedItem.PrelienAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.Prelien ?? "", CellValues.String),
                        ConstructCell(selectedItem.PrelienCompany ?? "", CellValues.String),
                        ConstructCell(selectedItem.PrelienDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Notes ?? "", CellValues.String),
                        ConstructCell(selectedItem.SubmittedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(CreatedByName ?? "", CellValues.String),
                        ConstructCell(selectedItem.CreatedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(LastModifiedByName ?? "", CellValues.String),
                        ConstructCell(selectedItem.LastModifiedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.IsLockRecord.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CompanyName ?? "", CellValues.String),
                        ConstructCell(selectedItem.ShipToName ?? "", CellValues.String),
                        ConstructCell(selectedItem.JobSheet_POAmount.ToString() == string.Empty ? "" : selectedItem.JobSheet_POAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.OrderAmount.ToString() == string.Empty ? "" : selectedItem.OrderAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltage.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ShipToAddress ?? "", CellValues.String),
                        ConstructCell(ProjectManagerName ?? "", CellValues.String),
                        ConstructCell(selectedItem.ContractDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CustomerRequestedDeliveryDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltageDoc ?? "", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltageWhom ?? "", CellValues.String),
                        ConstructCell(selectedItem.VendorPromisedShipDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CreditNotes ?? "", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltageDocURL ?? "", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltageCreatedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.VerifiedVoltageModifiedDate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Estimator ?? "", CellValues.String),
                        ConstructCell(selectedItem.IsVerifiedVoltageReq.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Material.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Labor.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Returnage.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.Duration.ToString() ?? "", CellValues.String),
                        //////order info ,
                        ConstructCell(selectedItem.JobOrders_OrderNo.ToString(), CellValues.String),
                        ConstructCell(selectedItem.JobOrders_OrderSurf.ToString(), CellValues.String),
                        ConstructCell(selectedItem.AccountName, CellValues.String),
                        ConstructCell(selectedItem.JobOrders_ShipTo, CellValues.String),
                        ConstructCell(selectedItem.CustPO, CellValues.String),
                        ConstructCell(selectedItem.InvoiceNo.ToString(), CellValues.String), //cust po
                        ConstructCell(selectedItem.TotalOrderAmount.ToString() == string.Empty ? "" : selectedItem.TotalOrderAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.OrderCreatedDate.ToString(), CellValues.String),
                        ConstructCell(selectedItem.InvoiceDate.ToString(), CellValues.String),
                        ////orderline info 
                        ConstructCell(selectedItem.ProjectOrderLine_OrderNo.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_OrderSurf.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.LineNumber.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ModelPart ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_POAmount.ToString() == string.Empty ? "" : selectedItem.ProjectOrderLine_POAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.QuantityOrdered.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.QuantityShipped.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_CustomerRequestedDeliveryDate.ToString() ?? "", CellValues.String),
                        ConstructCell(VendorName ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_VerifiedVoltage.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.AccountName ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_ShipTo ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_VendorNumber.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_OrderAmount.ToString() == string.Empty ? "" : selectedItem.ProjectOrderLine_OrderAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_NetAmount.ToString() == string.Empty ? "" : selectedItem.ProjectOrderLine_NetAmount.ToString() + " " + "$", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_OrderAltNo.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.ProjectOrderLine_OrderLineCreatedDate.ToString() ?? "", CellValues.String),
                        ////CarrierSapReportHeader
                        ConstructCell(selectedItem.CarrierSapReportHeader_SalesOrder.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportHeader_PurchaseOrder ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportHeader_LastChanged.ToString(), CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportHeader_Consignee ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportHeader_OverallOrderStatus ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportHeader_DateEntered.ToString() ?? "", CellValues.String),
                        ////CarrierSapReportLineItems
                        ConstructCell(selectedItem.CarrierSapReportLineItems_SalesOrder.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_LineNumber.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_Material ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_Description ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_OrderQty.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_ShippedQty.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_CRSD.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_CPSD.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_PreviousCPSD.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_Rate.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_NetPrice.ToString() ?? "", CellValues.String),
                        ConstructCell(selectedItem.CarrierSapReportLineItems_ItemValue.ToString() ?? "", CellValues.String)
                        );

                    sheetData.AppendChild(row);
                }
                var autoFilter1 = new AutoFilter()
                { Reference = "A1:DJ" + result.Count + 1 };


                worksheetPart.Worksheet.Append(autoFilter1);

                //Create New sheet for Prpject step 
                var worksheetPart2 = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart2.Worksheet = new Worksheet();
                var columns2 = ColWidth();

                ////Added code to freeze job sheet name col
                var sheetViewstep = new SheetViews();
                var sheetView3 = new SheetView() { TabSelected = true, WorkbookViewId = (UInt32Value)0U };
                var pane3 = new Pane() { ActivePane = PaneValues.TopRight, HorizontalSplit = 1D, State = PaneStateValues.Frozen, TopLeftCell = "B1" };
                var selection3 = new Selection() { Pane = PaneValues.TopRight };
                sheetView3.Append(pane3);
                sheetView3.Append(selection3);
                sheetViewstep.Append(sheetView3);

                ////Added code to freeze header
                var sheetView2 = new SheetView() { TabSelected = true, WorkbookViewId = (UInt32Value)0U };
                var pane2 = new Pane() { VerticalSplit = 1D, TopLeftCell = "A2", ActivePane = PaneValues.BottomLeft, State = PaneStateValues.Frozen };
                var selection2 = new Selection() { Pane = PaneValues.TopRight };
                sheetView2.Append(pane2);
                sheetView2.Append(selection2);
                sheetViewstep.Append(sheetView2);

                worksheetPart2.Worksheet.Append(sheetViewstep);
                worksheetPart2.Worksheet.AppendChild(columns2);
                var sheetStep = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart2), SheetId = 2, Name = "Project Step" };
                sheets.Append(sheetStep);
                workbookPart.Workbook.Save();

                connection = new CommPmApplicationConnection();
                var result2 = new List<DashbordJobStatusViewModel>();

                result2 = GetJobSheetStatusByJobSheetId(jobId).ToList();
                var sheetDataStep = worksheetPart2.Worksheet.AppendChild(new SheetData());
                // Constructing header
                var rowstep = new Row();

                rowstep.Append(
                    ConstructCell("Project Name" ?? "", CellValues.String),
                    ConstructCell("Account Name" ?? "", CellValues.String),
                    ConstructCell("Account Number" ?? "", CellValues.String),
                    ConstructCell("Job Or ShipTo Number" ?? "", CellValues.String),
                    ConstructCell("Pre Submittal" ?? "", CellValues.String),
                    ConstructCell("Submitted To Contractor" ?? "", CellValues.String),
                    ConstructCell("Submitted To Contractor Date" ?? "", CellValues.String),
                    ConstructCell("Returned from Contractor" ?? "", CellValues.String),
                    ConstructCell("Returned from Contractor Date" ?? "", CellValues.String),
                    ConstructCell("Submitted To Credit" ?? "", CellValues.String),
                    ConstructCell("Submitted To Credit Date" ?? "", CellValues.String),
                    ConstructCell("Returned By Credit" ?? "", CellValues.String),
                    ConstructCell("Returned By Credit Date" ?? "", CellValues.String),
                    ConstructCell("Approved By Credit" ?? "", CellValues.String),
                    ConstructCell("Approved By Credit Date" ?? "", CellValues.String),
                    ConstructCell("Orders Created" ?? "", CellValues.String)
                );
                sheetDataStep.AppendChild(rowstep);

                if (result2.Count > 0)
                {
                    foreach (var selectedItem in result2)
                    {
                        var rowsDate = new Row();

                        var sheetCreated = string.Empty;
                        var submittedToContractor = string.Empty;
                        var returnedFromContractor = string.Empty;
                        var approvedByCredit = string.Empty;
                        var submittedToCredit = string.Empty;
                        var rejectedByCredit = string.Empty;
                        var ordersCreated = string.Empty;

                        if (selectedItem.sheetCreated == true)
                            sheetCreated = "Completed";
                        else
                            sheetCreated = "Not Completed";

                        if (selectedItem.submittedToContractor == true)
                            submittedToContractor = "Completed";
                        else
                            submittedToContractor = "Not Completed";

                        if (selectedItem.returnedFromContractor == true)
                            returnedFromContractor = "Completed";
                        else
                            returnedFromContractor = "Not Completed";

                        if (selectedItem.submittedToCredit == true)
                            submittedToCredit = "Completed";
                        else
                            submittedToCredit = "Not Completed";

                        if (selectedItem.rejectedByCredit == true)
                            rejectedByCredit = "Completed";
                        else
                            rejectedByCredit = "Not Completed";

                        if (selectedItem.approvedByCredit == true)
                            approvedByCredit = "Completed";
                        else
                            approvedByCredit = "Not Completed";

                        if (selectedItem.ordersCreated == true)
                            ordersCreated = "Completed";
                        else
                            ordersCreated = "Not Completed";

                        rowsDate.Append(
                            ConstructCell(selectedItem.JobSheetName ?? "", CellValues.String),
                            ConstructCell(selectedItem.AccountName ?? "", CellValues.String),
                            ConstructCell(selectedItem.AccountNumber ?? "", CellValues.String),
                            ConstructCell(selectedItem.JobOrShipToNumber ?? "", CellValues.String),
                            ConstructCell(sheetCreated ?? "", CellValues.String),
                            ConstructCell(submittedToContractor ?? "", CellValues.String),
                            ConstructCell(selectedItem.submittedToContractorCreatedDate ?? "", CellValues.String),
                            ConstructCell(returnedFromContractor ?? "", CellValues.String),
                            ConstructCell(selectedItem.returnedFromContractorCreatedDate ?? "", CellValues.String),
                            ConstructCell(submittedToCredit ?? "", CellValues.String),
                            ConstructCell(selectedItem.submittedToCreditCreatedDate ?? "", CellValues.String),
                            ConstructCell(rejectedByCredit ?? "", CellValues.String),
                            ConstructCell(selectedItem.rejectedByCreditCreatedDate ?? "", CellValues.String),
                            ConstructCell(approvedByCredit ?? "", CellValues.String),
                            ConstructCell(selectedItem.approvedByCreditCreatedDate ?? "", CellValues.String),
                            ConstructCell(ordersCreated ?? "", CellValues.String)

                            );

                        sheetDataStep.AppendChild(rowsDate);
                    }

                }

                var autoFilter2 = new AutoFilter()
                { Reference = "A1:P1" + result2.Count + 1 };


                worksheetPart2.Worksheet.Append(autoFilter2);

                worksheetPart.Worksheet.Save();
                document.Close();

                if (exportType == "excel")
                {
                    var req = new WebClient();
                    var response = HttpContext.Current.Response;

                    var Date = DateTime.Now.ToString("MM-dd-yy");
                    var Filename = "PM_Tracker_Download_" + Date + ".xlsx";
                    response.Clear();

                    response.ClearContent();

                    response.ClearHeaders();

                    response.Buffer = true;

                    response.AddHeader("cache-control", "private");

                    response.AddHeader("Content-Disposition", "attachment;filename=" + Filename);

                    response.AddHeader("Content-type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    var data = req.DownloadData(worksheetName);

                    response.BinaryWrite(data);

                    response.End();
                }
                else if (exportType == "email")
                {
                    Web WebClient;
                    using (var clientContext = new ClientContext("https://mingledorffs.sharepoint.com/InformationTechnology"))
                    {
                        var securePassword = new SecureString();
                        foreach (var c in "Pr0jMgm+!")
                        {
                            securePassword.AppendChar(c);
                        }

                        clientContext.Credentials = new SharePointOnlineCredentials("projectmanagement@Mingledorffs.onmicrosoft.com", securePassword);
                        WebClient = clientContext.Web;
                        using (var fs = System.IO.File.Open(worksheetName, FileMode.Open))
                        {

                            clientContext.Load(WebClient, website => website.Lists, website => website.ServerRelativeUrl);
                            clientContext.ExecuteQuery();
                            var fi = new FileInfo(worksheetName);

                            var filename = "PM_Tracker_Download_" + fi.Name.Split('.')[0].ToString() + '-' + $"{(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds:0}" + "." + fi.Name.Split('.')[1].ToString();

                            var folder =
                                WebClient.GetFolderByServerRelativeUrl(
                                    "Shared Documents/Software Development/PM Application/JobSheetReports");
                            clientContext.Load(folder);
                            clientContext.ExecuteQuery();
                            var fileUrl = string.Format("{0}/{1}", folder.ServerRelativeUrl, filename);

                            Microsoft.SharePoint.Client.File.SaveBinaryDirect(clientContext, fileUrl, fs, true);

                            fs.Dispose();
                            fs.Close();

                            emailService.ExcelExportEmail(emailAddress, fileUrl, username, filename);
                        }
                    }

                }
            }
        }


        private static Columns ColWidth()
        {
            var columns = new Columns(
                    new Column
                    {
                        Min = 1,
                        Max = 150,
                        Width = 30,
                        CustomWidth = true
                    });

            return columns;
        }


        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)

            };
        }
        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            var fonts = new Fonts(

                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },

                    new Color() { Rgb = "#FF0000" }

                ),
                new Font( // Index 2 - body
                    new FontSize() { Val = 10 },
                    new Bold(),
                    new Color() { Rgb = "FFFFFF" }

                ));

            var fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "66666666" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            var borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            var cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }

        public IEnumerable<JobSheetCreditViewModel> JobSheetCreditRead(string IsActive, string approvalSort)
        {
            return GetAllJobSheetCreditSortApproval(IsActive , approvalSort);
        }

        public IList<JobSheetCreditViewModel> GetAllJobSheetCreditSortApproval(string IsActive , string approvalSort)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<JobSheetCreditViewModel>();
            var Creditresult = new List<JobSheetCreditViewModel>();
            var approvedIds = new List<int>();
            var rejectedIds = new List<int>();
            var pendingIds = new List<int>();
            var SubmmitedToCreditIds = new List<int>();
            var jobSheetCreditIds = new List<int>();
            var incompleteIds = new List<int>();
            var paymentIssueIds = new List<int>();
            var completedIds = new List<int>();

            var pmIds = new List<int>();
            var outsidesalesrepIds = new List<int>();
            var managerpmIds = new List<int>();

            var mpmslsrep = new List<int>();
            try
            {

                approvedIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 11) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                rejectedIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 8) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                pendingIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 12) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                // jobSheetCreditIds = connection.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8 || u.JobStepId == 11 || u.JobStepId == 12 || u.JobStepId == 13 || u.JobStepId == 14 || u.JobStepId == 15)) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                // SubmmitedToCreditIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 7) && (!jobSheetCreditIds.Contains(u.JobSheetId)) && (u.IsActive == true)).Select(u => u.JobSheetId).ToList();
                jobSheetCreditIds = connection.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8 || u.JobStepId == 11 || u.JobStepId == 12 || u.JobStepId == 15)) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                SubmmitedToCreditIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 7) && (!jobSheetCreditIds.Contains(u.JobSheetId)) && (u.IsActive == true)).Select(u => u.JobSheetId).ToList();


                incompleteIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 13) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                paymentIssueIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 14) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                completedIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 15) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();

                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());

                if (approvalSort == "approved")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 11)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 11)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Approved by Credit",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,

                    }).Where(u => approvedIds.Contains(u.JobSheetId)).ToList();

                }
                if (approvalSort == "rejected")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Rejected by Credit",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    }).Where(u => rejectedIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "Submitted")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },

                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },

                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 7)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 7)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Submitted",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,


                    }).Where(u => SubmmitedToCreditIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "pending")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 12)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 12)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Pending Approval",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    }).Where(u => pendingIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "incompletedocumentation")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 13)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 13)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Incomplete Documentation",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    }).Where(u => incompleteIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "paymentissue")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 14)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 14)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Payment Issue",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    }).Where(u => paymentIssueIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "completed")
                {
                    result = connection.JobSheet.Select(jobSheetService => new JobSheetCreditViewModel
                    {
                        JobSheetId = jobSheetService.JobSheetId,
                        JobSheetName = jobSheetService.JobSheetName,
                        AccountName = string.IsNullOrEmpty(jobSheetService.AccountName) ? string.Empty : jobSheetService.AccountName,
                        AccountNumber = string.IsNullOrEmpty(jobSheetService.AccountNumber) ? string.Empty : jobSheetService.AccountNumber,
                        JobOrShipToNumber = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : jobSheetService.JobOrShipToNumber,
                        JobName = jobSheetService.JobName,
                        Customer = new CustomerViewModel()
                        {
                            CustNo = string.IsNullOrEmpty(jobSheetService.Customer.CustNo) ? string.Empty : jobSheetService.Customer.CustNo,
                            CustomerName = string.IsNullOrEmpty(jobSheetService.Customer.CustomerName) ? string.Empty : jobSheetService.Customer.CustomerName

                        },
                        TotalPurchases = jobSheetService.TotalPurchases,
                        CreditNotes = jobSheetService.CreditNotes,
                        User1 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User1.UserId,
                            SlsRep = jobSheetService.User1.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User1.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User1.SMSN.Name
                            }
                        },
                        User7 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User7.UserId,
                            SlsRep = jobSheetService.User7.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User7.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User7.SMSN.Name
                            }
                        },
                        User8 =
                        new JobSheetUserViewModel()
                        {
                            UserId = jobSheetService.User8.UserId,
                            SlsRep = jobSheetService.User8.SlsRep,
                            SMSN = new SMSNViewModel()
                            {
                                SlsRep = jobSheetService.User8.SMSN.SlsRep.ToUpper(),
                                Name = jobSheetService.User8.SMSN.Name
                            }
                        },
                        ManagerApprovedDate = jobSheetService.ManagerApprovedDate,
                        LastStepCompleted = string.IsNullOrEmpty(jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 15)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString()) ? string.Empty : jobSheetService.JobSheetCompletedStep.Where(u => ((u.JobStepId == 15)) && (u.IsActive == true) && (u.JobSheetId == jobSheetService.JobSheetId)).Select(p => p.LastModifiedDate).FirstOrDefault().ToString(),
                        StepName = "Completed",
                        IsActive = jobSheetService.IsActive,
                        IsLockRecord = jobSheetService.IsLockRecord,
                        ProjectManager = jobSheetService.ProjectManager,
                        TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                        TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                        OrderAmount = jobSheetService.OrderAmount,
                        Company = jobSheetService.Company,
                        RequestedShipToNumber = jobSheetService.ReqShipToNumber,
                        ShipToName = string.IsNullOrEmpty(jobSheetService.JobOrShipToNumber) ? string.Empty : "-" + jobSheetService.JobOrShipToNumber + " : " + jobSheetService.ShipToName,
                    }).Where(u => completedIds.Contains(u.JobSheetId)).ToList();
                }
                if (approvalSort == "all")
                {

                    result = (from jobsheet in connection.JobSheet
                              join step in connection.JobSheetCompletedStep on jobsheet.JobSheetId equals step.JobSheetId
                              join jobStep in connection.JobSteps on step.JobStepId equals jobStep.JobStepId
                              where jobStep.IsActive == true && step.IsActive == true
                              select new JobSheetCreditViewModel
                              {
                                  JobSheetId = jobsheet.JobSheetId,
                                  JobSheetName = jobsheet.JobSheetName,
                                  AccountName = string.IsNullOrEmpty(jobsheet.AccountName) ? string.Empty : jobsheet.AccountName,
                                  AccountNumber = string.IsNullOrEmpty(jobsheet.AccountNumber) ? string.Empty : jobsheet.AccountNumber,
                                  JobOrShipToNumber = string.IsNullOrEmpty(jobsheet.JobOrShipToNumber) ? string.Empty : jobsheet.JobOrShipToNumber,
                                  JobName = jobsheet.JobName,
                                  Customer = new CustomerViewModel()
                                  {
                                      CustNo = string.IsNullOrEmpty(jobsheet.Customer.CustNo) ? string.Empty : jobsheet.Customer.CustNo,
                                      CustomerName = string.IsNullOrEmpty(jobsheet.Customer.CustomerName) ? string.Empty : jobsheet.Customer.CustomerName

                                  },
                                  TotalPurchases = jobsheet.TotalPurchases,
                                  CreditNotes = jobsheet.CreditNotes,
                                  User1 =
                                    new JobSheetUserViewModel()
                                    {
                                        UserId = jobsheet.User1.UserId,
                                        SlsRep = jobsheet.User1.SlsRep,
                                        SMSN = new SMSNViewModel()
                                        {
                                            SlsRep = jobsheet.User1.SMSN.SlsRep.ToUpper(),
                                            Name = jobsheet.User1.SMSN.Name
                                        }
                                    },
                                  User7 =
                                    new JobSheetUserViewModel()
                                    {
                                        UserId = jobsheet.User7.UserId,
                                        SlsRep = jobsheet.User7.SlsRep,
                                        SMSN = new SMSNViewModel()
                                        {
                                            SlsRep = jobsheet.User7.SMSN.SlsRep.ToUpper(),
                                            Name = jobsheet.User7.SMSN.Name
                                        }
                                    },
                                  User8 =
                                    new JobSheetUserViewModel()
                                    {
                                        UserId = jobsheet.User8.UserId,
                                        SlsRep = jobsheet.User8.SlsRep,
                                        SMSN = new SMSNViewModel()
                                        {
                                            SlsRep = jobsheet.User8.SMSN.SlsRep.ToUpper(),
                                            Name = jobsheet.User8.SMSN.Name
                                        }
                                    },
                                  
                                  ManagerApprovedDate = jobsheet.ManagerApprovedDate,
                                  LastStepCompleted = step.LastModifiedDate.ToString(),
                                  StepName = jobStep.StepName,
                                  IsActive = jobsheet.IsActive,
                                  IsLockRecord = jobsheet.IsLockRecord,
                                  ProjectManager = jobsheet.ProjectManager,
                                  TMCommissionPosition1Id = jobsheet.TMCommissionPosition1Id,
                                  TMCommissionPosition2Id = jobsheet.TMCommissionPosition2Id,
                                  OrderAmount = jobsheet.OrderAmount,
                                  Company = jobsheet.Company,
                                  RequestedShipToNumber = jobsheet.ReqShipToNumber,
                                  ShipToName = string.IsNullOrEmpty(jobsheet.JobOrShipToNumber) ? string.Empty : "-" + jobsheet.JobOrShipToNumber + " : " + jobsheet.ShipToName,

                              }).ToList();
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "3")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    pmIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    var pmsusers = result.Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                    var ossusers = result.Where(u => pmIds.Contains(u.TMCommissionPosition1Id)).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    result = allusers.Where(e => e.Company == conoid).ToList();

                    //result.Where(u => pmIds.Contains(u.ProjectManager)).ToList();
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    var manageruserid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    managerpmIds = connection.ManagerPM.Where(p => (p.IsActive == true) && (p.ManagerId == manageruserid)).Select(p => p.PMId).ToList();
                    mpmslsrep = connection.User.Where(p => (p.IsActive == true) && (managerpmIds.Contains(p.UserId))).Select(p => p.UserId).ToList();

                    var musers = result.Where(u => mpmslsrep.Contains(u.ProjectManager)).ToList();

                    result = musers.Where(e => e.Company == conoid).ToList();

                    //result.Where(u => (mpmslsrep.Contains(u.ProjectManager)) && (u.Company == conoid)).ToList();
                }

                if (HttpContext.Current.Session["userRoleId"].ToString() == "8")
                {
                    var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    outsidesalesrepIds = connection.User.Where(p => (p.IsActive == true) && (p.UserId == userid)).Select(p => p.UserId).ToList();

                    //result.Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();
                    var pmsusers = result.Where(u => outsidesalesrepIds.Contains(u.ProjectManager)).ToList();
                    var ossusers = result.Where(u => outsidesalesrepIds.Contains(u.TMCommissionPosition1Id)).ToList();

                    var allusers = pmsusers.Union(ossusers);

                    result = allusers.Where(e => e.Company == conoid).ToList();
                }

                if (HttpContext.Current.Session["userRoleId"].ToString() == "4")
                {
                    var musers = result.Where(u => u.Company == conoid).ToList();
                    result = musers.ToList();
                }
                if (HttpContext.Current.Session["userRoleId"].ToString() == "5" || HttpContext.Current.Session["userRoleId"].ToString() == "6" || HttpContext.Current.Session["userRoleId"].ToString() == "7")
                {
                    var musers = result.ToList();
                    result = musers.ToList();
                }
                if(IsActive == "True")
                {
                    Creditresult =  result.Where(u => u.IsActive == true).ToList();
                }
                else if (IsActive == "False")
                {
                    Creditresult = result.Where(u => u.IsActive == false).ToList();
                }
                else if (IsActive == "All")
                {
                    Creditresult = result.ToList();
                }

                return Creditresult;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAllJobSheetCredit method");
                return result.ToList();
            }

        }

        public void JobSheetCreditUpdate(JobSheetCreditViewModel jobSheet)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var emailService = new EmailService(connection);

                if (jobSheet.JobSheetId != 0)
                {
                    var dbRec = connection.JobSheet
                        .Include(p => p.JobSteps)
                        .FirstOrDefault(p => p.JobSheetId == jobSheet.JobSheetId);
                    var NewNote = "";
                    if (dbRec.CreditNotes == "" || dbRec.CreditNotes == null || dbRec.CreditNotes == "null")
                    {
                        NewNote = jobSheet.CreditNotes;
                    }
                    else
                    {
                        NewNote = " " + System.DateTime.Now + " " + jobSheet.CreditNotes.Replace(dbRec.CreditNotes, " ");
                    }
                    if (dbRec != null)
                    {
                        if (jobSheet.StepName == "Approve")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = jobSheet.IsLockRecord;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 11);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditApprovedEmail(jobSheet.JobSheetId);
                            var sqlconnection = new SqlConnection(ConfigurationManager.ConnectionStrings["CommPmApplicationConnectionString"].ToString());
                            updateJobSheetByShipToFromSXE(jobSheet, sqlconnection);

                        }
                        else if (jobSheet.StepName == "Reject")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = jobSheet.IsLockRecord;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 8);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditRejectEmail(jobSheet.JobSheetId);

                        }
                        else if (jobSheet.StepName == "Pending Approval")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = true;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 12);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditPendingEmail(jobSheet.JobSheetId);

                        }
                        else if (jobSheet.StepName == "Incomplete Documentation")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = false;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 13);
                            updateSubmittedToCreditFalse(jobSheet.JobSheetId);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditIncompleteEmail(jobSheet.JobSheetId);

                        }
                        else if (jobSheet.StepName == "Payment Issue")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = false;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 14);
                            updateSubmittedToCreditFalse(jobSheet.JobSheetId);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditPaymentEmail(jobSheet.JobSheetId);

                        }
                        else if (jobSheet.StepName == "Completed")
                        {
                            dbRec.JobOrShipToNumber = jobSheet.JobOrShipToNumber;
                            dbRec.CreditNotes = dbRec.CreditNotes + NewNote;
                            dbRec.IsLockRecord = jobSheet.IsLockRecord;
                            connection.SaveChanges();

                            updateJobSteps(jobSheet.JobSheetId, 15);
                            updateInCompletePaymentFalse(jobSheet.JobSheetId);
                            JobSheetCreditJobsheet(jobSheet);
                            emailService.CreditCompletedEmail(jobSheet.JobSheetId);

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class JobSheetCreditUpdate method");
            }

        }

        private void updateSubmittedToCreditFalse(int jobSheetId)
        {
            try
            {
                // In Active Submitted to Credit
                var SubmmitedToCreditIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 7)).Count();
                if (SubmmitedToCreditIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 7));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(7, true, jobSheetId);
                }
                // InActive Rejected by Credit
                var RejectedbyCreditIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 8)).Count();
                if (RejectedbyCreditIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 8));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(8, true, jobSheetId);
                }
                // InActive Approved by Credit
                var ApprovedbyCreditIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 11)).Count();
                if (ApprovedbyCreditIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 11));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(11, true, jobSheetId);
                }
                // InActive Pending
                var PendingIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 12)).Count();
                if (PendingIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 12));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(12, true, jobSheetId);
                }
                //// InActive Incomplete Documentation
                //var IncompleteDocumentationIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 13)).Count();
                //if (IncompleteDocumentationIds > 0)
                //{
                //    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 13));
                //    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                //    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                //    jobSheetCompleted.IsActive = false;
                //    connection.SaveChanges();
                //    CreateAuditTrailJobSteps(13, true, jobSheetId);
                //}
                //// InActive Payment Issue
                //var PaymentIssueIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 14)).Count();
                //if (PaymentIssueIds > 0)
                //{
                //    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 14));
                //    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                //    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                //    jobSheetCompleted.IsActive = false;
                //    connection.SaveChanges();
                //    CreateAuditTrailJobSteps(14, true, jobSheetId);
                //}
                // InActive Completed
                var CompletedIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 15)).Count();
                if (CompletedIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 15));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(15, true, jobSheetId);
                }
                //var emailService = new EmailService(connection);
                //var isStepExists = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 7)).Count();

                //if (isStepExists > 0)
                //{
                //    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 7));
                //    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                //    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                //    jobSheetCompleted.IsActive = false;
                //    connection.SaveChanges();
                //    CreateAuditTrailJobSteps(7, true, jobSheetId);
                //}
                //else
                //{
                //    var entity = new JobSheetCompletedStep();
                //    //entity.StatusId = statusId;
                //    entity.JobSheetId = jobSheetId;
                //    entity.JobStepId = 7;
                //    entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                //    entity.CreatedDate = System.DateTime.Now;
                //    entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                //    entity.LastModifiedDate = System.DateTime.Now;

                //    entity.IsActive = false;

                //    connection.JobSheetCompletedStep.Add(entity);
                //    connection.SaveChanges();

                //    CreateAuditTrailJobSteps(7, true, jobSheetId);

                //}
            }
            catch (Exception)
            {

            }
        }


        private void updateInCompletePaymentFalse(int jobSheetId)
        {
            try
            {
                // InActive Incomplete Documentation as flow is completed 
                var IncompleteDocumentationIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 13)).Count();
                if (IncompleteDocumentationIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 13));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(13, true, jobSheetId);
                }
                // InActive Payment Issue as flow is completed 
                var PaymentIssueIds = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 14)).Count();
                if (PaymentIssueIds > 0)
                {
                    var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == jobSheetId) && (p.JobStepId == 14));
                    jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    jobSheetCompleted.LastModifiedDate = System.DateTime.Now;
                    jobSheetCompleted.IsActive = false;
                    connection.SaveChanges();
                    CreateAuditTrailJobSteps(14, true, jobSheetId);
                }
                

            }
            catch (Exception)
            {

            }
        }

        private void updateJobSheetByShipToFromSXE(JobSheetCreditViewModel jobSheet, SqlConnection sqlconnection)
        {
            try
            {
                if (sxeQueryService.ARSS(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, jobSheet.Company, sqlconnection).Tables[0].Rows.Count > 0)
                {
                    if (jobSheet.JobSheetId != 0)
                    {
                        var dbRec = connection.JobSheet
                            .Include(p => p.JobSteps)
                            .FirstOrDefault(p => p.JobSheetId == jobSheet.JobSheetId);
                        if (dbRec != null)
                        {
                            dbRec.ShipToName = sxeQueryService.ARSS(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, jobSheet.Company, sqlconnection).Tables[0].Rows[0]["name"].ToString();
                            dbRec.ShipToAddress = sxeQueryService.ARSS(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, jobSheet.Company, sqlconnection).Tables[0].Rows[0]["addr##1"].ToString();
                            connection.SaveChanges();

                            getOrdersByShipTo(jobSheet, sqlconnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class updateJobSheetByShipToFromSXE method");
            }
        }

        private void getOrdersByShipTo(JobSheetCreditViewModel jobSheet, SqlConnection sqlconnection)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var OrdersCreated = false;
                if (sxeQueryService.OEEH(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, jobSheet.Company, sqlconnection).Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in sxeQueryService.OEEH(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, jobSheet.Company, sqlconnection).Tables[0].Rows)
                    {
                        var custno = Convert.ToDecimal(dr["custno"].ToString());
                        var orderno = Convert.ToInt32(dr["orderno"].ToString());
                        var ordersurf = Convert.ToInt32(dr["ordersuf"].ToString());
                        var shipto = dr["shipto"].ToString();
                        int cono = Convert.ToInt32(jobSheet.Company);

                        var isOrderExists = connection.JobOrders.Where(p => (p.CustNo == custno) && (p.ShipTo == shipto) && (p.OrderNo == orderno) && (p.OrderSurf == ordersurf) && (p.Cono == cono)).Count();
                        if (isOrderExists > 0)
                        {
                            var jobOrder = connection.JobOrders.FirstOrDefault(p => (p.CustNo == custno) && (p.ShipTo == shipto) && (p.OrderNo == orderno) && (p.OrderSurf == ordersurf) && (p.Cono == cono));

                            jobOrder.CustPO = dr["custpo"].ToString();

                            if (dr["invno"].ToString() == "")
                            {
                                jobOrder.InvoiceNo = 0;
                            }
                            else
                            {
                                jobOrder.InvoiceNo = Convert.ToInt32(dr["invno"].ToString());
                            }
                            if (dr["totordamt"].ToString() == "")
                            {
                                jobOrder.TotalOrderAmount = 0;
                            }
                            else
                            {
                                jobOrder.TotalOrderAmount = Convert.ToDecimal(dr["totordamt"].ToString());
                            }
                            if (dr["enterdt"].ToString() == "")
                            {
                                jobOrder.OrderCreatedDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrder.OrderCreatedDate = Convert.ToDateTime(dr["enterdt"].ToString());
                            }

                            jobOrder.Cono = cono;

                            connection.SaveChanges();

                            getOrdersLinesByShipTo(jobSheet, dr["orderno"].ToString(), dr["ordersuf"].ToString(), sqlconnection);
                            //update order created status in JobSheetCompletedStep
                            OrdersCreated = true;
                        }
                        else
                        {
                            var jobOrder = new JobOrders();
                            //entity.StatusId = statusId;
                            jobOrder.OrderNo = Convert.ToInt32(dr["orderno"].ToString());
                            jobOrder.OrderSurf = Convert.ToInt32(dr["ordersuf"].ToString());
                            jobOrder.CustNo = Convert.ToDecimal(dr["custno"].ToString());
                            jobOrder.ShipTo = dr["shipto"].ToString();

                            jobOrder.CustPO = dr["custpo"].ToString();
                            if (dr["invno"].ToString() == "")
                            {
                                jobOrder.InvoiceNo = 0;
                            }
                            else
                            {
                                jobOrder.InvoiceNo = Convert.ToInt32(dr["invno"].ToString());
                            }
                            if (dr["totordamt"].ToString() == "")
                            {
                                jobOrder.TotalOrderAmount = 0;
                            }
                            else
                            {
                                jobOrder.TotalOrderAmount = Convert.ToDecimal(dr["totordamt"].ToString());
                            }
                            if (dr["enterdt"].ToString() == "")
                            {
                                jobOrder.OrderCreatedDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrder.OrderCreatedDate = Convert.ToDateTime(dr["enterdt"].ToString());
                            }

                            jobOrder.IsActive = 1;
                            jobOrder.Cono = cono;

                            connection.JobOrders.Add(jobOrder);
                            connection.SaveChanges();

                            getOrdersLinesByShipTo(jobSheet, dr["orderno"].ToString(), dr["ordersuf"].ToString(), sqlconnection);
                            //update order created status in JobSheetCompletedStep
                            OrdersCreated = false;
                        }
                    }
                    UpdateStatusById(jobSheet.JobSheetId, "Orders Created", OrdersCreated);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class getOrdersByShipTo method");
            }
        }

        private void getOrdersLinesByShipTo(JobSheetCreditViewModel jobSheet, string orderNo, string orderSurf, SqlConnection sqlconnection)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                if (sxeQueryService.OEEL(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, Convert.ToInt32(orderNo), Convert.ToInt32(orderSurf), jobSheet.Company, sqlconnection).Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in sxeQueryService.OEEL(Convert.ToDecimal(jobSheet.AccountNumber), jobSheet.JobOrShipToNumber, Convert.ToInt32(orderNo), Convert.ToInt32(orderSurf), jobSheet.Company, sqlconnection).Tables[0].Rows)
                    {
                        var custno = Convert.ToDecimal(dr["custno"].ToString());
                        var orderno = Convert.ToInt32(dr["orderno"].ToString());
                        var ordersurf = Convert.ToInt32(dr["ordersuf"].ToString());
                        var linenumber = Convert.ToInt32(dr["lineno_"].ToString());
                        var shipto = dr["shipto"].ToString();

                        int cono = Convert.ToInt32(jobSheet.Company);

                        var isOrderLinesExists = connection.ProjectOrderLine.Where(p => (p.CustNo == custno) && (p.ShipTo == shipto) && (p.OrderNo == orderno) && (p.OrderSurf == ordersurf) && (p.LineNumber == linenumber) && (p.Cono == cono)).Count();
                        if (isOrderLinesExists > 0)
                        {
                            var jobOrderLine = connection.ProjectOrderLine.FirstOrDefault(p => (p.CustNo == custno) && (p.ShipTo == shipto) && (p.OrderNo == orderno) && (p.OrderSurf == ordersurf) && (p.LineNumber == linenumber) && (p.Cono == cono));

                            jobOrderLine.ModelPart = dr["shipprod"].ToString();

                            if (dr["qtyord"].ToString() == "")
                            {
                                jobOrderLine.QuantityOrdered = 0;
                            }
                            else
                            {
                                jobOrderLine.QuantityOrdered = Convert.ToDecimal(dr["qtyord"].ToString());
                            }
                            if (dr["qtyship"].ToString() == "")
                            {
                                jobOrderLine.QuantityShipped = 0;
                            }
                            else
                            {
                                jobOrderLine.QuantityShipped = Convert.ToDecimal(dr["qtyship"].ToString());
                            }
                            if (dr["reqshipdt"].ToString() == "")
                            {
                                jobOrderLine.CustomerRequestedDeliveryDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrderLine.CustomerRequestedDeliveryDate = Convert.ToDateTime(dr["reqshipdt"].ToString());
                            }
                            if (dr["vendno"].ToString() == "")
                            {
                                jobOrderLine.VendorName = 0;
                                jobOrderLine.VendorNumber = 0;
                            }
                            else
                            {
                                jobOrderLine.VendorName = Convert.ToDecimal(dr["vendno"].ToString());
                                jobOrderLine.VendorNumber = Convert.ToDecimal(dr["vendno"].ToString());
                            }
                            if (dr["price"].ToString() == "")
                            {
                                jobOrderLine.OrderAmount = 0;
                            }
                            else
                            {
                                jobOrderLine.OrderAmount = Convert.ToDecimal(dr["price"].ToString());
                            }
                            if (dr["netamt"].ToString() == "")
                            {
                                jobOrderLine.NetAmount = 0;
                            }
                            else
                            {
                                jobOrderLine.NetAmount = Convert.ToDecimal(dr["netamt"].ToString());
                            }
                            if (dr["orderaltno"].ToString() == "")
                            {
                                jobOrderLine.OrderAltNo = 0;
                            }
                            else
                            {
                                jobOrderLine.OrderAltNo = Convert.ToInt32(dr["orderaltno"].ToString());
                            }

                            if (dr["enterdt"].ToString() == "")
                            {
                                jobOrderLine.OrderLineCreatedDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrderLine.OrderLineCreatedDate = Convert.ToDateTime(dr["enterdt"].ToString());
                            }

                            jobOrderLine.Cono = cono;

                            connection.SaveChanges();
                        }
                        else
                        {
                            var jobOrderLine = new ProjectOrderLine();
                            //entity.StatusId = statusId;
                            jobOrderLine.OrderNo = Convert.ToInt32(dr["orderno"].ToString());
                            jobOrderLine.OrderSurf = Convert.ToInt32(dr["ordersuf"].ToString());
                            jobOrderLine.CustNo = Convert.ToDecimal(dr["custno"].ToString());
                            jobOrderLine.ShipTo = dr["shipto"].ToString();
                            jobOrderLine.LineNumber = Convert.ToInt32(dr["lineno_"].ToString());
                            jobOrderLine.ModelPart = dr["shipprod"].ToString();
                            jobOrderLine.POAmount = 0;
                            jobOrderLine.VerifiedVoltage = false;


                            if (dr["qtyord"].ToString() == "")
                            {
                                jobOrderLine.QuantityOrdered = 0;
                            }
                            else
                            {
                                jobOrderLine.QuantityOrdered = Convert.ToDecimal(dr["qtyord"].ToString());
                            }
                            if (dr["qtyship"].ToString() == "")
                            {
                                jobOrderLine.QuantityShipped = 0;
                            }
                            else
                            {
                                jobOrderLine.QuantityShipped = Convert.ToDecimal(dr["qtyship"].ToString());
                            }
                            if (dr["reqshipdt"].ToString() == "")
                            {
                                jobOrderLine.CustomerRequestedDeliveryDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrderLine.CustomerRequestedDeliveryDate = Convert.ToDateTime(dr["reqshipdt"].ToString());
                            }
                            if (dr["vendno"].ToString() == "")
                            {
                                jobOrderLine.VendorName = 0;
                                jobOrderLine.VendorNumber = 0;
                            }
                            else
                            {
                                jobOrderLine.VendorName = Convert.ToDecimal(dr["vendno"].ToString());
                                jobOrderLine.VendorNumber = Convert.ToDecimal(dr["vendno"].ToString());
                            }
                            if (dr["price"].ToString() == "")
                            {
                                jobOrderLine.OrderAmount = 0;
                            }
                            else
                            {
                                jobOrderLine.OrderAmount = Convert.ToDecimal(dr["price"].ToString());
                            }
                            if (dr["netamt"].ToString() == "")
                            {
                                jobOrderLine.NetAmount = 0;
                            }
                            else
                            {
                                jobOrderLine.NetAmount = Convert.ToDecimal(dr["netamt"].ToString());
                            }
                            if (dr["orderaltno"].ToString() == "")
                            {
                                jobOrderLine.OrderAltNo = 0;
                            }
                            else
                            {
                                jobOrderLine.OrderAltNo = Convert.ToInt32(dr["orderaltno"].ToString());
                            }

                            if (dr["enterdt"].ToString() == "")
                            {
                                jobOrderLine.OrderLineCreatedDate = System.DateTime.Now;
                            }
                            else
                            {
                                jobOrderLine.OrderLineCreatedDate = Convert.ToDateTime(dr["enterdt"].ToString());
                            }
                            jobOrderLine.IsActive = true;
                            jobOrderLine.Cono = cono;

                            connection.ProjectOrderLine.Add(jobOrderLine);
                            connection.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class getOrdersLinesByShipTo method");
            }
        }

        private void updateJobSteps(int JobSheetId, int jobStepId)
        {
            var emailService = new EmailService(connection);
            var isStepExists = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == JobSheetId) && (p.JobStepId == jobStepId)).Count();

            if (isStepExists > 0)
            {
                var jobSheetCompleted = connection.JobSheetCompletedStep.FirstOrDefault(p => (p.JobSheetId == JobSheetId) && (p.JobStepId == jobStepId));

                jobSheetCompleted.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                jobSheetCompleted.LastModifiedDate = System.DateTime.Now;

                jobSheetCompleted.IsActive = true;

                connection.SaveChanges();

                CreateAuditTrailJobSteps(jobStepId, true, JobSheetId);

            }
            else
            {
                var entity = new JobSheetCompletedStep();
                //entity.StatusId = statusId;
                entity.JobSheetId = JobSheetId;
                entity.JobStepId = jobStepId;
                entity.CreatedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.CreatedDate = System.DateTime.Now;
                entity.LastModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                entity.LastModifiedDate = System.DateTime.Now;

                entity.IsActive = true;

                connection.JobSheetCompletedStep.Add(entity);
                connection.SaveChanges();

                CreateAuditTrailJobSteps(jobStepId, true, JobSheetId);

            }
        }

        public void EmailToPM(JobSheet jobsheet, string status)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var pm = connection.User.SingleOrDefault(r => r.UserId == jobsheet.ProjectManager);

                var emailText = "";

                var subjectText = "Commercial Project Management - Credit status change";

                emailText += "<table style='width:100%'>";
                emailText += "<tr>";
                emailText += "<td>";
                emailText += "The Job: <b>" + jobsheet.JobSheetName + " - " + jobsheet.JobName + "</b> has been " + status;
                emailText += "</td>";
                emailText += "</tr>";
                emailText += "</table>";

                var toEmail = "projectmanagement@Mingledorffs.onmicrosoft.com";

                //toEmail = pm.SMSN.Email;                

                PM_Emailer.Email.SendMail(emailText, toEmail, subjectText);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class EmailToPM method");
            }
        }

        public void JobSheetCreditJobsheet(JobSheetCreditViewModel jobSheet)
        {
            try
            {
                connection = new CommPmApplicationConnection();


                if (jobSheet.JobSheetId != 0)
                {


                    var result = connection.JobSheet.FirstOrDefault(p => (p.JobSheetId == jobSheet.JobSheetId));

                    result.ManagerApproval = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    //result.CreditRep = jobSheet.User1.UserId;
                    result.ManagerApprovedDate = System.DateTime.Now;
                    connection.SaveChanges();



                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class JobSheetCreditUpdate method");
            }

        }

        public void JobSheetUpdateCreditJobsheet(int jobSheetId)
        {
            try
            {
                connection = new CommPmApplicationConnection();


                if (jobSheetId != 0)
                {
                    var result = connection.JobSheet.FirstOrDefault(p => (p.JobSheetId == jobSheetId));

                    result.ManagerApproval = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    result.CreditRep = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                    result.ManagerApprovedDate = System.DateTime.Now;
                    connection.SaveChanges();



                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class JobSheetCreditUpdate method");
            }

        }

        public string InActiveJobSheetById(string JobSheetId)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var id = Convert.ToInt32(JobSheetId);
                var jobSheet = connection.JobSheet.FirstOrDefault(p => (p.JobSheetId == id));

                jobSheet.IsActive = false;

                connection.SaveChanges();
                return "SUCCESS";


            }
            catch (Exception)
            {
                return "FAILED";
            }
        }

        public int ExcelExportCount(List<int> jobSheetIds)
        {
            connection = new CommPmApplicationConnection();
            var TotalRecords = 0;
            try
            {
                TotalRecords = (from jobsheet in connection.JobSheet
                                join order in connection.JobOrders on jobsheet.JobOrShipToNumber equals order.ShipTo into job
                                from b in (from order in job
                                           where order.CustNo.ToString() == jobsheet.AccountNumber
                                           select order).DefaultIfEmpty()
                                join orderline in connection.ProjectOrderLine on b.ShipTo equals orderline.ShipTo into projectline
                                from x in (from orderline in projectline
                                           where orderline.OrderSurf == b.OrderSurf && orderline.CustNo == b.CustNo
                                           select orderline).DefaultIfEmpty()
                                select jobsheet).Where(j => jobSheetIds.Contains(j.JobSheetId)).Count();

                return TotalRecords;


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class ExcelExportCount method");
                return TotalRecords;
            }
        }
        public string getUserName(int UserId)
        {
            var UserName = string.Empty;
            try
            {
                var result = connection.User.Select(user => new UserViewModel
                {
                    UserId = user.UserId,
                    SMSN = new SMSNViewModel()
                    {
                        SlsRep = user.SMSN.SlsRep,
                        Name = user.SMSN.Name,
                        Email = user.SMSN.Email
                    }
                }).Where(u => u.UserId == UserId).SingleOrDefault();


                if (result != null)
                {
                    UserName = result.SMSN.Name.ToString();
                }
                return UserName;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class getPM method");
                return UserName;
            }
        }
        public string geVendorName(int VendorId)
        {
            var VendorName = string.Empty;
            try
            {
                var result = connection.Vendor.Select(vendor => new VendorViewModel
                {
                    VendorNumber = vendor.VendorNumber,
                    VendorName = vendor.VendorName,
                }).SingleOrDefault(u => u.VendorNumber == VendorId);

                if (result != null)
                {
                    VendorName = result.VendorName.ToString();
                }
                return VendorName;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class geVendorName method");
                return VendorName;
            }
        }
        public string DeleteJobSheetById(string JobSheetId)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var id = Convert.ToInt32(JobSheetId);
                var jobSheetChangeLogs = connection.JobSheetChangeLog.Where(p => (p.JobSheetId == id)).ToList();
                var jobSheetCompletedSteps = connection.JobSheetCompletedStep.Where(p => (p.JobSheetId == id)).ToList();
                var jobStepLog = connection.JobStepLog.Where(p => (p.JobSheetId == id)).ToList();
                var jobSheet = connection.JobSheet.Where(p => (p.JobSheetId == id)).ToList();


                if (jobSheetChangeLogs.Count != 0)
                {
                    connection.JobSheetChangeLog.RemoveRange(connection.JobSheetChangeLog.Where(x => x.JobSheetId == id));
                    connection.SaveChanges();
                }
                if (jobSheetCompletedSteps.Count != 0)
                {
                    connection.JobSheetCompletedStep.RemoveRange(connection.JobSheetCompletedStep.Where(x => x.JobSheetId == id));
                    connection.SaveChanges();
                }
                if (jobStepLog.Count != 0)
                {
                    connection.JobStepLog.RemoveRange(connection.JobStepLog.Where(x => x.JobSheetId == id));
                    connection.SaveChanges();
                }
                if (jobSheet.Count != 0)
                {
                    connection.JobSheet.RemoveRange(connection.JobSheet.Where(x => x.JobSheetId == id));
                    connection.SaveChanges();
                }
                return "SUCCESS";


            }
            catch (Exception)
            {
                return "FAILED";
            }
        }

        public IList<JobSheetCCGFilesViewModel> GetAllCCGFiles(string JobSheetId)
        {

            connection = new CommPmApplicationConnection();
            var result = new List<JobSheetCCGFilesViewModel>();

            try
            {
                var jsid = 0;
                if (JobSheetId == "" || JobSheetId == "0")
                {
                    jsid = 0;
                }
                else
                {
                    jsid = Convert.ToInt32(JobSheetId);
                }
                result = connection.JobSheetCCGFiles.Select(jobSheetService => new JobSheetCCGFilesViewModel
                {
                    JobSheetId = jobSheetService.JobSheetId,
                    CCGFileId = jobSheetService.CCGFileId,
                    CCGFileName = jobSheetService.CCGFileName,
                    CCGFileURL = jobSheetService.CCGFileURL,
                    IsActive = jobSheetService.IsActive

                }).Where(u => (u.JobSheetId == jsid) && (u.IsActive == true)).ToList();

                return result.ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAll method");
                return result.ToList();
            }

            // return result;
        }

        public IEnumerable<JobSheetCCGFilesViewModel> ReadCCGFiles(string JobSheetId)
        {
            return GetAllCCGFiles(JobSheetId);
        }

        public string InActiveJobSheetCCGFileById(string CCGFileId)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var id = Convert.ToInt32(CCGFileId);
                var ccgfiles = connection.JobSheetCCGFiles.FirstOrDefault(p => (p.CCGFileId == id));

                ccgfiles.ModifiedDate = System.DateTime.Now;
                ccgfiles.ModifiedBy = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                ccgfiles.IsActive = false;

                connection.SaveChanges();
                return "SUCCESS";


            }
            catch (Exception)
            {
                return "FAILED";
            }
        }
        public void JobSheetAlertUpdate(JobSheetAlertViewModel jobSheetAlert)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var dbRec = connection.JobSheetAlert.FirstOrDefault(p => p.AlertId == jobSheetAlert.AlertId);

                if (dbRec != null)
                {
                    dbRec.NumberofDaysSnooze = jobSheetAlert.NumberofDaysSnooze;
                    dbRec.AlertSnoozeDate = DateTime.Today.AddDays(Convert.ToDouble(jobSheetAlert.NumberofDaysSnooze));
                    connection.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheetAlertUpdate Update action");
            }

        }
        public void JobSheetAlertDelete()
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                var dbRec = connection.JobSheetAlert.FirstOrDefault(p => p.UserId == userid && p.NumberofDaysSnooze == null && p.AlertSnoozeDate == null);

                if (dbRec != null)
                {
                    connection.JobSheetAlert.RemoveRange(connection.JobSheetAlert.Where(p => p.UserId == userid && p.NumberofDaysSnooze == null && p.AlertSnoozeDate == null));
                    connection.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheetAlert Delete action");
            }

        }

        public IList<ProjectReportViewModel> GetAllProjectReports(string IsActive, string Id, string ReportType)
        {

            connection = new CommPmApplicationConnection();
            var finalResult = new List<ProjectReportViewModel>();

            try
            {
                var result = connection.JobSheet.Select(jobSheetService => new ProjectReportViewModel
                {
                    JobSheetId = jobSheetService.JobSheetId,
                    JobSheetName = jobSheetService.JobSheetName,
                    AccountName = jobSheetService.AccountName,
                    AccountNumber = jobSheetService.AccountNumber,
                    JobOrShipToNumber = jobSheetService.JobOrShipToNumber,
                    JobName = jobSheetService.JobName,
                    IsActive = jobSheetService.IsActive,
                    ProjectManager = jobSheetService.ProjectManager,
                    TMCommissionPosition1Id = jobSheetService.TMCommissionPosition1Id,
                    TMCommissionPosition2Id = jobSheetService.TMCommissionPosition2Id,
                    LastModifiedDate = jobSheetService.LastModifiedDate,
                    LastModifiedUser = jobSheetService.User1.SMSN.Name

                });

                if (ReportType == "" || ReportType == null)
                {
                    //result = result.ToList();
                }
                else
                {
                    if (ReportType == "ByCustomer")
                    {
                        if (Id == "" || Id == null)
                        {
                            //result = result.ToList();
                        }
                        else
                        {
                            result = result.Where(u => u.AccountNumber == Id);
                        }
                    }
                    else if (ReportType == "ByPM")
                    {
                        if (Id == "" || Id == null)
                        {
                            //result = result.ToList();
                        }
                        else
                        {
                            var pmid = Convert.ToInt32(Id);
                            result = result.Where(u => u.ProjectManager == pmid);
                        }
                    }
                    else if (ReportType == "ByOSS")
                    {
                        if (Id == "" || Id == null)
                        {
                            //result = result.ToList();
                        }
                        else
                        {
                            var ossid = Convert.ToInt32(Id);
                            result = result.Where(u => u.TMCommissionPosition1Id == ossid);
                        }
                    }
                    else if (ReportType == "ByAlertStatus")
                    {
                        var jobsheetIds = new List<int?>();
                        jobsheetIds = connection.JobSheetAlert.Select(p => p.JobSheetId).ToList();
                        result = result.Where(u => jobsheetIds.Contains(u.JobSheetId));
                    }
                    else if (ReportType == "ByAwaitingCreditApproval")
                    {
                        var approvedIds = new List<int>();
                        var rejectedIds = new List<int>();
                        var SubmmitedToCreditIds = new List<int>();
                        var jobSheetCreditIds = new List<int>();

                        approvedIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 11) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                        rejectedIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 8) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                        jobSheetCreditIds = connection.JobSheetCompletedStep.Where(u => ((u.JobStepId == 8 || u.JobStepId == 11)) && (u.IsActive == true)).Select(u => u.JobSheetId).Distinct().ToList();
                        SubmmitedToCreditIds = connection.JobSheetCompletedStep.Where(u => (u.JobStepId == 7) && (!jobSheetCreditIds.Contains(u.JobSheetId)) && (u.IsActive == true)).Select(u => u.JobSheetId).ToList();

                        result = result.Where(u => SubmmitedToCreditIds.Contains(u.JobSheetId));
                    }
                    else
                    {
                        //result = result.ToList();
                    }
                }


                if (IsActive == "True")
                {
                    return result.Where(u => u.IsActive == true).ToList();

                }
                else if (IsActive == "False")
                {
                    return result.Where(u => u.IsActive == false).ToList();
                }
                else
                {
                    return result.ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetAllProjectReports method");
                return finalResult.ToList();
            }

            // return result;
        }

        public IEnumerable<ProjectReportViewModel> ProjectReportsRead(string IsActive, string Id, string ReportType)
        {
            return GetAllProjectReports(IsActive, Id, ReportType);
        }

        public List<ProjectsListBoxViewModel> getProjects(string IsActive, string Id, string ReportType)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<ProjectsListBoxViewModel>();

            IQueryable<JobSheet> resultSheets = connection.JobSheet;

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                switch (ReportType)
                {
                    case "customer":
                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            resultSheets = connection.JobSheet.Where(js => js.AccountNumber == Id);
                        }
                        else
                        {
                            resultSheets = connection.JobSheet.Where(js => (js.AccountNumber == Id) && (js.Company == conoid));
                        }

                        break;

                    case "pm":
                        var pmId = int.Parse(Id);

                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            resultSheets = connection.JobSheet.Where(js => js.ProjectManager == pmId);
                        }
                        else
                        {
                            resultSheets = connection.JobSheet.Where(js => (js.ProjectManager == pmId) && (js.Company == conoid));
                        }

                        break;

                    case "oss":
                        var ossId = int.Parse(Id);

                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            resultSheets = connection.JobSheet.Where(js => (js.TMCommissionPosition1Id == ossId) && (js.Company == conoid));
                        }
                        else
                        {
                            resultSheets = connection.JobSheet.Where(js => (js.TMCommissionPosition1Id == ossId) && (js.Company == conoid));
                        }

                        break;

                    case "alert":
                        var jobsheetIds = connection.JobSheetAlert.Select(p => p.JobSheetId).ToList();
                        resultSheets = connection.JobSheet.Where(js => jobsheetIds.Contains(js.JobSheetId));
                        break;

                    case "project":

                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            resultSheets = connection.JobSheet.Where(js => js.JobSheetName.Contains(Id));
                        }
                        else
                        {
                            resultSheets = connection.JobSheet.Where(js => (js.JobSheetName.Contains(Id)) && (js.Company == conoid));
                        }

                        break;
                }

                switch (IsActive)
                {
                    case "True":
                        return resultSheets.Where(sh => sh.IsActive == true).Select(js => new ProjectsListBoxViewModel
                        {
                            JobSheetId = js.JobSheetId,
                            JobSheetName = js.JobSheetName
                        }).OrderBy(js => js.JobSheetName).ToList();

                    case "False":
                        return resultSheets.Where(sh => sh.IsActive == false).Select(js => new ProjectsListBoxViewModel
                        {
                            JobSheetId = js.JobSheetId,
                            JobSheetName = js.JobSheetName
                        }).OrderBy(js => js.JobSheetName).ToList();

                    default:
                        return resultSheets.Select(js => new ProjectsListBoxViewModel
                        {
                            JobSheetId = js.JobSheetId,
                            JobSheetName = js.JobSheetName
                        }).OrderBy(js => js.JobSheetName).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class getProjects method");
            }

            return result;
        }

        public List<CustomerViewModel> GetCustomer()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<CustomerViewModel>();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    result = connection.Customer.Select(c => new CustomerViewModel
                    {
                        CustNo = c.CustNo,
                        CustomerName = c.CustomerName,
                        Cono = c.Cono
                    }).OrderBy(c => c.CustomerName).ToList();
                }
                else
                {
                    result = connection.Customer.Select(c => new CustomerViewModel
                    {
                        CustNo = c.CustNo,
                        CustomerName = c.CustomerName,
                        Cono = c.Cono
                    }).Where(c => c.Cono == conoid).OrderBy(c => c.CustomerName).ToList();

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetCustomer method");
            }

            return result;
        }

        public List<PMViewModel> GetProjectManagers()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<PMViewModel>();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());

                if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    result = connection.User.Where(u => u.RoleId == 3).Select(u => new PMViewModel
                    {
                        PmName = u.SMSN.Name,
                        PmId = u.UserId
                    }).OrderBy(u => u.PmName).ToList();
                }
                else
                {
                    result = connection.User.Where(u => (u.RoleId == 3) && (u.Cono == conoid)).Select(u => new PMViewModel
                    {
                        PmName = u.SMSN.Name,
                        PmId = u.UserId
                    }).OrderBy(u => u.PmName).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetProjectManagers method");
            }

            return result;
        }

        public List<PMViewModel> GetOutsideSalesReps()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<PMViewModel>();

            try
            {
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());

                if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    result = connection.User.Where(u => u.RoleId == 8).Select(c => new PMViewModel
                    {
                        PmName = c.SMSN.Name,
                        PmId = c.UserId
                    }).OrderBy(c => c.PmName).ToList();
                }
                else
                {
                    result = connection.User.Where(u => (u.RoleId == 8) && (u.Cono == conoid)).Select(c => new PMViewModel
                    {
                        PmName = c.SMSN.Name,
                        PmId = c.UserId
                    }).OrderBy(c => c.PmName).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetOutsideSalesReps method");
            }

            return result;
        }

        public List<CarrierSapReportLineModel> GetVendorData(string PurchaseOrder, string Material, string qty)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<CarrierSapReportLineModel>();

            try
            {
                var theQty = decimal.Parse(qty);
                result = connection.CarrierSapReportLineItems
                    .Where(c => c.CarrierSapReportHeader.PurchaseOrder == PurchaseOrder && c.Material == Material && c.OrderQty == theQty)
                    .Select(c => new CarrierSapReportLineModel
                    {
                        CPSD = c.CPSD ?? DateTime.MinValue,
                        CRSD = c.CRSD ?? DateTime.MinValue,
                        SalesOrder = c.SalesOrder,
                        PrevCPSD = c.PreviousCPSD ?? DateTime.MinValue,
                        Description = c.Description,
                        Id = c.Id,
                        ItemValue = c.ItemValue ?? 0,
                        LineNumber = c.LineNumber ?? 0,
                        Material = c.Material,
                        NetPrice = c.NetPrice ?? 0,
                        OrderQty = c.OrderQty ?? 0,
                        ShippedQty = c.ShippedQty ?? 0,
                        Rate = c.Rate ?? 0
                    }).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetVendorData method");
            }

            return result;
        }
        public IEnumerable<DashbordJobStatusViewModel> GetJobSheetStatusByJobSheetId(string JobSheetIds)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<DashbordJobStatusViewModel>();
            var JobSheetId = JobSheetIds.Split(',').Select(int.Parse).ToList();

            try
            {
                var sheets = connection.JobSheet.Where(p => p.IsActive == true && JobSheetId.Contains(p.JobSheetId));

                foreach (var sheet in sheets)
                {
                    var StepResult = new DashbordJobStatusViewModel();

                    StepResult.JobSheetId = sheet.JobSheetId;
                    StepResult.JobSheetName = sheet.JobSheetName;
                    StepResult.jobName = sheet.JobName;
                    StepResult.AccountName = sheet.AccountName;
                    StepResult.AccountNumber = sheet.AccountNumber;
                    StepResult.JobOrShipToNumber = sheet.JobOrShipToNumber;

                    var jobSheetStatus = connection.JobSheetCompletedStep.Where(p => p.JobSheetId == sheet.JobSheetId);

                    foreach (var jobStatus in jobSheetStatus)
                    {
                        if (jobStatus.JobSteps.StepName == "Pre-Submittal")
                        {
                            StepResult.sheetCreated = true;
                        }

                        if (jobStatus.JobSteps.StepName == "Submitted to Engineer")
                        {
                            StepResult.submittedToContractor = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                StepResult.submittedToContractorCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                StepResult.submittedToContractorCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Reviewed by Engineer")
                        {
                            StepResult.reviewedByContractor = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Approved by Engineer")
                        {
                            StepResult.approvedByContractor = jobStatus.IsActive == true;

                        }

                        if (jobStatus.JobSteps.StepName == "Returned by Engineer")
                        {
                            StepResult.returnedFromContractor = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                StepResult.returnedFromContractorCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                StepResult.returnedFromContractorCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Resubmitted to Engineer")
                        {
                            StepResult.resubmittedToContractor = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Submitted to Credit")
                        {
                            StepResult.submittedToCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                StepResult.submittedToCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                StepResult.submittedToCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Approved by Credit")
                        {
                            StepResult.approvedByCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                StepResult.approvedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                StepResult.approvedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Rejected by Credit")
                        {
                            StepResult.rejectedByCredit = jobStatus.IsActive == true;
                            if (jobStatus.LastModifiedDate.ToString() == string.Empty || jobStatus.LastModifiedDate.ToString() == null)
                                StepResult.rejectedByCreditCreatedDate = jobStatus.CreatedDate.ToString();
                            else
                                StepResult.rejectedByCreditCreatedDate = jobStatus.LastModifiedDate.ToString();
                        }

                        if (jobStatus.JobSteps.StepName == "Orders Created")
                        {
                            StepResult.ordersCreated = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Job Closed")
                        {
                            StepResult.jobclosed = jobStatus.IsActive == true;
                        }

                        if (jobStatus.JobSteps.StepName == "Job Synced")
                        {
                            StepResult.jobSynced = jobStatus.IsActive == true;
                        }
                    }

                    result.Add(StepResult);
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectsService class GetStatus method");
                return result.ToList();

            }
        }
        public string getSalesRep(string UserId)
        {
            var UserName = string.Empty;
            try
            {
                var result = connection.SMSN.Select(rep => new SMSNViewModel
                {

                    SlsRep = rep.SlsRep,
                    Name = rep.Name,
                    Email = rep.Email

                }).Where(u => u.SlsRep == UserId).SingleOrDefault();


                if (result != null)
                {
                    UserName = result.Name.ToString();
                }
                return UserName;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class getSalesRep method");
                return UserName;
            }
        }

        public Dictionary<string, Dictionary<string, string>> GetAlerts(int jobSheetId)
        {
            var result = new Dictionary<string, Dictionary<string, string>>();

            try
            {
                connection = new CommPmApplicationConnection();

                var JobSheet = connection.JobSheet.Single(u => u.IsActive == true && u.JobSheetId == jobSheetId);
                var orders = OrdersRead(JobSheet.JobOrShipToNumber, int.Parse(JobSheet.Customer.CustNo)).ToList();

                if (JobSheet.VerifiedVoltage == null || !JobSheet.VerifiedVoltage.Value)
                {
                    if (orders.Any())
                    {
                        result.Add("voltageVerified", new Dictionary<string, string>
                        {
                            {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                            {"JobSheetId", JobSheet.JobSheetId.ToString()},
                            {"NumberOfOrders", orders.Count().ToString()}
                        });
                    }
                }

                if (JobSheet.VendorPromisedShipDate != null && JobSheet.CustomerRequestedDeliveryDate != null &&
                    JobSheet.VendorPromisedShipDate.Value > JobSheet.CustomerRequestedDeliveryDate.Value)
                {
                    result.Add("shipDate", new Dictionary<string, string>
                    {
                        {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                        {"JobSheetId", JobSheet.JobSheetId.ToString()}
                    });
                }

                try
                {
                    var jobStep = JobSheet.JobSheetCompletedStep.OrderByDescending(js => js.JobSheetId)
                        .ThenByDescending(js => js.CreatedDate).First();
                    if (jobStep.JobStepId != 0 && jobStep.JobStepId == 8)
                    {
                        result.Add("creditRejected", new Dictionary<string, string>
                        {
                            {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                            {"JobSheetId", JobSheet.JobSheetId.ToString()}
                        });
                    }
                }
                catch
                {
                }

                if (orders.Any())
                {
                    decimal TotalAmount = 0;
                    foreach (var jobOrder in orders)
                    {
                        TotalAmount += Convert.ToDecimal(jobOrder.TotalOrderAmount);
                    }
                    if (TotalAmount > JobSheet.TotalPurchases)
                    {
                        result.Add("totalpoamount", new Dictionary<string, string>
                            {
                                {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                {"TotalAmount", TotalAmount.ToString()},
                                {"POAmount", JobSheet.TotalPurchases.ToString()}
                            });
                    }
                }

                if (orders.Count != 0)
                {
                    var OrderwithoutInvoice = orders.Where(x => (x.InvoiceDate >= DateTime.MinValue || x.InvoiceDate == null)).ToList();
                    if (OrderwithoutInvoice.Any())
                    {
                        foreach (var orderlist in OrderwithoutInvoice)
                        {
                            result.Add("InvoiceVerified", new Dictionary<string, string>
                            {
                                {"JobSheet", JobSheet.JobSheetName + " - " + JobSheet.JobName},
                                {"JobSheetId", JobSheet.JobSheetId.ToString()},
                                {"OrderNo", orderlist.OrderNo.ToString()}
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Debug(e, "Error occured in JobSheet class GetAlerts method");
            }

            return result;
        }

        public JobOrderViewModel UpdateJobOrderShipDate(JobOrderViewModel jobOrder)
        {
            connection = new CommPmApplicationConnection();

            try
            {
                var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
                if (connection.JobOrderShipDates.Any(sd => sd.JobOrderId == jobOrder.OrderId))
                {
                    var shipDate = connection.JobOrderShipDates.OrderByDescending(sd => sd.ModifiedDate)
                        .First(sd => sd.JobOrderId == jobOrder.OrderId);
                    if (shipDate.PrevCPSD != null)
                    {
                        var newShipDate = new JobOrderShipDates
                        {
                            JobOrderId = int.Parse(jobOrder.OrderId.ToString()),
                            CRSD = jobOrder.CRSD,
                            CPSD = jobOrder.CPSD,
                            PrevCPSD = shipDate.CPSD,
                            CreatedUser = userId,
                            CreatedDate = DateTime.Now,
                            ModifiedUser = userId,
                            ModifiedDate = DateTime.Now
                        };
                        connection.JobOrderShipDates.Attach(newShipDate);
                        connection.Entry(newShipDate).State = EntityState.Added;
                        connection.SaveChanges();

                        jobOrder.PrevCPSD = newShipDate.PrevCPSD;
                    }
                    else
                    {
                        shipDate.PrevCPSD = shipDate.CPSD;
                        shipDate.CPSD = jobOrder.CPSD;
                        shipDate.ModifiedDate = DateTime.Now;
                        shipDate.ModifiedUser = userId;

                        connection.JobOrderShipDates.Attach(shipDate);
                        connection.Entry(shipDate).State = EntityState.Modified;
                        connection.SaveChanges();

                        jobOrder.PrevCPSD = shipDate.PrevCPSD;
                    }
                }
                else
                {
                    var newShipDate = new JobOrderShipDates
                    {
                        JobOrderId = int.Parse(jobOrder.OrderId.ToString()),
                        CRSD = jobOrder.CRSD,
                        CPSD = jobOrder.CPSD,
                        CreatedUser = userId,
                        CreatedDate = DateTime.Now,
                        ModifiedUser = userId,
                        ModifiedDate = DateTime.Now
                    };

                    connection.JobOrderShipDates.Attach(newShipDate);
                    connection.Entry(newShipDate).State = EntityState.Added;
                    connection.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in JobSheet class UpdateJobOrderShipDate method");
            }

            return jobOrder;
        }
        public IEnumerable<ProjectOrderLineViewModel> OrderLineRead(string ShipTo, int CustNo, int OrderNo, int OrderSurf)
        {
            return GetAllOrderLines(ShipTo, CustNo, OrderNo, OrderSurf);
        }

        public IList<ProjectOrderLineViewModel> GetAllOrderLines(string ShipTo, int CustNo, int OrderNo, int OrderSurf)
        {
            connection = new CommPmApplicationConnection();
            List<ProjectOrderLineViewModel> result = new List<ProjectOrderLineViewModel>();

            try
            {
                result = connection.ProjectOrderLine.Select(projectorderline => new ProjectOrderLineViewModel
                {
                    ProjectOrderLineId = projectorderline.ProjectOrderLineId,
                    OrderNo = projectorderline.OrderNo,
                    OrderSurf = projectorderline.OrderSurf,
                    LineNumber = projectorderline.LineNumber,
                    ModelPart = projectorderline.ModelPart,
                    POAmount = projectorderline.POAmount,
                    QuantityOrdered = projectorderline.QuantityOrdered,
                    QuantityShipped = projectorderline.QuantityShipped,
                    CustomerRequestedDeliveryDate = projectorderline.CustomerRequestedDeliveryDate,
                    VendorName = projectorderline.VendorName,
                    VerifiedVoltage = projectorderline.VerifiedVoltage,
                    CustNo = projectorderline.CustNo,
                    ShipTo = projectorderline.ShipTo,
                    VendorNumber = projectorderline.VendorNumber,
                    OrderAmount = projectorderline.OrderAmount * projectorderline.QuantityOrdered,
                    NetAmount = projectorderline.NetAmount,
                    OrderAltNo = projectorderline.OrderAltNo,
                    OrderLineCreatedDate = projectorderline.OrderLineCreatedDate,
                    Vendor = new VendorViewModel()
                    {
                        VendorNumber = projectorderline.Vendor.VendorNumber,
                        VendorName = projectorderline.Vendor.VendorName
                    }
                }).Where(p => (p.ShipTo == ShipTo) && (p.CustNo == CustNo) && (p.OrderNo == OrderNo) && (p.OrderSurf == OrderSurf)).ToList();


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ProjectService class GetAllOrderLines method");
            }

            return result;
        }
    }
}
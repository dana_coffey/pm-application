﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using ActiveUp.Net.Mail;
using ExcelDataReader;
using Microsoft.SharePoint.Client;
using NLog;
using PM_Website.Models;
using Logger = NLog.Logger;

namespace PM_Website.Services
{
    public static class ReportService
    {
        static Logger logger = LogManager.GetLogger("ErrorLog");
        private static CommPmApplicationConnection connection;

        public static void Dispose()
        {
            connection.Dispose();
        }

        public static DataTable GetOrderStatusReport()
        {
            var rep = new IMAPService("outlook.office365.com", 993, true, "jcoffey@mingledorffs.com", "1l0v3g1n@");
            foreach (var email in rep.GetAllMails("Inbox"))
            {
                if (email.Attachments.Count <= 0) continue;
                foreach (MimePart attachment in email.Attachments)
                {
                    try
                    {
                        if (attachment.ContentType.MimeType ==
                            "application/vnd.ms-excel")
                        {
                            var theStream = new MemoryStream(attachment.BinaryContent);
                            IExcelDataReader excelReader = ExcelReaderFactory.CreateCsvReader(theStream);

                            var conf = new ExcelDataSetConfiguration
                            {
                                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                                {
                                    UseHeaderRow = true
                                }
                            };

                            return excelReader.AsDataSet(conf).Tables[0];
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex, "Error occured in EOOReportService class GetOrderStatusReport method");
                    }
                }
            }
            return new DataTable();
        }

        public static T getItem<T> (this string text, int start, int length)
        {
            var theString = text.Substring(start, length).Trim();

            try
            {
                if (typeof(T) == typeof(int))
                {
                    return (T) Convert.ChangeType(int.Parse(theString), typeof(T));
                }

                if (typeof(T) == typeof(decimal))
                {
                    return (T) Convert.ChangeType(decimal.Parse(theString), typeof(T));
                }

                if (typeof(T) == typeof(string))
                {
                    return (T) Convert.ChangeType(theString, typeof(T));
                }

                if (typeof(T) == typeof(bool))
                {
                    return (T) Convert.ChangeType(theString == "X", typeof(T));
                }

                if (typeof(T) == typeof(DateTime))
                {
                    while (theString.Length < 8)
                    {
                        theString = "0" + theString;
                    }

                    if (int.Parse(theString) == 0)
                    {
                        return default(T);
                    }

                    var year = int.Parse(theString.Substring(0, 4));
                    var month = int.Parse(theString.Substring(4, 2));
                    var day = int.Parse(theString.Substring(6, 2));
                    var theDate = new DateTime(year, month, day);

                    return (T) Convert.ChangeType(theDate, typeof(T));
                }

                if (typeof(T) == typeof(TimeSpan))
                {
                    while (theString.Length < 6)
                    {
                        theString = "0" + theString;
                    }

                    theString = theString.Length == 6 ? theString : "0" + theString;
                    var hour = int.Parse(theString.Substring(0, 2));
                    var min = int.Parse(theString.Substring(2, 2));
                    var sec = int.Parse(theString.Substring(4, 2));
                    var theTime = new TimeSpan(hour, min, sec);

                    return (T) Convert.ChangeType(theTime, typeof(T));
                }
            }
            catch
            {
                return default(T);
            }

            return default(T);
        }

        public static string ImportCarrierSapReport()
        {
            connection = new CommPmApplicationConnection();
            var theFile = System.IO.File.ReadAllLines("C:/Projects/EMail/20080_OrderStatus_03112019_100126.dat");
            var result = "";

            var existingOrders = connection.CarrierSapReportHeader.Select(srh => srh.SalesOrder).ToList();

            foreach (var line in theFile)
            {
                var salesOrder = line.Substring(0, 10);

                var header = new CarrierSapReportHeader();
                var shipToItem = new CarrierSapReportShipTo();
                var lineItem = new CarrierSapReportLineItems();

                if (line.Contains(salesOrder + "H1"))
                {

                    var salesDocNumber = line.getItem<int>(0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var soldToParty = line.getItem<int>( 12, 10);
                    var shipToPartner = line.getItem<int>( 22, 10);
                    var divisionAtHeader = line.getItem<string>( 32, 2);
                    var purchaseOrderNo = line.getItem<string>( 34, 35);
                    var customerPurchaseOrderDate = line.getItem<DateTime>( 69, 8);
                    var shipToPoNumber = line.getItem<string>( 77, 12);
                    var salesDocumentType = line.getItem<string>( 89, 4);
                    var completeDeliveryFlag = line.getItem<bool>( 93, 1);
                    var deliveryBlock = line.getItem<string>( 94, 2);
                    var overallOrderStatus = line.getItem<string>( 96, 30);
                    var dateEntered = line.getItem<DateTime>( 126, 8);
                    var timeEntered = line.getItem<TimeSpan>( 134, 6);
                    var createdBy = line.getItem<string>( 140, 12);
                    var shippingConditions = line.getItem<string>( 152, 2);
                    var incoterms = line.getItem<string>( 154, 3);
                    var netValue = line.getItem<decimal>( 157, 21);
                    var countryCode = line.getItem<string>( 178, 3);

                    var dateTimeEntered = dateEntered + timeEntered;

                    header.SalesOrder = salesDocNumber;
                    header.PurchaseOrder = purchaseOrderNo;
                    //header.LastChanged = lastChangedDate;
                    header.Consignee = shipToPoNumber;
                    header.OverallOrderStatus = overallOrderStatus;
                    header.DateEntered = dateTimeEntered;

                    if (!existingOrders.Contains(header.SalesOrder))
                    {
                        connection.CarrierSapReportHeader.Attach(header);
                        connection.Entry(header).State = EntityState.Added;
                        connection.SaveChanges();
                    }
                }
                if (line.Contains(salesOrder + "H2"))
                {
                    var salesDocNumber = line.getItem<int>( 0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var from = line.getItem<string>( 12, 35);
                    var careOf = line.getItem<string>( 47, 35);
                    var address = line.getItem<string>( 82, 35);
                    var city = line.getItem<string>( 117, 35);
                    var state = line.getItem<string>( 152, 3);
                    var zip = line.getItem<string>( 155, 10);
                }

                if (line.Contains(salesOrder + "H3"))
                {
                    var salesDocNumber = line.getItem<int>( 0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var to = line.getItem<string>( 12, 35);
                    var careOf = line.getItem<string>( 47, 35);
                    var address = line.getItem<string>( 82, 35);
                    var city = line.getItem<string>( 117, 35);
                    var state = line.getItem<string>( 152, 3);
                    var zip = line.getItem<string>( 155, 10);

                    shipToItem.SalesOrder = salesDocNumber;
                    shipToItem.ShipTo = to;
                    shipToItem.CareOf = careOf;
                    shipToItem.Address = address;
                    shipToItem.City = city;
                    shipToItem.State = state;
                    shipToItem.Zip = zip;

                    connection.CarrierSapReportShipTo.Attach(shipToItem);
                    connection.Entry(shipToItem).State = EntityState.Added;
                    connection.SaveChanges();
                }

                if (line.Contains(salesOrder + "H4"))
                {
                    var salesDocNumber = line.getItem<int>( 0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var to = line.getItem<string>( 12, 35);
                    var phone = line.getItem<string>( 47, 16);

                    shipToItem = connection.CarrierSapReportShipTo.First(e => e.SalesOrder == salesDocNumber);
                    
                    shipToItem.Attn = to;
                    shipToItem.Phone = phone;

                    connection.CarrierSapReportShipTo.Attach(shipToItem);
                    connection.Entry(shipToItem).State = EntityState.Modified;
                    connection.SaveChanges();
                }

                if (line.Contains(salesOrder + "H5"))
                {
                    var salesDocNumber = line.getItem<int>( 0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var notes = line.getItem<string>( 12, 132);
                }

                if (line.Contains(salesOrder + "L1"))
                {
                    var salesDocNumber = line.getItem<int>( 0, 10);
                    var recordType = line.getItem<string>( 10, 2);
                    var item = line.getItem<int>( 12, 6);
                    var lineItemNumberCustomerOrder = line.getItem<int>( 18, 6);
                    var scheduleLineNumber = line.getItem<int>( 24, 4);
                    var material = line.getItem<string>( 28, 18);
                    var description = line.getItem<string>( 46, 40);
                    var materialGroup = line.getItem<string>( 86, 9);
                    var paymentTerms = line.getItem<string>( 95, 4);
                    var paymentTermsDesc = line.getItem<string>( 99, 30);
                    var division = line.getItem<string>( 129, 2);
                    var targetQtyinSalesUnits = line.getItem<decimal>(131, 17);
                    var scheduleLineQty = line.getItem<decimal>(148, 17);
                    var materialType = line.getItem<string>(165, 25);
                    var deliveryBlock = line.getItem<string>(190, 2);
                    var shippingPoint = line.getItem<string>(192, 4);
                    var CRSD = line.getItem<DateTime>(196, 8);
                    var CPSD = line.getItem<DateTime>(204, 8);
                    var PreviousCPSD = line.getItem<DateTime>(212, 8);
                    var orderStatus = line.getItem<string>(220, 30);
                    var confirmedQty = line.getItem<decimal>(250, 17);
                    var masterListPrice = line.getItem<decimal>(267, 16);
                    var netPrice = line.getItem<decimal>(283, 14);
                    var netValue = line.getItem<decimal>(297, 16);
                    var shippedQty = line.getItem<decimal>(313, 17);
                    var cancelQty = line.getItem<decimal>(330, 17);
                    var openQty = line.getItem<decimal>(347, 17);
                    var partialDelivery = line.getItem<bool>(364, 1);
                    var deliveryPriority = line.getItem<string>(365, 2);
                    var productHierarchy = line.getItem<string>(367, 18);
                    var freightOptions1 = line.getItem<string>(385, 2);
                    var freightType = line.getItem<string>(387, 4);
                    var freightOptions2 = line.getItem<string>(391, 4);
                    var name1 = line.getItem<string>(395, 35);
                    var name2 = line.getItem<string>(430, 35);
                    var street = line.getItem<string>(465, 35);
                    var city = line.getItem<string>(500, 35);
                    var state = line.getItem<string>(535, 3);
                    var zip = line.getItem<string>(538, 10);
                    var netValue2 = line.getItem<decimal>(548, 20);

                    lineItem.SalesOrder = salesDocNumber;
                    lineItem.LineNumber = item;
                    lineItem.Material = material;
                    lineItem.Description = description;
                    lineItem.OrderQty = confirmedQty;
                    lineItem.ShippedQty = shippedQty;
                    if (CRSD != "00000000".getItem<DateTime>(0, 8))
                    {
                        lineItem.CRSD = CRSD;
                    }
                    else
                    {
                       
                    }
                    if (CPSD != "00000000".getItem<DateTime>(0, 8))
                    {
                        lineItem.CPSD = CPSD;
                    }
                    else
                    {

                    }
                    if (PreviousCPSD != "00000000".getItem<DateTime>(0, 8))
                    {
                        lineItem.PreviousCPSD = PreviousCPSD;
                    }
                    else
                    {

                    }
                    lineItem.Rate = targetQtyinSalesUnits;
                    lineItem.NetPrice = netPrice;
                    lineItem.ItemValue = netValue;

                    try
                    {
                        connection.CarrierSapReportLineItems.Attach(lineItem);
                        connection.Entry(lineItem).State = EntityState.Added;
                        connection.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                    }
                }
            }

            System.IO.File.Move("C:/Projects/EMail/20080_OrderStatus_03112019_100126.dat", "C:/Projects/EMail/20080_OrderStatus_03112019_100126.completed.dat");

            return result;
        }

        public static List<CarrierSapReportModel> GetSAPReport()
        {
            connection = new CommPmApplicationConnection();
            var result = new List<CarrierSapReportModel>();

            try
            {
                result = connection.CarrierSapReportHeader.Select(c=>new CarrierSapReportModel
                {
                    SalesOrder = c.SalesOrder,
                    Consignee = c.Consignee,
                    DateEntered = c.DateEntered ?? DateTime.MinValue,
                    LastChanged = c.LastChanged ?? DateTime.MinValue,
                    OverallOrderStatus = c.OverallOrderStatus,
                    PurchaseOrder = c.PurchaseOrder
                }).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ReportService class GetSAPReport method");
            }

            return result;
        }

        public static List<CarrierSapReportLineModel> GetSAPReportLineItems(int salesOrder)
        {
            connection = new CommPmApplicationConnection();
            var result = new List<CarrierSapReportLineModel>();

            try
            {
                result = connection.CarrierSapReportLineItems.Select(c => new CarrierSapReportLineModel
                {
                    SalesOrder = c.SalesOrder,
                    Material = c.Material,
                    Description = c.Description,
                    LineNumber = c.LineNumber.Value,
                    OrderQty = c.OrderQty.Value,
                    ShippedQty = c.ShippedQty.Value,
                    CRSD = c.CRSD ?? DateTime.MinValue,
                    CPSD = c.CPSD ?? DateTime.MinValue,
                    PrevCPSD = c.PreviousCPSD ?? DateTime.MinValue,
                    Rate = c.Rate.Value,
                    NetPrice = c.NetPrice.Value,
                    ItemValue = c.ItemValue.Value
                }).Where(c=>c.SalesOrder == salesOrder).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in ReportService class GetSAPReport method");
            }

            return result;
        }
    }
}
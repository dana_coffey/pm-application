﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLog.Internal;
using PM_Website.Models;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace PM_Website
{
    public class SecurityHelper
    {
        private static CommPmApplicationConnection connection;
        UserService userService = new UserService(connection);
        private string GetWindowsVcnUserName()
        {
            // Get windows user  
            var loggedUser = System.Security.Principal.WindowsIdentity.GetCurrent();
            if (loggedUser != null)
            {
               
                string username = loggedUser.Name;  // here is the Username
                if(username.Contains("IIS APPPOOL"))
                {
                    username = HttpContext.Current.User.Identity.Name;
                    username = username.Substring(username.IndexOf('\\') + 1);
                }
                else
                {
                    username = username.Substring(username.IndexOf('\\') + 1);
                }
                
                return username;
            }
            return null;
        }

        public virtual bool Authenticate()
        {
            string userName = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["testModeUser"]) ? ConfigurationManager.AppSettings["testModeUser"] : GetWindowsVcnUserName();

            //// Get user from external authorization system  
            var users = userService.GetUser(userName + "@mingledorffs.com");
            if (users == null || users.Count() == 0)
            {
                createEmptySessions();
                var users1 = userService.GetUser(userName + "@holdenassoc.com");
                if(users1 == null || users1.Count() == 0)
                {
                    createEmptySessions();
                }
                else
                {
                    createUserSessions(users1, userName);
                }
                
            }
            else
            {
                createUserSessions(users, userName);
            }
            return true;
        }

        private void createUserSessions(IEnumerable<UserViewModel> users, string userName)
        {
            try
            {
                foreach (UserViewModel user in users)
                {
                    HttpContext.Current.Session.Add("mainUserRole", user.UserRole.RoleName);
                    HttpContext.Current.Session.Add("mainUserRoleId", user.UserRole.RoleId);
                    HttpContext.Current.Session.Add("mainUserId", user.UserId);
                    HttpContext.Current.Session.Add("mainUserName", user.Email);
                    HttpContext.Current.Session.Add("mainUserfirstNameLastName", user.SMSN.Name);
                    HttpContext.Current.Session.Add("mainUserHomepage", userService.GetHomepage(user.UserId));
                    HttpContext.Current.Session.Add("mainUserCono", user.Cono);

                    HttpContext.Current.Session.Add("userRole", user.UserRole.RoleName);
                    HttpContext.Current.Session.Add("userRoleId", user.UserRole.RoleId);
                    HttpContext.Current.Session.Add("userId", user.UserId);
                    HttpContext.Current.Session.Add("userName", user.Email);
                    HttpContext.Current.Session.Add("firstNameLastName", user.SMSN.Name);
                    HttpContext.Current.Session.Add("userLogin", userName);
                    HttpContext.Current.Session.Add("userHomepage", userService.GetHomepage(user.UserId));
                    HttpContext.Current.Session.Add("userCono", user.Cono);
                }
            }
            catch(Exception ex)
            {
                createEmptySessions();
            }
        }

        private void createEmptySessions()
        {
            try
            {
                HttpContext.Current.Session.Add("mainUserRole", "");
                HttpContext.Current.Session.Add("mainUserRoleId", "0");
                HttpContext.Current.Session.Add("mainUserId", "0");
                HttpContext.Current.Session.Add("mainUserName", "");
                HttpContext.Current.Session.Add("mainUserfirstNameLastName", "");
                HttpContext.Current.Session.Add("mainUserHomepage", "");
                HttpContext.Current.Session.Add("mainUserCono", "0");

                HttpContext.Current.Session.Add("userRole", "");
                HttpContext.Current.Session.Add("userRoleId", "0");
                HttpContext.Current.Session.Add("userId", "0");
                HttpContext.Current.Session.Add("userName", "");
                HttpContext.Current.Session.Add("firstNameLastName", "");
                HttpContext.Current.Session.Add("userLogin", "");
                HttpContext.Current.Session.Add("userHomepage", "");
                HttpContext.Current.Session.Add("userCono", "0");
            }
            catch(Exception ex)
            {

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Configuration;

namespace PM_Website.Services
{
    public class SxeQueryService
    {
        //SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CommPmApplicationConnectionString"].ToString());       

        public DataSet ARSS(decimal custno, string shipto, int cono, SqlConnection connection)
        {
            SqlCommand cmdARSS = new SqlCommand("stp_getARSSFromSXE", connection);
            cmdARSS.CommandType = CommandType.StoredProcedure;
            cmdARSS.Parameters.AddWithValue("@custno", custno);
            cmdARSS.Parameters.AddWithValue("@shipto", shipto);
            cmdARSS.Parameters.AddWithValue("@cono", cono);

            SqlDataAdapter daARSS = new SqlDataAdapter(cmdARSS);
            DataSet dsARSS = new DataSet();
            daARSS.Fill(dsARSS);

            return dsARSS;
        }

        public DataSet OEEH(decimal custno, string shipto, int cono, SqlConnection connection)
        {
            SqlCommand cmdOEEH = new SqlCommand("stp_getOEEHFromSXE", connection);
            cmdOEEH.CommandType = CommandType.StoredProcedure;
            cmdOEEH.Parameters.AddWithValue("@custno", custno);
            cmdOEEH.Parameters.AddWithValue("@shipto", shipto);
            cmdOEEH.Parameters.AddWithValue("@cono", cono);

            SqlDataAdapter daOEEH = new SqlDataAdapter(cmdOEEH);
            DataSet dsOEEH = new DataSet();
            daOEEH.Fill(dsOEEH);

            return dsOEEH;
        }

        public DataSet OEEL(decimal custno, string shipto, int orderno, int ordersuf, int cono, SqlConnection connection)
        {
            SqlCommand cmdOEEL = new SqlCommand("stp_getOEELFromSXE", connection);
            cmdOEEL.CommandType = CommandType.StoredProcedure;
            cmdOEEL.Parameters.AddWithValue("@custno", custno);
            cmdOEEL.Parameters.AddWithValue("@shipto", shipto);
            cmdOEEL.Parameters.AddWithValue("@orderno", orderno);
            cmdOEEL.Parameters.AddWithValue("@ordersuf", ordersuf);
            cmdOEEL.Parameters.AddWithValue("@cono", cono);

            SqlDataAdapter daOEEL = new SqlDataAdapter(cmdOEEL);
            DataSet dsOEEL = new DataSet();
            daOEEL.Fill(dsOEEL);

            return dsOEEL;
        }
    }
}
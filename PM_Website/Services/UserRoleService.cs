﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using NLog;

namespace PM_Website
{
    public class UserRoleService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        
        private CommPmApplicationConnection connection;

        public UserRoleService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        }

        public IList<UserRoleViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<UserRoleViewModel> result = new List<UserRoleViewModel>();
            try
            {
                result = connection.UserRoles.Select(role => new UserRoleViewModel
                {
                    RoleId = role.RoleId,
                    RoleName = role.RoleName,
                    Permissions = role.Permissions,
                    IsActive = role.IsActive
                }).ToList();

                if (IsActive == "True")
                {
                    return result.Where(u => u.IsActive == true).ToList();
                }
                else if (IsActive == "False")
                {
                    return result.Where(u => u.IsActive == false).ToList();
                }
                else
                {
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserRoleService class GetAll method");
                return result.ToList();
            }           
        }

        public IEnumerable<UserRoleViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Update(UserRoleViewModel role)
        {
            try
            {
                var entity = new UserRoles();
                entity.RoleId = role.RoleId;
                entity.RoleName = role.RoleName;
                entity.Permissions = role.Permissions;
                entity.IsActive = role.IsActive;
                connection = new CommPmApplicationConnection();
                connection.UserRoles.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserRoleService class Update method");
            }
        }
    }
}
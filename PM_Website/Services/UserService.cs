﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PM_Website.Models;
using System.Data.Entity;
using Newtonsoft.Json;
using NLog;

namespace PM_Website
{
    public class UserService : IDisposable
    {
        Logger logger = LogManager.GetLogger("ErrorLog");
        
        private CommPmApplicationConnection connection;

        public UserService(CommPmApplicationConnection connection)
        {
            this.connection = connection;
        }

        public void Dispose()
        {
            connection.Dispose();
        } 
        
        public IList<UserViewModel> GetAll(string IsActive)
        {
            connection = new CommPmApplicationConnection();
            List<UserViewModel> result = new List<UserViewModel>();
            try
            {
                result = connection.User.Select(user => new UserViewModel
                {
                    UserId = user.UserId,
                    RoleId = user.RoleId,
                    IsActive = user.IsActive,
                    Email = user.Email,
                    SlsRep = user.SlsRep,
                    Name = user.SMSN.Name,
                    Cono = user.Cono,
                    UserRole = new UsersUserRoleViewModel()
                    {
                        RoleId = user.UserRoles.RoleId,
                        RoleName = user.UserRoles.RoleName
                    },
                    SMSN = new SMSNViewModel()
                    {
                        SlsRep = user.SMSN.SlsRep,
                        Name = user.SMSN.Name,
                        Email = user.SMSN.Email
                    }
                    //,
                    //Company = new CompanyViewModel()
                    //{
                    //    Cono = user.Company.Cono,
                    //    CompanyName = user.Company.CompanyName
                    //}
                }).ToList();

                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    if (IsActive == "True")
                    {
                        return result.Where(u => (u.IsActive == true) && (u.RoleId == 3 || u.RoleId == 8) && (u.Cono==conoid)).ToList();
                    }
                    else if (IsActive == "False")
                    {
                        return result.Where(u => (u.IsActive == false) && (u.RoleId == 3 || u.RoleId == 8) && (u.Cono == conoid)).ToList();
                    }
                    else
                    {
                        return result.Where(u => (u.RoleId == 3 || u.RoleId == 8) && (u.Cono == conoid)).ToList();
                    }
                }
                else if (HttpContext.Current.Session["userRoleId"].ToString() == "7")
                {
                    if (IsActive == "True")
                    {
                        return result.Where(u => (u.IsActive == true) && (u.RoleId == 5 || u.RoleId == 6 || u.RoleId == 7) && (u.Cono == conoid)).ToList();
                    }
                    else if (IsActive == "False")
                    {
                        return result.Where(u => (u.IsActive == false) && (u.RoleId == 5 || u.RoleId == 6 || u.RoleId == 7) && (u.Cono == conoid)).ToList();
                    }
                    else
                    {
                        return result.Where(u => (u.RoleId == 5 || u.RoleId == 6 || u.RoleId == 7) && (u.Cono == conoid)).ToList();
                    }
                }
                else
                {
                    if (IsActive == "True")
                    {
                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            return result.Where(u => u.IsActive == true).ToList();
                        }
                        else
                        {
                            return result.Where(u => (u.IsActive == true) && (u.Cono==conoid)).ToList();
                        }
                            
                    }
                    else if (IsActive == "False")
                    {
                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            return result.Where(u => u.IsActive == false).ToList();
                        }
                        else
                        {
                            return result.Where(u => (u.IsActive == false) && (u.Cono == conoid)).ToList();
                        }
                    }
                    else
                    {
                        if (HttpContext.Current.Session["userRoleId"].ToString() == "1")
                        {
                            return result.ToList();
                        }
                        else
                        {
                            return result.Where(u => u.Cono == conoid).ToList();
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetAll method");
                return result.ToList();
            }            
        }

        public IEnumerable<UserViewModel> Read(string IsActive)
        {
            return GetAll(IsActive);
        }

        public void Create(UserViewModel user)
        {
            var emailService = new EmailService(connection);
            try
            {
                var entity = new User();

                entity.RoleId = user.RoleId;                

                entity.IsActive = user.IsActive;

                entity.Cono = user.Cono;

                if (user.SMSN != null)
                {
                    entity.SlsRep = user.SMSN.SlsRep;
                    entity.Email = user.SMSN.Email;
                }
                connection = new CommPmApplicationConnection();

                connection.User.Add(entity);
                connection.SaveChanges();

                user.UserId = entity.UserId;

                if(HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    if(entity.RoleId == 3)
                    {
                        List<ManagerPMViewModel> checkManagerPM = new List<ManagerPMViewModel>();
                        int managerid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                        checkManagerPM = connection.ManagerPM.Select(c => new ManagerPMViewModel
                        {
                            ManagerPMId = c.ManagerPMId,
                            ManagerId = c.ManagerId,
                            PMId = c.PMId

                        }).Where(e => (e.ManagerId == managerid) && (e.PMId == user.UserId)).ToList();

                        if (checkManagerPM.Count > 0)
                        {
                            
                        }
                        else
                        {
                            var entitymanager = new ManagerPM();

                            entitymanager.ManagerId = managerid;

                            entitymanager.PMId = user.UserId;

                            entitymanager.CreatedBy = managerid;

                            entitymanager.IsActive = true;

                            connection.ManagerPM.Add(entitymanager);
                            connection.SaveChanges();
                        }
                    }
                   
                }
                emailService.CreateNewUserEmail(user.UserId);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class Create method");
            }
        }

        public void Update(UserViewModel user)
        {
            try
            {
                var entity = new User();

                entity.UserId = user.UserId;
                
                entity.IsActive = user.IsActive;

                entity.RoleId = user.RoleId;

                entity.Cono = user.Cono;

                if (user.SMSN != null)
                {
                    entity.SlsRep = user.SMSN.SlsRep;
                    entity.Email = user.SMSN.Email;
                }

                connection = new CommPmApplicationConnection();
                connection.User.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;
                connection.SaveChanges();

                if (HttpContext.Current.Session["userRoleId"].ToString() == "2")
                {
                    if (user.RoleId == 3)
                    {
                        List<ManagerPMViewModel> checkManagerPM = new List<ManagerPMViewModel>();
                        int managerid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                        checkManagerPM = connection.ManagerPM.Select(c => new ManagerPMViewModel
                        {
                            ManagerPMId = c.ManagerPMId,
                            ManagerId = c.ManagerId,
                            PMId = c.PMId

                        }).Where(e => (e.ManagerId == managerid) && (e.PMId == user.UserId)).ToList();

                        if (checkManagerPM.Count > 0)
                        {

                        }
                        else
                        {
                            var entitymanager = new ManagerPM();

                            entitymanager.ManagerId = managerid;

                            entitymanager.PMId = user.UserId;

                            entitymanager.CreatedBy = managerid;

                            entitymanager.IsActive = true;

                            connection.ManagerPM.Add(entitymanager);
                            connection.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class Update method");
            }
        }

        public IEnumerable<UserViewModel> GetUser(string Username)
        {
            return GetUserByUsername(Username);
        }

        public IList<UserViewModel> GetUserByUsername(string Username)
        {
            connection = new CommPmApplicationConnection();
            List<UserViewModel> result = new List<UserViewModel>();

            try
            {
                if (Username == "" || Username == null)
                {
                    
                }
                else
                {
                    result = connection.User.Select(user => new UserViewModel
                    {
                        UserId = user.UserId,
                        RoleId = user.RoleId,
                        IsActive = user.IsActive,
                        Email = user.Email,
                        SlsRep = user.SlsRep,
                        Cono = user.Cono,
                        UserRole = new UsersUserRoleViewModel()
                        {
                            RoleId = user.UserRoles.RoleId,
                            RoleName = user.UserRoles.RoleName
                        },
                        SMSN = new SMSNViewModel()
                        {
                            SlsRep = user.SMSN.SlsRep,
                            Name = user.SMSN.Name,
                            Email = user.SMSN.Email
                        }
                        //,
                        //Company = new CompanyViewModel()
                        //{
                        //    Cono = user.Company.Cono,
                        //    CompanyName = user.Company.CompanyName
                        //}
                    }).Where(u => (u.IsActive == true) && (u.Email == Username)).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetUserByUsername method");
            }

            return result;
        }


        public List<SMSNViewModel> GetEmailByUser(string id)
        {
            connection = new CommPmApplicationConnection();
            List<SMSNViewModel> smsnEmail = new List<SMSNViewModel>();
            try
            {
                smsnEmail = connection.SMSN.Select(SMSN => new SMSNViewModel
                {

                    SlsRep = SMSN.SlsRep,
                    CoNo = SMSN.CoNo,
                    SlsType = SMSN.SlsType,
                    Name = SMSN.Name,
                    Email = SMSN.Email,
                    IsActive = SMSN.IsActive

                }).Where(p => p.Name == id).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetEmailByUser method");
            }
            return smsnEmail;
        }

        public IList<UserViewModel> GetAllUsersByRole(int RoleId)
        {
            connection = new CommPmApplicationConnection();
            List<UserViewModel> result = new List<UserViewModel>();
            try
            {
               
                int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());

                if(HttpContext.Current.Session["userRoleId"].ToString() == "1")
                {
                    result = connection.User.Select(user => new UserViewModel
                    {
                        UserId = user.UserId,
                        RoleId = user.RoleId,
                        IsActive = user.IsActive,
                        Email = user.Email,
                        SlsRep = user.SlsRep,
                        Cono = user.Cono,
                        UserRole = new UsersUserRoleViewModel()
                        {
                            RoleId = user.UserRoles.RoleId,
                            RoleName = user.UserRoles.RoleName
                        },
                        SMSN = new SMSNViewModel()
                        {
                            SlsRep = user.SMSN.SlsRep,
                            Name = user.SMSN.Name.TrimStart(),
                            Email = user.SMSN.Email
                        }
                        //,
                        //Company = new CompanyViewModel()
                        //{
                        //    Cono = user.Company.Cono,
                        //    CompanyName = user.Company.CompanyName
                        //}
                    }).Where(p => p.RoleId == RoleId).OrderBy(x => x.SMSN.Name).ToList();

                }
                else
                {
                    result = connection.User.Select(user => new UserViewModel
                    {
                        UserId = user.UserId,
                        RoleId = user.RoleId,
                        IsActive = user.IsActive,
                        Email = user.Email,
                        SlsRep = user.SlsRep,
                        Cono = user.Cono,
                        UserRole = new UsersUserRoleViewModel()
                        {
                            RoleId = user.UserRoles.RoleId,
                            RoleName = user.UserRoles.RoleName
                        },
                        SMSN = new SMSNViewModel()
                        {
                            SlsRep = user.SMSN.SlsRep,
                            Name = user.SMSN.Name.TrimStart(),
                            Email = user.SMSN.Email
                        }
                        //,
                        //Company = new CompanyViewModel()
                        //{
                        //    Cono = user.Company.Cono,
                        //    CompanyName = user.Company.CompanyName
                        //}
                    }).Where(p => (p.RoleId == RoleId) && (p.Cono == conoid)).OrderBy(x => x.SMSN.Name).ToList();                    
                }

                return result.ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetAll method");
                return result.ToList();
            }
        }
        public IEnumerable<UserViewModel> ReadByRoleId(int RoleId)
        {
            return GetAllUsersByRole(RoleId);
        }

        public void SetHomepage(string theURL, int userID)
        {
            connection = new CommPmApplicationConnection();
            var theUser = connection.User.Where(user => user.UserId == userID);
            var currentUser = theUser.First();
            currentUser.UserHomepage = theURL;

            HttpContext.Current.Session.Add("userHomepage",theURL);
            HttpContext.Current.Session.Add("mainUserHomepage", theURL);

            connection.User.Attach(currentUser);
            connection.Entry(currentUser).State = EntityState.Modified;
            connection.SaveChanges();
        }

        public void RemoveHomepage(string theURL, int userID)
        {
            connection = new CommPmApplicationConnection();
            var theUser = connection.User.Where(user => user.UserId == userID);
            var currentUser = theUser.First();
            currentUser.UserHomepage = "";

            HttpContext.Current.Session.Add("userHomepage", "");
            HttpContext.Current.Session.Add("mainUserHomepage", "");

            connection.User.Attach(currentUser);
            connection.Entry(currentUser).State = EntityState.Modified;
            connection.SaveChanges();
        }

        public string GetHomepage(int userID)
        {
            connection = new CommPmApplicationConnection();
            var theUser = connection.User.Where(user => user.UserId == userID);
            var currentUser = theUser.First();

            return currentUser.UserHomepage ?? "";
        }

        public void SaveFilters(string gridName, string filter, string filterName, bool isDefault)
        {
            connection = new CommPmApplicationConnection();
            var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
            var theUser = connection.User.Single(u => u.UserId == userId);
            var filters = theUser.UserFilters != null ? (Dictionary<string, string>)JsonConvert.DeserializeObject(theUser.UserFilters, typeof(Dictionary<string, string>)) : new Dictionary<string, string>();
            if (filters.ContainsKey(gridName))
            {
                filters[gridName] = filter;
            }
            else
            {
                filters.Add(gridName, filter);
            }

            var saveFilters = JsonConvert.SerializeObject(filters);

            theUser.UserFilters = saveFilters;

            connection.User.Attach(theUser);
            connection.Entry(theUser).State = EntityState.Modified;
            connection.SaveChanges();

            var entityFilter = new UserFilter();

            if (filterName == "" || filterName == "null" || filterName == null)
            {

            }
            else
            {
                entityFilter.UserId = userId;
                entityFilter.Filter = saveFilters;
                entityFilter.FilterName = filterName;
                entityFilter.IsActive = true;
                entityFilter.IsDefault = isDefault;
                connection = new CommPmApplicationConnection();
                connection.UserFilter.Add(entityFilter);
                connection.SaveChanges();
            }

        }

        public string GetFilter(string gridName)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
                int IDefault = connection.UserFilter.Where(p => (p.IsDefault == true) && (p.UserId == userId)).Count();
                if(IDefault > 0)
                {
                    var theUser = connection.UserFilter.Single(u => (u.UserId == userId) && (u.IsDefault == true));


                    var filters = theUser.Filter != null ? (Dictionary<string, string>)JsonConvert.DeserializeObject(theUser.Filter, typeof(Dictionary<string, string>)) : new Dictionary<string, string>();
                    return filters.ContainsKey(gridName) ? filters[gridName] : string.Empty;
                }
                else
                {
                    return string.Empty;
                }
                
            }
            catch(Exception ex)
            {
                return string.Empty;
            }
            
        }

        public List<UserFilterViewModel> GetAllFilters()
        {
            connection = new CommPmApplicationConnection();
            

            int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
            

            var userFilter = connection.UserFilter.Select(c => new UserFilterViewModel()
            {
                UserId = c.UserId,
                IsActive = c.IsActive,
                Filter = c.Filter,
                FilterName = c.FilterName,
                UserFilterId = c.UserFilterId,
                IsDefault = c.IsDefault
            }).Where(u => (u.IsActive == true) && (u.UserId==userid)).ToList();

            return userFilter;
        }

        public string GetFilterBySelect(string gridName, string filterid)
        {
            connection = new CommPmApplicationConnection();
            int fid = Convert.ToInt32(filterid);
            var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
            var theUser = connection.UserFilter.Single(u => u.UserId == userId && u.UserFilterId == fid);


            var filters = theUser.Filter != null ? (Dictionary<string, string>)JsonConvert.DeserializeObject(theUser.Filter, typeof(Dictionary<string, string>)) : new Dictionary<string, string>();
            return filters.ContainsKey(gridName) ? filters[gridName] : string.Empty;
        }

        public string DeleteFilterById(string UserFilterId)
        {
            try
            {
              connection = new CommPmApplicationConnection();
                int id = Convert.ToInt32(UserFilterId);
                
                var userFilter = connection.UserFilter.Where(p => (p.UserFilterId == id)).ToList();
               
                if (userFilter.Count != 0)
                {
                    connection.UserFilter.RemoveRange(connection.UserFilter.Where(x => x.UserFilterId == id));
                    connection.SaveChanges();
                }
                return "SUCCESS";


            }
            catch (Exception ex)
            {
                return "FAILED";
            }
        }

        public string CheckFilterName(string filterName, bool isDefault)
        {
            try
            {
                connection = new CommPmApplicationConnection();
                var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
                int fName = connection.UserFilter.Where(p => (p.FilterName == filterName) && (p.UserId == userId)).Count();

                if (fName > 0)
                {
                    return "FilterNameExist";
                }
                else
                {
                    if(isDefault == true)
                    {
                        int IDefault = connection.UserFilter.Where(p => (p.IsDefault == isDefault) && (p.UserId == userId)).Count();
                        if (IDefault > 0)
                        {
                            return "IsDeafultExist";
                        }
                        else
                        {
                            return "True";
                        }
                    }
                    else
                    {
                        return "True";
                    }
                    
                }
               
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User class CheckFilterName method");
                return "False";

            }
        }

        public IEnumerable<UserFilterViewModel> FiltersRead()
        {
            return GetAllUserFilters();
        }
        public IList<UserFilterViewModel> GetAllUserFilters()
        {
            connection = new CommPmApplicationConnection();
            List<UserFilterViewModel> result = new List<UserFilterViewModel>();

            try
            {
                int userid = Convert.ToInt32(HttpContext.Current.Session["userId"].ToString());
                result = connection.UserFilter.Select(c => new UserFilterViewModel
                {
                    UserId = c.UserId,
                    IsActive = c.IsActive,
                    Filter = c.Filter,
                    FilterName = c.FilterName,
                    UserFilterId = c.UserFilterId,
                    IsDefault = c.IsDefault
                }).Where(u => (u.IsActive == true) && (u.UserId == userid)).ToList();
                return result.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetAllFilters method");
                return result.ToList();
            }
        }

        public string IsDefault_Update(int UserId, int FilterId, bool check)
        {
            try
            {
                connection = new CommPmApplicationConnection();

                try
                {                
                var theUser = connection.UserFilter.Single(u => (u.UserId == UserId) && (u.IsDefault == true));

                theUser.IsDefault = false;

                connection.UserFilter.Attach(theUser);
                connection.Entry(theUser).State = EntityState.Modified;
                connection.SaveChanges();
                }
                catch
                {

                }

                var theFilter = connection.UserFilter.Single(u => u.UserFilterId == FilterId);

                if(check)
                {
                    theFilter.IsDefault = true;
                }
                else
                {
                    theFilter.IsDefault = false;
                }
                

                connection.UserFilter.Attach(theFilter);
                connection.Entry(theFilter).State = EntityState.Modified;
                connection.SaveChanges();

                return "True";
             
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in User class IsDefault_Update method");
                return "False";

            }
        }

        public string GetUserDefaultFilterId()
        {
            try
            {
                connection = new CommPmApplicationConnection();

                var userId = int.Parse(HttpContext.Current.Session["userId"].ToString());
                int IDefault = connection.UserFilter.Where(p => (p.IsDefault == true) && (p.UserId == userId)).Count();
                if (IDefault > 0)
                {
                    var theUser = connection.UserFilter.Single(u => (u.UserId == userId) && (u.IsDefault == true));
                   
                    return theUser.UserFilterId.ToString();
                }
                else
                {
                    return "0";
                }

            }
            catch (Exception ex)
            {
                return "0";
            }

        }

        public IList<CreditUserRoleViewModel> GetAllCreditUserRoles(string IsActive, int CUserId, int CRoleId)
        {
            connection = new CommPmApplicationConnection();
            List<CreditUserRoleViewModel> result = new List<CreditUserRoleViewModel>();
            try
            {
                result = connection.CreditUserRole.Select(user => new CreditUserRoleViewModel
                {
                    CreditUserRoleId = user.CreditUserRoleId,
                    UserId = user.UserId,
                    RoleId = user.RoleId,
                    IsActive = user.IsActive,                    
                    UserRoles = new UsersUserRoleViewModel()
                    {
                        RoleId = user.UserRoles.RoleId,
                        RoleName = user.UserRoles.RoleName
                    }                
                }).ToList();

                    int conoid = Convert.ToInt32(HttpContext.Current.Session["userCono"].ToString());
               
                    if (IsActive == "True")
                    {
                        return result.Where(u => (u.IsActive == true) && (u.UserId == CUserId)).ToList();
                    }
                    else if (IsActive == "False")
                    {
                        return result.Where(u => (u.IsActive == false) && (u.UserId == CUserId)).ToList();
                    }
                    else
                    {
                        return result.Where(u => (u.UserId == CUserId)).ToList();
                    }               

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class GetAllCreditUserRoles method");
                return result.ToList();
            }
        }

        public IEnumerable<CreditUserRoleViewModel> CreditUserRoleRead(string IsActive, int CUserId, int CRoleId)
        {
            return GetAllCreditUserRoles(IsActive, CUserId, CRoleId);
        }

        public void CreditUserRoleCreate(CreditUserRoleViewModel user, int CUserId, int CRoleId)
        {            
            try
            {
                var entity = new CreditUserRole();

                entity.RoleId = user.RoleId;

                entity.IsActive = user.IsActive;

                entity.UserId = CUserId;

                
                connection = new CommPmApplicationConnection();

                connection.CreditUserRole.Add(entity);
                connection.SaveChanges();

                user.UserId = entity.UserId;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class CreditUserRoleCreate method");
            }
        }

        public void CreditUserRoleUpdate(CreditUserRoleViewModel user, int CUserId, int CRoleId)
        {
            try
            {
                var entity = new CreditUserRole();

                entity.CreditUserRoleId = user.CreditUserRoleId;

                entity.UserId = CUserId;

                entity.IsActive = user.IsActive;

                entity.RoleId = user.RoleId;

                connection = new CommPmApplicationConnection();
                connection.CreditUserRole.Attach(entity);
                connection.Entry(entity).State = EntityState.Modified;
                connection.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error occured in UserService class Update method");
            }
        }
    }
}